#! /bin/env python2

def main(useEventStore,silent,useNewEvent,iterations):
    import HLeptonsCPRW
    import array
    import ROOT
    import os
    import shutil
    ngtot = 0
    ng0 = 0
    ng1 = 0
    ng2 = 0
    nevents = 0
    nerr = 0
    ptcut = 25.
    masscut = 500.
    deletacut = 4.
    m_dtilde = 0.05
    eventNumber = 0
    dataFile = ROOT.TFile("/home/ss944/projekt/daten/mc16a/group.phys-higgs.Htt_lh_V02.mc16_13TeV.346191.PoPy8_NNPDF30_VBFH125_tautaulm15hp20.D2.e7259_s3126_r9364_p3759.smPre_w_2_HS/DAOD_HIGG4D2.17261780._000001.pool.root.1.root")
    tree = dataFile.Get("NOMINAL")
    outFileName1 = "oo1dc"
    outFileName2 = "oo2d"
    oo1list = []
    oo2list = []
    oo1Hist = ROOT.TH1D("oo1","oo1",14,-15.,15.)
    oo2Hist = ROOT.TH1D("oo2","oo2",14,-15.,15.)
    ooES = HLeptonsCPRW.OptObsEventStore();
    if (useEventStore):
        if (silent == False):
            print("Using EventStore")
        ooES.initPDFSet("CT10", 0, 91.2)
    for entryNum in range (0,tree.GetEntries()):
        tree.GetEntry(entryNum)
        jet0_p4 = tree.jet_0_wztruth_p4
        jet1_p4 = tree.jet_1_wztruth_p4
        jet2_p4 = tree.jet_2_wztruth_p4
        #jet0_p4 = tree.truth_reweight_info_parton_2_p4
        #jet1_p4 = tree.truth_reweight_info_parton_3_p4
        #jet2_p4 = tree.truth_reweight_info_parton_4_p4
        flavour1In = tree.truth_reweight_info_parton_0_pdgId;               
        flavour2In = tree.truth_reweight_info_parton_1_pdgId;               
        flavour0Out = tree.truth_reweight_info_parton_2_pdgId;              
        flavour1Out = tree.truth_reweight_info_parton_3_pdgId;              
        flavour2Out = tree.truth_reweight_info_parton_4_pdgId;
        
        
        higgs_p4 = tree.boson_0_truth_p4
        if jet0_p4.Pt()>=jet1_p4.Pt()>=jet2_p4.Pt():
            pass
        else:
            print "pT sorting not ok"
        if jet1_p4.Pt() < 10.**(-5):
            continue
   
    
        pjet0 = array.array('d', [jet0_p4.Energy(), jet0_p4.Px(),jet0_p4.Py() ,jet0_p4.Pz() ])               # E,px,py,pz of nth final state parton
        pjet1 = array.array('d', [jet1_p4.Energy(), jet1_p4.Px(),jet1_p4.Py() ,jet1_p4.Pz() ])
        pjet2 = array.array('d', [jet2_p4.Energy(), jet2_p4.Px(),jet2_p4.Py() ,jet2_p4.Pz() ])
        phiggs = array.array('d',[higgs_p4.Energy(), higgs_p4.Px(),higgs_p4.Py() ,higgs_p4.Pz() ])            # E,px,py,pz of Higgs boson make sure that four-momentum conservation holds 
        ecm = 13000.;                           #proton-proton center-of mass energy in GeV
        mH = 124.999901;                       #mass of Higgs boson in Gev
        jetlist = [pjet0,pjet1,pjet2]
        if jet2_p4.Pt() > 10.**(-5):
            npafin = 3;  #number of partons in final state  either  2 or 3
        else:
            npafin = 2;
        #print "npafin:",npafin
        #print pjet0[1],pjet1[1],pjet2[1]
        delEtaAbs = abs(jet0_p4.Eta()-jet1_p4.Eta())
        mjj = (jet0_p4 + jet1_p4).M()
        if (jet0_p4.Pt() > ptcut and jet1_p4.Pt() > ptcut and  delEtaAbs > deletacut and mjj > masscut and abs(jet0_p4.Eta()) < 4.5 and abs(jet1_p4.Eta()) < 4.5 ):
        #Hjj_p4 = jet0_p4 + jet1_p4 + jet2_p4 + higgs_p4
        #mHjj = Hjj_p4.M()
        #yHjj = Hjj_p4.Rapidity()
            nevents += 1
            print entryNum
            x1 = tree.truth_event_info_Bjorken_x1;                  #Bjorken x of incoming partons, 1 in + z , 2 in -z direction
            x2 = tree.truth_event_info_Bjorken_x2;
            Q = 125;
#flavour assignment: t = 6  b = 5 c = 4, s = 3, u = 2, d = 1 
 #anti-qarks with negative sign
 #gluon = 0 


            #flavour0Out = tree.jet_0_flavorlabel_part;             
            #flavour1Out = tree.jet_1_flavorlabel_part;          
            #flavour2Out = tree.jet_2_flavorlabel_part;
            ng = 0 #number of gluons Out
            flavourlist = [flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out]
            
            print flavourlist
            for i,flav in enumerate(flavourlist):
                if flav == 21:
                    flavourlist[i] = 0  #pdg of gluon is 21, but input has to be 0
                elif flav == 5:
                    flavourlist[i] = 3
                elif flav == -5:
                    flavourlist[i] = -3
                    #ngtot +=1
            flavourOutlist = flavourlist[2:5]
            for i ,flav in enumerate(flavourOutlist):
                if flav == 0: 
                    ng += 1
                    indexg = i
                    ngtot +=1
            if ng > 1:
                ng2 += 1
            elif ng == 1:
                if indexg != len(flavourOutlist)-1:
                    flavourOutlist.append(flavourOutlist[indexg])
                    del flavourOutlist[indexg]
                    jetlist.append(jetlist[indexg])
                    del jetlist[indexg]
                ng1 += 1
            elif ng == 0:
                ng0 += 1 
        #print flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out
            eventNumber += 1;

        # Only used when ran without the Event-store:
            #pdf1 = array.array('d',[0, 0.0391232, 0.0541232, 0.0845228, 0.105186,  0.129429,  0.86471,  0.345418, 0.561297, 0.0845228, 0.0541232, 0.0391232, 0])  #from -6 to 6: pdfs for 1st parton
            #pdf2 = array.array('d',[0, 0.0143834, 0.0205766, 0.0334123, 0.0462144, 0.0601489, 0.345621, 0.246406, 0.468401, 0.0334123, 0.0205766, 0.0143834, 0])  #from -6 to 6: pdfs for 2nd parton

           

            for i in range(0,iterations):
                if not silent:
                    print("Running iteration {}".format(i+1))
                if useEventStore:
                    if (useNewEvent):
                        eventNumber+=i


        #oo1 = ooES.getWeightsDtilde(0, eventNumber, ecm, mH , npafin,flavour1In,flavour2In,flavour1Out,flavour2Out,flavour3Out,x1,x2,pjet0,pjet1,pjet2,phiggs);
        #oo2 = ooES.getWeightsDtilde(1, eventNumber, ecm, mH , npafin,flavour1In,flavour2In,flavour1Out,flavour2Out,flavour3Out,x1,x2,pjet0,pjet1,pjet2,phiggs);
            if not silent:
                print("Calling getWeightsDtilde(...)! Results are: {} , {}".format(oo1,oo2))

            oo1 = ooES.getOptObs(0, eventNumber, ecm, mH ,x1,x2,Q,pjet0,pjet1,phiggs);
        #oo2 = ooES.getOptObs(1, eventNumber, ecm, mH ,x1,x2,Q,pjet0,pjet1,phiggs);
        #oo1list.append(oo1)
        #oo2list.append(oo2)
            if not silent:
                print("Calling getOptObs(...)! Results are: {} , {}".format(oo1,oo2))

            rw =  ooES.getReweight(ecm, mH, 1 , \
          #0, 0, 0, 0, 0, # rsmin,din,dbin,dtin,dtbin \
          #0, 0, 0,           # a1hwwin,a2hwwin,a3hwwin \
          #0, 0, 0,           # a1haain,a2haain,a3haain \
          #0, 0, 0,           # a1hazin,a2hazin,a3hazin \
          #0, 0, 0,           # a1hzzin,a2hzzin,a3hzzin \
          #0,                     # lambdahvvin for formfactor if set to positive value \
          1,0,0, m_dtilde, m_dtilde, -1, \
          0,0,0,        \
          0,0,0,        \
          0,0,0,        \
          0,0,0,        \

          npafin,flavour1In,flavour2In,flavourOutlist[0],flavourOutlist[1],flavourOutlist[2], \
          x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs)
            if not silent:
                print("Calling getReweight(...)! Result is: {}".format(rw))

            else:
                pass
            if rw ==0.:
                nerr +=1
       #     weights = HLeptonsCPRW.getWeightsDtilde(ecm, mH , npafin,flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out,x1,x2,pjet1,pjet2,pjet3,phiggs);
       #     if not silent:
       #         print("Calling getWeightsDtilde(...)! Results are: {} , {}".format(weights.first,weights.second))

       #         w = HLeptonsCPRW.getOptObs(ecm, mH ,x1,x2,pdf1,pdf2,pjet1,pjet2,phiggs);
       #         if not silent:
       #             print("Calling getOptObs(...)! Results are: {} , {}".format(oo.first,oo.second))

                 #   rw = \
                 #       HLeptonsCPRW.getReweight(ecm, mH, 1 , \
                 #                                    0.5, 0.5, 0.5, 0.5, 0.5, #rsmin,din,dbin,dtin,dtbin \
                 #                                    0.5, 0.5, 0.5,           #a1hwwin,a2hwwin,a3hwwin \
                 #                                    0.5, 0.5, 0.5,           #a1haain,a2haain,a3haain \
                 #                                    0.5, 0.5, 0.5,           #a1hazin,a2hazin,a3hazin \
                 #                                    0.5, 0.5, 0.5,           #a1hzzin,a2hzzin,a3hzzin \
                 #                                    0.5,                     #lambdahvvin for formfactor if set to positive value \
                 #                                    npafin,flavour1In,flavour2In,flavour1Out,flavour2Out,flavour3Out, \
                 #                                    x1,x2,pjet1,pjet2,pjet3,phiggs)
                 #   if not silent:
                 #       print("Calling getReweight(...)! Result is: {}".format(rw))
            print flavourlist
            print flavourOutlist
            
            oo1Hist.Fill(oo1,rw)
        #oo2Hist.Fill(oo2)
            print "nerr", nerr
            print "nevents", nevents
    
    if os.path.exists(outFileName1):
        shutil.rmtree(outFileName1)
    os.mkdir(outFileName1)
    os.chdir(outFileName1)

    
    outHistFile = ROOT.TFile.Open(outFileName1+".root","RECREATE")
    outHistFile.cd()
     
    oo1Hist.Write()
    outHistFile.Close()
#canvas1 = ROOT.TCanvas("canvas1")
    #canvas1.cd()
    #oo1Hist.Draw("h")
    #canvas1.SaveAs(outFileName1+".pdf")
    #canvas2 = ROOT.TCanvas("canvas2")
    #canvas2.cd()
    #oo2Hist.Draw("h")
    #canvas2.SaveAs(outFileName2+".pdf")
    print "ngtot, ng0, ng1, ng2,nevents", ngtot,ng0,ng1,ng2,nevents
    #print oo1list
    #print oo2list

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Dumping tables for the effect of sys')
    parser.add_argument ("-s","--silent",
                        dest="silent",
                        action='store_true',
                        default=True)
    parser.add_argument ("-e","--useEventStore",
                        dest="useEventStore",
                        action='store_true',
                        default=True)
    parser.add_argument ("-u","--useNewEvent",
                        dest="useNewEvent",
                        action='store_true',
                        default=False)
    parser.add_argument ("-i","--iterations",
                        dest="iterations",
                        default=1)

    args = parser.parse_args()

    #for(int i=0; i<argc; ++i)
    #{   
      #if (TString(argv[i]).Contains("--useEventStore")) useEventStore = true;
      #if (TString(argv[i]).Contains("--silent")) silent = true;
      #if (TString(argv[i]).Contains("--useNewEvent")) useNewEvent = true;
      #if (TString(argv[i]) == "-i") iterations = atoi(argv[i+1]);
    #}
    main(args.useEventStore,args.silent,args.useNewEvent,int(args.iterations))
