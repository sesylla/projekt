###This returns directory with histogram file including histograms pT, eta, phi, E ###

import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os
import shutil

if len(sys.argv) != 5:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file> <jet 0 branch name> <jet 1 branch name> <jet 2 branch name> "%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
jet0Name = sys.argv[2]
jet1Name = sys.argv[3]
jet2Name = sys.argv[4]

#print "name:", jet2Name, "type:", type(jet2Name)
# readToHisto returns list of histograms of 4-vector-components
def readToHisto(tree,jet0name,jet1name,jet2name):
    #print "data types:",type(jet0name),type(jet1name),type(jet2name)
    #if not (isinstance(jet0, str) and isinstance(jet1, str) and isinstance(jet2, str)):
        #print "one of the branch names is not a string!"
        #sys.exit(1)
  
    delPhihist = ROOT.TH1D("delPhi","delPhi nach pT sortiert",100,-4.,4.)
    #delPhihist = ROOT.TH1D("delPhi","delPhi nach pT sortiert",100,-0.2,6.4)
    delPhihist.Sumw2()


    for entryNum in range(0,tree.GetEntries()):
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)
        if getattr(tree,jet1name).Pt() == 0 and  getattr(tree,jet2name).Pt() == 0:
            continue

        pT0  = getattr(tree,jet0name).Pt()
        eta0 = getattr(tree,jet0name).Eta()
        phi0 = getattr(tree,jet0name).Phi()
        nrg0 = getattr(tree,jet0name).Et()
        pT1  = getattr(tree,jet1name).Pt()
        eta1 = getattr(tree,jet1name).Eta()
        phi1 = getattr(tree,jet1name).Phi()
        nrg1 = getattr(tree,jet1name).Et()
        pT2  = getattr(tree,jet2name).Pt()
        eta2 = getattr(tree,jet2name).Eta()
        phi2 = getattr(tree,jet2name).Phi()
        nrg2 = getattr(tree,jet2name).Et()


        jet0 = ROOT.TLorentzVector()
        jet0.SetPtEtaPhiE(pT0,eta0,phi0,nrg0)
        jet1 = ROOT.TLorentzVector()
        jet1.SetPtEtaPhiE(pT1,eta1,phi1,nrg1)
        jet2 = ROOT.TLorentzVector()
        jet2.SetPtEtaPhiE(pT2,eta2,phi2,nrg2)
        #liste = sorted([jet0,jet1,jet2], key = lambda jet: jet.Pt(),reverse = True)
        delPhi = jet0.Phi()-jet1.Phi()

        #make sure Phi is in [-pi,pi):
        if delPhi < -ROOT.TMath.Pi():
            delPhi += 2*ROOT.TMath.Pi()
        elif delPhi >= ROOT.TMath.Pi():
            delPhi -= 2*ROOT.TMath.Pi()
        delPhihist.Fill(delPhi)
 
        #phi in [0,2pi):
        #if delPhi < 0.:
        #    delPhi += 2*ROOT.TMath.Pi()
        #elif delPhi >= 2*ROOT.TMath.Pi():
        #    delPhi -= 2*ROOT.TMath.Pi()
        #delPhihist.Fill(delPhi)
    return delPhihist



print "Running over data..."

dataFile  = ROOT.TFile(dataFileName,"READ")
dataTree  = dataFile.Get("NOMINAL")
dataHisto = readToHisto(dataTree,jet0Name,jet1Name,jet2Name)



# Write the two histograms to an output root file
print "Writing histograms to output file..."
outFileName = jet0Name.strip("0")+"_delPhi_pTsorted"
#  overwriting previous files with the same name
if os.path.exists(outFileName):
    shutil.rmtree(outFileName)

os.mkdir(outFileName)
os.chdir(outFileName)
outHistFile = ROOT.TFile.Open("histo_"+outFileName+".root","RECREATE")
outHistFile.cd()
dataHisto.Write()

outHistFile.Close()

print "Done writing histogram to output file"
print "directory %s created"%(outFileName)
print "file %s created with delPhi histogram (jets sorted by pT)"%(outFileName+".root")


