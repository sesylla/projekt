#! /bin/env python2
#This script adds OO and sgndDelPhi Data with given Parameters to a .root file.
#all dtilde values are included in dtildelist:
# dtildelist not currently used in this script.
import sys
import os
import shutil
import HLeptonsCPRW
import array
import ROOT
import glob
import csv

dtildelist = ['-1.00', '-0.99', '-0.98', '-0.97', '-0.96', '-0.95', '-0.94', '-0.93', '-0.92', '-0.91', '-0.90', '-0.89', '-0.88', '-0.87', '-0.86', '-0.85', '-0.84', '-0.83', '-0.82', '-0.81', '-0.80', '-0.79', '-0.78', '-0.77', '-0.76', '-0.75', '-0.74', '-0.73', '-0.72', '-0.71', '-0.70', '-0.69', '-0.68', '-0.67', '-0.66', '-0.65', '-0.64', '-0.63', '-0.62', '-0.61', '-0.60', '-0.59', '-0.58', '-0.57', '-0.56', '-0.55', '-0.54', '-0.53', '-0.52', '-0.51', '-0.50', '-0.49', '-0.48', '-0.47', '-0.46', '-0.45', '-0.44', '-0.43', '-0.42', '-0.41', '-0.40', '-0.39', '-0.38', '-0.37', '-0.36', '-0.35', '-0.34', '-0.33', '-0.32', '-0.31', '-0.30', '-0.29', '-0.28', '-0.27', '-0.26', '-0.25', '-0.24', '-0.23', '-0.22', '-0.21', '-0.20', '-0.19', '-0.18', '-0.17', '-0.16', '-0.15', '-0.14', '-0.13', '-0.12', '-0.11', '-0.10', '-0.09', '-0.08', '-0.07', '-0.06', '-0.05', '-0.04', '-0.03', '-0.02', '-0.01', '0.00', '0.01', '0.02', '0.03', '0.04', '0.05', '0.06', '0.07', '0.08', '0.09', '0.10', '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17', '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24', '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31', '0.32', '0.33', '0.34', '0.35', '0.36', '0.37', '0.38', '0.39', '0.40', '0.41', '0.42', '0.43', '0.44', '0.45', '0.46', '0.47', '0.48', '0.49', '0.50', '0.51', '0.52', '0.53', '0.54', '0.55', '0.56', '0.57', '0.58', '0.59', '0.60', '0.61', '0.62', '0.63', '0.64', '0.65', '0.66', '0.67', '0.68', '0.69', '0.70', '0.71', '0.72', '0.73', '0.74', '0.75', '0.76', '0.77', '0.78', '0.79', '0.80', '0.81', '0.82', '0.83', '0.84', '0.85', '0.86', '0.87', '0.88', '0.89', '0.90', '0.91', '0.92', '0.93', '0.94', '0.95', '0.96', '0.97', '0.98', '0.99', '1.00']

if len(sys.argv) != 7:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s  <hist file name (for output)>  <pT cut value> <mass cut value> <delta Eta cut value> <hemisphere cut (0 or 1)> <partons_out cut (0,2 or 3 | 0 means no cut)> . dtildelist included in code."%(sys.argv[0])
    # End the program
    sys.exit(1)

#HistFileName is actually the output csv file name
HistFileName = sys.argv[1]+".csv"

pTCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]
################## turn different hemispheres cut on or off ##################
hemisc0 = int(sys.argv[5])
if hemisc0 == 1: 
    hemisc = True
elif hemisc0 == 0:
    hemisc = False
else:
    print "hemisphere cut has to be 0 or 1."
    sys.exit(1)

#################turn N_quarks_out cut to 2,3,False ########################
partcut0 = int(sys.argv[6])
if partcut0 == 0:
    partcut = False
else:
    partcut = partcut0
if not partcut in [2,3,False]:
    print "partcut has to be 2,3 or 0 = no cut "
#############################################################################
silent=True
useEventStore = True
useNewEvent = False
iterations = 1

ngtot = 0
ng0 = 0
ng1 = 0
ng2 = 0
nevents = 0
neventshemi = 0
neventspartcut = 0
nerr = 0
n3jet = 0
n2jet = 0
nevtot = 0
ng0In = 0
ng1In =  0
ng2In = 0
nq0Out = 0
nq1Out =  0
nq2Out = 0
nq3Out = 0
N2 = 0 # number of partons_out with pT > ptcut
N3 = 0
eventNumber = 1




# make list of all data files
dirname  = "/home/ss944/projekt/daten/"
a = "mc16a/"
e = "mc16e/"
d = "mc16d/"
dir1 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346191.PoPy8_NNPDF30_VBFH125_tautaulm15hp20.D2.e7259_s3126_r9364_p3759.smPre_w_2_HS/"
dir2 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346192.PoPy8_NNPDF30_VBFH125_tautaulp15hm20.D2.e7259_s3126_r10724_p3759.smPre_w_0_HS/"
dirlist = []
filelist = []

for i in [a,e,d]:
#for i in [a]:
    os.chdir(dirname+i)
    for j in glob.glob("*VBF*"):
        dirlist.append(dirname+i+j)
        
#print dirlist
for di in dirlist:
    os.chdir(di)
    #for f in glob.glob("*.root")[0:1]:
    for f in glob.glob("*.root"):
        filelist.append(di+"/"+f)
#test if glob worked:
#os.chdir("/home/ss944/projekt/rest")
#alldirs = []
#allfiles = []
#print dirname
#for i in [a,e,d]:
#    print i
#    print dirname+i
#for i in [a]:
#    os.chdir(dirname+i)
#    for j in glob.glob("*"):
#        alldirs.append(dirname+i+j)
#for di in alldirs:
#    os.chdir(di)
    #for f in glob.glob("*.root")[0:1]:
#    for f in glob.glob("*.root"):
#        allfiles.append(di+"/"+f)

#print(len(filelist),len(allfiles))
#print(len(dirlist),len(alldirs))
#sys.exit(1)
#print filelist
os.chdir("/home/ss944/projekt/rest")





  


ooES = HLeptonsCPRW.OptObsEventStore();
    
if (useEventStore):
    if (silent == False):
        print("Using EventStore")
    ooES.initPDFSet("CT10", 0, 91.2)

def readToHisto(tree,ptCut,massCut,delEtaCut):
    global ooES,ngtot,ng0,ng1,ng2,nevents,silent,useEventStore,useNewEvent,iterations,nerr,n3jet,n2jet,eventNumber,nevtot,ng0In,ng1In,ng2In,nq0Out,nq1Out,nq2Out,nq3Out,hemisc,neventshemi,partcut,neventspartcut,N2,N3
    #m_dtilde = float(dtilde)
    ptcut = float(ptCut)
    masscut = float(massCut)
    deletacut = float(delEtaCut)
    reslistoo1,reslistdP =[], []

    for entryNum in range (0,tree.GetEntries()):
        tree.GetEntry(entryNum)
        #jet0_p40_wztruth = tree.jet_0_wztruth_p4
        #jet1_p40_wztruth = tree.jet_1_wztruth_p4
        #jet2_p40_wztruth = tree.jet_2_wztruth_p4
        jet0_p40 = tree.truth_reweight_info_parton_2_p4
        jet1_p40 = tree.truth_reweight_info_parton_3_p4
        jet2_p40 = tree.truth_reweight_info_parton_4_p4
        flavour1In = tree.truth_reweight_info_parton_0_pdgId;               
        flavour2In = tree.truth_reweight_info_parton_1_pdgId;               
        flavour0Out0 = tree.truth_reweight_info_parton_2_pdgId;              
        flavour1Out0 = tree.truth_reweight_info_parton_3_pdgId;              
        flavour2Out0 = tree.truth_reweight_info_parton_4_pdgId;

        flavour0Out0_wztruth = int(tree.jet_0_wztruth_pdgid);             
        flavour1Out0_wztruth = int(tree.jet_1_wztruth_pdgid);             
        flavour2Out0_wztruth = int(tree.jet_2_wztruth_pdgid); 

        jetlist0 = sorted([[jet0_p40,flavour0Out0,flavour0Out0_wztruth],[jet1_p40,flavour1Out0,flavour1Out0_wztruth],[jet2_p40,flavour2Out0,flavour2Out0_wztruth]],key = lambda x: x[0].Pt(), reverse = True)
        
        jet0_p4 = jetlist0[0][0]
        jet1_p4 = jetlist0[1][0]
        jet2_p4 = jetlist0[2][0]
        #jet0_p4_wztruth = jetlist0_wztruth[0][0]
        #jet1_p4_wztruth = jetlist0_wztruth[1][0]
        #jet2_p4_wztruth = jetlist0_wztruth[2][0]

        flavour0Out = jetlist0[0][1]
        flavour1Out = jetlist0[1][1]
        flavour2Out = jetlist0[2][1]
        flavour0Out_wztruth = jetlist0[0][2]             
        flavour1Out_wztruth = jetlist0[1][2]        
        flavour2Out_wztruth = jetlist0[2][2]

        
        higgs_p4 = tree.boson_0_truth_p4
        if jet0_p4.Pt()>=jet1_p4.Pt()>=jet2_p4.Pt():
            pass
        else:
            print "pT sorting not ok"
       
        #if jet1_p4.Pt() <= ptcut:
        #    continue
        
         
        pjet0 = array.array('d', [jet0_p4.Energy(), jet0_p4.Px(),jet0_p4.Py() ,jet0_p4.Pz() ])               # E,px,py,pz of nth final state parton
        pjet1 = array.array('d', [jet1_p4.Energy(), jet1_p4.Px(),jet1_p4.Py() ,jet1_p4.Pz() ])
        pjet2 = array.array('d', [jet2_p4.Energy(), jet2_p4.Px(),jet2_p4.Py() ,jet2_p4.Pz() ])
        phiggs = array.array('d',[higgs_p4.Energy(), higgs_p4.Px(),higgs_p4.Py() ,higgs_p4.Pz() ])            # E,px,py,pz of Higgs boson make sure that four-momentum conservation holds 
        ecm = 13000.;                           #proton-proton center-of mass energy in GeV
        mH = 124.999901;                       #mass of Higgs boson in Gev
        if jet2_p4.Pt() > 0.:
            npafin = 3
        else:
            npafin = 2
        if flavour2Out_wztruth != 0:
            NpartCut = 3;  #number of partons in final state  either  2 or 3
            n3jet += 1;
           
        else:
            NpartCut = 2;
            n2jet += 1;
        #if jet2_p4.Pt() > ptcut:
        #    NpartCut = 3;  #number of partons in final state  either  2 or 3
        #    N3 +=1;
        #else:
        #    NpartCut = 2;
        #    N2   += 1;

        nevtot += 1
        
        if partcut:
            if NpartCut != partcut:
                continue
        neventspartcut += 1
        
        #print "npafin:",npafin
        #print pjet0[1],pjet1[1],pjet2[1]
        delEtaAbs = abs(jet0_p4.Eta()-jet1_p4.Eta())
        mjj = (jet0_p4 + jet1_p4).M()
        
        if (jet0_p4.Pt() > ptcut and jet1_p4.Pt() > ptcut and  delEtaAbs > deletacut and mjj > masscut and abs(jet0_p4.Eta()) < 4.5 and abs(jet1_p4.Eta()) < 4.5 ):
        #Hjj_p4 = jet0_p4 + jet1_p4 + jet2_p4 + higgs_p4
        #mHjj = Hjj_p4.M()
        #yHjj = Hjj_p4.Rapidity()
        #print m_dtilde,ptcut,deletacut,masscut
        
            nevents += 1
            #print entryNum
            x1 = tree.truth_event_info_Bjorken_x1;                  #Bjorken x of incoming partons, 1 in + z , 2 in -z direction
            x2 = tree.truth_event_info_Bjorken_x2;
            Q = 125;
#flavour assignment: t = 6  b = 5 c = 4, s = 3, u = 2, d = 1 
 #anti-qarks with negative sign
 #gluon = 0 


            #flavour0Out = tree.jet_0_flavorlabel_part;             
            #flavour1Out = tree.jet_1_flavorlabel_part;          
            #flavour2Out = tree.jet_2_flavorlabel_part;
            ng = 0 #number of gluons Out
            nq=0
            flavourlist = [flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out]
            jetlist = [pjet0,pjet1,pjet2]
            jet_p4list = [jet0_p4,jet1_p4,jet2_p4]
            #print "-------------------"
            #print "flavourlist: ",flavourlist 
            for i,flav in enumerate(flavourlist):
                if flav == 21:
                    
                    flavourlist[i] = 0  #pdg of gluon is 21, but input has to be 0
                elif flav == 5:
                    flavourlist[i] = 3
                elif flav == -5:
                    flavourlist[i] = -3
                    #ngtot +=1
            flavourOutlist = flavourlist[2:5]
            wzOutlist = [flavour0Out_wztruth,flavour1Out_wztruth,flavour2Out_wztruth]
            for i ,flav in enumerate(flavourOutlist):
                if flav == 0: 
                    ng += 1
                    ngtot +=1
                    indexg = i
                else:
                    nq += 1
            if ng ==2:
                ng2 += 1
            elif ng == 1:
                #if indexg != len(flavourOutlist)-1:
                #    flavourOutlist.append(flavourOutlist[indexg])
                #    del flavourOutlist[indexg]
                #    jetlist.append(jetlist[indexg])
                #    del jetlist[indexg]
                    #jet_p4list.append(jet_p4list[indexg])
                    #del jet_p4list[indexg]
                ng1 += 1
            elif ng == 0:
                ng0 += 1 
            
            if nq == 0:
                nq0Out+=1
            elif nq==1:
                nq1Out += 1
            elif nq==2:
                
                nq2Out += 1
            elif nq == 3:
                
                nq3Out +=1
            ngIn = 0
            for flav in flavourlist[0:2]:
                if flav == 0:
                    ngIn +=1
            if ngIn == 2:
                ng2In += 1
            elif ngIn == 1:
                ng1In +=1
            else:
                ng0In+=1

        

           

            #for i in range(0,iterations):
            #    if not silent:
            #        print("Running iteration {}".format(i+1))
            #    if useEventStore:
            #        if (useNewEvent):
            #            eventNumber+=i


        

            oo1 = ooES.getOptObs(0, eventNumber, ecm, mH ,x1,x2,Q,pjet0,pjet1,phiggs);
        
            #print type(m_dtilde)
            #rw =  ooES.getReweight(ecm, mH, 1 , \
          #0, 0, 0, 0, 0, # rsmin,din,dbin,dtin,dtbin \
          #0, 0, 0,           # a1hwwin,a2hwwin,a3hwwin \
          #0, 0, 0,           # a1haain,a2haain,a3haain \
          #0, 0, 0,           # a1hazin,a2hazin,a3hazin \
          #0, 0, 0,           # a1hzzin,a2hzzin,a3hzzin \
          #0,                     # lambdahvvin for formfactor if set to positive value \
          #1,0,0, m_dtilde, m_dtilde, -1, \
          #0,0,0,        \
          #0,0,0,        \
          #0,0,0,        \
          #0,0,0,        \
 
          #npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2], \
          #x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs)
            
            #oo1Hist.Fill(oo1,rw)
            #if rw ==0.:
            #    nerr +=1

            w1 = ooES.getWeightsDtilde(0, eventNumber, ecm, mH , npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2],x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs);
            w2 = ooES.getWeightsDtilde(1, eventNumber, ecm, mH , npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2],x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs);
            
            
            if (w1==0. and w2 == 0.):
                nerr += 1
            
            #print "flavours in : ", flavourlist[0:2]
            #print "flavours out: ", flavourOutlist
            #print "wz_truth flavours out: ", wzOutlist
            #print "-------------------"
                
            #print flavourlist
            #print flavourOutlist
            
            
       
           

            liste = sorted([jet_p4list[0],jet_p4list[1]], key = lambda jetx: jetx.Eta(), reverse = True)
            #liste = sorted([jetlist0[0][0],jetlist0[1][0]],key = lambda jetx: jetx.Eta(),reverse = True)
            #liste = jet_p4list
            delPhi_Etasorted = liste[0].DeltaPhi(liste[1])
            #if liste[0].Eta()*liste[1].Eta() < 0.:
            #    delPhiHist.Fill(delPhi_Etasorted,rw)
            delPhi_fin = None
            oo1_fin = None
            if hemisc:
                
                if liste[0].Eta()*liste[1].Eta() < 0.:
                    
                    
                    oo1_fin = oo1
                    delPhi_fin = delPhi_Etasorted
                            
                    neventshemi+=1
                            
                            
                    
                    
                    
                else:
                    
                    oo1_fin = oo1                                                
                    
                  
            else:
               
                    
                delPhi_fin = delPhi_Etasorted
                oo1_fin = oo1
                        
                            
                
                        
                
            if delPhi_fin:
                reslistdP.append([delPhi_fin, w1, w2])    
            if oo1_fin:
                reslistoo1.append([oo1_fin,w1,w2])    

            
            eventNumber += 1
    return reslistoo1,reslistdP
           
        


def addDataToCSV(dataFileList,histfilename,ptc,massc,deletac):
    #save OO;delPhi;w1,w2 to csv file.
    #set hemisc=False if different hemisphere cut is not wanted.
    
    eventlistoo1,eventlistdP =[], []
    if not os.path.exists(histfilename[0:-4]):
        os.mkdir(histfilename[0:-4])
        print "directory "+histfilename[0:-4]+" created."
    else:
        print "directory "+histfilename[0:-4]+" already exists."
        sys.exit(1)
    os.chdir(histfilename[0:-4])
    print "FILENAMEs: ",histfilename[0:-4]+"_oo1.csv",histfilename[0:-4]+"_dP.csv"
    
    filenr = 0
    
     ###############loop over data files
    for dataFileName in dataFileList:
        print "opening file ", filenr+1, " of ", len(dataFileList)
        #print "histlist at this point" , histlist

        dataFile = ROOT.TFile.Open(dataFileName, "READ")
        tree = dataFile.Get("NOMINAL")

        if not tree:
            print "no tree found!"
            sys.exit(1)
        
        #append [oo,dp,w1,w2] to list
        reslistoo1,reslistdP = readToHisto(tree,ptc,massc,deletac) 
        for res in reslistoo1:
            eventlistoo1.append(res)
        for res in reslistdP:
            eventlistdP.append(res)
        
        filenr+=1
        dataFile.Close()

    print "looped over all data files."
    print "writing to csv file "+histfilename+" ..."
    
    with open(histfilename[0:-4]+"_oo1.csv",'w') as csvfile:
        writer = csv.writer(csvfile,delimiter=';')
        for ev in eventlistoo1:
            writer.writerow(ev)
    with open(histfilename[0:-4]+"_dP.csv",'w') as csvfile:
        writer = csv.writer(csvfile,delimiter=';')
        for ev in eventlistdP:
            writer.writerow(ev)
    
    print "done."


   
        
 


addDataToCSV(filelist,HistFileName,pTCut,massCut,delEtaCut)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

#doAll(tree,dtilde,pTCut,massCut,delEtaCut,outFileName)

#print "ngtot, ng0, ng1, ng2,nevents", ngtot,ng0,ng1,ng2,nevents
print "-----------------------------------------------------------"
print "parton_out cut: ",partcut
print "nerr", nerr
print "nevents_tot",nevtot
print "nevents_cut", nevents
print "nevents_dP",neventshemi, "   (without hemisphere cut nevents_dP = 0 )"
print "nevents_parton cut",neventspartcut


print "-----------------------------------------------------------"
print "percentage of 0 gluon_out events: ",float(ng0)/float(nevents)
print "percentage of 1 gluon_out events: ",float(ng1)/float(nevents)
print "percentage of 2 gluon_out events: ",float(ng2)/float(nevents)


print "-----------------------------------------------------------"
print "percentage of 0 gluon_In events: ",float(ng0In)/float(nevents)
print "percentage of 1 gluon_In events: ",float(ng1In)/float(nevents)
print "percentage of 2 gluon_In events: ",float(ng2In)/float(nevents)

print "-----------------------------------------------------------"
print "percentage of 2 parton events: ",float(n2jet)/float(nevtot)
print "percentage of 3 parton events: ",float(n3jet)/float(nevtot)



print "-----------------------------------------------------------"
print "percentage of 2 parton events with pT>ptcut: ",float(N2)/float(nevtot)
print "percentage of 3 parton events with pT>ptcut: ",float(N3)/float(nevtot)

print "-----------------------------------------------------------"
print "percentage of 0 quark_out : ",float(nq0Out)/float(nevents)
print "percentage of 1 quark_out : ",float(nq1Out)/float(nevents)
print "percentage of 2 quark_out : ",float(nq2Out)/float(nevents)
print "percentage of 3 quark_out : ",float(nq3Out)/float(nevents)
