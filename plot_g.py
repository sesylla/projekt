import os
import matplotlib.pyplot as plt
import ROOT
import csv

os.chdir("testres")
os.chdir("SMVBFPoisResults3")
if not os.path.exists("final_plots"):
    os.mkdir("final_plots")
dtildelist = ["0.00","0.20","-0.05"]
bkglist = ["0.5","0.8","1.0","1.5","2.0"]

filenames,dblist = [],[]
for d in dtildelist:
    for b in bkglist:
        filenames.append("SMVBFPoisRes"+d+"_b"+b+".root")
        dblist.append([d,b])
print(filenames)
for l,n in zip(dblist,filenames):
    print(n)
    hfile = ROOT.TFile.Open(n,"READ")
    h_oo0 =hfile.Get("h_est_oo")
    h_dp0 =hfile.Get("h_est_dp")
    
 
    h_oo0.SetName("h_oo_d"+d+"_b"+b)
    h_dp0.SetName("h_dp_d"+d+"_b"+b)
    h_oo = h_oo0.Clone()
    h_dp = h_dp0.Clone()
    ROOT.SetOwnership(h_oo,False)
    h_oo.SetDirectory(0)
    ROOT.SetOwnership(h_dp,False)
    h_dp.SetDirectory(0)
    h_oo.SetTitle(" ; #hat{#lower[0.44]{#tilde{d}}}  ; Anzahl" )
    h_oo.GetXaxis().SetTitleOffset(0.8)
    h_dp.SetTitle(" ; #hat{#lower[0.44]{#tilde{d}}}  ; Anzahl" )
    h_dp.GetXaxis().SetTitleOffset(0.8)
    h_oo.SetStats(0)
    h_dp.SetStats(0)
    #h_oo.Sumw2()
    #h_dp.Sumw2()
    if l[0] == "0.00":
        h_oo.SetLineColor(1)
        h_dp.SetLineColor(1)
    elif l[0] == "0.20":
        h_oo.SetLineColor(2)
        h_dp.SetLineColor(2)
    elif l[0] == "-0.05":
        h_oo.SetLineColor(4)
        h_dp.SetLineColor(4)
    os.chdir("final_plots")
    coo = ROOT.TCanvas("coo")
    cdp = ROOT.TCanvas("cdp")
    coo.SetGrid()
    cdp.SetGrid()
   
    coo.cd()
    h_oo.Draw("HistE")
    coo.SaveAs("gpois_oo_d"+l[0]+"_b"+l[1]+".pdf")
    cdp.cd()
    h_dp.Draw("HistE")
    cdp.SaveAs("gpois_dp_d"+l[0]+"_b"+l[1]+".pdf")
    os.chdir("..")
    curr_dir=os.getcwd()
    os.chdir("/home/ss944/projekt/rest/MCvals")
    with open("MCmeans.csv", 'a') as csvfile:
        writer = csv.writer(csvfile,delimiter = ";")
        writer.writerow([n])
        writer.writerow(["OO",h_oo.GetMean(),h_oo.GetMeanError()])
        writer.writerow(["delPhi",h_dp.GetMean(),h_dp.GetMeanError()])
    os.chdir(curr_dir)
