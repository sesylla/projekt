import csv

dirname = "evColl_255004"
filenameoo = dirname+"/"+dirname+"_oo1.csv"
filenamedP = dirname+"/"+dirname+"_dP.csv"
listew1oo,listew2oo,listew1dp,listew2dp =[],[], [],[]
with open(filenameoo) as csvfile:
    reader = csv.reader(csvfile,delimiter=";")
    for row in reader:
        listew1oo.append(row[1])
        listew2oo.append(row[2])

with open(filenamedP) as csvfile2:
    reader2 = csv.reader(csvfile2,delimiter=";")
    for row in reader2:
        listew1dp.append(row[1])
        listew2dp.append(row[2])

maxw1oo = max(listew1oo)
maxw2oo = max(listew2oo)
maxw1dp = max(listew1dp)
maxw2dp = max(listew2dp)

print("lengths oo : ",len(listew1oo),len(listew2oo))
print("lengths dp : ",len(listew1dp),len(listew2dp))
print("maximum w1oo: ",maxw1oo)
print("maximum w2oo: ",maxw2oo)
print("maximum w1dp: ",maxw1dp)
print("maximum w2dp: ",maxw2dp)

