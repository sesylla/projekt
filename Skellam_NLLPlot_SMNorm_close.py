import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt

from scipy.special import iv
from scipy.special import ive
from scipy.optimize import minimize
#import matplotlib.pyplot as plt


#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
import os

if len(sys.argv) != 5:
    print "USAGE:  <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)


dtilde = sys.argv[1]
pTCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]

dirname = "SMNorm_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
HistFileName =  dirname+"/"+dirname+".root"

templateHistFileName = "PartColl_0.01_255004/PartColl_0.01_255004.root"

#these are for diff Histo:
HistFile = ROOT.TFile.Open(HistFileName,"READ")

oo1HistName= "oo1Hist_diff"
delPhiHistName= "delPhiHist_diff"

outdirname = "Skellam_NLLPlots"

#dtildelist for templates:

dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']






def makearray(hist): #for diff histo
    datalist = []
    for bin in range(hist.GetSize()):
        if hist.GetBinContent(bin)==0.:
            continue
        
        datalist.append(hist.GetBinContent(bin))
    #print datalist
    data = np.array([datalist[i] for i in range(len(datalist)/2,len(datalist))],dtype = np.double)
    #print data
    return data

def maketemplarray(hist): #returns numpy  array of arrays(posdata,mirrdata)
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()):
        

        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
    mirrlist = [datalist[len(datalist)-i-1] for i in range(len(datalist))]    
    hist_data     = np.array([datalist[i] for i in range(len(datalist)/2,len(datalist))],dtype = np.double)
    mirr_data = np.array([mirrlist[i] for i in range(len(mirrlist)/2,len(mirrlist))],dtype = np.double)
    
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return np.array([hist_data , mirr_data])

def maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName):
    templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
    print templateHistFileName
    #print "oo1Hist_d"+str(dtildelist[0])+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

    #get SM Norm
    
    SMhistoo1 = templateHistFile.Get("oo1Hist_d"+"0.00"+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut) )
    SMhistdP = templateHistFile.Get("delPhiHist_d"+"0.00"+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut) )

    Normfacoo1 = 200./SMhistoo1.Integral()
    NormfacdP = 200./SMhistdP.Integral()
    
    print "Norm factors: ",Normfacoo1,NormfacdP

    for dtilde in dtildelist:
        oo1tmplHistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        delPhitmplHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        
        oo1Hist = templateHistFile.Get(oo1tmplHistName)
        delPhiHist = templateHistFile.Get(delPhitmplHistName)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist:
            print "histogram %s not found!"%(oo1tmplHistName)
        if not delPhiHist:
            print "histogram %s not found!"%(delPhitmplHistName)
        
        oo1Hist.Scale( Normfacoo1)
        delPhiHist.Scale(NormfacdP)
        
        oo1tmplarray = maketemplarray(oo1Hist)
        delPhitmplarray = maketemplarray(delPhiHist)
        #print "+++++ dtilde = "+dtilde
        #print delPhitmplarray
        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return oo1templatelist,delPhitemplatelist



    

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildevals0,templates):
    #print "d value: ",d
    dtildevals = [float(j) for j in dtildevals0] 
    
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d, [x0,x1]       
        return [x0,x1],[f0,f1]

    else:
        print "dtilde out of range!"
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is array of arrays, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print "could not find x values for interpolation."
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            f = f1
            #return f
        else:
                
            f = (f0*(x1-x)/(x1-x0))+f1*(x-x0)/(x1-x0)
            
                    
                        
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu1,mu2): #log(pdf)
    #return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+np.log(iv(abs(k),2.*np.sqrt(mu1*mu2)))
    return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+2.*np.sqrt(mu1*mu2)+np.log(ive(abs(k),2.*np.sqrt(mu1*mu2)))
def NLL(dtilde,hist_data,dtildelist,templatelist):
    
    res = 0
    #print x,dtilde
    #oo1templatelist, delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)
    hist_templates = interpol(dtilde,dtildelist,templatelist)
    hist_template1 = hist_templates[0]
    hist_template2 = hist_templates[1]
    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    #print "++++++dtilde = " , dtilde 
    for d, t1,t2 in zip(hist_data,hist_template1,hist_template2):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = lpdf(d,t1,t2)
        res += f
        #print d,t1,t2,t1-t2,-res
    
    return -res


#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(hist_data,dtildelist,templatelist):
    
    

    #print hist_data
    #print hist_template
    startpar = np.array([0.05])
    
    res = minimize(NLL,startpar,args=(hist_data,dtildelist,templatelist),bounds=[(0.,1.)] )

    print(res)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    err = np.diag(res.hess_inv.todense())
    print res.hess_inv.todense()
    print err

    print("result for dtilde: %s +- %s" % (res.x[0],err[0]))
    #print("result for x: %s +- ??" % (res.x[0]))
    #print("result for dtilde: %s +- ??" % (res.x[1]))





####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist):
    tmp = [x-0.5 for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    if indexmin == 0:
        print "minimum is at lower bound!"
    elif indexmin == len(tmp)-1:
        print "minimum is at upper bound!"
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]
    index_lo = tmp_lo.index(min(tmp_lo))
    index_hi = tmp_hi.index(min(tmp_hi))
    d_val_lo = dlow[index_lo]
    d_val_hi = dhi[index_hi]
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

delPhiHist = HistFile.Get(delPhiHistName)
oo1Hist = HistFile.Get(oo1HistName)
#delPhiHist.Sumw2()
#oo1Hist.Sumw2()
#oo1Hist.Scale( 200/oo1Hist.Integral())
#delPhiHist.Scale( 200/delPhiHist.Integral())


oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)

############## create output directory #################### 
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print "directory "+outdirname+" created!"
os.chdir(outdirname)
print "############################# dtilde = "+ str(dtilde) + " #######################################"


print "*************************oo1 Fit**************************"
#minNLL(oo1Hist,dtildelist,oo1templatelist)
hist_data = makearray(oo1Hist)
dd = np.linspace(-1.0,1.0,30000)
NLLvals0oo1 = [NLL(d,hist_data,dtildelist,oo1templatelist) for d in dd]
minoo1 = min(NLLvals0oo1)
NLLvalsoo1 = array("d",[x - minoo1 for x in NLLvals0oo1])

print "----------------------------------------------------------------"
print "find minimum and confidence interval:"
dminoo1 = findMin(dd,NLLvalsoo1)
dlimoo1 = findConfInt(dd,NLLvalsoo1)
print "result with asymm. errors: %f + %f / - %f"%(dminoo1,dlimoo1[1]-dminoo1,dlimoo1[0]-dminoo1)
print dlimoo1

print "-----------------------------------------------------------------"
    
oo1Graph = ROOT.TGraph(len(dd),dd,NLLvalsoo1)

oo1Graph.SetTitle("NLL for OO, #tilde{d}_{true} = %s ; #tilde{d}  ; -log(L(#tilde{d}))"%(dtilde) )
oo1Graph.SetMarkerStyle(1)
oo1Graph.SetMarkerColor(1)

canv_oo1 = ROOT.TCanvas("canvasoo1")
canv_oo1.cd()
canv_oo1.SetGrid()
oo1Graph.Draw("AP")
oo1Graph.GetXaxis().SetRangeUser(dlimoo1[0]-0.01,dlimoo1[1]+0.01)
oo1Graph.GetYaxis().SetRangeUser(0,0.6)
oo1Graph.Draw("AP")
canv_oo1.Update()

lineoo1_lo = ROOT.TLine(dlimoo1[0],0.,dlimoo1[0],0.6)
lineoo1_hi = ROOT.TLine(dlimoo1[1],0.,dlimoo1[1],0.6)
lineoo1_h = ROOT.TLine(dlimoo1[0]-0.01,0.5,dlimoo1[1]+0.01,0.5)
lineoo1_lo.SetLineColor(2)
lineoo1_hi.SetLineColor(2)
lineoo1_h.SetLineColor(2)


lineoo1_lo.Draw("SAME") 
lineoo1_hi.Draw("SAME") 
lineoo1_h.Draw("SAME") 

#canv_oo1.SaveAs("NLL_oo1_close_dtilde"+str(dtilde)+".pdf")
canv_oo1.SaveAs("close_NLL_oo1_close_dtilde"+str(dtilde)+".png")
#plt.plot(dd,NLLvalsoo1,'o',color = "Black",ms = .1)
#plt.show()


print "************************delta Phi Fit**************************"
#minNLL(delPhiHist,dtildelist,delPhitemplatelist)

hist_data = makearray(delPhiHist)
#dd = np.linspace(-1.0,1.0,10000)
NLLvals0dP = [NLL(d,hist_data,dtildelist,delPhitemplatelist) for d in dd]
mindP = min(NLLvals0dP)
NLLvalsdP = array("d",[x - mindP for x in NLLvals0dP])
print "----------------------------------------------------------------"
print "find minimum and confidence interval:"
dmindP = findMin(dd,NLLvalsdP)
dlimdP = findConfInt(dd,NLLvalsdP)
print "result with asymm. errors: %f + %f / - %f"%(dmindP,dlimdP[1]-dmindP,dlimdP[0]-dmindP)
print dlimdP

print "-----------------------------------------------------------------"
dPGraph = ROOT.TGraph(len(dd),dd,NLLvalsdP)

dPGraph.SetTitle("NLL for signed #Delta#Phi_{jj}, #tilde{d}_{true} = %s ; #tilde{d}  ; -log(L(#tilde{d}))"%(dtilde) )
dPGraph.SetMarkerStyle(1)
oo1Graph.SetMarkerColor(1)

canv_dP = ROOT.TCanvas("canvasdP")
canv_dP.cd()
canv_dP.SetGrid()
dPGraph.Draw("AP")
dPGraph.GetXaxis().SetRangeUser(dlimdP[0]-0.01,dlimdP[1]+0.01)
dPGraph.GetYaxis().SetRangeUser(0,0.6)
dPGraph.Draw("AP")
canv_dP.Update()

linedP_lo = ROOT.TLine(dlimdP[0],0.,dlimdP[0],0.6)
linedP_hi = ROOT.TLine(dlimdP[1],0.,dlimdP[1],0.6)
linedP_h = ROOT.TLine(dlimdP[0]-0.01,0.5,dlimdP[1]+0.01,0.5)
linedP_lo.SetLineColor(2)
linedP_hi.SetLineColor(2)
linedP_h.SetLineColor(2)


linedP_lo.Draw("SAME") 
linedP_hi.Draw("SAME") 
linedP_h.Draw("SAME") 


#canv_dP.SaveAs("NLL_delPhi_close_dtilde"+str(dtilde)+".pdf")
canv_dP.SaveAs("close_NLL_delPhi_close_dtilde_"+str(dtilde)+".png")
#plt.plot(dd,NLLvalsdP,'o',color = "Blue",ms = .1)

#plt.show()
