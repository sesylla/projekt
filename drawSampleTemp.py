import csv
import ROOT
import sys
import os



if len(sys.argv) != 3:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print( "USAGE: %s  <directory of input csv file> <dtilde> "%(sys.argv[0]))
    # End the program
    sys.exit(1)


dtilde = sys.argv[2]

dirname = sys.argv[1]
os.chdir(dirname)

oo1filename = dirname+"_oo1.csv"
dpfilename = dirname+"_dP.csv"


inputfilename = sys.argv[1]+".root"
outfilename = sys.argv[1]+"_"+dtilde+".root"



infile = ROOT.TFile.Open(inputfilename,"READ")

TGen = ROOT.TRandom3()

#constants from fit N(d) = 200. + c*d**2
c_oo = 3147.5214538421055
c_dp = 3611.706482059846



dtildelist = [dtilde]

#dtildelist = ['0.00','-0.05','0.20']
#dtildelist = ['0.00']
oo1list,dplist = [],[]
oo1histlist,dphistlist = [],[]
oo1namelist,dpnamelist = [],[]
for d in dtildelist:
    oo1list.append( [  ] ) 
    oo1histlist.append( [ ROOT.TH1D("oo1_dtilde"+d+"_0"," ",14,-15.,15.)] )
    oo1namelist.append(["oo1_dtilde"+d+"_0"])
    dplist.append( [  ] )
    dphistlist.append( [ROOT.TH1D("dp_dtilde"+d+"_0"," ",14,-4.,4.) ] )
    dpnamelist.append(["dp_dtilde"+d+"_0"])




tree_oo = infile.Get("too1")

tree_dp = infile.Get("tdp")


def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def drawindices(gen0,size_sp): #generate sample of size s_sp

        
    sp = [gen0.__next__() for i in range(size_sp)]
    
    return sp

def makeSampleList(tmpl_hist,tmpl_name,N_sp,TGen):
    sp_list = []
    tmpl_bins = [tmpl_hist.GetBinContent(bin) for bin in range(tmpl_hist.GetSize())]
    for isample in range(N_sp):
        
        newname = "_".join(name+"_"+str(isample))
        h = ROOT.TH1D(newname," ",tmpl_hist.GetSize()-2,tmpl_hist.GetXaxis().GetXmin(),tmpl_hist.GetXaxis().GetXmax()))
            
        smpl_bins = [TGen.PoissonD(bincont) for bincont in tmpl_bins]
        for bin,bincont in enumerate(smpl_bins):
            h.SetBinContent(bin,bincont)
            h.Sumw2()
        sp_list.append(h)
    return sp_list

def makeAllSamples(tmpl_histlist,namelist,outfile,N_sp):
        
    for hist,name in zip(tmpl_histlist,namelist):
        sp_list = makeSampleList(hist,name,N_sp,TGen)
        
            
        

def duplicate(tree,dtildelist): #returns !!!not shuffled!!! list of events duplicated by their respective weights
    dtildevals = [float(d) for d in dtildelist]
    obslist = [ [] for d in dtildelist]
    
    for i,d in enumerate(dtildevals):
        print("#### dtilde = ",d)
        for entryNum in range(0,tree.GetEntries()):
            
            
            tree.GetEntry(entryNum)
            obs = tree.obs
            w1 = tree.w1
            w2 = tree.w2
            
            rw = 1 + w1*d + w2*d**2
            

            for j in range(int(round(rw))):
                obslist[i].append(obs)
    
    return obslist
                

def draw(obslist,dtildelist,histlist,namelist,outfilename,c,poisgen):

    # poisgen is the same TRandom3 object as randT3gen above
    
    dtildevals = [float(d) for d in dtildelist]
    

    for i,d in enumerate(dtildevals):
        
        print("#### dtilde = ",d)
        len_data = len(obslist[i])
        gen = sample_gen(len_data,poisgen)
        
        N_sp_fl = poisgen.PoissonD(200. + c*d**2)
        N_sp = int(round(N_sp_fl))
        isample = 0
        NdrawnTot = 0
        N_remain = len_data

        while N_remain > N_sp:

            rand_indices = drawindices(gen,N_sp)
            for j in rand_indices:
                histlist[i][isample].Fill(obslist[i][j])

            NdrawnTot += N_sp
            N_remain -= N_sp
            N_sp_fl = poisgen.PoissonD(200. + c*d**2)
            N_sp = int(round(N_sp_fl))

            #print(namelist[i][isample])
            newname = "_".join(namelist[i][isample].split("_")[0:-1])+"_"+str(isample+1)
            #print(newname)
            histlist[i].append(ROOT.TH1D(newname," ",histlist[i][isample].GetSize()-2,histlist[i][isample].GetXaxis().GetXmin(),histlist[i][isample].GetXaxis().GetXmax()))

            namelist[i].append(newname)
            
            isample += 1
            if isample%100 == 0:
                print(isample, " samples drawn.")
    
    return histlist



def writeToHist(outfilename,dtildelist,histlist1,histlist2):
    
    outfile = ROOT.TFile.Open(outfilename,"RECREATE")
    
    outfile.cd()
        
    
    for h in histlist1:
        for i in h:
            i.Sumw2()
            i.Write()
    for h in histlist2:
        for i in h:
            i.Sumw2()
            i.Write()


print("########################################################################################")
print("***************** read oo1 tree and duplicate events by weights ******************")
oo1list_rw = duplicate(tree_oo,dtildelist)
print("draw samples and fill into histograms ...")
hoo1list_out = draw(oo1list_rw,dtildelist,oo1histlist,oo1namelist,outfilename,c_oo,TGen)


print("***************** read delta phi tree and duplicate events  *************")
dplist_rw = duplicate(tree_dp,dtildelist)
print("draw samples and fill into histograms ...")
hdplist_out = draw(dplist_rw,dtildelist,dphistlist,dpnamelist,outfilename,c_dp,TGen)
print("########################################################################################")
print ("save oo and delphi to file ", outfilename, " ... ")

if not os.path.exists("AllSamples"):
    os.mkdir("AllSamples")
else:
    print("directory ","AllSamples"," already exists!")
os.chdir("AllSamples")
writeToHist(outfilename,dtildelist,oo1histlist,dphistlist)



print("DONE: file ",outfilename, " created .")

sys.exit(1)    
            
