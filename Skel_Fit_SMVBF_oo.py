from multiprocessing import Pool
import numpy as np
#import scipy.stats
from scipy.optimize import minimize
from scipy.special import ive
#from scipy.optimize import minimize
#from scipy.optimize import NonlinearConstraint
from scipy.optimize import Bounds
#from scipy.optimize import BFGS
#from scipy.optimize import SR1
from array import array
import matplotlib.pyplot as plt
#import timeit
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import csv
#fig, ax = plt.subplots(1, 1)




import ROOT
import sys
import os

if len(sys.argv) != 5:
    print ("USAGE: <source directory name> <dtilde> <N_samples> <Bkg ratio>")
    sys.exit(1)

outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 

dtilde = sys.argv[2]
N_sp = int(sys.argv[3])

HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+"_sumdiff.root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")


#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3(423)
BGen = ROOT.TRandom3(137)
#constants from fit N(d) = 200. + c*d**2
#c_oo = 3147.5214538421055
#c_dp = 3611.706482059846
#rebinned, but also with 255004: 
c_oo =  3147.521454469851  #+-  [0.61616453]
c_dp =  3611.706482060036  #+-  [0.43232619]
Background_Ratio = float(sys.argv[4])

# dtildelist for fixed hypotheses (interpolation in between)
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

# these values are not used anymore
ci_oo = [256.67985876604547, 236.37902929944582, 119.18474917359077, 62.95976291095352, 38.508098540335766, 22.803682056052256, 15.919852484639478]
cierrs_oo = [0.34921445655286226, 0.3038219949611821, 0.2714139513440568, 0.22423599451122544, 0.2098730747921924, 0.18872349525623347, 0.16710806526367844]
ci_dP = [62.62255827521881, 167.38329595698812, 219.93972800634108, 200.42929342082726, 118.06756711728525, 15.885299902836735,0.]
cierrs_dP = [0.22881847404023611, 0.3947626204659464, 0.5320246854890918, 0.48166615436006305, 0.2957030309665562, 0.1000841089701317,1.]


def backGen(b_ratio,nbins,dtilde,c,gen): #returns sum,diff arrays of background
    N = 200.
    
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = [gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)]
    
    b_mirr = [b_list[len(b_list)-i-1] for i in range(len(b_list))]
    b_arr_pos = np.array(b_list[len(b_list)//2:])
    b_mirr_pos = np.array(b_mirr[len(b_mirr)//2:])
    #print(b_list)
    
#print("backg_sum: ",b_arr_pos+b_mirr_pos)
    #print("backg_diff: ",b_arr_pos-b_mirr_pos)
    return b_arr_pos+b_mirr_pos,b_arr_pos-b_mirr_pos

#print("******* index: ",dtildelist.index("0.00")) 
#Normalize templates to data
def getnormfac(templateHistFile,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" )
        
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ) 
        

    else:
        print("problem in getnormfac.")
        sys.exit(1)

    
    N_SM = 200.
    
   
    SM_Integral = SMhist.Integral()
    

    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    
    
    
    return Normfac
    
def maketemplarray(hist): #returns numpy  array of arrays(posdata,mirrdata)
    datalist = []
   
    
    for bin in range(hist.GetSize()):
        
        
        datalist.append(hist.GetBinContent(bin))


    mirrlist = [datalist[len(datalist)-i-1] for i in range(len(datalist))]    
    hist_data     = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1)],dtype = np.double)
    mirr_data = np.array([mirrlist[i] for i in range(len(mirrlist)//2,len(mirrlist)-1) ],dtype = np.double)
    
    
    return np.array([hist_data , mirr_data])



def maketemplatelist(dtildelist,templateHistFile):
    
    oo1templatelist, delPhitemplatelist = [],[]
   
    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()

        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
        
        
        oo1tmplarray = maketemplarray(oo1Hist1)
        delPhitmplarray = maketemplarray(delPhiHist1)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return np.array(oo1templatelist),np.array(delPhitemplatelist)



################# samples  ###########################


def makearray(hist): #for diff histo
    datalist = []
    for bin in range(hist.GetSize()-1):
        bin+=1
        datalist.append(hist.GetBinContent(bin))
    data = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1) ],dtype = np.double)
    
    return data



def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of samples histograms [[sum,diff], ... ]
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    #make list of [name_sum,name_diff]:
    for s in hnames:
        if "oo1_dtilde"+dtilde in s :
            
            if "sum" in s:
                hnamesoo.append([s,s[0:-3]+"diff"])
                #print([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for oo1.")
        elif "dp_dtilde"+dtilde in s:
            if "sum" in s:
                hnamesdp.append([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for dp.")
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        

        if sample_file.Get(hnamesoo[i_sp_oo][0]).Integral() != 0.:
            
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            print(hnamesoo[i_sp_oo])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])

        if sample_file.Get(hnamesdp[i_sp_dp][0]).Integral() != 0.:
            
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        else:
            i_sp_dp = gen_dp.__next__()
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        
    return hsamplesoo,hsamplesdp


def subtrAdd(Hist,normfac,d):

    #Hist.Sumw2()
    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  
    
        
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    

    
        
    return Hist_diff,Hist_sum
                         

######### interpolation from templates to arbitrary dtilde value###########################################################################
# not used anymore. interpolation is done with scipy interp1d

def findx0x1(d,dtildevals0,templates):
    #print( "d value: ",d)
    dtildevals = [float(j) for j in dtildevals0] 
    
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d, [x0,x1]       
        return [x0,x1],[f0,f1]

    else:
        print("dtilde out of range!")
        #sys.exit(1)
        #return [np.nan,np.nan],[np.nan,np.nan]
        return np.nan


    

############################################ Fit Functions ########################################################################################################

def lpdf(b,k_pl,k_min,dtilde,diff_templ,slam,dtil): #log(pdf)
    mu1 = 0.5*(slam+diff_templ)
    mu2 = 0.5*(slam-diff_templ)
    
    
    
    #print("----------------")

    #no background:
    #result = -(mu1+mu2)+(k_min/2.)*np.log(abs(mu1/mu2))+2.*np.sqrt(abs(mu1*mu2))+np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))))+k_pl*np.log(abs(slam))-slam
    
#background:
    result = -(mu1+mu2)+(k_min/2.)*np.log(abs(mu1/mu2))+2.*np.sqrt(abs(mu1*mu2))+np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))))+k_pl*np.log(abs(slam+2*b))-slam-2*b
    return result

def NLL(params,dtil,sum_data,diff_data,diff_template):
    b,sum_lam = params[0],params[1:]
    
    f = lpdf(b,sum_data,diff_data,dtilde,diff_template,sum_lam,dtil)
    res += f
    
    return -np.sum(res)

def deletezeros(indexlist,arr):
        
    tmp = [arr]
    n = -1
    for i,j in enumerate(indexlist):
        # j  have to be sorted negative indices
        n+=1
        tmp.append(np.delete(tmp[i],j+n))
    return tmp[-1]     

def minNLL(args):
    dtil,sum_data,diff_data,diff_template,nr = args
  

    
    if not len(sum_data) == len(diff_data):
        print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
        sys.exit(1)

    sumlam_start = [sumval for sumval in sum_data]
    b_start = [Background_Ratio*sum(sumlam_start)/(2*len(sumlam_start))]
    
    
    startpar = np.array(b_start+sumlam_start)
    #print(startpar)
    
    #def cons_f(x):
    #    return x**2-diff_template**2
    

#make bounds

    
    lbnds_slam = [0. for i in range(len(diff_data))]
    ubnds_slam = [np.inf for i in range(len(diff_data))]

    lbnd_b = [1.]
    ubnd_b = [np.inf]

    lbnds = lbnd_b+lbnds_slam
    ubnds = ubnd_b+ubnds_slam
    
    bounds = Bounds(lbnds,ubnds)
   

 
    cons = ({'type': 'ineq', 'fun': lambda x:  x[1:]**2 - diff_template**2})
    
    
    
    err = 0

    res = minimize(NLL,startpar,method="SLSQP",args=(dtil,sum_data,diff_data,diff_template),constraints = cons,bounds=bounds,options={'maxiter':10000}  )
    if not res.success:
        #print("fit failed at dtilde = ",dtil)
        #print(res.message)
        err = 1
    #print(sum_data)
    #print(res.x)
    
    #nllres = NLL(res.x,dtilde,sum_data,diff_data,ci_list)
    nllres = res.fun
    
    #print(nllres)
    
    #print("NLL at minimum: ",nllres)
    return nr,nllres ,err,res.x[0]  



    

####### find minimum ######################


def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    #plt.plot(ddlist,tmp)
    #plt.show()
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
        
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)

def getSensitivity(func, tempMean,cal_range):# check if confidence interval exists within range of NLL curve
    
    if (func(cal_range[0])-tempMean)*(func(cal_range[1])-tempMean) > 0.:
        return None
    else:
        return root_scalar(lambda x: func(x) - tempMean, bracket = [cal_range[0],cal_range[1]]).root


def estimate(dtildelist,samplelist,templateHistFile,dtilde,c):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    Nerrs = 0
    errlist = []
    Nzeros = []
    
    #dd = np.linspace(float(dtilde)-0.5,float(dtilde)+0.5,1001)
    
    # pool for multiprocessing (e.g. 4 parallel processes)
    p = Pool(4)
    
    #print(dd)
    #dd = np.linspace(-1.,1.,100)
    dtildevals = np.array([float(x) for x in dtildelist])
    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi,bkg_estimators =[], [],[],[],[],[]
    #intlist,sampleintlist =[], []
    
    
        #sampleintlist.append(sample.Integral())
    normfac = getnormfac(templateHistFile,c)

    oo1tmplarrlist,delPhitmplarrlist = maketemplatelist(dtildelist,templateHistFile)
    
        
    if c == c_oo:
        oo1tmplarrlist *= normfac
        
        difftemplatelist =np.array([oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
            #sp = samplelist[0]
            #ci_list = np.array(ci_oo)
    elif c == c_dp:
        delPhitmplarrlist *= normfac
        difftemplatelist =np.array([delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])
            #ci_list = np.array(ci_dP)
    else:
        print("problem with normfac in estimate.")

        
    for k,sample in enumerate(samplelist):
        if np.sum(sample) == 0.:
            print("############# 0 histo found !! ##############")
            continue
        #if k < 16:
        #    continue
        if k%500 == 0:
            print(k, " of ",len(samplelist), "samples processed. ")
        dd = np.linspace(float(dtilde)-0.2,float(dtilde)+0.2,2001)
        N_dd = len(dd)
        f_inter = interp1d(dtildevals,difftemplatelist,axis=0)
        
        bkg_sum,bkg_diff = backGen(Background_Ratio,2*len(sample[0]),dtilde,c,BGen)
        sum_sample = sample[0] + bkg_sum
        diff_sample = sample[1] + bkg_diff
        
        #print("sum sample before/after background:")
        #print(sample[0])
        #print(sum_sample)
            
        diff_templates1 = f_inter(dd)
        diff_templates = np.array(diff_templates1)
        
        NLLvals0 = []
        errindices = []
        backg_list = []

        # map to pool for multiprocessing
        args = [(d,sum_sample,diff_sample,templ,nr) for nr,(d,templ) in enumerate(zip(dd,diff_templates))]
        fitresults = p.map(minNLL,args)

        fitresults.sort(key=lambda x : x[0])
        res_nr,res_nll,res_nerrs = [],[],[]
        for nr,result in enumerate(fitresults):
            
            if result[2] != 0:
                Nerrs+=1
                errindices.append(result[0]-len(dd))
            else:
                NLLvals0.append(result[1])
                backg_list.append(result[3])
        

        
        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        
#delete d values with errors (will be visible as gaps in likelihood curve):
        errindices.sort(reverse = True)
        
        dd_tmp = deletezeros(errindices,dd)

        dd = dd_tmp
        errlist.append(Nerrs)
        Nerrs = 0
        

        
        dmin = findMin(dd,NLLvals)
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))

        
        bkg_estimators.append(backg_list[NLLvals0.index(min1)])
        
        
        

    print("Number of Bins per sample with content 0 : ",Nzeros)
    #print("background estimators : ", bkg_estimators)
    
    print("max. number of errors per sample : ", max(errlist))
    print("average number of errors per sample : ", sum(errlist)/len(errlist))
    print("average relative number of errors per sample : ", sum(errlist)/(len(errlist)*N_dd))
    print("maximum relative number of errors per sample : ",max(errlist)/(len(errlist)*N_dd))
    return np.array(estlist), np.array(err1sig_lo), np.array(err1sig_hi), np.array(err2sig_lo), np.array(err2sig_hi),errlist,bkg_estimators





#delPhiHist.Sumw2()
#oo1Hist.Sumw2()






oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)



samplelist_oo,samplelist_dp = makesamplelist(N_sp,dtilde,HistFile,TGen)




############## FIT  #################### 
#start = timeit.default_timer()

print("oo fit in process ...")
oo_estarr ,oo_1sigarr_lo,oo_1sigarr_hi,oo_2sigarr_lo,oo_2sigarr_hi,Nerrsoo, oo_bkg = estimate(dtildelist,samplelist_oo,templateHistFile,dtilde,c_oo)
xxo = np.arange(len(oo_estarr))


print("#######################################################")

#print("Nerrs per sample OO :",Nerrsoo)
#print("Nerrs per sample delPhi :",Nerrsdp)
#print("number of successes OO/dP: ",len(oo_estarr),len(dp_estarr))
print("ratios of coverage:")
cov1oo =  cover_ratio(oo_estarr,oo_1sigarr_lo,oo_1sigarr_hi,dtilde)
cov2oo =  cover_ratio(oo_estarr,oo_2sigarr_lo,oo_2sigarr_hi,dtilde)
#cov1dp =  cover_ratio(dp_estarr,dp_1sigarr_lo,dp_1sigarr_hi,dtilde)
#cov2dp =  cover_ratio(dp_estarr,dp_2sigarr_lo,dp_2sigarr_hi,dtilde)
print("OO 1 sigma :", cov1oo)
print("OO 2 sigma :", cov2oo)
#print("delPhi 1 sigma :", cov1dp)
#print("delPhi 2 sigma :", cov2dp)

oomean = np.mean(oo_estarr) 
#dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
#dpvar = np.var(dp_estarr)
#print("sample means :",oomean,dpmean)
#print("sample variances : ", oovar,dpvar)
#print("min, max estimators :",(np.amin(oo_estarr),np.amax(oo_estarr)),(np.amin(dp_estarr),np.amax(dp_estarr)))
#print("max limits of NLL+2sig region : ", (np.amin(oo_estarr-oo_2sigarr_lo),np.amax(oo_estarr+oo_2sigarr_hi)),(np.amin(dp_estarr-dp_2sigarr_lo),np.amax(dp_estarr+dp_2sigarr_hi)))

oomean = np.mean(oo_estarr) 
#dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
#dpvar = np.var(dp_estarr)
oo1sigperc_lo = np.percentile(oo_estarr,(100.-68.27)/2.)

oo2sigperc_lo = np.percentile(oo_estarr,(100.-95.44)/2.)


oo1sigperc_hi = np.percentile(oo_estarr,100.-(100.-68.27)/2.)

oo2sigperc_hi = np.percentile(oo_estarr,100.-(100.-95.44)/2.)

oo1sigperc = [oo1sigperc_lo,oo1sigperc_hi]
oo2sigperc = [oo2sigperc_lo,oo2sigperc_hi]

print("*********OO Results**********")
print("sample mean :",oomean)
print("sample variance : ", oovar)

print("1sig percentiles oo : ", oo1sigperc)
print("2sig percentiles oo : ",oo2sigperc)




########### histograms of estimators ###########
os.chdir(outdirname)
coo = ROOT.TCanvas("oo")
h_est_oo = ROOT.TH1D("h_est_oo", " ; #hat{#tilde{d}} ; Anzahl ", 20, oomean-0.1, oomean+0.1)
h_est_oo.SetStats(0)
if dtilde == "0.00":
    h_est_oo.SetLineColor(1)
    #delPhiHist.SetLineColor(1)
elif dtilde == "0.20":
    h_est_oo.SetLineColor(2)
    #delPhiHist.SetLineColor(2)
elif dtilde == "-0.05":
    h_est_oo.SetLineColor(4)
    #delPhiHist.SetLineColor(4)
h_est_oo.Sumw2()
for est in oo_estarr:
    h_est_oo.Fill(est)
coo.cd()
h_est_oo.Draw("HistE")






########################## save data to csv and root files #################################
if not os.path.exists("SMVBF_SkPResults3"):
    os.mkdir("SMVBF_SkPResults3")
os.chdir("SMVBF_SkPResults3")
coo.SaveAs("SMVBF_oo_est_skp_"+dtilde+"_b"+sys.argv[4]+".pdf")
#cdp.SaveAs("new_dp_est_skp_"+dtilde+"_b"+sys.argv[4]+".pdf")
with open("SMVBF_SkPResults_oo_"+dtilde+"_b"+sys.argv[4]+".csv", 'a') as csvfile:
    writer = csv.writer(csvfile,delimiter = ";")
    writer.writerow([Background_Ratio,oomean,oo1sigperc_lo,oo1sigperc_hi,oo2sigperc_lo,oo2sigperc_hi,cov1oo,cov2oo])
#with open("new_SkPResults_dp_"+dtilde+".csv", 'a') as csvfile:
#    writer = csv.writer(csvfile,delimiter = ";")
#    writer.writerow([Background_Ratio,dpmean,dp1sigperc_lo,dp1sigperc_hi,dp2sigperc_lo,dp2sigperc_hi,cov1dp,cov2dp] ) 
outfile = ROOT.TFile.Open("SMVBF_SkPRes"+dtilde+"_b"+sys.argv[4]+".root","UPDATE")
outfile.cd()
h_est_oo.Write()
#h_est_dp.Write()


############# PLOT ####################
#plt.errorbar(xxo,oo_estarr,yerr=[oo_1sigarr_lo,oo_1sigarr_hi], fmt = "X")
#plt.title("OO 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxo,oo_estarr,yerr=[oo_2sigarr_lo,oo_2sigarr_hi], fmt = "X")
#plt.title("OO 2 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_1sigarr_lo,dp_1sigarr_hi], fmt = "X")
#plt.title("delPhi 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_2sigarr_lo,dp_2sigarr_hi], fmt = "X")
#plt.title("delPhi 2 sigma intervals")
#plt.grid()
#plt.show()



