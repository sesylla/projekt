###This returns directory with histogram file including histograms pT, eta, phi, E ###

import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os
import shutil

if len(sys.argv) != 4:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file> < branch name>  <out file name>"%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
branchName = sys.argv[2]

outFileName = sys.argv[3]

# readToHisto returns list of histograms of 4-vector-components
def readToHisto(tree,branch):
   
 
  
    mjjhist = ROOT.TH1D("Mjj","M",100,0.,200.)
    mjjhist.Sumw2()
 

    for entryNum in range(0,tree.GetEntries()):
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)
               
      

        v = getattr(tree,branch)
            
            



                

            
        mjjhist.Fill(v.M())
 
 
    return mjjhist


print "Running over data..."

dataFile  = ROOT.TFile(dataFileName,"READ")
dataTree  = dataFile.Get("NOMINAL")
dataHisto = readToHisto(dataTree,branchName)



# Write the two histograms to an output root file
print "Writing histograms to output file..."

#  overwriting previous files with the same name
if os.path.exists(outFileName):
    shutil.rmtree(outFileName)

os.mkdir(outFileName)
os.chdir(outFileName)
outHistFile = ROOT.TFile.Open("histo_"+outFileName+".root","RECREATE")
outHistFile.cd()
dataHisto.Write()

outHistFile.Close()

print "Done writing histogram to output file"
print "directory %s created"%(outFileName)
print "file %s created with delPhi histogram (sorted by Eta)"%(outFileName+".root")


