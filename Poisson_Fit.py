import numpy as np
import scipy.stats
from scipy.optimize import minimize
import matplotlib.pyplot as plt


#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
#import os

if len(sys.argv) != 6:
    print("USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)

ScFac = .1

dirname = sys.argv[1] 
HistFileName =  sys.argv[1]+"/"+sys.argv[1]+".root"
dtilde = sys.argv[2]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]

templateHistFileName = "PartColl_0.01_255004/PartColl_0.01_255004.root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

delPhiHist0 = HistFile.Get(delPhiHistName)
oo1Hist0 = HistFile.Get(oo1HistName)

delPhiHist = delPhiHist0.Clone()
oo1Hist = oo1Hist0.Clone()
delPhiHist.SetDirectory(0)
oo1Hist.SetDirectory(0)


#delPhiHist.Sumw2()
#oo1Hist.Sumw2()
oo1Hist.Scale( ScFac*200/oo1Hist.Integral())
delPhiHist.Scale( ScFac*200/delPhiHist.Integral())

#dtildelist for templates:
#dtildelist = [-1.0,-0.95,-0.9,-0.85,-0.8,-0.75,-0.7,-0.6,-0.55,-0.5,-0.45,-0.4,-0.35,-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,0.0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.7,0.75,0.8,0.85,0.9,0.95,1.0]
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

def maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName1):
    #templateHistFile1 = ROOT.TFile.Open(templateHistFileName1,"READ")
    templateHistFile = ROOT.TFile.Open(HistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
    for dtilde in dtildelist:
        oo1HistName1= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        delPhiHistName1= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        
        #oo1Hist1 = templateHistFile1.Get(oo1HistName1)
        #delPhiHist1 = templateHistFile1.Get(delPhiHistName1)
        oo1Hist1 = HistFile.Get(oo1HistName1)
        delPhiHist1 = HistFile.Get(delPhiHistName1)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1HistName))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhiHistName))
        
        oo1Hist1.Scale( 200/oo1Hist1.Integral())
        delPhiHist1.Scale( 200/delPhiHist1.Integral())

        oo1templatelist.append(oo1Hist1)
        delPhitemplatelist.append(delPhiHist1)
    return oo1templatelist,delPhitemplatelist

def makearray(hist): #returns numpy  array of data bins
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildestrings,templates):
    #print "d value: ",d
    i=0
    dtildevals = [float(j) for j in dtildestrings]
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d,[x0,x1]       
        return [x0,x1],[f0,f1]
    else:
        print( "dtilde out of range!")
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print( "could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
                        
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu): #log(pdf)
    return -mu + k*np.log(mu)

def NLL(params,hist_data,dtildelist,templatelist):
    x,dtilde = params
    res = 0
    #print x,dtilde
    #oo1templatelist, delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)
    hist_template = makearray(interpol(dtilde,dtildelist,templatelist))
    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    
    for d, t in zip(hist_data,hist_template):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = lpdf(d,x*t)
        res += f
    return -res


#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(hist,dtildelist,templatelist):
    
    hist_data = makearray(hist)

    #print hist_data
    #print hist_template
    startpar = np.array([0.1,0.05])
    
    res = minimize(NLL,startpar,args=(hist_data,dtildelist,templatelist),bounds=[(1e-6,10),(-1.0,1.)] )

    print(res)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    err = np.diag(res.hess_inv.todense())
    print("covariance matrix: ")
    print( res.hess_inv.todense())
    
    print( err)
    print("result for x: %s +- %s" % (res.x[0],np.sqrt(err[0])))
    print("result for dtilde: %s +- %s" % (res.x[1],np.sqrt(err[1])))




oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)

print( "*************************oo1 Fit**************************")
minNLL(oo1Hist,dtildelist,oo1templatelist)

print( "************************delta Phi Fit**************************")
minNLL(delPhiHist,dtildelist,delPhitemplatelist)


