import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import matplotlib.pyplot as plt



from array import array

import ROOT
import sys
import os

if len(sys.argv) != 6:
    print ("USAGE: <source directory name> <output directory name> <N_samples> <dtilde hypothesis> <background ratio>")
    sys.exit(1)

Background_Ratio = float(sys.argv[5])
TGen = ROOT.TRandom3()
bGen = ROOT.TRandom3()

#constants from fit N(d) = 200. + c*d**2
c_oo = 3147.5214538421055
c_dp = 3611.706482059846

#dtildelist = ['-1.00','-0.05','0.00','0.20','-0.11','1.00']
#dtildelist = ['-1.00', '-0.99', '-0.98', '-0.97', '-0.96', '-0.95', '-0.94', '-0.93', '-0.92', '-0.91', '-0.90', '-0.89', '-0.88', '-0.87', '-0.86', '-0.85', '-0.84', '-0.83', '-0.82', '-0.81', '-0.80', '-0.79', '-0.78', '-0.77', '-0.76', '-0.75', '-0.74', '-0.73', '-0.72', '-0.71', '-0.70', '-0.69', '-0.68', '-0.67', '-0.66', '-0.65', '-0.64', '-0.63', '-0.62', '-0.61', '-0.60', '-0.59', '-0.58', '-0.57', '-0.56', '-0.55', '-0.54', '-0.53', '-0.52', '-0.51', '-0.50', '-0.49', '-0.48', '-0.47', '-0.46', '-0.45', '-0.44', '-0.43', '-0.42', '-0.41', '-0.40', '-0.39', '-0.38', '-0.37', '-0.36', '-0.35', '-0.34', '-0.33', '-0.32', '-0.31', '-0.30', '-0.29', '-0.28', '-0.27', '-0.26', '-0.25', '-0.24', '-0.23', '-0.22', '-0.21', '-0.20', '-0.19', '-0.18', '-0.17', '-0.16', '-0.15', '-0.14', '-0.13', '-0.12', '-0.11', '-0.10', '-0.09', '-0.08', '-0.07', '-0.06', '-0.05', '-0.04', '-0.03', '-0.02', '-0.01', '0.00', '0.01', '0.02', '0.03', '0.04', '0.05', '0.06', '0.07', '0.08', '0.09', '0.10', '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17', '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24', '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31', '0.32', '0.33', '0.34', '0.35', '0.36', '0.37', '0.38', '0.39', '0.40', '0.41', '0.42', '0.43', '0.44', '0.45', '0.46', '0.47', '0.48', '0.49', '0.50', '0.51', '0.52', '0.53', '0.54', '0.55', '0.56', '0.57', '0.58', '0.59', '0.60', '0.61', '0.62', '0.63', '0.64', '0.65', '0.66', '0.67', '0.68', '0.69', '0.70', '0.71', '0.72', '0.73', '0.74', '0.75', '0.76', '0.77', '0.78', '0.79', '0.80', '0.81', '0.82', '0.83', '0.84', '0.85', '0.86', '0.87', '0.88', '0.89', '0.90', '0.91', '0.92', '0.93', '0.94', '0.95', '0.96', '0.97', '0.98', '0.99', '1.00']

dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

outdirname = sys.argv[2]

#dirname = sys.argv[1] 
#HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+".root"
indirname = sys.argv[1]
Nsamples = int(sys.argv[3])
dtilde_hyp = sys.argv[4]
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

#HistFile = ROOT.TFile.Open(HistFileName,"READ")

#oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
#delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)


############### template ####################

def getHypMeans(d_hyp,templatehfilename):
    tmplFile = ROOT.TFile.Open(templatehfilename,"READ")
    
    oo1HistName1= "oo1_dtilde"+d_hyp
    delPhiHistName1= "dp_dtilde"+d_hyp
        
    oo1Hist1 = tmplFile.Get(oo1HistName1)
    delPhiHist1 = tmplFile.Get(delPhiHistName1)

    
    if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
    if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
    
    return oo1Hist1.GetMean(),delPhiHist1.GetMean()

################# samples  ###########################

def backGen(b_ratio,nbins,dtilde,hist,c,gen):
    N = 200.+c*float(dtilde)**2
    #nbins = len(histarr)
    #nbins = hist.GetSize()-2
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = [gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)]
    #print(b_list)
    bh = ROOT.TH1D(hist.GetName()+"_b"," " , hist.GetSize()-2, hist.GetXaxis().GetXmin(),hist.GetXaxis().GetXmax())
    for bin,cont in enumerate(b_list):
        bh.SetBinContent(bin+1,cont)
    bh.Sumw2()
    return bh

def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)



def makeSampleMeanList(N_samples,dtilde,sample_file,genT3): #returns list of sample histograms
    #print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    for s in hnames:
        if "oo1_dtilde"+dtilde in s:
            hnamesoo.append(s)
        elif "dp_dtilde"+dtilde in s:
            hnamesdp.append(s)
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    samplemeansoo,samplemeansdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        #print("random indices :", i_sp_oo,i_sp_dp) 

        if sample_file.Get(hnamesoo[i_sp_oo]).Integral() != 0.:
            
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        
        dph = sample_file.Get(hnamesdp[i_sp_dp])
        if sample_file.Get(hnamesdp[i_sp_dp]).Integral() != 0.:
            
            dph = sample_file.Get(hnamesdp[i_sp_dp])
        else:
            i_sp_dp = gen_dp.__next__()
            dph = sample_file.Get(hnamesdp[i_sp_dp])
        
#background:
        nbinsoo = ooh.GetSize()-2
        nbinsdp = dph.GetSize()-4
        b_histoo = backGen(Background_Ratio,nbinsoo,dtilde,ooh,c_oo,bGen)
        b_histdp = backGen(Background_Ratio,nbinsdp,dtilde,dph,c_dp,bGen)
        
        
        #print("------")
        #print( [ooh.GetBinContent(bin+1) for bin in range(ooh.GetSize()-2)])
        #print(ooh.GetMean())
        #print(b_listoo)
        ooh.Add(b_histoo)
        dph.Add(b_histdp)
        
        #print(ooh.GetMean())
        samplemeansoo.append( ooh.GetMean())
        samplemeansdp.append( dph.GetMean())
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return np.array(samplemeansoo),np.array(samplemeansdp)

def getConfIntM(sp_meanarr): #get mean and percentiles from meanlist
    
    arr_mean = np.mean(sp_meanarr)

    arr_1sigperc_lo = np.percentile(sp_meanarr,(100.-68.27)/2.)
    arr_2sigperc_lo = np.percentile(sp_meanarr,(100.-95.44)/2.)
    
    arr_1sigperc_hi = np.percentile(sp_meanarr,100.-(100.-68.27)/2.)
    arr_2sigperc_hi = np.percentile(sp_meanarr,100.-(100.-95.44)/2.)
    #print(arr_mean, [arr_1sigperc_lo, arr_1sigperc_hi],[arr_2sigperc_lo,arr_2sigperc_hi])
    return arr_mean, [arr_1sigperc_lo, arr_1sigperc_hi],[arr_2sigperc_lo,arr_2sigperc_hi]

def getValues(N_Samples,dtildelist,genT3,in_dirname):
    oo_means,oo_CI1sig,oo_CI2sig = [],[],[]
    dp_means,dp_CI1sig,dp_CI2sig = [],[],[]
    for d in dtildelist:
        HistFileName =  in_dirname+"/"+"AllSamples/"+in_dirname+"_"+d+".root"
        #HistFileName = in_dirname+"/"+in_dirname+"_samples.root"
        sp_file = ROOT.TFile.Open(HistFileName,"READ")
        oo_mean_arr,dp_mean_arr = makeSampleMeanList(N_Samples,d,sp_file,genT3)
        
        m_oo,c1s_oo,c2s_oo = getConfIntM(oo_mean_arr)
        m_dp,c1s_dp,c2s_dp = getConfIntM(dp_mean_arr)

        oo_means.append(m_oo)
        oo_CI1sig.append(c1s_oo)
        oo_CI2sig.append(c2s_oo)

        dp_means.append(m_dp)
        dp_CI1sig.append(c1s_dp)
        dp_CI2sig.append(c2s_dp)

    return [oo_means,oo_CI1sig,oo_CI2sig],[dp_means,dp_CI1sig,dp_CI2sig]
        

def getSensitivity(func, tempMean,cal_range):# check if confidence interval exists within range of calibration curve
    
    if (func(cal_range[0])-tempMean)*(func(cal_range[1])-tempMean) > 0.:
        return None
    else:
        return root_scalar(lambda x: func(x) - tempMean, bracket = [cal_range[0],cal_range[1]]).root

def getConfIntHyp(templateMean,dvals,ResList):
    
    
    f_inter = interp1d(dvals,ResList[0],axis=0)
    f_inter_neg = lambda x: -f_inter(x)
    
    err1_l = np.array([i[0] for i in ResList[1]])
    err1_h = np.array([i[1] for i in ResList[1]])
    err2_l = np.array([i[0] for i in ResList[2]])
    err2_h = np.array([i[1] for i in ResList[2]])

    f_err1_l = interp1d(dvals,err1_l,axis=0)
    f_err1_h = interp1d(dvals,err1_h,axis=0)
    f_err2_l = interp1d(dvals,err2_l,axis=0)
    f_err2_h = interp1d(dvals,err2_h,axis=0)
    
    
    #mean1 = root_scalar(lambda x : f_inter(x) - templateMean , bracket = [-0.99,0.99]).root
    #CI1_l = root_scalar(lambda x: f_err1_l(x) - templateMean, bracket = [-0.99,0.99]).root
    #CI1_h = root_scalar(lambda x: f_err1_h(x) - templateMean, bracket = [-0.99,0.99]).root
    #CI2_l = root_scalar(lambda x: f_err2_l(x) - templateMean, bracket = [-0.99,0.99]).root
    #CI2_h = root_scalar(lambda x: f_err2_h(x) - templateMean, bracket = [-0.99,0.99]).root

    #get range of unambiguous calibration curve
    min_range = minimize_scalar(f_inter,method='bounded',bounds=[-0.99,0.]).x
    max_range = minimize_scalar(f_inter_neg,method='bounded',bounds=[0.,0.99]).x
    calib_range = [min_range,max_range]
    #find confidence intervals where they exist
    mean1 = getSensitivity(f_inter,templateMean,calib_range)
    CI1_l = getSensitivity(f_err1_l,templateMean,calib_range)
    CI1_h = getSensitivity(f_err1_h,templateMean,calib_range)
    CI2_l = getSensitivity(f_err2_l,templateMean,calib_range)
    CI2_h = getSensitivity(f_err2_h,templateMean,calib_range)
    return mean1,[CI1_l,CI1_h],[CI2_l,CI2_h]

def calcErrs(mean,CI):
    if CI[0] and CI[1] and mean:
        err_l = abs(CI[0]-mean)
        err_h = abs(CI[1]-mean)
    else:
        err_l = None
        err_h = None
    return [err_l,err_h]

oo_reslist,dp_reslist = getValues(Nsamples,dtildelist,TGen,indirname)

#print ("*************************oo1**************************")
#print("dtilde values : ",dtildelist)
#print("mean values : ",oo_reslist[0])
#print("1 sig CIs : ",oo_reslist[1])
#print("2 sig CIs : ",oo_reslist[2])


#print("************************delta Phi**************************")
#print("dtilde values : ",dtildelist)
#print("mean values : ",dp_reslist[0])
#print("1 sig CIs : ",dp_reslist[1])
#print("2 sig CIs : ",dp_reslist[2])


############## create output directory #################### 
#if os.path.exists(outdirname):
#        pass
#else:
#    os.mkdir(outdirname)
#    print "directory "+outdirname+" created!"
#os.chdir(outdirname)

############################################################

#dd = np.linspace(-1.0,1.0,10000)
#ddarr = array('d',dd)
#ex = array('d',[0. for d in dd])
#oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)

#print "*************************oo1**************************"

#oo1hists = [interpol(d,dtildelist,oo1templatelist) for d in dd]



#oo1meanlist = array('d',oo_reslist[0])




oox = np.array([float(d) for d in dtildelist])
oo1meanlist = np.array(oo_reslist[0])

ooerr1_l,ooerr1_h,ooerr2_l,ooerr2_h =[],[], [], []
for i,j in enumerate(oo_reslist[1]):
    ooerr1_l.append(abs(j[0]-oo1meanlist[i]))
    ooerr1_h.append(abs(j[1]-oo1meanlist[i]))
for i,j in enumerate(oo_reslist[2]):    
    ooerr2_l.append(abs(j[0]-oo1meanlist[i]))
    ooerr2_h.append(abs(j[1]-oo1meanlist[i]))
#1 sigma errs
oo1errlist_l = np.array(ooerr1_l)
oo1errlist_h = np.array(ooerr1_h)
#2 sigma errs
oo2errlist_l = np.array(ooerr2_l)
oo2errlist_h = np.array(ooerr2_h)


dpx = np.array([float(d) for d in dtildelist])
dp1meanlist = np.array(dp_reslist[0])

dperr1_l,dperr1_h,dperr2_l,dperr2_h = [],[],[], []
for i,j in enumerate(dp_reslist[1]):
    dperr1_l.append(abs(j[0]-dp1meanlist[i]))
    dperr1_h.append(abs(j[1]-dp1meanlist[i]))
for i,j in enumerate(dp_reslist[2]):    
    dperr2_l.append(abs(j[0]-dp1meanlist[i]))
    dperr2_h.append(abs(j[1]-dp1meanlist[i]))
#1 sigma errs
dp1errlist_l = np.array(dperr1_l)
dp1errlist_h = np.array(dperr1_h)
#2 sigma errs
dp2errlist_l = np.array(dperr2_l)
dp2errlist_h = np.array(dperr2_h)


######### find confidence intervals ########
print("#### bkg_ratio : ",Background_Ratio , " ####")
print("#### dtilde true : ",dtilde_hyp , " ####")
print("**** OO Estimator **** ")
mean_hyp_oo,mean_hyp_dp = getHypMeans(dtilde_hyp,templateHistFileName)
oo_mean_est , oo_CI1, oo_CI2 = getConfIntHyp(mean_hyp_oo,oox,oo_reslist)

print("dtilde_est : ",oo_mean_est)
print("1 sigma CI : ",oo_CI1)
print("2 sigma CI : ",oo_CI2) 
oo_1sigerrs = calcErrs(oo_mean_est,oo_CI1)
oo_2sigerrs = calcErrs(oo_mean_est,oo_CI2)
print("result with 1 sig errs: ", oo_mean_est , " + ",oo_1sigerrs[1]," - ", oo_1sigerrs[0])
print("result with 2 sig errs: ", oo_mean_est , " + ",oo_2sigerrs[1]," - ", oo_2sigerrs[0])

print("**** delPhi Estimator **** ")

dp_mean_est , dp_CI1, dp_CI2 = getConfIntHyp(mean_hyp_dp,dpx,dp_reslist)

print("dtilde_est : ",dp_mean_est)
print("1 sigma CI : ",dp_CI1)
print("2 sigma CI : ",dp_CI2) 
dp_1sigerrs = calcErrs(dp_mean_est,dp_CI1)
dp_2sigerrs = calcErrs(dp_mean_est,dp_CI2)
print("result with 1 sig errs: ", dp_mean_est , " + ",dp_1sigerrs[1]," - ", dp_1sigerrs[0])
print("result with 2 sig errs: ", dp_mean_est , " + ",dp_2sigerrs[1]," - ", dp_2sigerrs[0])

errxl = np.array([0. for x in oox])
errxh= errxl
#plt.errorbar(oox,oo1meanlist,yerr=[oo1errlist_l,oo1errlist_h],fmt = "X" )
#plt.title("OO 1 sigma intervals")
#plt.grid()
#plt.show()

    
oo1Graph = ROOT.TGraphAsymmErrors(len(oox),oox,oo1meanlist,errxl,errxh,oo1errlist_l,oo1errlist_h)

oo1Graph.SetTitle("< OO > ; #tilde{d}  ; < OO >" )
oo1Graph.SetMarkerStyle(1)
oo1Graph.SetMarkerColor(1)

canv_oo1 = ROOT.TCanvas("canvasoo1")
canv_oo1.cd()
canv_oo1.SetGrid()
oo1Graph.Draw("AP")


canv_oo1.SaveAs("Mean_oo1_dtilde.png")





errxl = np.array([0. for x in dpx])
errxh= errxl
#plt.errorbar(dpx,dp1meanlist,yerr=[dp1errlist_l,dp1errlist_h],fmt = "X" )
#plt.title("delPhi 1 sigma intervals")
#plt.grid()
#plt.show()

    
dp1Graph = ROOT.TGraphAsymmErrors(len(dpx),dpx,dp1meanlist,errxl,errxh,dp1errlist_l,dp1errlist_h)

dp1Graph.SetTitle("< #Delta #Phi_{jj} > ; #tilde{d}  ; < #Delta #Phi_{jj} >" )
dp1Graph.SetMarkerStyle(1)
dp1Graph.SetMarkerColor(1)

canv_dp1 = ROOT.TCanvas("canvasdp1")
canv_dp1.cd()
canv_dp1.SetGrid()
dp1Graph.Draw("AP")


canv_dp1.SaveAs("Mean_delPhi_dtilde.png")

