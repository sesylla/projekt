import ROOT
import sys
import os
import numpy as np
from scipy.stats import skellam
from scipy.optimize import minimize
from scipy.special import ive
from scipy.special import iv
import matplotlib.pyplot as plt

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirHist = sys.argv[1]
dtilde = sys.argv[2]

#dtilde = -0.5
print dtilde,type(dtilde)
pTc = sys.argv[3]
massc = sys.argv[4]
deletac = sys.argv[5]

dirDiff = "diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)

HistFileName = dirHist+"/"+dirHist+".root"
diffFileName = dirDiff+"/"+dirDiff+".root"
oo1diffHistName = "oo1Hist_diff" 

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
def Lpdf(k,mu1,mu2):
    
    #return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+np.log(iv(k,2*np.sqrt(mu1*mu2)))
    #return -mu1-mu2+(k/2.)*np.log(mu1/mu2)+2*np.sqrt(mu1*mu2)+np.log(ive(k,2*np.sqrt(mu1*mu2)))
    return np.exp(-(mu1+mu2))*(mu1/mu2)**(k/2)*np.exp(-2*np.sqrt(mu1*mu2))+ive(k,2*np.sqrt(mu1+mu2))

def NLL(x,hist_data,hist_template1,hist_template2):
    res = 0
    #print "................................................."
    #print x
    for d, t1, t2 in zip(hist_data,hist_template1,hist_template2):
        f = Lpdf(d,x*t1,x*t2)
        #print [x,d,t1,t2,np.sqrt(2*x*np.sqrt(t1*t2)),ive(d,2*x*np.sqrt(t1*t2))]
        #print kv(d,2*x*np.sqrt(t1*t2))
        #print f
        res += f
    print x,2*x*np.sqrt(t1*t2),ive(d,x*np.sqrt(t1*t2)),-res
    return -res




def minNLL(hist_data,template1,template2): #np arrays as arguments 
    
    

    #print hist_data
    #print hist_template
    x0 = np.array([0.8])

    res = minimize(NLL,x0,args=(hist_data,template1,template2),bounds=[(0.5,10)] )

    print(res)
    err = np.diag(res.hess_inv.todense())

    print("result for x: %s +- %s" % (res.x, err))

print "*************************oo1 Fit**************************"


dtildelist = [-1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
x0 = np.array([5.])
minvals=[]


#plt.plot(dtildelist,minvals,'o')
#plt.show()
#print "************************delta Phi Fit**************************"


#minNLL(delPhiHist)
    
#def NLL(x,hist_data,hist_template):
#    res = 0
#    for d, t in zip(hist_data,hist_template):
#        f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
#        res += f
#    return -res
dtilde = -0.5
print "------------- "+str(dtilde)+"------------------------------------------------------------------------------------------------------------------"
dirDiff = "diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
diffFileName = dirDiff+"/"+dirDiff+".root"
HistFile = ROOT.TFile.Open(HistFileName,"READ")
diffFile = ROOT.TFile.Open(diffFileName, "READ")
oo1Hist = HistFile.Get(oo1HistName)
oo1diffHist = diffFile.Get(oo1diffHistName)
oo1Hist.Scale( 200/oo1Hist.Integral())
print "INTEGRALS: ",oo1Hist.Integral(),oo1diffHist.Integral()
for name,hist in zip([oo1diffHistName,oo1HistName],[oo1diffHist,oo1Hist]):
    if not hist:
        print "hist %s  not found" % (name)
        sys.exit(1)
oo1list = [oo1Hist.GetBinContent(bin+1) for bin in range(oo1Hist.GetSize()-2)]
if oo1diffHist.GetBinContent(1)>=0:
    oo1difflist = [oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
else:
    oo1difflist = [-oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
mirroo1list = [oo1list[len(oo1list)-i-1] for i in range(len(oo1list))]
data = np.array([oo1difflist[i] for i in range(len(oo1difflist)/2,len(oo1difflist))],dtype = np.double)
#template = data
template1 = np.array([oo1list[i] for i in range(len(oo1list)/2,len(oo1list))],dtype = np.double)
template2 = np.array([mirroo1list[i] for i in range(len(mirroo1list)/2,len(mirroo1list))],dtype = np.double)
print template1
print template2
print data
print data-(template1-template2)
minNLL(data,template1,template2)
res = minimize(NLL,x0,args=(data,template1,template2),bounds=[(0.5,10)] )
minvals.append(res.x)
#minNLL(data,template1,template2)
print dtilde,res.x
