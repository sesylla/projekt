#from mpl_toolkits.mplot3d import axes3d, Axes3D

import numpy as np
from array import array

import matplotlib.pyplot as plt
from matplotlib import cm
import ROOT

#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
#import os

if len(sys.argv) != 4:
    print("USAGE: <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)


ptcut = sys.argv[1]
masscut = sys.argv[2]
deletacut = sys.argv[3]

PartCollDirName = "PartColl_0.01_255004"





#dtildelist for templates:
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']


def makediffarrs(dtildelist,pTCut,massCut,delEtaCut):
    binarroo1,binarrdP,binerrsoo1,binerrsdP =[],[], [], [] #[[first bins for all dtilde],[second bins],....]
    
    for i,dtilde in enumerate(dtildelist):
            
            
        dirname = "SMNorm_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
            
    
        HistFileName =  dirname+"/"+dirname+".root"
        #these are for diff Histo:
        HistFile = ROOT.TFile.Open(HistFileName,"READ")

        oo1HistName= "oo1Hist_diff"
        delPhiHistName= "delPhiHist_diff"

        oo1Hist = HistFile.Get(oo1HistName)
        delPhiHist = HistFile.Get(delPhiHistName)
        
        if i==0:
            for bin in range(oo1Hist.GetSize()):
                binarroo1.append([oo1Hist.GetBinContent(bin)])
                binerrsoo1.append([oo1Hist.GetBinError(bin)])
            for bin in range(delPhiHist.GetSize()):
                binarrdP.append([delPhiHist.GetBinContent(bin)])
                binerrsdP.append([delPhiHist.GetBinError(bin)])
        else:
            for bin in range(oo1Hist.GetSize()):
                binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
            for bin in range(delPhiHist.GetSize()):
                binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                binerrsdP[bin].append(delPhiHist.GetBinError(bin))
   
       

    return binarroo1,binarrdP,binerrsoo1,binerrsdP


def makeplotoo1(dtildelist,binarr):
    
    
    for bin in range(len(binarr)):
        plt.plot(dtildelist,binarr[bin],label="bin "+str(bin))
    plt.legend(loc='best', frameon=False)
    plt.grid()
    plt.title("OO")
    plt.xlabel(r"$\tilde{d}$")
    plt.ylabel(r"Bin Content")
    plt.show()

def makeplotdP(dtildelist,binarr):
    
    
    for bin in range(len(binarr)):
        plt.plot(dtildelist,binarr[bin],label="bin "+str(bin))
    plt.legend(loc='best', frameon=False)
    plt.grid()
    plt.title(r"$\Delta \Phi_{jj}$")
    plt.xlabel(r"$\tilde{d}$")
    plt.ylabel(r"Bin Content")
    plt.show()

def getciROOT(dtildelist,binlist):
    ci_list,ci_err_list = [],[]
    dtildearr = array('d',dtildelist)
    
    for bin in range(len(binlist)):
        binarr = array('d',binlist[bin])
        g = ROOT.TGraph(len(binarr),dtildearr,binarr)
        f1 = ROOT.TF1("f1","x*[0]",-1,1)
        f1.SetParameter(0,1)
        g.Fit(f1,"WQ")
        ci_list.append(f1.GetParameter(0))
        ci_err_list.append(f1.GetParError(0))
    return ci_list,ci_err_list

def getwciROOT(dtildelist,binlist,errlist): #weighted fit x*[0]
    ci_list,ci_err_list = [],[]
    dtildearr = array('d',dtildelist)
    f1 = ROOT.TF1("f1","x*[0]",-1,1)
    f1.SetParameter(0,1)
    
    for bin in range(len(binlist)):
        binarr = array('d',binlist[bin])
        errarr = array('d',errlist[bin])
        xerrarr = array('d',[0 for i in range(len(dtildelist))])
        g = ROOT.TGraphErrors(len(binarr),dtildearr,binarr,xerrarr,errarr)
        
        g.Fit(f1,"Q")
        ci_list.append(f1.GetParameter(0))
        ci_err_list.append(f1.GetParError(0))
    return ci_list,ci_err_list

def getwciROOT2(dtildelist,binlist,errlist): #weighted fit x*[0]+[1]
    ci_list,ci_err_list = [],[]
    dtildearr = array('d',dtildelist)
    f1 = ROOT.TF1("f1","x*[0]+[1]",-1,1)
    f1.SetParameter(0,1)
    f1.SetParameter(1,1)
    for bin in range(len(binlist)):
        binarr = array('d',binlist[bin])
        errarr = array('d',errlist[bin])
        xerrarr = array('d',[0 for i in range(len(dtildelist))])
        g = ROOT.TGraphErrors(len(binarr),dtildearr,binarr,xerrarr,errarr)
        
        g.Fit(f1,"Q")
        ci_list.append(f1.GetParameter(0))
        ci_err_list.append(f1.GetParError(0))
        #print("b, bin ",bin, ": ",f1.GetParameter(1), " +- ",f1.GetParError(1))
    return ci_list,ci_err_list

def getci(dtildelist,binarr):
    ci_list,ci_err_list = [] ,[]
    for bin in range(len(binarr)):
        l,cov=np.polyfit(dtildelist,binarr[bin],1,cov=True)
        #p=np.poly1d(l)
        #print "Bin Nr. ", bin
        #print 'Slope and y-intercept:'
        #print l
        #print "covariance matrix:"
        #print cov
        #print 'Error on slope and y-intercept:'
        errs=np.sqrt(np.diag(cov))
        #print errs
        ci_list.append(l[0])
        ci_err_list.append(errs[0])
    return ci_list,ci_err_list

binarroo10,binarrdP0,binerrsoo10,binerrsdP0 = makediffarrs(dtildelist,ptcut,masscut,deletacut)


dtildevals = [float(dtildelist[d]) for d in range(len(dtildelist))] 
binarroo1 = [[binarroo10[i][n] for n in range(len(dtildelist))] for i in range(len(binarroo10)//2,len(binarroo10))]
binarrdP = [[binarrdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binarrdP0)//2,len(binarrdP0))]
binerrsoo1 = [[binerrsoo10[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsoo10)//2,len(binerrsoo10))]
binerrsdP = [[binerrsdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsdP0)//2,len(binerrsdP0))]

print("********************************************************")
print("get ci from np.polyfit , f(d) = bi0 + ci*dtilde ")
print("OO : ")
cioo,erroo = getci(dtildevals,binarroo1)
print(cioo)
print("get errs:")
print(erroo)

print("delPhi : ")
cidp,errdp = getci(dtildevals,binarrdP)
print(cidp)
print("get errs:")
print(errdp)

print("********************************************************")
print("get ci from ROOT , f(d) = ci*dtilde ")
print("OO : ")
cioo,erroo = getciROOT(dtildevals,binarroo1)
print(cioo)
print("get errs:")
print(erroo)

print("delPhi : ")
cidp,errdp = getciROOT(dtildevals,binarrdP)
print(cidp)
print("get errs:")
print(errdp)

print("********************************************************")
print("get ci from ROOT/weighted fit , f(d) = ci*dtilde ")
print("OO : ")
cioo,erroo = getwciROOT(dtildevals,binarroo1,binerrsoo1)
print(cioo)
print("get errs:")
print(erroo)

print("delPhi : ")
cidp,errdp = getwciROOT(dtildevals,binarrdP,binerrsdP)
print(cidp)
print("get errs:")
print(errdp)

print("********************************************************")
print("get ci from ROOT/weighted fit , f(d) = ci*dtilde + b ")
print("OO : ")
cioo,erroo = getwciROOT2(dtildevals,binarroo1,binerrsoo1)
print(cioo)
print("get errs:")
print(erroo)

print("delPhi : ")
cidp,errdp = getwciROOT2(dtildevals,binarrdP,binerrsdP)
print(cidp)
print("get errs:")
print(errdp)


#makeplotoo1(dtildevals,binarroo1)

#makeplotdP(dtildevals,binarrdP)

