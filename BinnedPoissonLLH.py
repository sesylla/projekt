import numpy as np
import scipy.stats
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import sys

fig, ax = plt.subplots(1, 1)


import ROOT
import sys
import os

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirname = sys.argv[1] 
HistFileName =  sys.argv[1]+"/"+sys.argv[1]+".root"
dtilde = sys.argv[2]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]

HistFile = ROOT.TFile.Open(HistFileName,"READ")

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)



def makearrays(hist):
    datalist = []
    templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1
    #datalist.append(int(oo1Hist.GetBinContent(bin)))
    #templatelist.append(int(oo1Hist.GetBinContent(bin)))
        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data,hist_template

   

def pdf(k,mu): #log(pdf)
    return -mu + k*np.log(mu)

def NLL(x,hist_data,hist_template):
    res = 0
    for d, t in zip(hist_data,hist_template):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = pdf(d,x*t)
        res += f
    return -res


#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(hist):
    
    hist_data, hist_template =  makearrays(hist)

    print hist_data
    print hist_template
    x0 = np.array([5])

    res = minimize(NLL,x0,args=(hist_data,hist_template),bounds=[(1e-6,10)] )

    print(res)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    err = np.diag(res.hess_inv.todense())

    print("result for x: %s +- %s" % (res.x, err))

delPhiHist = HistFile.Get(delPhiHistName)
oo1Hist = HistFile.Get(oo1HistName)


print "*************************oo1 Fit**************************"
minNLL(oo1Hist)
print "************************delta Phi Fit**************************"
minNLL(delPhiHist)
