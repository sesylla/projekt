import csv
import os
import matplotlib.pyplot as plt
import numpy as np
path = "/home/ss944/projekt/CovPlots/"
plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral' 
plt.rcParams.update({'lines.markeredgewidth': 1})

dtildelist = ["0.00","0.20","-0.05"]
oo_names = ["oo_"+d+"_res.csv" for d in dtildelist]
dp_names = ["dp_"+d+"_res.csv" for d in dtildelist]

print(os.path.abspath(path))
if not os.path.exists(path):
    os.mkdir(path)
    print("ok.")
def plotConfInts(dist,names):
    blist,estlist,lci1list,hci1list,lci2list,hci2list,dlist,cov1list,cov2list = [],[],[],[],[],[],[],[],[]
    for d,i in zip(dtildelist,names):
        with open(i) as csvfile:
            reader = csv.reader(csvfile,delimiter=';')
            for row in reader:
                dlist.append(d)
                blist.append(float(row[0]))
                estlist.append(float(row[1]))
                lci1list.append(float(row[2]))
                hci1list.append(float(row[3]))
                lci2list.append(float(row[4]))
                hci2list.append(float(row[5]))
                cov1list.append(float(row[6]))
                cov2list.append(float(row[7]))
    cov1errs=[np.sqrt(j*(1-j)/1700.) for j in cov1list]
    cov2errs=[np.sqrt(j*(1-j)/1700.) for j in cov2list]
    
    err1 = [[abs(estlist[k]-lci1list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci1list[k]) for k in range(len(estlist))]]
    err2 = [[abs(estlist[k]-lci2list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci2list[k]) for k in range(len(estlist))]]

    blist000=blist[0:5]
    blist020=blist[5:10]
    blist005=blist[10:]

    estlist000=estlist[0:5]

    err1list000=[err1[0][0:5],err1[1][0:5]]
    err2list000=[err2[0][0:5],err2[1][0:5]]

    cov1list000=cov1list[0:5]
    cov2list000=cov2list[0:5]
    cov1errs000=cov1errs[0:5]
    cov2errs000=cov2errs[0:5]

    estlist020=estlist[5:10]

    err1list020=[err1[0][5:10],err1[1][5:10]]
    err2list020=[err2[0][5:10],err2[1][5:10]]

    cov1list020=cov1list[5:10]
    cov2list020=cov2list[5:10]
    cov1errs020=cov1errs[5:10]
    cov2errs020=cov2errs[5:10]

    estlist005=estlist[10:]

    err1list005=[err1[0][10:],err1[1][10:]]
    err2list005=[err2[0][10:],err2[1][10:]]

    cov1list005=cov1list[10:]
    cov2list005=cov2list[10:]
    cov1errs005=cov1errs[10:]
    cov2errs005=cov2errs[10:]
    ### 1 sigma ###
    #print(blist000)
    plt.errorbar(blist000,estlist000,yerr=err1list000,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.00 $",color="Black",marker="x",ms=6.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist020,estlist020,yerr=err1list020,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20 $",color="Red",marker="s",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist005,estlist005,yerr=err1list005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
    plt.grid()
    plt.xlabel(r"$r_b$",fontsize=12)
    plt.ylabel(r"$\hat{\tilde{d}}$",fontsize=12)
    plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_CI1.pdf")
    plt.close()
    #### 2sigma ###
    plt.errorbar(blist000,estlist000,yerr=err2list000,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.00 $",color="Black",marker="x",ms=6.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist020,estlist020,yerr=err2list020,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20 $",color="Red",marker="s",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist005,estlist005,yerr=err2list005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
    plt.grid()
    plt.xlabel(r"$r_b$",fontsize=12)
    plt.ylabel(r"$\hat{\tilde{d}}$",fontsize=12)
    plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_CI2.pdf")
    plt.close()

def getConfIntsSM(dist,names):
    blist,estlist,lci1list,hci1list,lci2list,hci2list,dlist,cov1list,cov2list = [],[],[],[],[],[],[],[],[]
    for d,i in zip(dtildelist,names):
        with open(i) as csvfile:
            reader = csv.reader(csvfile,delimiter=';')
            for row in reader:
                dlist.append(d)
                blist.append(float(row[0]))
                estlist.append(float(row[1]))
                lci1list.append(float(row[2]))
                hci1list.append(float(row[3]))
                lci2list.append(float(row[4]))
                hci2list.append(float(row[5]))
                cov1list.append(float(row[6]))
                cov2list.append(float(row[7]))
    cov1errs=[np.sqrt(j*(1-j)/1700.) for j in cov1list]
    cov2errs=[np.sqrt(j*(1-j)/1700.) for j in cov2list]
    
    err1 = [[abs(estlist[k]-lci1list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci1list[k]) for k in range(len(estlist))]]
    err2 = [[abs(estlist[k]-lci2list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci2list[k]) for k in range(len(estlist))]]

    blist000=blist[0:5]
    blist020=blist[5:10]
    blist005=blist[10:]

    estlist000=estlist[0:5]

    err1list000=[err1[0][0:5],err1[1][0:5]]
    err2list000=[err2[0][0:5],err2[1][0:5]]

    return blist000,estlist000,err2list000


def plotCov1(dist,names):
    blist,estlist,lci1list,hci1list,lci2list,hci2list,dlist,cov1list,cov2list = [],[],[],[],[],[],[],[],[]
    for d,i in zip(dtildelist,names):
        with open(i) as csvfile:
            reader = csv.reader(csvfile,delimiter=';')
            for row in reader:
                dlist.append(d)
                blist.append(float(row[0]))
                estlist.append(float(row[1]))
                lci1list.append(float(row[2]))
                hci1list.append(float(row[3]))
                lci2list.append(float(row[4]))
                hci2list.append(float(row[5]))
                cov1list.append(float(row[6]))
                cov2list.append(float(row[7]))
    cov1errs=[np.sqrt(j*(1-j)/1700.) for j in cov1list]
    cov2errs=[np.sqrt(j*(1-j)/1700.) for j in cov2list]
    
    err1 = [[abs(estlist[k]-lci1list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci1list[k]) for k in range(len(estlist))]]
    err2 = [[abs(estlist[k]-lci2list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci2list[k]) for k in range(len(estlist))]]

    blist000=blist[0:5]
    blist020=blist[5:10]
    blist005=blist[10:]

    estlist000=estlist[0:5]

    err1list000=[err1[0][0:5],err1[1][0:5]]
    err2list000=[err2[0][0:5],err2[1][0:5]]

    cov1list000=cov1list[0:5]
    cov2list000=cov2list[0:5]
    cov1errs000=cov1errs[0:5]
    cov2errs000=cov2errs[0:5]

    estlist020=estlist[5:10]

    err1list020=[err1[0][5:10],err1[1][5:10]]
    err2list020=[err2[0][5:10],err2[1][5:10]]

    cov1list020=cov1list[5:10]
    cov2list020=cov2list[5:10]
    cov1errs020=cov1errs[5:10]
    cov2errs020=cov2errs[5:10]

    estlist005=estlist[10:]

    err1list005=[err1[0][10:],err1[1][10:]]
    err2list005=[err2[0][10:],err2[1][10:]]

    cov1list005=cov1list[10:]
    cov2list005=cov2list[10:]
    cov1errs005=cov1errs[10:]
    cov2errs005=cov2errs[10:]

    #print(blist000)
    plt.errorbar(blist000,cov1list000,yerr=cov1errs000,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.00 $",color="Black",marker="x",ms=6.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist020,cov1list020,yerr=cov1errs020,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20 $",color="Red",marker="s",ms=4.,linestyle="None",capsize=4,linewidth=1)
    plt.errorbar(blist005,cov1list005,yerr=cov1errs005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
    plt.grid()
    plt.axhline(y=0.683,linestyle="--",color="Green")
    plt.text(2.1,0.680,r"$68.3\%$",color="green",fontsize=13)
    plt.xlabel(r"$r_b$",fontsize=12)
    plt.ylabel(r"$r_1$",fontsize=12)
    plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_cov1.pdf",dpi=100)
    plt.close()

def plotCov2(dist,names):
    blist,estlist,lci1list,hci1list,lci2list,hci2list,dlist,cov1list,cov2list = [],[],[],[],[],[],[],[],[]
    for d,i in zip(dtildelist,names):
        with open(i) as csvfile:
            reader = csv.reader(csvfile,delimiter=';')
            for row in reader:
                dlist.append(d)
                blist.append(float(row[0]))
                estlist.append(float(row[1]))
                lci1list.append(float(row[2]))
                hci1list.append(float(row[3]))
                lci2list.append(float(row[4]))
                hci2list.append(float(row[5]))
                cov1list.append(float(row[6]))
                cov2list.append(float(row[7]))
    cov1errs=[np.sqrt(j*(1-j)/1700.) for j in cov1list]
    cov2errs=[np.sqrt(j*(1-j)/1700.) for j in cov2list]
    
    err1 = [[abs(estlist[k]-lci1list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci1list[k]) for k in range(len(estlist))]]
    err2 = [[abs(estlist[k]-lci2list[k]) for k in range(len(estlist))],[abs(estlist[k]-hci2list[k]) for k in range(len(estlist))]]

    blist000=blist[0:5]
    blist020=blist[5:10]
    blist005=blist[10:]

    estlist000=estlist[0:5]

    err1list000=[err1[0][0:5],err1[1][0:5]]
    err2list000=[err2[0][0:5],err2[1][0:5]]

    cov1list000=cov1list[0:5]
    cov2list000=cov2list[0:5]
    cov1errs000=cov1errs[0:5]
    cov2errs000=cov2errs[0:5]

    estlist020=estlist[5:10]

    err1list020=[err1[0][5:10],err1[1][5:10]]
    err2list020=[err2[0][5:10],err2[1][5:10]]

    cov1list020=cov1list[5:10]
    cov2list020=cov2list[5:10]
    cov1errs020=cov1errs[5:10]
    cov2errs020=cov2errs[5:10]

    estlist005=estlist[10:]

    err1list005=[err1[0][10:],err1[1][10:]]
    err2list005=[err2[0][10:],err2[1][10:]]

    cov1list005=cov1list[10:]
    cov2list005=cov2list[10:]
    cov1errs005=cov1errs[10:]
    cov2errs005=cov2errs[10:]

    #print(blist000)
    plt.errorbar(blist000,cov2list000,yerr=cov2errs000,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.00 $",color="Black",marker="x",ms=6.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist020,cov2list020,yerr=cov2errs020,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20 $",color="Red",marker="s",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    plt.errorbar(blist005,cov2list005,yerr=cov2errs005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
    plt.grid()
    plt.axhline(y=0.954,linestyle="--",color="Green")
    plt.text(2.1,0.953,r"$95.4\%$",color="green",fontsize=13)
    
    plt.xlabel(r"$r_b$",fontsize=12)
    plt.ylabel(r"$r_2$",fontsize=12)
    plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_cov2.pdf")
    plt.close()

os.chdir("testres")




os.chdir("SMVBF_SkPResults3")

#plotConfInts("skel",oo_names)
#plotCov1("skel",oo_names)
#plotCov2("skel",oo_names)

#plotConfInts("skel",dp_names)
#plotCov1("skel",dp_names)
#plotCov2("skel",dp_names)

os.chdir("..")
os.chdir("SMVBFPoisResults3")

#plotConfInts("pois",oo_names)
#plotCov1("pois",oo_names)
#plotCov2("pois",oo_names)

#plotConfInts("pois",dp_names)
#plotCov1("pois",dp_names)
#plotCov2("pois",dp_names)

os.chdir("..")

os.chdir("SMVBF_SkPResults3")
b,ooskest,ooskerrs2 = getConfIntsSM("skel",oo_names)
b,dpskest,dpskerrs2 = getConfIntsSM("skel",dp_names)
os.chdir("..")
os.chdir("SMVBFPoisResults3")
b,oopoisest,oopoiserrs2=getConfIntsSM("pois",oo_names)
b,dppoisest,dppoiserrs2=getConfIntsSM("pois",dp_names)
os.chdir("..")

plt.errorbar(b,oopoisest,yerr=oopoiserrs2,xerr=None,label=r"Verteilung",color="g",marker="s",ms=6.,linestyle="None",capsize=4,linewidth=1.)
plt.errorbar(b,ooskest,yerr=ooskerrs2,xerr=None,label=r"Asymmetrie",color="m",marker="x",ms=10.,linestyle="None",capsize=4,linewidth=1.)
#plt.errorbar(blist005,estlist005,yerr=err1list005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
plt.grid()
plt.xlabel(r"$r_b$",fontsize=12)
plt.ylabel(r"$\hat{\tilde{d}}$",fontsize=12)
plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_CI1.pdf")
plt.close()

plt.errorbar(b,dppoisest,yerr=dppoiserrs2,xerr=None,label=r"Verteilung",color="g",marker="s",ms=6.,linestyle="None",capsize=4,linewidth=1.)
plt.errorbar(b,dpskest,yerr=dpskerrs2,xerr=None,label=r"Asymmetrie",color="m",marker="x",ms=10.,linestyle="None",capsize=4,linewidth=1.)
#plt.errorbar(blist005,estlist005,yerr=err1list005,xerr=None,label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05 $",color="Blue",marker="^",ms=4.,linestyle="None",capsize=4,linewidth=1.)
    
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=3)
plt.grid()
plt.xlabel(r"$r_b$",fontsize=12)
plt.ylabel(r"$\hat{\tilde{d}}$",fontsize=12)
plt.show()
    #plt.savefig(path+dist+"_"+names[0][:2]+"_CI1.pdf")
plt.close()


