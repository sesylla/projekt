import ROOT
import os
dtildelist = ["0.00","0.20","-0.05"]
os.chdir("evColl_test0.001")
f = ROOT.TFile.Open("evColl_test0.001.root","READ")


for d_hyp in dtildelist:
    oo1HistName1= "oo1_dtilde"+d_hyp
    delPhiHistName1= "dp_dtilde"+d_hyp
    oo1Hist1 = f.Get(oo1HistName1)
    delPhiHist1 = f.Get(delPhiHistName1)
    print("oo: ",d_hyp,round(oo1Hist1.GetMean(),3), round(oo1Hist1.GetMeanError(),3))
    print("dp: ",d_hyp,round(delPhiHist1.GetMean(),3),round(delPhiHist1.GetMeanError(),3))
