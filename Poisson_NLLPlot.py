import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
#import matplotlib.pyplot as plt


#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
import os

if len(sys.argv) != 7:
    print "USAGE: <source directory name> <output directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

outdirname = sys.argv[2]

#dirname = sys.argv[1] 
HistFileName =  sys.argv[1]+"/"+sys.argv[1]+".root"
dtilde = sys.argv[3]
pTCut = sys.argv[4]
massCut = sys.argv[5]
delEtaCut = sys.argv[6]

templateHistFileName = "PartColl/PartColl.root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

#dtildelist for templates:
#dtildelist = [-1.0,-0.95,-0.9,-0.85,-0.8,-0.75,-0.7,-0.65,-0.6,-0.55,-0.5,-0.45,-0.4,-0.35,-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,0.00,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.00]
#dtildelist = [-1.0,-.9,-.8,-.7,-.6,-.5,-.4,-.3,-.2,-.1,0.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1.0]

dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

def maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName):
    templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
    for dtilde in dtildelist:
        oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        
        oo1Hist = HistFile.Get(oo1HistName)
        delPhiHist = HistFile.Get(delPhiHistName)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist:
            print "histogram %s not found!"%(oo1HistName)
        if not delPhiHist:
            print "histogram %s not found!"%(delPhiHistName)
        
        oo1Hist.Scale( 200/oo1Hist.Integral())
        delPhiHist.Scale( 200/delPhiHist.Integral())

        oo1templatelist.append(oo1Hist)
        delPhitemplatelist.append(delPhiHist)
    return oo1templatelist,delPhitemplatelist

def makearray(hist): #returns numpy  array of data bins
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildelist,templates):
    #print "d value: ",d
    i=0
    dtildevals = [float(x) for x in dtildelist]
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print "dtilde out of range!"
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print "could not find x values for interpolation."
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #print "x= %d == x0 = %d"%(x,x0)
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            #print "x= %d == x0 = %d"%(x,x1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
            
            
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu): #log(pdf)
    return -mu + k*np.log(mu)

def NLL(params,hist_data,dtildelist,templatelist):
    x,dtilde = params
    res = 0
    #print x,dtilde
    #oo1templatelist, delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)
    hist_template = makearray(interpol(dtilde,dtildelist,templatelist))
    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    
    for d, t in zip(hist_data,hist_template):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = lpdf(d,x*t)
        res += f
    return -res


#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(hist,dtildelist,templatelist):
    
    hist_data = makearray(hist)

    #print hist_data
    #print hist_template
    startpar = np.array([0.1,-0.1])
    
    res = minimize(NLL,startpar,args=(hist_data,dtildelist,templatelist),bounds=[(1e-6,10),(-0.999,0.999)] )

    print(res)
    print res.x
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    err = np.diag(res.hess_inv.todense())
    print err
    print res.hess_inv.todense()
    print("result for x: %s +- ??" % (res.x[0]))
    print("result for dtilde: %s +- ??" % (res.x[1]))

####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]
    index_lo = tmp_lo.index(min(tmp_lo))
    index_hi = tmp_hi.index(min(tmp_hi))
    d_val_lo = dlow[index_lo]
    d_val_hi = dhi[index_hi]
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

delPhiHist = HistFile.Get(delPhiHistName)
oo1Hist = HistFile.Get(oo1HistName)
#delPhiHist.Sumw2()
#oo1Hist.Sumw2()
oo1Hist.Scale( 200/oo1Hist.Integral())
delPhiHist.Scale( 200/delPhiHist.Integral())

oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)

############## create output directory #################### 
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print "directory "+outdirname+" created!"
os.chdir(outdirname)

print "*************************oo1 Fit**************************"
#minNLL(oo1Hist,dtildelist,oo1templatelist)
hist_data = makearray(oo1Hist)
dd = np.linspace(-1.,1.0,10000)
NLLvals0oo1 = [NLL([1,d],hist_data,dtildelist,oo1templatelist) for d in dd]
minoo1 = min(NLLvals0oo1)
NLLvalsoo1 = array("d",[x - minoo1 for x in NLLvals0oo1])
print "----------------------------------------------------------------"

dminoo = findMin(dd,NLLvalsoo1)
dlim1oo = findConfInt(dd,NLLvalsoo1,1.)
dlim2oo = findConfInt(dd,NLLvalsoo1,2.)
print "result for OO with 1 sigma confidence interval: %f + %f / - %f"%(dminoo,dlim1oo[1]-dminoo,dlim1oo[0]-dminoo)
print "result for OO with 2 sigma confidence interval: %f + %f / - %f"%(dminoo,dlim2oo[1]-dminoo,dlim2oo[0]-dminoo)


print "-----------------------------------------------------------------"
    
oo1Graph = ROOT.TGraph(len(dd),dd,NLLvalsoo1)

oo1Graph.SetTitle("NLL for OO, #tilde{d}_{true} = %s ; #tilde{d}  ; -log(L(#tilde{d}))"%(dtilde) )
oo1Graph.SetMarkerStyle(1)
oo1Graph.SetMarkerColor(1)

canv_oo1 = ROOT.TCanvas("canvasoo1")
canv_oo1.cd()
canv_oo1.SetGrid()
oo1Graph.Draw("AP")
#canv_oo1.SaveAs("NLL_oo1_dtilde"+str(dtilde)+".pdf")
canv_oo1.SaveAs("NLL_oo1_dtilde"+str(dtilde)+".png")

#plt.plot(dd,NLLvalsoo1,'o',color = "Black",ms = .1)
#plt.show()


print "************************delta Phi Fit**************************"
#minNLL(delPhiHist,dtildelist,delPhitemplatelist)

hist_data = makearray(delPhiHist)
#dd = np.linspace(-1.0,1.0,10000)
NLLvals0dP = [NLL([1,d],hist_data,dtildelist,delPhitemplatelist) for d in dd]
mindP = min(NLLvals0dP)
NLLvalsdP = array("d",[x - mindP for x in NLLvals0dP])
print "----------------------------------------------------------------"

dmindp = findMin(dd,NLLvalsdP)
dlim1dp = findConfInt(dd,NLLvalsdP,1.)
dlim2dp = findConfInt(dd,NLLvalsdP,2.)
print "result for delPhi with 1 sigma confidence inverval: %f + %f / - %f"%(dmindp,dlim1dp[1]-dmindp,dlim1dp[0]-dmindp)
print "result for delPhi  with 2 sigma confidence interval: %f + %f / - %f"%(dmindp,dlim2dp[1]-dmindp,dlim2dp[0]-dmindp)


print "-----------------------------------------------------------------"
dPGraph = ROOT.TGraph(len(dd),dd,NLLvalsdP)

#dPGraph.SetTitle("NLL for  L(#tilde{d} | signed #Delta#Phi_{jj}), #tilde{d}_{true} = %s ; #tilde{d}  ; -log(L(#tilde{d}))" )
dPGraph.SetTitle("NLL for  signed #Delta#Phi_{jj}, #tilde{d}_{true} = %s ; #tilde{d}  ; -log(L(#tilde{d}))"%(dtilde) )
dPGraph.SetMarkerStyle(1)
oo1Graph.SetMarkerColor(1)

canv_dP = ROOT.TCanvas("canvasdP")
canv_dP.cd()
canv_dP.SetGrid()
dPGraph.Draw("AP")
#canv_dP.SaveAs("NLL_delPhi_dtilde"+str(dtilde)+".pdf")
canv_dP.SaveAs("NLL_delPhi_dtilde"+str(dtilde)+".png")
#plt.plot(dd,NLLvalsdP,'o',color = "Blue",ms = .1)

#plt.show()
