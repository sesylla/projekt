import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import csv
#fig, ax = plt.subplots(1, 1)




import ROOT
import sys
import os

if len(sys.argv) != 5:
    print ("USAGE: <source directory name> <dtilde> <N_samples> <Norm 0=SM, 1=N_Data >")
    sys.exit(1)
Norm = int(sys.argv[4])
outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 

dtilde = sys.argv[2]
N_sp = int(sys.argv[3])

#HistFileName =  sys.argv[1]+"/"+sys.argv[1]+"_samples.root"
#templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

#HistFile = ROOT.TFile.Open(HistFileName,"READ")
#templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")

HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+".root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3()

#constants from fit N(d) = 200. + c*d**2
c_oo = 3147.5214538421055
c_dp = 3611.706482059846



dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

print("******* index: ",dtildelist.index("0.00")) 
#Normalize templates to data
def getnormfac(sample_hist,templateHistFile,dtilde,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" ).Clone()
        #SMhist = ROOT.TH1D("oo1_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ).Clone() 
        #SMhist = ROOT.TH1D("dp_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())

    else:
        print("problem in getnormfac.")
        sys.exit(1)
    ROOT.SetOwnership(SMhist,False)
    SMhist.SetDirectory(0)
    #for bin in range(SMhist.GetSize()):
    #    SMhist.SetBinContent(bin,SMhist0.GetBinContent(bin))
    N_sample = sample_hist.Integral()
        
    #print("sample integral: ",N_sample)
    ########### das hier evtl noch auschecken!!! #########
    #N_SM = N_sample - c * float(dtilde)**2
    #######################################
    N_SM = 200.
    #print("N_SM expectation: ",N_SM)
    SM_Integral = SMhist.Integral()


    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    
    
    
    return Normfac
    


    
def maketemplatelist(dtildelist,templateHistFile):
    
    #print("make template lists ...")
    
    oo1templatelist, delPhitemplatelist = [],[]

   

    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()
        
        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)

        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1HistName1))
        if not delPhiHist1:
            print( "histogram %s not found!"%(delPhiHistName1))
        
        
        oo1templatelist.append(oo1Hist1)
        delPhitemplatelist.append(delPhiHist1)
    return oo1templatelist,delPhitemplatelist

################# samples  ###########################


def makearray(hist): #returns numpy  array of data bins. 
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template



def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of sample histograms
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    for s in hnames:
        if "oo1_dtilde"+dtilde in s:
            hnamesoo.append(s)
        elif "dp_dtilde"+dtilde in s:
            hnamesdp.append(s)
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        #print("random indices :", i_sp_oo,i_sp_dp) 

        if sample_file.Get(hnamesoo[i_sp_oo]).Integral() != 0.:
            
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        
        dph = sample_file.Get(hnamesdp[i_sp_dp])
        if sample_file.Get(hnamesdp[i_sp_dp]).Integral() != 0.:
            
            dph = sample_file.Get(hnamesdp[i_sp_dp])
        else:
            i_sp_dp = gen_dp.__next__()
            dph = sample_file.Get(hnamesdp[i_sp_dp])
            
        hsamplesoo.append( ooh)
        hsamplesdp.append( dph)
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return hsamplesoo,hsamplesdp

                         

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildelist,templates):
    #print "d value: ",d
    i=0
    dtildevals = [float(x) for x in dtildelist]
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print( "dtilde out of range!")
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print ("could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #print "x= %d == x0 = %d"%(x,x0)
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            #print "x= %d == x0 = %d"%(x,x1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
            
            
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu): #log(pdf)
    return -mu + k*np.log(mu)

def NLLx(x,dtilde,hist_data,dtildelist,templatelist):
    
    res = 0
    #print x,dtilde
    #oo1templatelist, delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)
    hist_template = makearray(interpol(dtilde,dtildelist,templatelist))
    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    
    for d, t in zip(hist_data,hist_template):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = lpdf(d,x*t)
        res += f
    return -res

def NLL(dtil,hist_data0,dtildelist,templatelist):
    res = 0
    
    hist_template1 = makearray(interpol(dtil,dtildelist,templatelist))
    
        
        
    hist_data1 = makearray(hist_data0)

    # theory prediction can't be zero because of log:
    if 0. in hist_template1 :
        hist_template =np.delete(np.delete(hist_template1,-1),0)
        hist_data =np.delete(np.delete(hist_data1,-1),0)
    else:
        hist_template = hist_template1
        hist_data = hist_data1
    #print("lenghts : ",len(hist_data),len(hist_template))
    #print("types: ",type(hist_data),type(hist_template))
    #asdf=0
    #if abs(dtil) <= 0.2:
    #    asdf=1
    #    fdsa = 0.
    #    print("dtil ",dtil)
    for d, t in zip(hist_data,hist_template):
        #print( d,t)
        #if asdf == 1:
        #    fdsa+=d-t
            
        #    print(d,t)
        
            
        f = lpdf(d,t)
        res += f
    #if asdf==1:
    #    #print("-res: ",-res)
    #    print(fdsa/len(hist_data))
    #    print("----------")
    #    asdf=0
    return -res

#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(dtilde,histarr,dtildelist,templatelist,nr):
    
    hist_data = histarr

    #print hist_data
    #print hist_template
    startpar = np.array([1.])
    
    res = minimize(NLL,startpar,args=(dtilde,hist_data,dtildelist,templatelist),bounds=[(1e-6,10)] )

    
    #print(res)
    #print(dtilde,res.x,res.fun)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    #err = np.diag(res.hess_inv.todense())
    #print err
    #print res.hess_inv.todense()
    #print("result for x: %s +- ??" % (res.x[0]))
    #print("result for dtilde: %s +- ??" % (res.x[1]))
    return nr, res.fun # res.x is [x]
    

####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

def findConfInt_inter1d(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = np.array([x-N**2/2. for x in NLLlist ])
    NLL_inter = interp1d
    
# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)


def estimate(dtildelist,samplelist,templateHistFile,dtilde,c,norm):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    
    
    dd = np.linspace(float(dtilde)-0.2,float(dtilde)+0.2,2001)

    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi = [],[],[],[],[]
    intlist,sampleintlist =[], []
    #if c == c_oo:
    #    templatelist = oo1templatelist
    #elif c == c_dp:
    #    templatelist = delPhitemplatelist
    #else:
    #    print("problem in estimate.")
    for k,sample in enumerate(samplelist):
        if sample.Integral() == 0.:
            print("############# 0 found ##############")
            continue
        if k%1 == 0:
            print(k, " of ",len(samplelist), "samples processed. ")
        
        oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)

        if c == c_oo:
            templatelist = oo1templatelist
        elif c == c_dp:
            templatelist = delPhitemplatelist
        else:
            print("problem in estimate.")

        normfac = getnormfac(sample,templateHistFile,dtilde,c)
        #print("NORMFAC : ",normfac)
        if norm == 0:
            for h in templatelist:            
                h.Scale(normfac)
        elif norm == 1:
            for h in templatelist:
                h.Scale(sample.Integral()/h.Integral())
            
        
        #print("scaled 0.01 Integral", templatelist[51].Integral())
        #intlist.append(templatelist[50].Integral())
        #args = [(d,sample,dtildelist,templatelist,nr) for nr,d in enumerate(dd)]
        #fitresults = p.map(minNLL,args)
        #fitresults.sort(key=lambda x : x[0])
        #NLLvals0 = [result[1] for result in fitresults]
        NLLvals0 = [NLL(d,sample,dtildelist,templatelist) for d in dd]

        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        
        

        

        
        dmin = findMin(dd,NLLvals)
        
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        
        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))
        
        #plt.plot(dd,NLLvals)
        #plt.show()

        #os.chdir(outdirname)
       
        
        #canvas = ROOT.TCanvas("canvas")
        #Legend = ROOT.TLegend(0.6,0.7,0.9,0.9)
            
            
        #reshist = interpol(dmin,dtildelist,templatelist)

        #canvas.cd()

        #sample.SetLineColor(4)
        #reshist.SetLineColor(2)
        #templatelist[dtildelist.index(dtilde)].SetLineColor(1)
        #sample.Draw("HistE")
        #reshist.Draw("HistSameE")
        #templatelist[dtildelist.index(dtilde)].Draw("HistSameE")
        #Legend.AddEntry(sample,"sample, d="+dtilde,"l")
        #Legend.AddEntry(reshist,"result, d="+str(dmin),"l")
        #Legend.AddEntry(templatelist[dtildelist.index(dtilde)],"dtilde= "+dtilde+" prediction","l")
        #Legend.Draw()
        #canvas.Update()
        #if c == c_dp:
        #    canvas.SaveAs("dpres"+str(k)+".png")
        #elif c == c_oo:
        #    canvas.SaveAs("oores"+str(k)+".png")
        #os.chdir("..")
            #print("Scaled SM Integral" ,templatelist[50].Integral())
            #print("sample Integral", sample.Integral())
            #print("reshist integral", reshist.Integral())

    #print("SM prediction Integrals : ",intlist)
    #print("sample integrals : ",sampleintlist)
    return np.array(estlist), np.array(err1sig_lo), np.array(err1sig_hi), np.array(err2sig_lo), np.array(err2sig_hi)





#delPhiHist.Sumw2()
#oo1Hist.Sumw2()





#hist_data = makearray(oo1Hist)
oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
samplelist_oo,samplelist_dp = makesamplelist(N_sp,dtilde,HistFile,TGen)


############## FIT  #################### 
print("oo fit in process ...")
oo_estarr ,oo_1sigarr_lo,oo_1sigarr_hi,oo_2sigarr_lo,oo_2sigarr_hi = estimate(dtildelist,samplelist_oo,templateHistFile,dtilde,c_oo,Norm)
xxo = np.arange(len(oo_estarr))
#print(oo_estarr)

#print("dp fit in process ...")
print("delPhi fit in process ...") 
dp_estarr ,dp_1sigarr_lo,dp_1sigarr_hi,dp_2sigarr_lo,dp_2sigarr_hi = estimate(dtildelist,samplelist_dp,templateHistFile,dtilde,c_dp,Norm)
xxd = np.arange(len(dp_estarr))
#print(dp_estarr)

print("done with fitting.")



print("#######################################################")

print("ratios of coverage:")
cov1oo =  cover_ratio(oo_estarr,oo_1sigarr_lo,oo_1sigarr_hi,dtilde)
cov2oo =  cover_ratio(oo_estarr,oo_2sigarr_lo,oo_2sigarr_hi,dtilde)
cov1dp =  cover_ratio(dp_estarr,dp_1sigarr_lo,dp_1sigarr_hi,dtilde)
cov2dp =  cover_ratio(dp_estarr,dp_2sigarr_lo,dp_2sigarr_hi,dtilde)
print("OO 1 sigma :", cov1oo)
print("OO 2 sigma :", cov2oo)
print("delPhi 1 sigma :", cov1dp)
print("delPhi 2 sigma :", cov2dp)
oomean = np.mean(oo_estarr) 
dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
dpvar = np.var(dp_estarr)
oo1sigperc_lo = np.percentile(oo_estarr,(100.-68.27)/2.)
dp1sigperc_lo = np.percentile(dp_estarr,(100.-68.27)/2.)
oo2sigperc_lo = np.percentile(oo_estarr,(100.-95.44)/2.)
dp2sigperc_lo = np.percentile(dp_estarr,(100.-95.44)/2.)
#print(oo1sigperc_lo,oo2sigperc_lo)

oo1sigperc_hi = np.percentile(oo_estarr,100.-(100.-68.27)/2.)
dp1sigperc_hi = np.percentile(dp_estarr,100.-(100.-68.27)/2.)
oo2sigperc_hi = np.percentile(oo_estarr,100.-(100.-95.44)/2.)
dp2sigperc_hi =np.percentile(dp_estarr,100.-(100.-95.44)/2.)
oo1sigperc = [oo1sigperc_lo,oo1sigperc_hi]
oo2sigperc = [oo2sigperc_lo,oo2sigperc_hi]
dp1sigperc = [dp1sigperc_lo,dp1sigperc_hi]
dp2sigperc = [dp2sigperc_lo,dp2sigperc_hi]
print("sample means :",oomean,dpmean)
print("1sig percentiles oo/dp : ", oo1sigperc,dp1sigperc)
print("2sig percentiles oo/dp : ",oo2sigperc,dp2sigperc)
 
########### histograms of estimators ###########
os.chdir(outdirname)
coo = ROOT.TCanvas("oo")
h_est_oo = ROOT.TH1D("h_est_oo", " ", 20, oomean-0.1, oomean+0.1)
h_est_oo.SetStats(0)
h_est_oo.SetTitle(" ; #hat{#tilde{d}}_{ML} ; Anzahl")
if dtilde == "0.00":
    h_est_oo.SetLineColor(1)
    #delPhiHist.SetLineColor(1)
elif dtilde == "0.20":
    h_est_oo.SetLineColor(2)
    #delPhiHist.SetLineColor(2)
elif dtilde == "-0.05":
    h_est_oo.SetLineColor(4)
    #delPhiHist.SetLineColor(4)
h_est_oo.Sumw2()
for est in oo_estarr:
    h_est_oo.Fill(est)
coo.cd()
h_est_oo.Draw("HistE")



cdp = ROOT.TCanvas("dp")
h_est_dp = ROOT.TH1D("h_est_dp", " ", 20, dpmean-0.1, dpmean+0.1)
h_est_dp.SetStats(0)
if dtilde == "0.00":
    h_est_dp.SetLineColor(1)
    #delPhiHist.SetLineColor(1)
elif dtilde == "0.20":
    h_est_dp.SetLineColor(2)
    #delPhiHist.SetLineColor(2)
elif dtilde == "-0.05":
    h_est_dp.SetLineColor(4)
    #delPhiHist.SetLineColor(4)
h_est_dp.Sumw2()
for est in dp_estarr:
    h_est_dp.Fill(est)
h_est_dp.SetTitle(" ; #hat{#tilde{d}}_{ML} ; Anzahl")
cdp.cd()
h_est_dp.Draw("HistE")


########################## save data to csv file #################################
if Norm == 1:
    if not os.path.exists("N200PoisResults_noBkg3"):
        os.mkdir("N200PoisResults_noBkg3")
    os.chdir("N200PoisResults_noBkg3")
    coo.SaveAs("oo_est_pois_N200_"+dtilde+".png")
    cdp.SaveAs("dp_est_pois_N200_"+dtilde+".png")
    with open("PoisResults_N200_oo_"+dtilde+".csv", 'a') as csvfile:
        writer = csv.writer(csvfile,delimiter = ";")
        writer.writerow([oomean,oo1sigperc_lo,oo1sigperc_hi,oo2sigperc_lo,oo2sigperc_hi,cov1oo,cov2oo])
    with open("PoisResults_N200_dp_"+dtilde+".csv", 'a') as csvfile:
        writer = csv.writer(csvfile,delimiter = ";")
        writer.writerow([dpmean,dp1sigperc_lo,dp1sigperc_hi,dp2sigperc_lo,dp2sigperc_hi,cov1dp,cov2dp] ) 
    outfile = ROOT.TFile.Open("PoisRes_N200_"+dtilde+".root","UPDATE")
    outfile.cd()
    h_est_oo.Write()
    h_est_dp.Write()

elif Norm == 0:
    if not os.path.exists("SMPoisResults_noBkg3"):
        os.mkdir("SMPoisResults_noBkg3")
    os.chdir("SMPoisResults_noBkg3")
    coo.SaveAs("oo_est_pois_SM_"+dtilde+".png")
    cdp.SaveAs("dp_est_pois_SM_"+dtilde+".png")
    with open("PoisResults_SM_oo_"+dtilde+".csv", 'a') as csvfile:
        writer = csv.writer(csvfile,delimiter = ";")
        writer.writerow([oomean,oo1sigperc_lo,oo1sigperc_hi,oo2sigperc_lo,oo2sigperc_hi,cov1oo,cov2oo])
    with open("PoisResults_SM_dp_"+dtilde+".csv", 'a') as csvfile:
        writer = csv.writer(csvfile,delimiter = ";")
        writer.writerow([dpmean,dp1sigperc_lo,dp1sigperc_hi,dp2sigperc_lo,dp2sigperc_hi,cov1dp,cov2dp] ) 
    outfile = ROOT.TFile.Open("PoisRes_SM_"+dtilde+".root","UPDATE")
    outfile.cd()
    h_est_oo.Write()
    h_est_dp.Write()

    
