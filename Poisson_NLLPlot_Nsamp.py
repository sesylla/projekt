import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import csv
#fig, ax = plt.subplots(1, 1)
plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral' 
plt.rcParams.update({'lines.markeredgewidth': 1})



import ROOT
import sys
import os

if len(sys.argv) != 3:
    print ("USAGE: <source directory name> <Norm 0=SMNorm, 1=N200>")
    sys.exit(1)

outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 

#dtilde = sys.argv[2]
#N_sp = int(sys.argv[3])

#HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+".root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

#HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")


#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3()
BGen = ROOT.TRandom3()
Norm = int(sys.argv[2])

#constants from fit N(d) = 200. + c*d**2
#c_oo = 3147.5214538421055
#c_dp = 3611.706482059846
#rebinned, but also with 255004: 
c_oo =  3147.521454469851  #+-  [0.61616453]
c_dp =  3611.706482060036  #+-  [0.43232619]


dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

d_hyp_list = ['0.20','-0.05','0.00']

print("******* index: ",dtildelist.index("0.00")) 



def backGen(b_ratio,nbins,dtilde,c,gen): #returns sum,diff arrays of background
    N = 200.+c*float(dtilde)**2
    #nbins = len(histarr)
    #nbins = hist.GetSize()-2
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = np.array([gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)])
    
    
    #print(b_list)
    
#print("backg_sum: ",b_arr_pos+b_mirr_pos)
    #print("backg_diff: ",b_arr_pos-b_mirr_pos)
    return b_list

def getnormfac(templateHistFile,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" )
        #SMhist = ROOT.TH1D("oo1_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ) 
        #SMhist = ROOT.TH1D("dp_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())

    else:
        print("problem in getnormfac.")
        sys.exit(1)

    #for bin in range(SMhist.GetSize()):
    #    SMhist.SetBinContent(bin,SMhist0.GetBinContent(bin))
    #N_sample = sample_hist.Integral()
    #N_sample = np.sum(sample_hist[0]) -  Background_Ratio*(200.+c*float(dtilde)**2)/(2.*len(sample_hist[0]))
    #print("sample integral: ",N_sample)
    N_SM = 200.
    #N_SM = N_sample - c * float(dtilde)**2
    #print("N_SM : ",N_SM)
    #print("N_SM expectation: ",N_SM)
    SM_Integral = SMhist.Integral()
    #print("SM Integral: ", SM_Integral)
    #SM_Integral = np.sum(SMhist)

    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    
    
    
    return Normfac
#Normalize templates to data

    
def makearray(hist): #returns numpy  array of data bins. 
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template

def maketemplatelist(dtildelist,templateHistFile,norm):
    #templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
   
    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()

        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)
#oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        #if norm==1:
        #    oo1Hist1.Scale(200./oo1Hist1.Integral())
        #    delPhiHist1.Scale(200./delPhiHist1.Integral())
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
        
        
        oo1tmplarray = makearray(oo1Hist1)
        delPhitmplarray = makearray(delPhiHist1)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return np.array(oo1templatelist),np.array(delPhitemplatelist)


################# samples  ###########################






def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)








def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of sample histograms
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    for s in hnames:
        if "oo1_dtilde"+dtilde in s:
            hnamesoo.append(s)
        elif "dp_dtilde"+dtilde in s:
            hnamesdp.append(s)
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        #print("random indices :", i_sp_oo,i_sp_dp) 

        if sample_file.Get(hnamesoo[i_sp_oo]).Integral() != 0.:
            
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        
        dph = sample_file.Get(hnamesdp[i_sp_dp])
        if sample_file.Get(hnamesdp[i_sp_dp]).Integral() != 0.:
            
            dph = sample_file.Get(hnamesdp[i_sp_dp])
        else:
            i_sp_dp = gen_dp.__next__()
            dph = sample_file.Get(hnamesdp[i_sp_dp])
            
        hsamplesoo.append( makearray(ooh))
        hsamplesdp.append( makearray(dph))
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return hsamplesoo,hsamplesdp

                         

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildelist,templates):
    #print "d value: ",d
    i=0
    dtildevals = [float(x) for x in dtildelist]
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print( "dtilde out of range!")
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print ("could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #print "x= %d == x0 = %d"%(x,x0)
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            #print "x= %d == x0 = %d"%(x,x1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
            
            
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu): #log(pdf)
    
    return -mu + k*np.log(mu)


def NLL(dtil,hist_data,template):
    res = 0
    
    
    
    f = lpdf(hist_data,template)    
    res+=f    
    #print(f)
    return -np.sum(res)



def minNLL(dtil,histarr,template):
    
    
    #print("####",dtilde)
    #print(histarr)
    #print(template)  
    
    b_start = np.array([Background_Ratio*sum(histarr)/len(histarr)])
    #b_start = np.array([20.])
    res = minimize(NLL,b_start,args=(dtil,histarr,template),method = 'SLSQP',bounds=[(0.,1000.)])
    if not res.success:
        print("fit failed at dtilde = ", dtil)
        print(res.message)
    #print(res.fun)
    #print(res.x)
    #print(res)
    #print(dtilde,res.x,res.fun)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    #err = np.diag(res.hess_inv.todense())
    #print err
    #print res.hess_inv.todense()
    #print("result for x: %s +- ??" % (res.x[0]))
    #print("result for dtilde: %s +- ??" % (res.x[1]))
    return res.fun,res.x[0] 
    

####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

#def findConfInt_inter1d(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
#    tmp = [x-N**2/2. for x in NLLlist ]
#    NLL_inter = interp1d(ddlist,tmp)
#    dtmp = np.arange(ddlist[0],ddlist[len(ddlist)-1],10000)
#    minimum = np.amin(interp1d(dtmp))
    
    
# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)
def deletezeros(indexlist,arr):
        
    tmp = [arr]
    n = -1
    for i,j in enumerate(indexlist):
        # j  have to be sorted positive indices
        n+=1
        tmp.append(np.delete(tmp[i],j-n))
    return tmp[-1]     


def NLLPlot(dhyplist,dtildelist,templateHistFile,c,norm):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    nll_arrays = []
    dtildevals = np.array([float(x) for x in dtildelist])
    dd = np.linspace(-1.,1.,20000)
    bkg_ests = []
    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi = [],[],[],[],[]
   
    #if c == c_oo:
    #    templatelist = oo1templatelist
    #elif c == c_dp:
    #    templatelist = delPhitemplatelist
    #else:
    #    print("problem in estimate.")
    normfac = getnormfac(templateHistFile,c)
    for k,dhyp in enumerate(dhyplist):
               
        oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile,norm)
        if norm == 0:
            if c == c_oo:
                templatelist = oo1templatelist
                templatelist *= normfac
            elif c == c_dp:
                templatelist = delPhitemplatelist
                templatelist *= normfac
        elif norm == 1:
            if c==c_oo:
                templatelist = oo1templatelist
            elif c == c_dp:
                templatelist = delPhitemplatelist
            
        else:
            print("problem with normfac in estimate.")
            sys.exit(1)
        f_inter = interp1d(dtildevals,templatelist,axis=0)
        sample = f_inter(float(dhyp))
        if norm == 1:
            sample_int = np.sum(sample)
            tmpl_int_list = [np.sum(t) for t in templatelist]
            
            normfaclist = [sample_int/tint for tint in tmpl_int_list]
            tmp = [i * normfaclist[j] for j,i in enumerate(templatelist)]
            templatelist = tmp
            #templatelist *= sample_int*(1./tmpl_int_list)
        #if c == c_dp:
        #    sample0 = deletezeros([0,-1],sample)
        #    bkg_sum = backGen(Background_Ratio,len(sample0),dtilde,c,BGen)
        #    sum_sample = sample0 + bkg_sum
        #    
        #    
        #    
        #    templates1 = f_inter(dd)
        #    templates = np.array([deletezeros([0,-1],templ) for templ in templates1])
        #elif c== c_oo:

        #bkg_sum = backGen(Background_Ratio,len(sample),dhyp,c,BGen)
        sum_sample = sample
        f_inter = interp1d(dtildevals,templatelist,axis=0)    
            
            
            
        templates = f_inter(dd)
            
        
        #print("NORMFAC : ",normfac)
        
        NLLvals0=[]
        backg_list = []
        #print(sum_sample)
        #print(templates)
        #for d,templ in zip(dd,templates):
        #    m,b = minNLL(d,sum_sample,templ)
        #    NLLvals0.append(m)
        #    backg_list.append(b)
        NLLvals0=[NLL(d,sum_sample,templ) for d,templ in zip(dd,templates)]
        

        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        nll_arrays.append(NLLvals)
        
        dmin = findMin(dd,NLLvals)
        #bkg_ests.append(backg_list[NLLvals0.index(min1)])
        
        
        
        
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        

        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))
    print("estimators: [%.3f,%.3f,%.3f]"%(estlist[0],estlist[1],estlist[2]))
    print("1sig errs: ", [[round(err1sig_lo[i],3),round(err1sig_hi[i],3)] for i in range(len(err1sig_lo))])
    print("2sig errs: ", [[round(err2sig_lo[i],3),round(err2sig_hi[i],3)] for i in range(len(err2sig_lo))])    
   
    
    return dd,nll_arrays
    
    
    




print("*********OO Fit********")
dd,NLLvals = NLLPlot(d_hyp_list,dtildelist,templateHistFile,c_oo,Norm)

plt.grid()
plt.xlabel(r"$\tilde{d}$")
plt.ylabel(r"$\Delta \mathrm{NLL}$")
plt.plot(dd,NLLvals[0],"-.",color="Red",label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20$",linewidth=0.8)
plt.plot(dd,NLLvals[1],"--",color="Blue",label = r"$\tilde{d}_{\mathrm{hyp}} = -0.05$",linewidth=0.8 )
plt.plot(dd,NLLvals[2],"-",color="Black",label = r"$\tilde{d}_{\mathrm{hyp}} = 0.00$",linewidth=0.8)
plt.legend(loc="best")
#plt.xlim(-0.5,0.5)
#plt.ylim(-10,600)
plt.show()
print("*********delPhi Fit********")
dd,NLLvals = NLLPlot(d_hyp_list,dtildelist,templateHistFile,c_dp,Norm)
plt.grid()
plt.xlabel(r"$\tilde{d}$")
plt.ylabel(r"$\Delta \mathrm{NLL}$")
plt.plot(dd,NLLvals[0],"-.",color="Red",label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20$",linewidth=0.8)
plt.plot(dd,NLLvals[1],"--",color="Blue",label = r"$\tilde{d}_{\mathrm{hyp}} = -0.05$",linewidth=0.8 )
plt.plot(dd,NLLvals[2],"-",color="Black",label = r"$\tilde{d}_{\mathrm{hyp}} = 0.00$",linewidth=0.8)
plt.legend(loc="best")
#plt.xlim(-0.5,0.5)
#plt.ylim(-10,600)

plt.show()
