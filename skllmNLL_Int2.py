import ROOT
import sys
import os
import numpy as np
from scipy.stats import skellam
from scipy.optimize import minimize
from scipy.special import ive
from scipy.special import iv
from scipy.special import gamma
import matplotlib.pyplot as plt

#from matplotlib import rc

#rc('text', usetex=True)

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirHist = sys.argv[1]
dtilde = sys.argv[2]
#dtilde = "-0.5"
#dtilde = -0.2
print dtilde,type(dtilde)
pTc = sys.argv[3]
massc = sys.argv[4]
deletac = sys.argv[5]
dirDiff = "new_diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)

HistFileName = dirHist+"/"+dirHist+".root"
diffFileName = dirDiff+"/"+dirDiff+".root"
oo1diffHistName = "oo1Hist_diff" 



def Lpdf(k,mu1,mu2):
    
    #return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+np.log(iv(abs(k),2.*np.sqrt(mu1*mu2)))
    #pdf = lambda r,m1,m2 : np.log(np.exp(-(m1+m2))*(m1/m2)**(r/2)*np.exp(2*np.sqrt(m1*m2))*ive(abs(r),2*np.sqrt(m1+m2)))
    #restore normalization:
    #logN = np.log(sum([pdf(r,mu1,mu2) for r in np.linspace(-1000.,1000.,100000
    #return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+np.log(iv(abs(k),2.*np.sqrt(mu1*mu2)))
    return -mu1-mu2+(k/2.)*np.log(mu1/mu2)+2.*np.sqrt(mu1*mu2)+np.log(ive(k,2.*np.sqrt(mu1*mu2)))
    #return -(mu1-mu2)+k*np.log(mu1-mu2)-np.log(gamma(k+1)) #poisson#################
    #return -(mu1-mu2)+k*np.log(mu1-mu2)
#return np.log(np.exp(-(mu1+mu2))*(mu1/mu2)**(k/2)*np.exp(2*np.sqrt(mu1*mu2))*ive(abs(k),2*np.sqrt(mu1+mu2)))
def negLxpdf(x,k,mu1,mu2):
    return (x*(mu1+mu2))-(k/2.)*np.log(mu1/mu2)-np.log(iv(k,2.*x*np.sqrt(mu1*mu2)))

    #return -(mu1+mu2)+np.log(iv(k,2.*np.sqrt(mu1*mu2)))
def NLL(x,hist_data,hist_template1,hist_template2):
    res = 0.
    #print "................................................."
    #print x
    for d, t1, t2 in zip(hist_data,hist_template1,hist_template2):
        #f = Lpdf(x*d,t1,t2)
        f = Lpdf(d,x*t1,x*t2)
        #f = -x*(t1+t2)+(d/2.)*np.log(t1/t2)+np.log(iv(d,2*x*np.sqrt(t1*t2)))
        #print d,t1,t2,ive(d,2*x*np.sqrt(t1*t2))
        #print kv(d,2*x*np.sqrt(t1*t2))
        #print f
        if np.isnan(f):
            print "NAN",d,2.*x*np.sqrt(t1*t2),ive(d,2.*x*np.sqrt(t1*t2))
        res += f
    #print res
    return -res

def calcNLLVals(hist_data,hist_template1,hist_template2):
    for x in [0.5,0.8,0.9,1.0,1.2]:
        print "x = ",x
        for d,t1,t2 in zip(hist_data,hist_template1,hist_template2):
            print [d,x*t1-x*t2,-Lpdf(d,x*t1,x*t2)]
        print "NLL SUM OVER ALL BINS:", [x,NLL(x,hist_data,hist_template1,hist_template2)]

def minNLLBin(hist_data,template1,template2):
    x0 = np.array([2])
    for i,(d,t1,t2) in enumerate( zip(hist_data,template1,template2)):
        result = minimize(negLxpdf,x0,args = (d,t1,t2),bounds=[(0.2,10)])
        print "Fit Result in Bin "+str(i)+" : ",result.x," +- ",np.diag(result.hess_inv.todense())

def minNLL(hist_data,template1,template2): #np arrays as arguments 
    
    

    #print hist_data
    #print hist_template
    x0 = np.array([2])

    res = minimize(NLL,x0,args=(hist_data,template1,template2),bounds=[(0.2,10)] )

    print(res)
    err = np.diag(res.hess_inv.todense())

    print("result for x: %s +- %s" % (res.x, err))

print "*************************oo1 Fit**************************"

def plotofdtildevals():
    global HistFileName,diffFileName,ptC,massc,deletac,oo1diffHistName
    #dtildelist = [-1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    dtildelist = ['-1.00', '-0.90', '-0.80', '-0.70', '-0.60', '-0.50', '-0.40', '-0.30', '-0.20', '-0.10', '0.10', '0.20', '0.30', '0.40', '0.50', '0.60', '0.70', '0.80', '0.90', '1.00']
    #dtildelist = ['-1.00', '-0.90', '-0.80', '-0.70', '-0.60', '-0.50', '-0.40', '-0.30', '-0.20',  '0.20', '0.30', '0.40', '0.50', '0.60', '0.70', '0.80', '0.90', '1.00']
    #dtildelist = ['0.10','0.20', '0.30', '0.40', '0.50', '0.60', '0.70', '0.80', '0.90', '1.00']
#dtildelist = ['0.30']
    i=0
    x0 = np.array([2.])
    minvals=[]
    errs=[]

    for dtilde in dtildelist:
        print "------------- "+str(dtilde)+"------------------------------------------------------------------------------------------------------------------"
        dirDiff1 = "new_diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)
        print dirDiff1
        oo1HistName1= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
        diffFileName1 = dirDiff1+"/"+dirDiff1+".root"
        
        HistFile1 = ROOT.TFile.Open(HistFileName,"READ")        
        oo1Hist1 = HistFile1.Get(oo1HistName1)
        oo1Hist1.Scale( 200/oo1Hist1.Integral())
        oo1list1 = [oo1Hist1.GetBinContent(bin) for bin in range(oo1Hist1.GetSize())]

        HistFile1.Close()

        diffFile1 = ROOT.TFile.Open(diffFileName1, "READ")
        oo1diffHist1 = diffFile1.Get(oo1diffHistName)
        oo1difflist1 = [oo1diffHist1.GetBinContent(bin) for bin in range(oo1diffHist1.GetSize())]

        diffFile1.Close()
        #print "bin contents right after .Get :" , oo1Hist1.GetBinContent(15),oo1diffHist1.GetBinContent(15)
        
        #print "bin contents right after .Scale :" , oo1Hist1.GetBinContent(15),oo1diffHist1.GetBinContent(15)
        #print "INTEGRALS: ",oo1Hist1.Integral(),oo1diffHist1.Integral()
        for name,hist in zip([oo1diffHistName,oo1HistName1],[oo1diffHist1,oo1Hist1]):
            if not hist:
                print "hist %s  not found" % (name)
                sys.exit(1)
        
        
        #print "contents of last bin (oo1,diffdata): ",oo1list1[-1],oo1difflist1[-1]
    #if oo1diffHist.GetBinContent(1)>=0:
    #    oo1difflist = [oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
    #else:
    #    oo1difflist = [-oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
        mirroo1list1 = [oo1list1[len(oo1list1)-i-1] for i in range(len(oo1list1))]
        #print "oo1list ",oo1list1
        #print "mirrlist",mirroo1list1
        #print "datalist",oo1difflist1
        data1 = np.array([oo1difflist1[i] for i in range(len(oo1difflist1)/2,len(oo1difflist1)-1)],dtype = np.double)
#template = data
        template11 = np.array([oo1list1[i] for i in range(len(oo1list1)/2,len(oo1list1)-1)],dtype = np.double)
        template21 = np.array([mirroo1list1[i] for i in range(len(mirroo1list1)/2,len(mirroo1list1)-1)],dtype = np.double)
            #print template1
            #print template2
            #print data
            #print data-(template1-template2)
        res1 = minimize(NLL,x0,args=(data1,template11,template21),bounds=[(0.01,10.)] )
        minvals.append(res1.x[0])
        errs.append(np.diag(res1.hess_inv.todense()))
        
        
#minNLL(data,template1,template2)
        #print dtildelist
        print "darray",data1
        print "oarray",template11
        print "marray",template21
        print "o-m :", template11-template21
        #print "sum temp_diffs: ",np.sum(template11-template21)
        #print "sum data: ", np.sum(data1)
        #print "sum oo1list: ", sum(oo1list1)
        #print "sum mirrlist: ",sum(mirroo1list1)
        #print "sum(oo1list-mirrlist): ", sum([i-j for i,j in zip(oo1list1,mirroo1list1)])
        #print "sum datalist: ",sum(oo1difflist1)
        print "fit result: ",res1.x[0], "+-", np.diag(res1.hess_inv.todense())
        #calcNLLVals(data1,template11,template21)
        minNLLBin(data1,template11,template21)
        #print "NLL sum for 1.0:", NLL(1.0,data1,template11,template21)
        
#print minvals
        #print dtilde,res1.x
        res1=0
        i+=1
    print i
    dtildevals = [float(x) for x in dtildelist]
    
    plt.errorbar(dtildevals,minvals,yerr = errs)
    plt.xlabel(r"$\tilde{d}$")
    plt.ylabel(r"$\hat{x}(\tilde{d})$")
    plt.title("Skellam NLL Fit Results for OO")
    plt.show()
plotofdtildevals()
#print "************************delta Phi Fit**************************"
#minNLL(delPhiHist)
    
#def NLL(x,hist_data,hist_template):
#    res = 0
#    for d, t in zip(hist_data,hist_template):
#        f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
#        res += f
#    return -res

