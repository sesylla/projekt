import csv
import ROOT
import sys
import os
from numpy import pi


if len(sys.argv) != 3:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print( "USAGE: %s  <directory of input csv file> <dtilde> "%(sys.argv[0]))
    # End the program
    sys.exit(1)


dtilde = sys.argv[2]

dirname = sys.argv[1]
os.chdir(dirname)

oo1filename = dirname+"_oo1.csv"
dpfilename = dirname+"_dP.csv"


inputfilename = sys.argv[1]+".root"
outfilename = sys.argv[1]+"_"+dtilde+".root"



infile = ROOT.TFile.Open(inputfilename,"READ")

TGen = ROOT.TRandom3()

#constants from fit N(d) = 200. + c*d**2
c_oo = 3147.5214538421055
c_dp = 3611.706482059846



dtildelist = [dtilde]

#dtildelist = ['0.00','-0.05','0.20']
#dtildelist = ['1.00']
oo1list,dplist = [],[]
oo1histlist,dphistlist = [],[]
oo1namelist,dpnamelist = [],[]
for d in dtildelist:
    oo1list.append( [  ] ) 
    oo1histlist.append( [ ROOT.TH1D("oo1_dtilde"+d+"_0"," ",12,-15.,15.)] )
    oo1namelist.append(["oo1_dtilde"+d+"_0"])
    dplist.append( [  ] )
    dphistlist.append( [ROOT.TH1D("dp_dtilde"+d+"_0"," ",12,-pi,pi) ] )
    dpnamelist.append(["dp_dtilde"+d+"_0"])




tree_oo = infile.Get("too1")

tree_dp = infile.Get("tdp")


def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def drawindices(gen0,size_sp): #generate sample of size s_sp

        
    sp = [gen0.__next__() for i in range(size_sp)]
    
    return sp

def getMinWeights(tree,dtildelist): # get min weights for multiplication for rw /= rw_min to avoid rounding errors
    dtildevals = [float(d) for d in dtildelist]
    weightlist = [ [] for d in dtildelist]
    min_weightlist = []
    for i,d in enumerate(dtildevals):
        print("#### dtilde = ",d)
        for entryNum in range(0,tree.GetEntries()):
            
            
            tree.GetEntry(entryNum)
            obs = tree.obs
            w1 = tree.w1
            w2 = tree.w2
            
            rw = 1 + w1*d + w2*d**2
            weightlist[i].append(rw)
    for weights in weightlist:
        min_weightlist.append(min(weights))
    print(min_weightlist)
    return min_weightlist

def duplicate(tree,dtildelist,rw_min_list): #returns !!!not shuffled!!! list of events duplicated by their respectif weights
    dtildevals = [float(d) for d in dtildelist]
    obslist = [ [] for d in dtildelist]
    
    for i,d in enumerate(dtildevals):
        print("#### dtilde = ",d)
        for entryNum in range(0,tree.GetEntries()):
            
            
            tree.GetEntry(entryNum)
            obs = tree.obs
            w1 = tree.w1
            w2 = tree.w2
            
            rw = 1 + w1*d + w2*d**2
            rw /= rw_min_list[i]

            for j in range(int(round(rw))):
                obslist[i].append(obs)
    
    return obslist
   
def duplicateC(tree,dtildelist): #weights are multiplied by constant
    dtildevals = [float(d) for d in dtildelist]
    obslist = [ [] for d in dtildelist]
    
    for i,d in enumerate(dtildevals):
        print("#### dtilde = ",d)
        for entryNum in range(0,tree.GetEntries()):
            
            
            tree.GetEntry(entryNum)
            obs = tree.obs
            w1 = tree.w1
            w2 = tree.w2
            
            rw = 1 + w1*d + w2*d**2
            rw *= 100.

            for j in range(int(round(rw))):
                obslist[i].append(obs)
    print(len(obslist[0]))
    return obslist             

def draw(obslist,dtildelist,histlist,namelist,outfilename,c,poisgen):

    # poisgen is the same TRandom3 object as randT3gen above
    
    dtildevals = [float(d) for d in dtildelist]
    

    for i,d in enumerate(dtildevals):
        
        print("#### dtilde = ",d)
        len_data = len(obslist[i])
        gen = sample_gen(len_data,poisgen)
        
        N_sp_fl = poisgen.PoissonD(200. + c*d**2)
        N_sp = int(round(N_sp_fl))
        isample = 0
        NdrawnTot = 0
        N_remain = len_data

        while N_remain > N_sp:

            rand_indices = drawindices(gen,N_sp)
            for j in rand_indices:
                histlist[i][isample].Fill(obslist[i][j])

            NdrawnTot += N_sp
            N_remain -= N_sp
            N_sp_fl = poisgen.PoissonD(200. + c*d**2)
            N_sp = int(round(N_sp_fl))

            #print(namelist[i][isample])
            newname = "_".join(namelist[i][isample].split("_")[0:-1])+"_"+str(isample+1)
            #print(newname)
            histlist[i].append(ROOT.TH1D(newname," ",histlist[i][isample].GetSize()-2,histlist[i][isample].GetXaxis().GetXmin(),histlist[i][isample].GetXaxis().GetXmax()))

            namelist[i].append(newname)
            
            isample += 1
            if isample%100 == 0:
                print(isample, " samples drawn.")
    
    return histlist

def drawC(obslist,dtildelist,histlist,namelist,outfilename,c,poisgen): #draw a constant number of 1700 samples

    # poisgen is the same TRandom3 object as randT3gen above
    
    dtildevals = [float(d) for d in dtildelist]
    

    for i,d in enumerate(dtildevals):
        
        #print("#### dtilde = ",d)
        len_data = len(obslist[i])
        gen = sample_gen(len_data,poisgen)
        
        N_sp_fl = poisgen.PoissonD(200. + c*d**2)
        N_sp = int(round(N_sp_fl))
        isample = 0
        NdrawnTot = 0
        N_remain = len_data
        print(N_sp,len_data)
        while isample <= 1750:
            #if N_remain > N_sp:

            rand_indices = drawindices(gen,N_sp)
            for j in rand_indices:
                histlist[i][isample].Fill(obslist[i][j])

            NdrawnTot += N_sp
            N_remain -= N_sp
            N_sp_fl = poisgen.PoissonD(200. + c*d**2)
            N_sp = int(round(N_sp_fl))

            #print(namelist[i][isample])
            newname = "_".join(namelist[i][isample].split("_")[0:-1])+"_"+str(isample+1)
            #print(newname)
            histlist[i].append(ROOT.TH1D(newname," ",histlist[i][isample].GetSize()-2,histlist[i][isample].GetXaxis().GetXmin(),histlist[i][isample].GetXaxis().GetXmax()))

            namelist[i].append(newname)
            
            isample += 1
                #if isample%1 == 0:
            print(isample)
                
        print(isample, " samples drawn.")
    
    return 

def writeToHist(histlist):
    
    #outfile = ROOT.TFile.Open(outfilename,"UPDATE")
    
    #outfile.cd()
        
    
    for h in histlist:
        for i in h:
            i.Sumw2()
            i.Write()
            ROOT.SetOwnership(i,False)
            i.SetDirectory(0)
    


print("########################################################################################")

if not os.path.exists("AllSamples"):
    os.mkdir("AllSamples")
else:
    print("directory ","AllSamples"," already exists!")
os.chdir("AllSamples")


outfile = ROOT.TFile.Open(outfilename,"RECREATE")
outfile.cd()


print("***************** read oo1 tree and duplicate events by weights ******************")
#min_weightlist_oo = getMinWeights(tree_oo,dtildelist)
#oo1list_rw = duplicate(tree_oo,dtildelist,min_weightlist_oo)
oo1list_rw = duplicateC(tree_oo,dtildelist)
print("draw oo samples and fill into histograms ...")

drawC(oo1list_rw,dtildelist,oo1histlist,oo1namelist,outfilename,c_oo,TGen)

print("save oo samples to file ", outfilename , " ... ")
writeToHist(oo1histlist)
del oo1list_rw
del oo1histlist

print("***************** read delta phi tree and duplicate events  *************")
#min_weightlist_dp = getMinWeights(tree_dp,dtildelist)
#dplist_rw = duplicate(tree_dp,dtildelist,min_weightlist_dp)
dplist_rw = duplicateC(tree_dp,dtildelist)

print("draw samples and fill into histograms ...")

drawC(dplist_rw,dtildelist,dphistlist,dpnamelist,outfilename,c_dp,TGen)

print("########################################################################################")
print ("save oo and delphi to file ", outfilename, " ... ")


writeToHist(dphistlist)
del dplist_rw
del dphistlist


print("DONE: file ",outfilename, " created .")

sys.exit(1)    
            
