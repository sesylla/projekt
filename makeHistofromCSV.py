import csv
import ROOT
import sys
import os


if len(sys.argv) != 4:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print("USAGE: %s  <csv file name (input) ><hist file name (for output)> <dtilde>"%(sys.argv[0]))
    # End the program
    sys.exit(1)

dirname_in = sys.argv[1]
dirname_out = sys.argv[2]
dtilde = sys.argv[3]
dtilde_fl = float(dtilde)


# Number of events in SM
NevSM = 200.
# constants for N = N_SM(1+c*dtilde**2)
c_oo = 1.
c_dP = 1.

Nsamples = 10

Nev_oo = NevSM*(1+c_oo*dtilde**2)
Nev_dP = NevSM*(1+c_dP*dtilde**2)




infilename = dirname_in+"/"+dirname_in+".csv"
outfilename = outfilename+".root"
rand_gen = ROOT.TRandom3()

#test trafo: a = c*x, c:=2
#f = ROOT.TF1("f","exp(-[0])*[0]**x/tgamma(1+x)",0,50)
#g = ROOT.TF1("g","(exp(-[0])*[0]**(x/[1]))/(tgamma(1+x/[1])*[1])",0,100)
#g.SetParameter(0,10.)
#f.SetParameter(0,10.)
#g.SetParameter(1,5.)

#print(g.GetParameter(0),g.GetParameter(1))
#flist,glist = [],[]
#for j in [10,20,50,100,1000,100000]:
#    for i in range(j):
#        flist.append(f.GetRandom())
#        glist.append(g.GetRandom())
#    print(sum(flist)/len(flist),sum(glist)/len(glist))
#    flist,glist = [],[]


def readAllToList(filename):
    obslist,w1list,w2list = [],[],[]
    with open(filename) as csvfile:
        reader = csv.reader(csvfile,delimiter=";")
        for row in reader:
            obslist.append(row[0])
            w1list.append(row[1])
            w2list.append(row[2])
    return obslist,w1list,w2list



def readPoisNeventsoo(filename,dtilde,NeventsMean,Nsamples,gen):
    
    i=0
    while i<Nsamples:
        N = gen.Poisson(NeventsMean)
        h0 = ROOT.TH1D("hoo"+str(i),"oo1",14,-15.,15.)
        histlist = [h0]
        
        with open(filename) as csvfile:
            reader = csv.reader(csvfile,delimiter=";")
            for j,row in enumerate(reader):
                
                h = histlist[i]
                
                while h.Integral() < NeventsMean:

                    h.Fill(row[0],row[1]*dtilde+row[2]*dtilde**2)
                
                i+=1
                histlist.append( ROOT.TH1D("hoo"+str(i),"oo1",14,-15.,15.))
    print("number of oo1 samples drawn: ",len(histlist))
    return histlist

def readPoisNeventsdP(filename,dtilde,NeventsMean,Nsamples,gen):
    #index of sample:
    i=0
    #number of events drawn.
    Nev = 0
    while i<Nsamples:
        N = gen.Poisson(NeventsMean)
        h0 = ROOT.TH1D("hdP"+str(i),"delPhi",14,-15.,15.)
        histlist = [h0]
        
        with open(filename) as csvfile:
            reader = csv.reader(csvfile,delimiter=";")
            for j,row in enumerate(reader):
                
                h = histlist[i]
                
                while (h.Integral() < N):

                    h.Fill(row[0],row[1]*dtilde+row[2]*dtilde**2)
                Nev+=N
                i+=1
                if len(reader)-
                histlist.append( ROOT.TH1D("hoo"+str(i),"oo1",14,-15.,15.))
    print("number of delPhi samples drawn: ",len(histlist))
    return histlist


def subtrHisto(Histo,i,sum=False):
    global i
    Hist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
        
    
    
    binlist = [Hist.GetBinContent(bin) for bin in range(Hist.GetSize()//2,Hist.GetSize())]
    mirrlist = [Hist.GetBinContent(Hist.GetSize()-bin-1) for bin in range(Hist_mirr.GetSize()//2,Hist_mirr.GetSize())]
    
    
    
    Hist_diff = ROOT.TH1D("Hist_diff"," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    if sum:
        Hist_diff.Add(Hist,Hist_mirr, 1, 1)
    else:
        Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    
   
   
    
    delPhiHist_diff.SetStats(0)
    oo1Hist_diff.SetStats(0)
    
    Hist_diff.GetXaxis().SetRange(Hist_diff.GetSize()//2,Hist_diff.GetSize()-2)
    
   
        
    return Hist_diff


def writesamples(outfilename,histlist):
    if not os.path.exists(outfilename[0:-5]):
        os.mkdir(outfilename[0:-5])
        print "directory "+outfilename[0:-5]+" created."
    else:
        print "directory "+outfilename[0:-5]+" already exists."
        sys.exit(1)
    os.chdir(histfilename[0:-5]) 
    
    histlistoo =  readPoisNeventsoo(infilename,dtilde,Nev_oo,Nsamples,rand_gen)
    histlistdP =  readPoisNeventsdP(infilename,dtilde,Nev_dP,Nsamples,rand_gen)
    HistFile = ROOT.TFile.Open(outfilename,"WRITE")
    histfile.cd()
    for histoo,histdp in zip(histlistoo,hist
    
    


                

