import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses

# Check that the user gave the correct number of arguments
if len(sys.argv) != 4:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file> <input MC file> <output histogram file>"%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
mcFileName   = sys.argv[2]
histFileName = sys.argv[3]

# Now we want to read each of the files to a histogram
# However, we don't want to write the code to do this twice
# Instead, write a function to read the file into a histogram
#   The first argument is the "tree" (which contains the event information)
#   The second argument is the switch for data or MC
# Note that functions must be defined before they are used
#   The use of this function is further down
def readTreeToMllHistogram(tree,dataType):
    
    # Check that the dataType is as expected
    if not (dataType == "data" or dataType == "MC"):
        # The argument is not what we expected
        # Tell the user and end the program
        print "The data type should be \"data\" or \"MC\", not",dataType
        sys.exit(1)

    # Make the histogram to store the events
    # Use the dataType as a part of the name and title
    # Make 150 bins of size 1 GeV, from 50 GeV to 200 GeV
    # Note that units are in MeV, so 200 GeV is 200x10^3, or equivalently 200.e3
    mll = ROOT.TH1D(dataType,"m_{ll}, %s"%(dataType),150,50.e3,200.e3)
    mll.Sumw2()

    # Run over all of the entries in the tree
    # In other words, run over all of the events
    for entryNum in range(0,tree.GetEntries()):
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)

        # Ensure this is a di-lepton event
        # A di-lepton event will have a lep(ton) n(umber) of 2
        # If the lepton number is not 2, then start the loop again
        if getattr(tree,"lep_n") != 2:
            continue

        # We now know that there are two leptons
        # Get their kinematics
        # Note that these are arrays/lists of size lep_n
        pT  = getattr(tree,"lep_pt")
        eta = getattr(tree,"lep_eta")
        phi = getattr(tree,"lep_phi")
        nrg = getattr(tree,"lep_E")

        # Convert the kinematics into a four-vector object
        # Recall that python is 0-indexed
        # That means that the first element in an array/list is 0, not 1
        lepton0 = ROOT.TLorentzVector()
        lepton0.SetPtEtaPhiE(pT[0],eta[0],phi[0],nrg[0])
        lepton1 = ROOT.TLorentzVector()
        lepton1.SetPtEtaPhiE(pT[1],eta[1],phi[1],nrg[1])

        # Calculate the parent particle four-vector
        # This is just the sum of the two lepton four-vectors
        dilepton = lepton0 + lepton1

        # Calculate any weights that may need to be applied (for MC)
        weight = 1
        if dataType == "MC":
            # If this is MC, we need to apply several weights
            # The "mcWeight" is needed to turn the generated distribution into a meaningful distribution
            #   The MC sample was generated in "slices", and all of the slices need to be combined
            #   The weight ensures that the slices have been combined to fit with the Standard Model
            # The scale factors correct for differences between data and MC
            weight *= getattr(tree,"mcWeight")
            weight *= getattr(tree,"scaleFactor_PILEUP")
            weight *= getattr(tree,"scaleFactor_MUON")
            weight *= getattr(tree,"scaleFactor_TRIGGER")
            
        # We now have the dilepton system and the event weight
        # Store the mass of the dilepton system with the relevant weight
        mll.Fill(dilepton.M(),weight)

    # We are done reading the tree into the histogram
    # Return the histogram
    return mll





# Build histogram of mll for the data file
print "Running over data..."
# Open the data file as read-only
dataFile  = ROOT.TFile(dataFileName,"READ")
# Get the tree in the file named "HASCO"
dataTree  = dataFile.Get("HASCO")
# Read the tree into the histogram
dataHisto = readTreeToMllHistogram(dataTree,"data")
# Set the histogram to continue to exist after closing the file
dataHisto.SetDirectory(0)
# Close the file as we are done reading the data
dataFile.Close()
print "Done running over data"


# Build histogram of mll for MC
print "Running over MC..."
# Open the MC file as read-only
mcFile  = ROOT.TFile(mcFileName,"READ")
# Get the tree in the fle named "HASCO"
mcTree  = mcFile.Get("HASCO")
# Read the tree into the histogram
mcHisto = readTreeToMllHistogram(mcTree,"MC")
# Set the histogram to continue to exist after closing the file
mcHisto.SetDirectory(0)
# Close the file as we are done reading the MC
mcFile.Close()
print "Done running over MC"

# Write the two histograms to an output root file
print "Writing histograms to output file..."
# First create the output file, overwriting previous files with the same name
outHistFile = ROOT.TFile.Open(histFileName,"RECREATE")
# Make the file your current storage location
outHistFile.cd()
# Write the data histogram to the output file
dataHisto.Write()
# Write the MC histogram to the output file
mcHisto.Write()
# Close the output file
outHistFile.Close()
print "Done writing histograms to output file"


# Done!

