from multiprocessing import Pool
import numpy as np
import scipy.stats
from scipy.optimize import minimize
from scipy.special import ive
#from scipy.optimize import minimize
#from scipy.optimize import NonlinearConstraint
from scipy.optimize import Bounds
#from scipy.optimize import BFGS
#from scipy.optimize import SR1
from array import array
import matplotlib.pyplot as plt
#import timeit
from scipy.interpolate import interp1d
import csv
#fig, ax = plt.subplots(1, 1)

plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral' 
plt.rcParams.update({'lines.markeredgewidth': 1})


import ROOT
import sys
import os

if len(sys.argv) != 3:
    print ("USAGE: <source directory name> <Norm 0=SMNorm, 1=N200>  ")
    sys.exit(1)

outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 


Norm = int(sys.argv[2])

#HistFileName =  sys.argv[1]+"/"+sys.argv[1]+"_samples_sumdiff.root"
#templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

#HistFile = ROOT.TFile.Open(HistFileName,"READ")
#templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
#HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+"_sumdiff.root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

#HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")

#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3(16)

#constants from fit N(d) = 200. + c*d**2
#c_oo = 3147.5214538421055
#c_dp = 3611.706482059846

c_oo =  3147.521454469851  #+-  [0.61616453]
c_dp =  3611.706482060036  #+-  [0.43232619]

d_hyp_list = ['0.20','-0.05','0.00']


dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

ci_oo = [256.67985876604547, 236.37902929944582, 119.18474917359077, 62.95976291095352, 38.508098540335766, 22.803682056052256, 15.919852484639478]
cierrs_oo = [0.34921445655286226, 0.3038219949611821, 0.2714139513440568, 0.22423599451122544, 0.2098730747921924, 0.18872349525623347, 0.16710806526367844]
ci_dP = [62.62255827521881, 167.38329595698812, 219.93972800634108, 200.42929342082726, 118.06756711728525, 15.885299902836735,0.]
cierrs_dP = [0.22881847404023611, 0.3947626204659464, 0.5320246854890918, 0.48166615436006305, 0.2957030309665562, 0.1000841089701317,1.]


print("******* index: ",dtildelist.index("0.00")) 
#Normalize templates to data
def getnormfac(templateHistFile,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" ).Clone()
        #SMhist = ROOT.TH1D("oo1_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ).Clone() 
        #SMhist = ROOT.TH1D("dp_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    
    else:
        print("problem in getnormfac.")
        sys.exit(1)
    ROOT.SetOwnership(SMhist,False)
    SMhist.SetDirectory(0)
    #for bin in range(SMhist.GetSize()):
    #    SMhist.SetBinContent(bin,SMhist0.GetBinContent(bin))
    #N_sample = sample_hist.Integral()
    #N_sample = np.sum(sample_hist[0]) -  Background_Ratio*(200.+c*float(dtilde)**2)/(2.*len(sample_hist[0]))
    #print("sample integral: ",N_sample)
    N_SM = 200.
    #N_SM = N_sample - c * float(dtilde)**2
    #print("N_SM : ",N_SM)
    #print("N_SM expectation: ",N_SM)
    SM_Integral = SMhist.Integral()
    #print("SM Integral: ", SM_Integral)
    #SM_Integral = np.sum(SMhist)

    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    return Normfac
    
def maketemplarray(hist): #returns numpy  array of arrays(posdata,mirrdata)
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()):
        
        #if hist.GetBinContent(bin) == 0.:
        #    continue
        datalist.append(hist.GetBinContent(bin))


    mirrlist = [datalist[len(datalist)-i-1] for i in range(len(datalist))]    
    hist_data     = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1)],dtype = np.double)
    mirr_data = np.array([mirrlist[i] for i in range(len(mirrlist)//2,len(mirrlist)-1) ],dtype = np.double)
    
    #delete empty last bin of delPhi histo:
    #if(hist_data[-1]==0. or mirr_data[-1] == 0.):
    #    return np.array([hist_data[:-1] , mirr_data[:-1]])
    #else:
    return np.array([hist_data , mirr_data])



def maketemplatelist(dtildelist,templateHistFile,norm):
    #templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
   
    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()

        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)
        if norm==1:
            oo1Hist1.Scale(200./oo1Hist1.Integral())
            delPhiHist1.Scale(200./delPhiHist1.Integral())
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
        
        
        oo1tmplarray = maketemplarray(oo1Hist1)
        delPhitmplarray = maketemplarray(delPhiHist1)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return np.array(oo1templatelist),np.array(delPhitemplatelist)



################# samples  ###########################


def makearray(hist): #for diff histo
    datalist = []
    for bin in range(hist.GetSize()-1):
        bin+=1
        datalist.append(hist.GetBinContent(bin))
    data = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1) ],dtype = np.double)
    
    return data



def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of samples histograms [[sum,diff], ... ]
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    #make list of [name_sum,name_diff]:
    for s in hnames:
        if "oo1_dtilde"+dtilde in s :
            
            if "sum" in s:
                hnamesoo.append([s,s[0:-3]+"diff"])
                #print([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for oo1.")
        elif "dp_dtilde"+dtilde in s:
            if "sum" in s:
                hnamesdp.append([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for dp.")
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        

        if sample_file.Get(hnamesoo[i_sp_oo][0]).Integral() != 0.:
            
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            print(hnamesoo[i_sp_oo])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])

        if sample_file.Get(hnamesdp[i_sp_dp][0]).Integral() != 0.:
            
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        else:
            i_sp_dp = gen_dp.__next__()
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        #print("random indices :", i_sp_oo,i_sp_dp) 
        #ooarr_sum = makearray(ooh_sum)
        #ooarr_diff = makearray(ooh_diff)
        #dparr_sum = makearray(dph_sum)
        #dparr_diff = makearray(dph_diff)
        #hsamplesoo.append( [ooarr_sum,ooarr_diff])
        #hsamplesdp.append( [dparr_sum,dparr_diff])
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return hsamplesoo,hsamplesdp


def subtrAdd(Hist,normfac,d):

    #Hist.Sumw2()
    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  
    #Hist_mirr.SetEntries(Hist.GetEntries())
        
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    

    #Hist_diff.GetXaxis().SetRange(Hist_diff.GetSize()//2,Hist_diff.GetSize()-2)
    #Hist_sum.GetXaxis().SetRange(Hist_sum.GetSize()//2,Hist_sum.GetSize()-2)
    
        
    return Hist_diff,Hist_sum
                         

######### interpolation from templates to arbitrary dtilde value###########################################################################


def findx0x1(d,dtildevals0,templates):
    #print( "d value: ",d)
    dtildevals = [float(j) for j in dtildevals0] 
    
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d, [x0,x1]       
        return [x0,x1],[f0,f1]

    else:
        print("dtilde out of range!")
        #sys.exit(1)
        #return [np.nan,np.nan],[np.nan,np.nan]
        return np.nan
#def interpol(x,x_list,f_list): #f_list is array of arrays, x_list list of corresp. x values
#    #tmp=[]
#    #for x in xx:
#    
#    xvals,fvals = findx0x1(x,x_list,f_list)
#        
#    #print xvals,fvals
#    if not (xvals and fvals):
#        print("could not find x values for interpolation.")
#        sys.exit(1)
#    else:
#        x0 = xvals[0]
#        x1 = xvals[1]
#        f0 = fvals[0]
#        f1 = fvals[1]
#    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

#            #if abs(x - x0) < 10**-6:
#        if x == x0:
#                #tmp.append(f0)
#            f = f0
#            #return f
#            #elif abs(x -x1) < 10**-6:
#        elif x == x1:
#                #tmp.append(f1)
#            f = f1
#            #return f
#        else:
#                
#            f = (f0*(x1-x)/(x1-x0))+f1*(x-x0)/(x1-x0)
#            
#                    
#                        
#    #print x,[x0,x1]
#    return f
dtildevals = np.array([float(x) for x in dtildelist])

    

############################################ Fit Functions ########################################################################################################

def lpdf(k_pl,k_min,diff_templ,slam): #log(pdf)
    mu1 = 0.5*(slam+diff_templ)
    mu2 = 0.5*(slam-diff_templ)
    #print("--- dtilde ",dtil)
    #print("slam ",slam)
    #print("k_pl ",k_pl)
    #print("k_min ",k_min)
    #print("diff_templ ",diff_templ)
    #print("+++",abs(mu1*mu2),abs(mu1/mu2),ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))),abs(slam), "+++")
    #print(np.log(abs(mu1/mu2)))
    #print(np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2)))))
    #print(slam**2-dtil**2*ci_list**2)

    #print("----------------")
    result = -(mu1+mu2)+(k_min/2.)*np.log(abs(mu1/mu2))+2.*np.sqrt(abs(mu1*mu2))+np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))))+k_pl*np.log(abs(slam))-slam
    return result

def NLL(params,sum_data,diff_data,diff_template):
    sum_lam = params
    #print("#####",dtilde)
    
    #print dtilde,del_lam
    res = 0
    
    
    #diff_template1 = interpol(dtil,dtildelist,templatearrlist)

    #if diff_template1[-1] == 0. :
    #    diff_template2 =np.delete(np.delete(diff_template1,-1),0)
    #    sum_data2 =np.delete(np.delete(sum_data1,-1),0)
    #    diff_data =np.delete(np.delete(diff_data1,-1),0)
    #else:
    #    diff_template2 = diff_template1
    #    sum_data2 = sum_data1
    #    diff_data2 = diff_data1
    
    # !!! can't fit bins with content 0 !!!
    # delete zeroes from end to beginning of list:
    
    
    #sum_template = hist_template1+hist_template2
    
    

    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    f = lpdf(sum_data,diff_data,diff_template,sum_lam)
    res += f
    
    return -np.sum(res)

def deletezeros(indexlist,arr):
        
    tmp = [arr]
    n = -1
    for i,j in enumerate(indexlist):
        # j  have to be sorted negative indices
        n+=1
        tmp.append(np.delete(tmp[i],j+n))
        #print("zero deleted.")
    return tmp[-1]     

def minNLL(sum_data,diff_data,diff_template):
    #print("#####",dtil)
    #if not len(sum_data) == len(diff_data):
    #    print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
    #    sys.exit(1)
    #ci_list =np.array(ci_list0)
    #print(ci_list)
    #print(sum_data)
    #print(diff_data)
    #print("mean of diff_data:", sum(diff_data)/float(len(diff_data)))
    #print hist_data
    #print hist_template

   
    

    #print(len(sum_data),len(diff_data),len(diff_template))
    
    if not len(sum_data) == len(diff_data):
        print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
        sys.exit(1)

    sumlam_start = [sumval for sumval in sum_data]
    
    #dtilde_start = [-0.01]
    startpar = np.array(sumlam_start)
    #print(startpar)
    
    def cons_f(x):
        return x**2-diff_template**2
    

#make bounds

    
    lbnds = [1e-6 for i in range(len(diff_data))]
    ubnds = [np.inf for i in range(len(diff_data))]
    #lbnds = startpar-1.
    #ubnds = startpar+1.
    #sum_lam_bounds = [[1e-6,np.inf] for l in range(len(diff_data))]
    
    bounds = Bounds(lbnds,ubnds)
    #print(bounds)

 
    #cons = ({'type': 'ineq', 'fun': lambda x:  x**2 - dtil**2*ci_list**2})
    cons = ({'type': 'ineq', 'fun': lambda x:  x**2 - diff_template**2})
    
    
    #nonlinear_constraint = NonlinearConstraint(cons_f,1e-8,np.inf,jac='2-point',hess=SR1())
    
    err = 0

    #res = minimize(NLL,startpar,method="trust-constr",args=(dtil,sum_data,diff_data,diff_template,ci_list),constraints = nonlinear_constraint,bounds=bounds,options={'maxiter':10000}  )
    res = minimize(NLL,startpar,method="SLSQP",args=(sum_data,diff_data,diff_template),constraints = cons,bounds=bounds,options={'maxiter':10000}  )
    #res = minimize(NLL,startpar,method="SLSQP",args=(dtil,sum_data,diff_data,diff_template,ci_list),bounds=bounds,options={'maxiter':10000}  )
    if not res.success:
        #print("fit failed at dtilde = ",dtil)
        #print(res.message)
        err = 1
    #print(sum_data)
    #print(res.x)
    #nllres = NLL(res.x,dtilde,sum_data,diff_data,ci_list)
    nllres = res.fun
    #print(nllres)
    #print(res)
    #print("NLL at minimum: ",nllres)
    return nllres ,err  



    

####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    #plt.plot(ddlist,tmp)
    #plt.show()
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
        
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)


def NLLPlot(dhyplist,dtildelist,templateHistFile,c,norm):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    Nerrs = 0
    errlist = []
    Nzeros = []
    nll_arrays=[]
    #dd = np.linspace(float(dtilde)-0.5,float(dtilde)+0.5,1001)
    
    #dd = np.linspace(-1..,1.,1000)
    dtildevals = np.array([float(x) for x in dtildelist])
    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi = [],[],[],[],[]
    #intlist,sampleintlist =[], []
    ddlist = []
    normfac = getnormfac(templateHistFile,c)
    for k,dhyp in enumerate(dhyplist):
        #dd = np.linspace(-1.,1.,20)
        dd = np.linspace(-1.,1.,20001)
        #dd = np.linspace(float(dhyp)-0.2,float(dhyp)+0.2,4001)
        

        oo1tmplarrlist,delPhitmplarrlist = maketemplatelist(dtildelist,templateHistFile,norm)
       
        #oo1difftmplarrlist = [oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))] 
        #delPhidifftmplarrlist = [delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))] 
        if norm == 0:
            if c == c_oo:
                oo1tmplarrlist *= normfac
            
                difftemplatelist =np.array([oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
                sumtemplatelist = np.array([oo1tmplarrlist[i][0]+oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
           
           
            elif c == c_dp:
                delPhitmplarrlist *= normfac
                difftemplatelist =np.array([delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])
                sumtemplatelist = np.array([delPhitmplarrlist[i][0]+delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])
        elif norm == 1:
            if c== c_oo:
                difftemplatelist =np.array([oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
                sumtemplatelist = np.array([oo1tmplarrlist[i][0]+oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
            if c==c_dp:
                difftemplatelist =np.array([delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])
                sumtemplatelist = np.array([delPhitmplarrlist[i][0]+delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])

        else:
            print("problem with normfac in estimate.")

        
        
        
        f_inter = interp1d(dtildevals,difftemplatelist,axis=0)
        f_sum_inter = interp1d(dtildevals,sumtemplatelist,axis=0)
        diff_templates = f_inter(dd)
        sum_templates = f_sum_inter(dd)
#diff_templates0 = [np.array(templ) for templ in diff_templates1])
        #sum_sample = f_sum_inter(float(dhyp))
        #diff_sample = f_inter(float(dhyp))
        
        #if norm == 0:
        #    if c == c_oo:
        #        sum_sample = makearray(templateHistFile.Get("oo1_sum_dtilde"+dhyp))
        #        diff_sample = makearray(templateHistFile.Get("oo1_diff_dtilde"+dhyp))
        #        #sum_sample = f_sum_inter(float(dhyp))
        #        #diff_sample = f_inter(float(dhyp))
        #    elif c == c_dp:
        #        sum_sample = makearray(templateHistFile.Get("dp_sum_dtilde"+dhyp))
        #        diff_sample = makearray(templateHistFile.Get("dp_diff_dtilde"+dhyp)) 
        #elif norm == 1:
        #    sum_sample = f_sum_inter(float(dhyp))
        #    diff_sample = f_inter(float(dhyp))
        sum_sample = f_sum_inter(float(dhyp))
        diff_sample = f_inter(float(dhyp))                      
        NLLvals0 = []
        errindices = []
        #NLLvals0 = [minNLL(d,sum_sample,diff_sample,templ,ci_list_sample) for d,templ in zip(dd,diff_templates)]
#NLLvals0oo1 = [minNLL(d,hist_data,dtildelist,oo1templatelist) for d in dd]
        for nr,templ in enumerate(diff_templates):
            m,err = minNLL(sum_sample,diff_sample,templ)
            if err == 0:
                NLLvals0.append(m)
            else:
                Nerrs+=1
                errindices.append(nr-len(dd))
                #print("err found.")
        
        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        nll_arrays.append(NLLvals)
#delete d values with errors:
        errindices.sort(reverse = True)
        ddlist.append(deletezeros(errindices,dd))
        dd_tmp = deletezeros(errindices,dd)

        dd = dd_tmp
        errlist.append(Nerrs)
        Nerrs = 0
        #ddlist.append(dd)
        
        #print("shapes: ",len(dd),len(nll_arrays[-1]))
        #print("shapes in ddlist: ",[len(a) for a in ddlist])
        dmin = findMin(dd,NLLvals)
        
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        
        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))
    #print("shapes in ddlist: ",[len(a) for a in ddlist])  
    print("estimators: [%.3f,%.3f,%.3f]"%(estlist[0],estlist[1],estlist[2]))
    print("1sig errs: ", [[round(err1sig_lo[i],3),round(err1sig_hi[i],3)] for i in range(len(err1sig_lo))])
    print("2sig errs: ", [[round(err2sig_lo[i],3),round(err2sig_hi[i],3)] for i in range(len(err2sig_lo))]) 
    print("************* tex compatible ***********")
    print("1 sigma:")
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[2],err1sig_lo[2],err1sig_hi[2]))
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[0],err1sig_lo[0],err1sig_hi[0]))
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[1],err1sig_lo[1],err1sig_hi[1]))
    print("1 sigma:")
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[2],err2sig_lo[2],err2sig_hi[2]))
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[0],err2sig_lo[0],err2sig_hi[0]))
    print("$%.3f_{-%.3f}^{+%.3f}$"%(estlist[1],err2sig_lo[1],err2sig_hi[1]))
    return ddlist,nll_arrays


dd,NLLvals = NLLPlot(d_hyp_list,dtildelist,templateHistFile,c_oo,Norm)
# build legend from scratch
import matplotlib.lines as mlines
leg_020 = mlines.Line2D([], [], color='Red',  linestyle='-',
                           label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20$",linewidth=0.5)
leg_005 = mlines.Line2D([], [], color='Blue', linestyle='-',
                           label=r"$\tilde{d}_{\mathrm{hyp}} = -0.05$",linewidth=0.5)
leg_000 = mlines.Line2D([], [], color='Black', linestyle='-',
                           label=r"$\tilde{d}_{\mathrm{hyp}} = 0.00$",linewidth=0.5)


plt.grid()
plt.xlabel(r"$\tilde{d}$")
plt.ylabel(r"$\Delta \mathrm{NLL}$")
plt.plot(dd[0],NLLvals[0],",",color="Red",label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20$",ms=0.5)#,linewidth=0.8)
plt.plot(dd[1],NLLvals[1],",",color="Blue",label = r"$\tilde{d}_{\mathrm{hyp}} = -0.05$",ms=0.5)#,linewidth=0.8 )
plt.plot(dd[2],NLLvals[2],",",color="Black",label = r"$\tilde{d}_{\mathrm{hyp}} = 0.00$",ms=0.5)#,linewidth=0.8)
plt.legend(loc="best",numpoints=3,handles=[leg_000, leg_020,leg_005])
#plt.legend(loc="best",numpoints=3,markerscale = 3.)
#plt.xlim(-0.5,0.5)
#plt.ylim(-10,900)

plt.show()

dd,NLLvals = NLLPlot(d_hyp_list,dtildelist,templateHistFile,c_dp,Norm)
plt.grid()
plt.xlabel(r"$\tilde{d}$")
plt.ylabel(r"$\Delta \mathrm{NLL}$")
plt.plot(dd[0],NLLvals[0],",",color="Red",label=r"$\tilde{d}_{\mathrm{hyp}} = 0.20$",ms=0.5)#,linewidth=0.8)
plt.plot(dd[1],NLLvals[1],",",color="Blue",label = r"$\tilde{d}_{\mathrm{hyp}} = -0.05$",ms=0.5)#,linewidth=0.8 )
plt.plot(dd[2],NLLvals[2],",",color="Black",label = r"$\tilde{d}_{\mathrm{hyp}} = 0.00$",ms=0.5)#,linewidth=0.8)
plt.legend(loc="best",numpoints=3,handles=[leg_000, leg_020,leg_005])
#plt.legend(loc="best",numpoints=3,markerscale = 3.)
#plt.xlim(-0.5,0.5)
#plt.ylim(-10,900)

plt.show()
