import os
from glob import glob

dirlist=["SMVBFPoisResults3","SMPoisResults_noBkg3","new_SkP_noBkg_Results4","SMVBF_SkPResults3"]


os.chdir("testres")
for n in dirlist:
    tmp=[]
    os.chdir(n)
    for j in glob("*.pdf"):
        tmp.append(j)
    for j in tmp:
        os.rename(j,j.replace("0.","0p").replace("1.","1p").replace("2.","2p").replace("ppdf",".pdf"))
                  
    os.chdir("..")
