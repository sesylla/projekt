import ROOT
from scipy.interpolate import interp1d
import os


os.chdir("testres")
filename = "MeanResults/MeanRes_2sig_b0.0.root"
filename2 = "MeanResults/MeanRes_2sig_b1.0.root"
#get Eichkurve
eGraph = ROOT.TFile.Open(filename,"READ").Get("oo_sig2;5")
eGraph2 = ROOT.TFile.Open(filename2,"READ").Get("oo_sig2;7")
os.chdir("/home/ss944/BscPlots")
#dd,m,errl,errh = [],[],[],[]
#eGraph.SetTitle(" ; #tilde{d}  ; < #Delta #Phi_{jj}^{sgd} >" )
eGraph2.SetTitle(" ; #tilde{d}  ; < O >" )
eGraph2.SetLineColor(18)
eGraph2.SetMarkerColor(1)
eGraph2.SetMarkerStyle(1)
eGraph.SetTitle(" ; #tilde{d}  ; < O >" )
eGraph.SetLineColor(18)
eGraph.SetMarkerColor(1)
eGraph.SetMarkerStyle(1)
############# b 1.0 construction plot #############
hline2 = ROOT.TLine(-0.2,0.,0.2,0.)
vline_l2 = ROOT.TLine(-0.097,0.,-0.097,-1.8)
vline_m2 = ROOT.TLine(0.00,0.0,-0.000,-1.8)
vline_h2 = ROOT.TLine(0.096,0.,0.096,-1.8)
a2 = ROOT.TText(-0.097,-2.1,"a")
b2 = ROOT.TText(0.096,-2.1,"b")
#d_est = ROOT.TText(0.,-0.6,"#hat{d}")
a2.SetTextColor(4)
b2.SetTextColor(4)
hline2.SetLineColor(2)
hline2.SetLineStyle(9)
vline_l2.SetLineColor(4)
vline_l2.SetLineStyle(9)
vline_m2.SetLineColor(4)
vline_m2.SetLineStyle(1) 
can2 = ROOT.TCanvas("can")
vline_h2.SetLineColor(4)
vline_h2.SetLineStyle(9)
can2.cd()
can2.SetGrid()
eGraph2.GetXaxis().SetRangeUser(-0.2,0.2)
#eGraph.Draw("APX")
#eGraph.Draw("ALP")
#eGraph.Draw("XSAME")
eGraph2.Draw("ALP")
eGraph2.Draw("PXSAME")

hline2.Draw("same")
vline_l2.Draw("same")
vline_h2.Draw("same")
vline_m2.Draw("same")
a2.Draw("same")
b2.Draw("same")
#d_est.Draw("same")
can2.SaveAs("eichk_ex_b1.0.pdf")



################# b 0.0 construction plot #####################

hline1 = ROOT.TLine(-0.2,0.,0.2,0.)
vline_l1 = ROOT.TLine(-0.032,0.,-0.032,-2.85)
vline_m1 = ROOT.TLine(-0.001,0.0,-0.001,-2.85)
vline_h1 = ROOT.TLine(0.030,0.,0.030,-2.85)
a1 = ROOT.TText(-0.035,-3.10,"a")
b1 = ROOT.TText(0.025,-3.10,"b")
#d_est = ROOT.TText(0.,-0.6,"#hat{d}")
a1.SetTextColor(4)
b1.SetTextColor(4)
hline1.SetLineColor(2)
hline1.SetLineStyle(9)
vline_l1.SetLineColor(4)
vline_l1.SetLineStyle(9)
vline_m1.SetLineColor(4)
vline_m1.SetLineStyle(1) 
can1 = ROOT.TCanvas("can")
vline_h1.SetLineColor(4)
vline_h1.SetLineStyle(9)
can1.cd()
can1.SetGrid()
eGraph.GetXaxis().SetRangeUser(-0.2,0.2)
#eGraph.Draw("APX")
#eGraph.Draw("ALP")
#eGraph.Draw("XSAME")
eGraph.Draw("ALP")
eGraph.Draw("PXSAME")

hline1.Draw("same")
vline_l1.Draw("same")
vline_h1.Draw("same")
vline_m1.Draw("same")
a1.Draw("same")
b1.Draw("same")
#d_est.Draw("same")
can1.SaveAs("eichk_ex_b0.0.pdf")



#dd = eGraph.GetX()
#m = eGraph.GetY()
#eh = eGraph.GetEYhigh()
#el = eGraph.GetEYlow()

########## b0.0 full plot ############################

eGraph.SetTitle(" ; #tilde{d}  ; < O >" )
eGraph.SetLineColor(1)
eGraph.SetMarkerColor(1)
eGraph.SetMarkerStyle(1)
can = ROOT.TCanvas("can2")
can.SetGrid()
eGraph.GetXaxis().SetRangeUser(-1.,1.)
can.cd()
eGraph.Draw("ACX")
can.SaveAs("eichk_full.pdf")


#print(dd)
#print(m)
#print(eh)
#print(el)

#calculate intersections



#draw lines for mean est and ci
#lest = ROOT.TLine(-1.,0.
