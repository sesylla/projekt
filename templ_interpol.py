#linear interpolation between templates
#from scipy.interpolate import interp1d
from numpy import linspace
from numpy import array
import matplotlib.pyplot as plt

templates = [-0.1,-0.2,-0.3,0.0,-0.1,0.0,0.5,0.8,0.3,0.4,0.5]
dtildevals = [-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2,0.3,0.4,0.5] # dtilde values corresponding to templates
#interpol = interp1d(dtildevals,templates)


def findx0x1(d,dtildevals,templates):
    print "d value: ",d
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print "dtilde out of range!"

print findx0x1(-0.1,dtildevals,templates)
print findx0x1(0.05,dtildevals,templates)        
print findx0x1(0.1,dtildevals,templates)  
print findx0x1(0.28,dtildevals,templates)  
print findx0x1(-0.18,dtildevals,templates)  
print findx0x1(0.6,dtildevals,templates)          

def interpol(xx,x_list,f_list): #f_list is list of histograms
    tmp=[]
    for x in xx:
        xvals,fvals = findx0x1(x,x_list,f_list)
        print xvals,fvals
        if not (xvals and fvals):
            print "interpolation failed."
        else:
            x0 = xvals[0]
            x1 = xvals[1]
            f0 = fvals[0]
            f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
            if x == x0:
                tmp.append(f0)
            #return f
            #elif abs(x -x1) < 10**-6:
            elif x == x1:
                tmp.append(f1)
            #return f
            else:
                #f = ((x1-x)/(x-x0))*(f0.Add(f1,(x-x0)/(x1-x)))
                #f = ((x1-x)/(x-x0))*(f0+f1*(x-x0)/(x1-x))
                f = f0*(x1-x)/(x1-x0) + f1*(x-x0)/(x1-x0)
                tmp.append(f)
    #print tmp
    return array(tmp)
    
x= linspace(-0.5,0.5,51)
plt.plot(dtildevals,templates,'o',x, interpol(x,dtildevals,templates),'.')   
plt.show()
