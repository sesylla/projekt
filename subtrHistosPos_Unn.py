import ROOT
import sys
import os
import shutil

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirname = sys.argv[1]
HistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

dtilde = sys.argv[2]
#dtilde0 = 
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]


outFileName = "xunn_new_diff_pos_"+dirname+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut 
HistFile = ROOT.TFile.Open(HistFileName, "READ")

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

def subtrHistos(dataFile,delPhiHistName,oo1HistName):
    delPhiHist = dataFile.Get(delPhiHistName)
    oo1Hist = dataFile.Get(oo1HistName)
    for histo in [delPhiHist,oo1Hist]:
        if not histo:
            print "no histogram found. check name!!"
            sys.exit(1)
    print "INTEGRAL BEFORE SCALING: ", oo1Hist.Integral()
    #oo1Hist.Scale( 200/oo1Hist.Integral())
    #delPhiHist.Scale( 200/delPhiHist.Integral())
    print "INTEGRAL oo1: ",oo1Hist.Integral()
    
    print [oo1Hist.GetBinContent(bin) for bin in range(oo1Hist.GetSize()/2,oo1Hist.GetSize())]
    print "oo1Hist.GetSize(): ",oo1Hist.GetSize()
#delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist_diff.GetSize()-2,delPhiHist_diff.GetMinimumBin(),delPhiHist_diff.GetMaximumBin())
    #oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist_diff.GetSize()-2,oo1Hist_diff.GetMinimumBin(),oo1Hist_diff.GetMaximumBin())
    delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
    oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())

    delPhiHist_mirr.SetEntries(delPhiHist.GetEntries())
    oo1Hist_mirr.SetEntries(oo1Hist.GetEntries())
    
    for bin in range(delPhiHist.GetSize()):
        
        delPhiHist_mirr.SetBinContent(bin,delPhiHist.GetBinContent(delPhiHist.GetSize()-bin-1))
        delPhiHist_mirr.SetBinError(bin,delPhiHist.GetBinError(delPhiHist.GetSize()-bin-1))
        
    for bin in range(oo1Hist.GetSize()):
        
        oo1Hist_mirr.SetBinContent(bin,oo1Hist.GetBinContent(oo1Hist.GetSize()-bin-1))
        oo1Hist_mirr.SetBinError(bin,oo1Hist.GetBinError(oo1Hist.GetSize()-bin-1))
    
    oo1list = [oo1Hist.GetBinContent(bin) for bin in range(oo1Hist.GetSize()/2,oo1Hist.GetSize())]
    mirrlist = [oo1Hist.GetBinContent(oo1Hist.GetSize()-bin-1) for bin in range(oo1Hist_mirr.GetSize()/2,oo1Hist_mirr.GetSize())]
    print oo1list
    print mirrlist
    print len(oo1list),len(mirrlist)
    print "INTEGRAL oo1Hist_mirr: ", oo1Hist_mirr.Integral()
    delPhiHist_diff = ROOT.TH1D("delPhiHist_diff"," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
    oo1Hist_diff = ROOT.TH1D("oo1Hist_diff"," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())

    delPhiHist_diff.Add(delPhiHist,delPhiHist_mirr, 1, -1)
    oo1Hist_diff.Add(oo1Hist,oo1Hist_mirr, 1, -1)
    
    print [oo1Hist_diff.GetBinContent(bin) for bin in range(oo1Hist_diff.GetSize()/2,oo1Hist_diff.GetSize())]
    #delPhiHist_diff.SetEntries(delPhiHist_mirr.GetEntries())
    #oo1Hist_diff.SetEntries(oo1Hist_mirr.GetEntries())

    delPhiHist_diff.SetNameTitle("delPhiHist_diff","delPhi difference")
    oo1Hist_diff.SetNameTitle("oo1Hist_diff","oo1 difference")
    
    delPhiHist_diff.SetStats(0)
    oo1Hist_diff.SetStats(0)
    
    #only range >0:
    #delPhiHist_pos = ROOT.TH1D("delPhiHist_pos"," ",(delPhiHist_diff.GetSize()-2)/2,0.,delPhiHist_diff.GetXaxis().GetXmax())
    #oo1Hist_pos = ROOT.TH1D("delPhiHist_pos"," ",(oo1Hist_diff.GetSize()-2)/2,0.,oo1Hist_diff.GetXaxis().GetXmax())
    
    ##SetRange takes bin number as argument
    delPhiHist_diff.GetXaxis().SetRange(delPhiHist_diff.GetSize()/2,delPhiHist_diff.GetSize()-2)
    oo1Hist_diff.GetXaxis().SetRange(oo1Hist_diff.GetSize()/2,oo1Hist_diff.GetSize()-2)
    #delPhiHist_diff.SetAxisRange(0.,delPhiHist_diff.GetXaxis().GetXmax(),"X")
    #oo1Hist_diff.SetAxisRange(0.,oo1Hist_diff.GetXaxis().GetXmax(),"X")
    #n=["Original_Histogram","MirrHist"]
    #for i, h in enumerate([oo1Hist,oo1Hist_mirr]):
    #    c = ROOT.TCanvas("c")
    #    c.SetGrid()
    #    h.Draw("h")
    #    c.SaveAs(n[i]+".pdf")
        
    return [delPhiHist_diff,oo1Hist_diff],["delPhiHist_diff"+dtilde,"oo1Hist_diff"+dtilde]
    

def writeHistosToFile(histlist,outFileName):    
    if os.path.exists(outFileName):
        shutil.rmtree(outFileName)
        print "overwriting dir "+outFileName+"  ........"
    os.mkdir(outFileName)
    print "directory "+outFileName+" created!"
    os.chdir(outFileName)

    
    outHistFile = ROOT.TFile.Open(outFileName+".root","RECREATE")
    outHistFile.cd()
    for hist in histlist:
        hist.Write()
    outHistFile.Close()

def plotAll(histlist,namelist,outFileName):
    for i,hist in enumerate(histlist):
        canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
        canvas.cd()
        canvas.SetGrid()
        hist.Draw("h")
        canvas.SaveAs(outFileName + namelist[i][0:-5]+".pdf")
    
def doAll(dataFile,outFileName):
    dataHistos, namelist = subtrHistos(dataFile,delPhiHistName,oo1HistName)
    writeHistosToFile(dataHistos,outFileName)
    plotAll(dataHistos,namelist,outFileName)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

doAll(HistFile,outFileName)
