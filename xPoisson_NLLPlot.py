import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt


#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
import os

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

outdirname = "NLLplots"

dirname = sys.argv[1] 
HistFileName =  sys.argv[1]+"/"+sys.argv[1]+".root"
dtilde = sys.argv[2]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]

templateHistFileName = "PartColl/PartColl.root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

#dtildelist for templates:
dtildelist = [-1.0,-0.95,-0.9,-0.85,-0.8,-0.75,-0.7,-0.65,-0.6,-0.55,-0.5,-0.45,-0.4,-0.35,-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,0.0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0]
#dtildelist = [-1.0,-.9,-.8,-.7,-.6,-.5,-.4,-.3,-.2,-.1,0.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1.0]
def maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName):
    templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
    for dtilde in dtildelist:
        oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        
        oo1Hist = HistFile.Get(oo1HistName)
        delPhiHist = HistFile.Get(delPhiHistName)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist:
            print "histogram %s not found!"%(oo1HistName)
        if not delPhiHist:
            print "histogram %s not found!"%(delPhiHistName)
        
        oo1Hist.Scale( 200/oo1Hist.Integral(),"width")
        delPhiHist.Scale( 200/delPhiHist.Integral(),"width")

        oo1templatelist.append(oo1Hist)
        delPhitemplatelist.append(delPhiHist)
    return oo1templatelist,delPhitemplatelist

def makearray(hist): #returns numpy  array of data bins
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildevals,templates):
    #print "d value: ",d
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print "dtilde out of range!"
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print "could not find x values for interpolation."
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #print "x= %d == x0 = %d"%(x,x0)
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            #print "x= %d == x0 = %d"%(x,x1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
            
            
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(k,mu): #log(pdf)
    return -mu + k*np.log(mu)

def NLL(params,hist_data,dtildelist,templatelist):
    x,dtilde = params
    res = 0
    #print x,dtilde
    #oo1templatelist, delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)
    hist_template = makearray(interpol(dtilde,dtildelist,templatelist))
    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    
    for d, t in zip(hist_data,hist_template):
        #f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
        f = lpdf(d,x*t)
        res += f
    return -res


#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(hist,dtildelist,templatelist):
    
    hist_data = makearray(hist)

    #print hist_data
    #print hist_template
    startpar = np.array([0.1,0.0])
    
    res = minimize(NLL,startpar,args=(hist_data,dtildelist,templatelist),bounds=[(1e-6,10),(1e-6,0.8)] )

    print(res)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    #err = np.diag(res.hess_inv.todense())

    print("result for x: %s +- ??" % (res.x[0]))
    print("result for dtilde: %s +- ??" % (res.x[1]))


delPhiHist = HistFile.Get(delPhiHistName)
oo1Hist = HistFile.Get(oo1HistName)
#delPhiHist.Sumw2()
#oo1Hist.Sumw2()
oo1Hist.Scale( 200/oo1Hist.Integral(),"width")
delPhiHist.Scale( 200/delPhiHist.Integral(),"width")

oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)

############## create output directory #################### 
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print "directory "+outdirname+" created!"
os.chdir(outdirname)

print "*************************oo1 Fit**************************"
#minNLL(oo1Hist,dtildelist,oo1templatelist)
hist_data = makearray(oo1Hist)
xx = np.linspace(1e-6,10.,100)
NLLvals0oo1 = [NLL([x,float(dtilde)],hist_data,dtildelist,oo1templatelist) for x in xx]
minoo1 = min(NLLvals0oo1)
NLLvalsoo1 = array("d",[x - minoo1 for x in NLLvals0oo1])

#oo1Graph = ROOT.TGraph(len(dd),dd,NLLvalsoo1)

#oo1Graph.SetTitle("NLL for  L(#tilde{d} | OO) ; #tilde{d}  ; -log(L(#tilde{d}))" )
#oo1Graph.SetMarkerStyle(1)
#oo1Graph.SetMarkerColor(1)

#canv_oo1 = ROOT.TCanvas("canvasoo1")
#canv_oo1.cd()
#canv_oo1.SetGrid()
#oo1Graph.Draw("AP")
#canv_oo1.SaveAs("NLL_oo1_dtilde"+str(dtilde)+".pdf")

plt.plot(xx,NLLvalsoo1,'o',color = "Black",ms = 1.)
plt.show()


