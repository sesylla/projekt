#! /bin/env python2
#This script adds OO and sgndDelPhi Data with given Parameters to a .root file.
silent=True
useEventStore = True
useNewEvent = False
iterations = 1
import sys
import os
import shutil
import HLeptonsCPRW
import array
import ROOT
import glob
ngtot = 0
ng0 = 0
ng1 = 0
ng2 = 0
nevents = 0
nerr = 0

if len(sys.argv) != 6:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s  <hist file name> <dtilde value> <pT cut value> <mass cut value> <delta Eta cut value>"%(sys.argv[0])
    # End the program
    sys.exit(1)

# make list of all data files
dirname  = "/home/ss944/projekt/daten/"
a = "mc16a/"
e = "mc16e/"
d = "mc16d/"
dir1 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346191.PoPy8_NNPDF30_VBFH125_tautaulm15hp20.D2.e7259_s3126_r9364_p3759.smPre_w_2_HS/"
dir2 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346192.PoPy8_NNPDF30_VBFH125_tautaulp15hm20.D2.e7259_s3126_r10724_p3759.smPre_w_0_HS/"
dirlist = []
filelist = []
for i in [a,e,d]:
    os.chdir(dirname+i)
    for j in glob.glob("*VBF*"):
        dirlist.append(dirname+i+j)
        #filelist.append(glob.glob("*.root"))
#print dirlist
for d in dirlist:
    os.chdir(d)
    for f in glob.glob("*.root"):
        filelist.append(d+"/"+f)

#print filelist
os.chdir("/home/ss944/projekt/rest")


#outFileName = "allHistos"


HistFileName = sys.argv[1]+".root"
dtilde = sys.argv[2]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]
outFileName = "part_coll_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut    


ooES = HLeptonsCPRW.OptObsEventStore();
    
if (useEventStore):
    if (silent == False):
        print("Using EventStore")
    ooES.initPDFSet("CT10", 0, 91.2)

def readToHisto(tree,namelist,dtilde,ptCut,massCut,delEtaCut):
    global ooES,ngtot,ng0,ng1,ng2,nevents,silent,useEventStore,useNewEvent,iterations,nerr
    m_dtilde = float(dtilde)
    ptcut = float(ptCut)
    masscut = float(massCut)
    deletacut = float(delEtaCut)
    
    oo1HistName = namelist[0]
    delPhiHistName = namelist[1]
    
    eventNumber = 0
    
    oo1Hist = ROOT.TH1D(oo1HistName+"New","oo1",14,-15.,15.)
    delPhiHist = ROOT.TH1D(delPhiHistName+"New","signed delta Phi",14,-4.,4.)
    
    

    for entryNum in range (0,tree.GetEntries()):
        tree.GetEntry(entryNum)
        #jet0_p4 = tree.jet_0_wztruth_p4
        #jet1_p4 = tree.jet_1_wztruth_p4
        #jet2_p4 = tree.jet_2_wztruth_p4
        jet0_p40 = tree.truth_reweight_info_parton_2_p4
        jet1_p40 = tree.truth_reweight_info_parton_3_p4
        jet2_p40 = tree.truth_reweight_info_parton_4_p4
        flavour1In = tree.truth_reweight_info_parton_0_pdgId;               
        flavour2In = tree.truth_reweight_info_parton_1_pdgId;               
        flavour0Out0 = tree.truth_reweight_info_parton_2_pdgId;              
        flavour1Out0 = tree.truth_reweight_info_parton_3_pdgId;              
        flavour2Out0 = tree.truth_reweight_info_parton_4_pdgId;
        
        jetlist0 = sorted([[jet0_p40,flavour0Out0],[jet1_p40,flavour1Out0],[jet2_p40,flavour2Out0]],key = lambda x: x[0].Pt(), reverse = True)
        
        jet0_p4 = jetlist0[0][0]
        jet1_p4 = jetlist0[1][0]
        jet2_p4 = jetlist0[2][0]
        
        flavour0Out = jetlist0[0][1]
        flavour1Out = jetlist0[1][1]
        flavour2Out = jetlist0[2][1]
        higgs_p4 = tree.boson_0_truth_p4
        if jet0_p4.Pt()>=jet1_p4.Pt()>=jet2_p4.Pt():
            pass
        else:
            print "pT sorting not ok"
        if jet1_p4.Pt() < 10.**(-5):
            continue
   
         
        pjet0 = array.array('d', [jet0_p4.Energy(), jet0_p4.Px(),jet0_p4.Py() ,jet0_p4.Pz() ])               # E,px,py,pz of nth final state parton
        pjet1 = array.array('d', [jet1_p4.Energy(), jet1_p4.Px(),jet1_p4.Py() ,jet1_p4.Pz() ])
        pjet2 = array.array('d', [jet2_p4.Energy(), jet2_p4.Px(),jet2_p4.Py() ,jet2_p4.Pz() ])
        phiggs = array.array('d',[higgs_p4.Energy(), higgs_p4.Px(),higgs_p4.Py() ,higgs_p4.Pz() ])            # E,px,py,pz of Higgs boson make sure that four-momentum conservation holds 
        ecm = 13000.;                           #proton-proton center-of mass energy in GeV
        mH = 124.999901;                       #mass of Higgs boson in Gev
        
        if jet2_p4.Pt() > 10.**(-5):
            npafin = 3;  #number of partons in final state  either  2 or 3
        else:
            npafin = 2;
        #print "npafin:",npafin
        #print pjet0[1],pjet1[1],pjet2[1]
        delEtaAbs = abs(jet0_p4.Eta()-jet1_p4.Eta())
        mjj = (jet0_p4 + jet1_p4).M()
        
        if (jet0_p4.Pt() > ptcut and jet1_p4.Pt() > ptcut and  delEtaAbs > deletacut and mjj > masscut and abs(jet0_p4.Eta()) < 4.5 and abs(jet1_p4.Eta()) < 4.5 ):
        #Hjj_p4 = jet0_p4 + jet1_p4 + jet2_p4 + higgs_p4
        #mHjj = Hjj_p4.M()
        #yHjj = Hjj_p4.Rapidity()
        #print m_dtilde,ptcut,deletacut,masscut
        
            nevents += 1
            #print entryNum
            x1 = tree.truth_event_info_Bjorken_x1;                  #Bjorken x of incoming partons, 1 in + z , 2 in -z direction
            x2 = tree.truth_event_info_Bjorken_x2;
            Q = 125;
#flavour assignment: t = 6  b = 5 c = 4, s = 3, u = 2, d = 1 
 #anti-qarks with negative sign
 #gluon = 0 


            #flavour0Out = tree.jet_0_flavorlabel_part;             
            #flavour1Out = tree.jet_1_flavorlabel_part;          
            #flavour2Out = tree.jet_2_flavorlabel_part;
            ng = 0 #number of gluons Out
            flavourlist = [flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out]
            jetlist = [pjet0,pjet1,pjet2]
            jet_p4list = [jet0_p4,jet1_p4,jet2_p4]
            #print flavourlist
            for i,flav in enumerate(flavourlist):
                if flav == 21:
                    flavourlist[i] = 0  #pdg of gluon is 21, but input has to be 0
                elif flav == 5:
                    flavourlist[i] = 3
                elif flav == -5:
                    flavourlist[i] = -3
                    #ngtot +=1
            flavourOutlist = flavourlist[2:5]
            for i ,flav in enumerate(flavourOutlist):
                if flav == 0: 
                    ng += 1
                    indexg = i
                    ngtot +=1
            if ng > 1:
                ng2 += 1
            elif ng == 1:
                if indexg != len(flavourOutlist)-1:
                    flavourOutlist.append(flavourOutlist[indexg])
                    del flavourOutlist[indexg]
                    jetlist.append(jetlist[indexg])
                    del jetlist[indexg]
                    #jet_p4list.append(jet_p4list[indexg])
                    #del jet_p4list[indexg]
                ng1 += 1
            elif ng == 0:
                ng0 += 1 
        
            eventNumber += 1;

        

           

            #for i in range(0,iterations):
            #    if not silent:
            #        print("Running iteration {}".format(i+1))
            #    if useEventStore:
            #        if (useNewEvent):
            #            eventNumber+=i


        

            oo1 = ooES.getOptObs(0, eventNumber, ecm, mH ,x1,x2,Q,pjet0,pjet1,phiggs);
        
            #print type(m_dtilde)
            rw =  ooES.getReweight(ecm, mH, 1 , \
          #0, 0, 0, 0, 0, # rsmin,din,dbin,dtin,dtbin \
          #0, 0, 0,           # a1hwwin,a2hwwin,a3hwwin \
          #0, 0, 0,           # a1haain,a2haain,a3haain \
          #0, 0, 0,           # a1hazin,a2hazin,a3hazin \
          #0, 0, 0,           # a1hzzin,a2hzzin,a3hzzin \
          #0,                     # lambdahvvin for formfactor if set to positive value \
          1,0,0, m_dtilde, m_dtilde, -1, \
          0,0,0,        \
          0,0,0,        \
          0,0,0,        \
          0,0,0,        \

          npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2], \
          x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs)
            if not silent:
                print("Calling getReweight(...)! Result is: {}".format(rw))

            else:
                pass
            if rw ==0.:
                nerr +=1

            #print flavourlist
            #print flavourOutlist
            
            oo1Hist.Fill(oo1,rw)
       
           

            liste = sorted([jet_p4list[0],jet_p4list[1]], key = lambda jetx: jetx.Eta(), reverse = True)
            #liste = sorted([jetlist0[0][0],jetlist0[1][0]],key = lambda jetx: jetx.Eta(),reverse = True)
            #liste = jet_p4list
            delPhi_Etasorted = liste[0].DeltaPhi(liste[1])
            if liste[0].Eta()*liste[1].Eta() < 0.:
                delPhiHist.Fill(delPhi_Etasorted,rw)
                
    print "nerr", nerr
    print "nevents", nevents    
    #print "resulting histos:", [delPhiHist,oo1Hist]        
    return [delPhiHist,oo1Hist],[delPhiHistName+"New",oo1HistName+"New"]

def addDataToFile(dataFileList,histfilename,dtilde,ptc,massc,deletac):
    

    
    oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(ptc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
    delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(ptc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
    namelist = [delPhiHistName,oo1HistName]
    if not os.path.exists(histfilename[0:-5]):
        os.mkdir(histfilename[0:-5])
    os.chdir(histfilename[0:-5])
    
    HistFile = ROOT.TFile.Open(histfilename,"UPDATE")
    
    filenr = 0
############ work needed in this part.       
    #print dataFileList
    oo1Hist = ROOT.TH1D(oo1HistName,"oo1",14,-15.,15.)
    delPhiHist = ROOT.TH1D(delPhiHistName,"signed delta Phi",14,-4.,4.)
    histlist = [delPhiHist,oo1Hist]
    for dataFileName in dataFileList:
        print "opening file ", filenr+1, " of ", len(dataFileList)
        #print "histlist at this point" , histlist

        dataFile = ROOT.TFile.Open(dataFileName, "READ")
        tree = dataFile.Get("NOMINAL")
        if not tree:
            print "no tree found!"
            sys.exit(1)
 
        if filenr == 0:
            oo1Hist0 = HistFile.Get(oo1HistName)
            delPhiHist0 = HistFile.Get(delPhiHistName)
            histlist0 = [delPhiHist0,oo1Hist0]
            for i,hist in enumerate(histlist0):
                
                oo1Hist = oo1Hist0
                delPhiHist = delPhiHist0
                histlist = [delPhiHist0,oo1Hist0]
            
            newhists,newnames = readToHisto(tree,namelist,dtilde,ptc,massc,deletac)
            print "entries: ",newhists[0].GetEntries()
            for i,hist in enumerate(newhists):
                histlist[i].Add(hist)
            #print "histlist at filenr = 0:",histlist
        else:
            for hist in histlist:
                if not hist:
                    print "no histo!!!!!!!!!!!!!"
                    sys.exit(1)
            newhists,newnames = readToHisto(tree,namelist,dtilde,ptc,massc,deletac)
            #print histlist, newhists, filenr
            print "entries: ",newhists[0].GetEntries()
            for i,hist in enumerate(newhists):
                histlist[i].Add(hist)
        #print histlist, filenr
    #print os.getcwd()
        
        #newhistlist.append(newhists)
        filenr += 1
        print "total number of entries in delPhi Hist: ",histlist[0].GetEntries()
        dataFile.Close()
#
    
        
    
    
    HistFile.cd()
    
    for hist in histlist:
        hist.Write()
    HistFile.Close()
    print "dtilde",dtilde,"ptcut",ptc,"masscut",massc,"deletacut",deletac," added to file "+histfilename



addDataToFile(filelist,HistFileName,dtilde,pTCut,massCut,delEtaCut)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

#doAll(tree,dtilde,pTCut,massCut,delEtaCut,outFileName)

print "ngtot, ng0, ng1, ng2,nevents", ngtot,ng0,ng1,ng2,nevents
