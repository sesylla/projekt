import ROOT
import sys
import os
import shutil

if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirname = sys.argv[1]
HistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

dtilde = sys.argv[2]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]

oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

HistFile = ROOT.TFile.Open(HistFileName, "READ")
 
outFileName = "stack_"+dirname+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut    

def makeMirrorHists(delPhiHist,oo1Hist):

    
   
    delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
    oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())

    delPhiHist_mirr.SetEntries(delPhiHist.GetEntries())
    oo1Hist_mirr.SetEntries(oo1Hist.GetEntries())

    for bin in range(delPhiHist.GetSize()-2):
        bin+=1 #skip underflow bin (bin 0 is underflow, last one is overflow)
        delPhiHist_mirr.SetBinContent(bin,delPhiHist.GetBinContent(delPhiHist.GetSize()-bin-1))
        delPhiHist_mirr.SetBinError(bin,delPhiHist.GetBinError(delPhiHist.GetSize()-bin-1))
        
    for bin in range(oo1Hist.GetSize()-2):
        bin+=1
        oo1Hist_mirr.SetBinContent(bin,oo1Hist.GetBinContent(oo1Hist.GetSize()-bin-1))
        oo1Hist_mirr.SetBinError(bin,oo1Hist.GetBinError(oo1Hist.GetSize()-bin-1))

    #delPhiHist_diff.Scale(1/delPhiHist_diff.Integral(), "width")
    #oo1Hist_diff.Scale(1/oo1Hist_diff.Integral(), "width")
    #delPhiHist_mirr.Scale(1/delPhiHist_mirr.Integral(), "width")
    #oo1Hist_mirr.Scale(1/oo1Hist_mirr.Integral(), "width")


    

    delPhiHist_mirr.SetNameTitle("delPhiHist_diff","delPhi mirrored")
    oo1Hist_mirr.SetNameTitle("oo1Hist_diff","oo1 mirrored")
    
    delPhiHist_mirr.SetStats(0)
    oo1Hist_mirr.SetStats(0)
    
    return [delPhiHist_mirr,oo1Hist_mirr],["delPhiHist_diff","oo1Hist_diff"]
    #return [delPhiHist_diff,oo1Hist_diff],["delPhiHist_diff","oo1Hist_diff"]


def stackHistos(dataFile,delPhiHistName,oo1HistName):
    
    delPhiHist = dataFile.Get(delPhiHistName)
    oo1Hist = dataFile.Get(oo1HistName)
    
    histlist_mirr, namelist_mirr = makeMirrorHists(delPhiHist,oo1Hist)
    delPhiHist_mirr = histlist_mirr[0]
    oo1Hist_mirr = histlist_mirr[1]
    
    oo1stack = ROOT.THStack("stack","")
    oo1Hist.SetFillColor(4)
    oo1stack.Add(oo1Hist)
    oo1Hist_mirr.SetFillColor(2)
    oo1stack.Add(oo1Hist_mirr)

    delPhistack = ROOT.THStack("stack","")
    delPhiHist.SetFillColor(4)
    delPhistack.Add(delPhiHist)
    delPhiHist_mirr.SetFillColor(2)
    delPhistack.Add(delPhiHist_mirr)

    return [delPhistack,oo1stack],["delPhiStack","oo1Stack"]

def writeHistosToFile(histlist,outFileName):    
    if os.path.exists(outFileName):
        shutil.rmtree(outFileName)
        print "overwriting dir "+outFileName+"  ........"
    os.mkdir(outFileName)
    print "directory "+outFileName+" created!"
    os.chdir(outFileName)

    
    outHistFile = ROOT.TFile.Open(outFileName+".root","RECREATE")
    outHistFile.cd()
    for hist in histlist:
        hist.Write()
    outHistFile.Close()

def plotAll(histlist,namelist,outFileName):
    for i,hist in enumerate(histlist):
        canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
        canvas.cd()
        canvas.SetGrid()
        hist.Draw("h")
        canvas.SaveAs(outFileName + namelist[i][0:-5]+".pdf")
    
def doAll(dataFile,outFileName):
    dataHistos, namelist = stackHistos(dataFile,delPhiHistName,oo1HistName)
    writeHistosToFile(dataHistos,outFileName)
    plotAll(dataHistos,namelist,outFileName)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

doAll(HistFile,outFileName)
