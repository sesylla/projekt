#from mpl_toolkits.mplot3d import axes3d, Axes3D

import numpy as np
#import scipy.stats
#from scipy.special import iv
#from scipy.optimize import minimize
#import matplotlib.pyplot as plt
#from matplotlib import cm
import os

#plt.rcParams['mathtext.fontset'] = 'stix'
#plt.rcParams['font.family'] = 'STIXGeneral' 
#plt.rcParams.update({'lines.markeredgewidth': 1})

#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
#import os

if len(sys.argv) !=5 :
    print ("USAGE: <0 = counts / 1 = difference / 2 = sum / 3 = Mirr> < 0=Unnormalized / 1 = Normalized (SMNorm = Unnormalized)> <evColl .root file name> <dtilde>")
    sys.exit(1)

diff = int(sys.argv[1])
norm = int(sys.argv[2])
dirname = sys.argv[3]

outdir = "/home/ss944/BscPlots/MirrPlots"






#dtildelist for templates:
#dtildelist = ["0.20"]
dtildelist = [sys.argv[4]]

def GetNormFacSM(HistFile):
    SMHistoo = HistFile.Get("oo1_dtilde0.00").Clone()
    ROOT.SetOwnership(SMHistoo,False)
    SMHistoo.SetDirectory(0)
    
    SMHistdp = HistFile.Get("dp_dtilde0.00").Clone()
    ROOT.SetOwnership(SMHistdp,False)
    SMHistdp.SetDirectory(0)

    Noo = 200./SMHistoo.Integral()
    Ndp = 200./SMHistdp.Integral()

    return Noo,Ndp

def subtrAdd(Hist,normfac,d):

    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  

    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    ROOT.SetOwnership(Hist_diff,False)
    Hist_diff.SetDirectory(0)
    ROOT.SetOwnership(Hist_sum,False)
    Hist_sum.SetDirectory(0)
        
    return Hist_diff,Hist_sum

def plotAll(dtildelist,Norm,Diff):
    binarroo1,binarrdP,binerrsoo1,binerrsdP = [],[],[], [] #[[first bins for all dtilde],[second bins],....]
    HistFileName =  dirname+"/"+dirname+".root"
    HistFile = ROOT.TFile.Open(HistFileName,"READ")
    canvoo = ROOT.TCanvas("canvoo")
    canvdp = ROOT.TCanvas("canvdp")
    canvoo.SetGrid()
    canvdp.SetGrid()
    legendoo = ROOT.TLegend(0.1,0.7,0.4,0.9)
    legenddp = ROOT.TLegend(0.1,0.7,0.4,0.9)
    normfacSMoo,normfacSMdp = GetNormFacSM(HistFile)
    if Diff == 1:
        if Norm == 1:
            tmpoo0 = HistFile.Get("oo1_dtilde-0.05").Clone()
            ROOT.SetOwnership(tmpoo0,False)
            tmpoo0.SetDirectory(0)
            normoo1 = 200./tmpoo0.Integral()
            tmp_h_oo = subtrAdd(tmpoo0,normoo1,"-0.05")[0]
            llimitoo = 1.1*tmp_h_oo.GetMinimum()
            tmpdp0 = HistFile.Get("dp_dtilde-0.05").Clone()
            ROOT.SetOwnership(tmpdp0,False)
            tmpdp0.SetDirectory(0)
            normdP = 200./tmpdp0.Integral()
            tmp_h_dp = subtrAdd(tmpdp0,normdP,"-0.05")[0]
            llimitdp = 1.1*tmp_h_dp.GetMinimum()
        elif Norm == 0:
            tmp_h_oo = subtrAdd(HistFile.Get("oo1_dtilde-0.05"),normfacSMoo,"-0.05")[0].Clone()
            llimitoo = 1.1*tmp_h_oo.GetMinimum()
            tmp_h_dp = subtrAdd(HistFile.Get("dp_dtilde-0.05"),normfacSMdp,"-0.05")[0].Clone()
            llimitdp = 1.1*tmp_h_dp.GetMinimum()
            tmp_h_oo.SetStats(0)
            tmp_h_dp.SetStats(0)
            ROOT.SetOwnership(tmp_h_oo,False)
            tmp_h_dp.SetDirectory(0)
            ROOT.SetOwnership(tmp_h_dp,False)
            tmp_h_dp.SetDirectory(0)
    
        for i,dtilde in enumerate(dtildelist):
            if Norm == 1:
               
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
             
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
                normoo1 = 200./oo1Hist0.Integral()
                normdP = 200./delPhiHist0.Integral()

                oo1Hist = subtrAdd(oo1Hist0,normoo1,dtilde)[0]
                delPhiHist = subtrAdd(delPhiHist0,normdP,dtilde)[0]
    
            elif Norm == 0:
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
             
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
             
                oo1Hist = subtrAdd(oo1Hist0,1.,dtilde)[0]
                delPhiHist = subtrAdd(delPhiHist0,1.,dtilde)[0]
               
            else:
                print("Normalize has to be 0 or 1")
                sys.exit(1)
            
            oo1Hist.SetTitle(" ; OO ; Differenzen #lambda_{i}^{-}")
            delPhiHist.SetTitle(" ; #Delta#Phi_{jj}^{sgd}  ; Differenzen #lambda_{i}^{-}")
            if dtilde == "0.00":
                oo1Hist.SetLineColor(1)
                delPhiHist.SetLineColor(1)
            elif dtilde == "0.20":
                oo1Hist.SetLineColor(2)
                delPhiHist.SetLineColor(2)
            elif dtilde == "-0.05":
                oo1Hist.SetLineColor(4)
                delPhiHist.SetLineColor(4)
            #legendoo.AddEntry(oo1Hist,"#tilde{d} = "+dtilde)
            #legenddp.AddEntry(delPhiHist,"#tilde{d} = "+dtilde)
            oo1Hist.GetXaxis().SetRangeUser(0.,15.)
            delPhiHist.GetXaxis().SetRangeUser(0.,np.pi)
            #if dtilde == "-0.20":
            #    oo1Hist.GetYaxis().SetRangeUser(llimitoo,oo1Hist.GetMaximum()*1.1)
            #    delPhiHist.GetYaxis().SetRangeUser(llimitdp,delPhiHist.GetMaximum()*1.1)
            oo1Hist.GetYaxis().SetRangeUser(llimitoo,0.0)
            delPhiHist.GetYaxis().SetRangeUser(llimitdp,0.0)
            oo1Hist.GetYaxis().SetTitleOffset(1.45)
            delPhiHist.GetYaxis().SetTitleOffset(1.45)
            oo1Hist.SetStats(0)
            delPhiHist.SetStats(0)
            for bin in range(oo1Hist0.GetSize()-2):
                print(oo1Hist0.GetBinContent(bin+1),oo1Hist.GetBinContent(bin+1))
            for bin in range((oo1Hist0.GetSize()-2)//2):
                print(oo1Hist0.GetBinContent(bin+1+(oo1Hist0.GetSize()-2)//2)-oo1Hist0.GetBinContent((oo1Hist0.GetSize()-2)//2 - bin))
            canvoo.cd()
            oo1Hist.Draw("HistESAME")
            canvdp.cd()
            delPhiHist.Draw("HistESame")
        #legendoo.SetTextSize(0.04)
        #legenddp.SetTextSize(0.04)
        #canvoo.cd()
        #legendoo.Draw()
        #canvdp.cd()
        #legenddp.Draw()
        os.chdir(outdir)
        if Norm == 1:
            canvoo.SaveAs("d"+dtilde+"_Diff_N200_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Diff_N200_dp.pdf")
        elif Norm == 0:
            canvoo.SaveAs("d"+dtilde+"_Diff_SMNorm_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Diff_SMNorm_dp.pdf")
        
    elif Diff == 0:
        
        
        
        for i,dtilde in enumerate(dtildelist):
            oo1HistName = "oo1_dtilde"+dtilde
            delPhiHistName = "dp_dtilde"+dtilde
            
            oo1Hist = HistFile.Get(oo1HistName)
            delPhiHist = HistFile.Get(delPhiHistName)
        
            if Norm == 1:
                oo1Hist.Scale(200/oo1Hist.Integral())
                delPhiHist.Scale(200/delPhiHist.Integral())
            elif Norm == 0:
                oo1Hist.Scale(normfacSMoo)
                delPhiHist.Scale(normfacSMdp)
            if dtilde == "0.00":
                oo1Hist.SetLineColor(1)
                delPhiHist.SetLineColor(1)
            elif dtilde == "0.20":
                oo1Hist.SetLineColor(2)
                delPhiHist.SetLineColor(2)
            elif dtilde == "-0.05":
                oo1Hist.SetLineColor(4)
                delPhiHist.SetLineColor(4)
            
            
            oo1Hist.SetTitle(" ; OO ; Anzahl Ereignisse #lambda_{ i}")
            delPhiHist.SetTitle(" ; #Delta#Phi_{jj}^{sgd}  ; Anzahl Ereignisse #lambda_{i}")
            oo1Hist.SetStats(0)
            delPhiHist.SetStats(0)
            
            #legendoo.AddEntry(oo1Hist,"#tilde{d} = "+dtilde)
            #legenddp.AddEntry(delPhiHist,"#tilde{d} = "+dtilde)
            
            canvoo.cd()
            oo1Hist.Draw("HistESAME")
            canvdp.cd()
            delPhiHist.Draw("HistESame")
        #legendoo.SetTextSize(0.04)
        #legenddp.SetTextSize(0.04)
        #canvoo.cd()
        #legendoo.Draw()
        #canvdp.cd()
        #legenddp.Draw()
        os.chdir(outdir)
        if Norm == 1:
            canvoo.SaveAs("d"+dtilde+"_Dist_N200_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Dist_N200_dp.pdf")
        elif Norm == 0:
            canvoo.SaveAs("d"+dtilde+"_Dist_SMNorm_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Dist_SMNorm_dp.pdf")
    elif Diff == 3:
        for i,dtilde in enumerate(dtildelist):
            oo1HistName = "oo1_dtilde"+dtilde
            delPhiHistName = "dp_dtilde"+dtilde
            
            oo1Hist = HistFile.Get(oo1HistName)
            delPhiHist = HistFile.Get(delPhiHistName)
        
            if Norm == 1:
                oo1Hist.Scale(200/oo1Hist.Integral())
                delPhiHist.Scale(200/delPhiHist.Integral())
            elif Norm == 0:
                oo1Hist.Scale(normfacSMoo)
                delPhiHist.Scale(normfacSMdp)
            if dtilde == "0.00":
                oo1Hist.SetLineColor(1)
                delPhiHist.SetLineColor(1)
            elif dtilde == "0.20":
                oo1Hist.SetLineColor(2)
                delPhiHist.SetLineColor(2)
            elif dtilde == "-0.05":
                oo1Hist.SetLineColor(4)
                delPhiHist.SetLineColor(4)

            Hist_mirr_oo = ROOT.TH1D("oo_Hist_mirr_dtilde"+dtilde," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())
            Hist_mirr_dp = ROOT.TH1D("dp_Hist_mirr_dtilde"+dtilde," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
            
            Hist_mirr_oo.Sumw2()
            Hist_mirr_dp.Sumw2()
            
            for bin in range(oo1Hist.GetSize()):
                Hist_mirr_oo.SetBinContent(bin,oo1Hist.GetBinContent(oo1Hist.GetSize()-bin-1))
                Hist_mirr_oo.SetBinError(bin,oo1Hist.GetBinError(oo1Hist.GetSize()-bin-1))
            for bin in range(delPhiHist.GetSize()):
                Hist_mirr_dp.SetBinContent(bin,delPhiHist.GetBinContent(delPhiHist.GetSize()-bin-1))
                Hist_mirr_dp.SetBinError(bin,delPhiHist.GetBinError(delPhiHist.GetSize()-bin-1))
            
            if dtilde == "0.00":
                Hist_mirr_oo.SetLineColor(1)
                Hist_mirr_dp.SetLineColor(1)
            elif dtilde == "0.20":
                Hist_mirr_oo.SetLineColor(2)
                Hist_mirr_dp.SetLineColor(2)
            elif dtilde == "-0.05":
                Hist_mirr_oo.SetLineColor(4)
                Hist_mirr_dp.SetLineColor(4)
            Hist_mirr_oo.SetTitle(" ; OO ; Anzahl Ereignisse #lambda_{ i}")
            Hist_mirr_dp.SetTitle(" ; #Delta#Phi_{jj}^{sgd}  ; Anzahl Ereignisse #lambda_{i}")
            Hist_mirr_oo.SetStats(0)
            Hist_mirr_dp.SetStats(0)
            
            
            #legendoo.AddEntry(,"#tilde{d} = "+dtilde)
            #legenddp.AddEntry(delPhiHist,"#tilde{d} = "+dtilde)
            
            canvoo.cd()
            Hist_mirr_oo.Draw("HistESAME")
            canvdp.cd()
            Hist_mirr_dp.Draw("HistESame")
        #legendoo.SetTextSize(0.04)
        #legenddp.SetTextSize(0.04)
        #canvoo.cd()
        #legendoo.Draw()
        #canvdp.cd()
        #legenddp.Draw()
        os.chdir(outdir)
        if Norm == 1:
            canvoo.SaveAs("d"+dtilde+"_Mirr_N200_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Mirr_N200_dp.pdf")
        elif Norm == 0:
            canvoo.SaveAs("d"+dtilde+"_Mirr_SMNorm_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Mirr_SMNorm_dp.pdf")
    elif Diff == 2:
        for i,dtilde in enumerate(dtildelist):
            if Norm == 1:
               
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
             
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
                normoo1 = 200./oo1Hist0.Integral()
                normdP = 200./delPhiHist0.Integral()

                oo1Hist = subtrAdd(oo1Hist0,normoo1,dtilde)[1]
                delPhiHist = subtrAdd(delPhiHist0,normdP,dtilde)[1]
    
            elif Norm == 0:
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
             
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
             
                oo1Hist = subtrAdd(oo1Hist0,normfacSMoo,dtilde)[1]
                delPhiHist = subtrAdd(delPhiHist0,normfacSMdp,dtilde)[1]
               
            else:
                print("Normalize has to be 0 or 1")
                sys.exit(1)
                
           
            oo1Hist.SetTitle(" ; OO ; Summen #lambda_{i}^{+}")
            delPhiHist.SetTitle(" ; #Delta#Phi_{jj}^{sgd}  ; Summen #lambda_{i}^{+}")
            
            if dtilde == "0.00":
                oo1Hist.SetLineColor(1)
                delPhiHist.SetLineColor(1)
            elif dtilde == "0.20":
                oo1Hist.SetLineColor(2)
                delPhiHist.SetLineColor(2)
            elif dtilde == "-0.05":
                oo1Hist.SetLineColor(4)
                delPhiHist.SetLineColor(4)
            #legendoo.AddEntry(oo1Hist,"#tilde{d} = "+dtilde)
            #legenddp.AddEntry(delPhiHist,"#tilde{d} = "+dtilde)
            oo1Hist.GetXaxis().SetRangeUser(0.,15.)
            delPhiHist.GetXaxis().SetRangeUser(0.,np.pi)
            if dtilde == "0.20":
                oo1Hist.GetYaxis().SetRangeUser(0.,oo1Hist.GetMaximum()*1.1)
            oo1Hist.SetStats(0)
            delPhiHist.SetStats(0)
            canvoo.cd()
            oo1Hist.Draw("HistESAME")
            canvdp.cd()
            delPhiHist.Draw("HistESame")
        #legendoo.SetTextSize(0.04)
        #legenddp.SetTextSize(0.04)
        #canvoo.cd()
        #legendoo.Draw()
        #canvdp.cd()
        #legenddp.Draw()
        os.chdir(outdir)
           
        if Norm == 1:
            canvoo.SaveAs("d"+dtilde+"_Sum_N200_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Sum_N200_dp.pdf")
        elif Norm == 0:
            canvoo.SaveAs("d"+dtilde+"_Sum_SMNorm_oo.pdf")
            canvdp.SaveAs("d"+dtilde+"_Sum_SMNorm_dp.pdf")







plotAll(dtildelist,norm,diff)
