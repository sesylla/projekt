#from mpl_toolkits.mplot3d import axes3d, Axes3D

import numpy as np
import scipy.stats
from scipy.special import iv
from scipy.optimize import minimize
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import cm


plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral' 
plt.rcParams.update({'lines.markeredgewidth': 1})

#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
#import os

if len(sys.argv) !=4 :
    print ("USAGE: <0 = counts / 1 = difference / 2 = sum> < 0=Unnormalized / 1 = Normalized (SMNorm = Unnormalized)> <evColl .root file name>")
    sys.exit(1)

diff = int(sys.argv[1])
norm = int(sys.argv[2])
dirname = sys.argv[3]







#dtildelist for templates:
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']


    
    


def subtrAdd(Hist,normfac,d):

    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  

    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)

        
    return Hist_diff,Hist_sum

def makediffarrs(dtildelist,Norm,Diff):
    binarroo1,binarrdP,binerrsoo1,binerrsdP = [],[],[], [] #[[first bins for all dtilde],[second bins],....]
    HistFileName =  dirname+"/"+dirname+".root"
    HistFile = ROOT.TFile.Open(HistFileName,"READ")
    if Diff == 1:
        for i,dtilde in enumerate(dtildelist):
            if Norm == 1:
                
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
                
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
                normoo1 = 200./oo1Hist0.Integral()
                normdP = 200./delPhiHist0.Integral()

                oo1Hist = subtrAdd(oo1Hist0,normoo1,dtilde)[0]
                delPhiHist = subtrAdd(delPhiHist0,normdP,dtilde)[0]
            elif Norm == 0:
                oo1HistName = "oo1_diff_dtilde"+dtilde
                delPhiHistName = "dp_diff_dtilde"+dtilde
                oo1Hist = HistFile.Get(oo1HistName)
                delPhiHist = HistFile.Get(delPhiHistName)
            else:
                print("Normalize has to be 0 or 1")
                sys.exit(1)
    
           
        #these are for diff Histo:
            

            
            #oo1HistName = "oo1_diff_dtilde"+dtilde
            #delPhiHistName = "dp_diff_dtilde"+dtilde
            
            #if Norm == 1:
            #    oo1Hist.Scale(200./oo1Hist.Integral())
            #    delPhiHist.Scale(200./delPhiHist.Integral())
            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])
            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))
    
    elif Diff == 0:
        
        
        
        for i,dtilde in enumerate(dtildelist):
            oo1HistName = "oo1_dtilde"+dtilde
            delPhiHistName = "dp_dtilde"+dtilde
            
            oo1Hist = HistFile.Get(oo1HistName)
            delPhiHist = HistFile.Get(delPhiHistName)
        
            if Norm == 1:
                oo1Hist.Scale(200/oo1Hist.Integral())
                delPhiHist.Scale(200/delPhiHist.Integral())
        
            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])
            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))
    elif Diff == 2:
        
        
       
        for i,dtilde in enumerate(dtildelist):
            if Norm == 1:
                
                oo1HistName = "oo1_dtilde"+dtilde
                delPhiHistName = "dp_dtilde"+dtilde
                
                oo1Hist0 = HistFile.Get(oo1HistName)
                delPhiHist0 = HistFile.Get(delPhiHistName)
                
                normoo1 = 200./oo1Hist0.Integral()
                normdP = 200./delPhiHist0.Integral()

                oo1Hist = subtrAdd(oo1Hist0,normoo1,dtilde)[1]
                delPhiHist = subtrAdd(delPhiHist0,normdP,dtilde)[1]
            elif Norm == 0:
                oo1HistName = "oo1_sum_dtilde"+dtilde
                delPhiHistName = "dp_sum_dtilde"+dtilde
                oo1Hist = HistFile.Get(oo1HistName)
                delPhiHist = HistFile.Get(delPhiHistName)
            else:
                print("Normalize has to be 0 or 1")
                sys.exit(1)
    
           
        #these are for diff Histo:
            

            
            #oo1HistName = "oo1_diff_dtilde"+dtilde
            #delPhiHistName = "dp_diff_dtilde"+dtilde
            
            #if Norm == 1:
            #    oo1Hist.Scale(200./oo1Hist.Integral())
            #    delPhiHist.Scale(200./delPhiHist.Integral())
            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])
            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))
    
                
            
         

    return binarroo1,binarrdP,binerrsoo1,binerrsdP


def makeplotoo1(dtildelist,binarr,errarr,Diff):
    
    
    for bin in range(len(binarr)):
        plt.errorbar(dtildelist,binarr[bin],yerr=errarr[bin],xerr=None,linestyle="None",label="bin "+str(bin+1),marker="x",ms = 1.,capsize=1,linewidth=0.5)
    plt.legend(loc='best')#,prop={'size': 10})
    plt.grid()
    #plt.title("$\mathcal{O}\mathcal{O}$")
    plt.xlabel(r"$\tilde{d}$")
    if Diff == 1:
        plt.ylabel("Differenzen $\lambda_i^-$")
    elif Diff == 0:
        plt.ylabel("Ereignisse $\lambda_{1, \, i}$")
    elif Diff == 2:
        plt.ylabel("Summen $\lambda_i^+$")
    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.show()

def makeplotdP(dtildelist,binarr,errarr,Diff):
    
    
    for bin in range(len(binarr)):
        plt.errorbar(dtildelist,binarr[bin],yerr=errarr[bin],xerr=None,label="bin "+str(bin+1),marker = "x",ms = 1.,linestyle="None",capsize=1,linewidth=0.5)
    plt.legend(loc='best')#,prop={'size': 8})
    plt.grid()
    #plt.title("$\Delta \Phi_{jj}^{\mathrm{sgd}}$")
    plt.xlabel(r"$\tilde{d}$")
    if Diff == 1:
        plt.ylabel("Differenzen $\lambda_i^-$ ")
    elif Diff == 0:
        plt.ylabel("Ereignisse $\lambda_{1, \, i}$")
    elif Diff == 2:
        plt.ylabel("Summen $\lambda_i^+$")

    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.show()




binarroo10,binarrdP0,binerrsoo10,binerrsdP0 = makediffarrs(dtildelist,norm,diff)


dtildevals = [float(dtildelist[d]) for d in range(len(dtildelist))] 
binarroo1 = [[binarroo10[i][n] for n in range(len(dtildelist))] for i in range(len(binarroo10)//2,len(binarroo10))]
binarrdP = [[binarrdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binarrdP0)//2,len(binarrdP0))]
binerrsoo1 = [[binerrsoo10[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsoo10)//2,len(binerrsoo10))]
binerrsdP = [[binerrsdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsdP0)//2,len(binerrsdP0))]
#print(binerrsoo1)

for bin in range(len(binarroo1)):
    print(binerrsoo1[bin])
makeplotoo1(dtildevals,binarroo1,binerrsoo1,diff)

makeplotdP(dtildevals,binarrdP,binerrsdP,diff)


def getci(dtildelist,binarr,errarr):
    ci_list,ci_err_list,ci_lin_list,ci_const_list  = [], [] ,[],[]
    
    for bin in range(len(binarr)-1):
        print("errarr: " ,np.array(errarr[bin]))
        l,cov=np.polyfit(np.array(dtildelist),np.array(binarr[bin]),1,cov=True)#,w=1./np.array(errarr[bin]))
        print("+++")
        #p=np.poly1d(l)
        #print "Bin Nr. ", bin
        #print 'Slope and y-intercept:'
        #print l
        #print "covariance matrix:"
        #print cov
        #print 'Error on slope and y-intercept:'
        errs=np.sqrt(np.diag(cov))
        #print errs
        ci_list.append(l[0])
        ci_err_list.append(errs[0])
        #ci_lin_list.append(l[0])
        ci_const_list.append(l[1])
    return ci_list,ci_const_list

print("********************************************************")
print("get ci from np.polyfit , f(d) = bi0 + ci*dtilde ")
print("OO : ")
dtildevals = np.array([float(dt) for dt in dtildelist])
cilinoo,ciconstoo = getci(dtildevals,binarroo1,binerrsoo1)
print(ciconstoo)
print(cilinoo)
#print(cioo)

print("get errs:")
#print(erroo)

print("delPhi : ")
cilindp,ciconstdp = getci(dtildevals,binarrdP,binerrsdP)
print(ciconstdp)
print(cilindp)
#print(cidp)
print("get errs:")
#print(errdp)

x = dtildevals
y = binarroo1[2]
def func(x,a):
    return a*x
popt, _ = curve_fit(func, x, y,sigma=binerrsoo1[2])



plt.errorbar(dtildevals,binarroo1[2],yerr=binerrsoo1[2],xerr=None,label="bin "+str(3),color="g",marker = "x",ms = 1.,linestyle="None",capsize=1,linewidth=0.5)
#plt.plot(dtildevals,ciconstoo[2]+cilinoo[2]*dtildevals,linestyle="-",color="r",linewidth=0.5,label="_g1")
plt.plot(x, func(x, *popt), linestyle='-',color="g",label="_g1",linewidth=0.5)
plt.errorbar(dtildevals,binarroo1[3],yerr=binerrsoo1[3],xerr=None,label="bin "+str(4),color="r",marker = "x",ms = 1.,linestyle="None",capsize=1,linewidth=0.5)

popt2, _ = curve_fit(func, x, binarroo1[3],sigma=binerrsoo1[3])
plt.plot(x, func(x, *popt2), linestyle='-',color="r",label="_g2",linewidth=0.5)
#plt.plot(dtildevals,ciconstoo[3]+cilinoo[3]*dtildevals,linestyle="-",color="g",linewidth = 0.5,label="_g2")
plt.legend(loc='best')#,prop={'size': 8})
plt.grid()
    #plt.title("$\Delta \Phi_{jj}^{\mathrm{sgd}}$")
plt.xlabel(r"$\tilde{d}$")

plt.ylabel("Differenzen $\lambda_i^-$ ")


    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.show()

plt.errorbar(dtildevals,binarrdP[2],yerr=binerrsdP[2],xerr=None,label="bin "+str(3),color="g",marker = "x",ms = 1.,linestyle="None",capsize=1,linewidth=0.5)
popt3, _ = curve_fit(func, x, binarrdP[2],sigma=binerrsdP[2])
plt.plot(x, func(x, *popt3), linestyle='-',color="g",label="_g1",linewidth=0.5)
#plt.plot(dtildevals,ciconstdp[2]+cilindp[2]*dtildevals,linestyle="-",color="r",linewidth=0.5,label="_g1")
plt.errorbar(dtildevals,binarrdP[3],yerr=binerrsdP[3],xerr=None,label="bin "+str(4),color="r",marker = "x",ms = 1.,linestyle="None",capsize=1,linewidth=0.5)
popt4, _ = curve_fit(func, x, binarrdP[3],sigma=binerrsdP[3])
plt.plot(x, func(x, *popt4), linestyle='-',color="r",label="_g2",linewidth=0.5)
#plt.plot(dtildevals,ciconstdp[3]+cilindp[3]*dtildevals,linestyle="-",color="g",linewidth = 0.5,label="_g2")
plt.legend(loc='best')#,prop={'size': 8})
plt.grid()
    #plt.title("$\Delta \Phi_{jj}^{\mathrm{sgd}}$")
plt.xlabel(r"$\tilde{d}$")

plt.ylabel("Differenzen $\lambda_i^-$ ")


    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.show()
