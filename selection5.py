###This returns directory with histogram file including histograms pT, eta, phi, E ###

import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os
import shutil

if len(sys.argv) != 6:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file> <jet 0 branch name> <jet 1 branch name> <jet 2 branch name> <out file name>"%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
jet0Name = sys.argv[2]
jet1Name = sys.argv[3]
jet2Name = sys.argv[4]
outFileName = sys.argv[5]

# readToHisto returns list of histograms of 4-vector-components
def readToHisto(tree,jet0name,jet1name,jet2name):
   
 
  
    delPhihist = ROOT.TH1D("delPhi","signed delPhi",100,-4.,4.)
    delPhihist.Sumw2()
    # Booleans are to check if sorting algorithm works
    tmp = True # for eta sorting
    tmp2 = True # for initial pT sorting

    for entryNum in range(0,tree.GetEntries()):
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)
               
        
        if ((getattr(tree,jet1name).Pt() == 0. ) or (getattr(tree,jet0name).Eta()*getattr(tree,jet1name).Eta() > 0)):
            continue
        else:
        
           # pT0  = getattr(tree,jet0name).Pt()
           # eta0 = getattr(tree,jet0name).Eta()
           # phi0 = getattr(tree,jet0name).Phi()
           # nrg0 = getattr(tree,jet0name).E()
           # pT1  = getattr(tree,jet1name).Pt()
           # eta1 = getattr(tree,jet1name).Eta()
           # phi1 = getattr(tree,jet1name).Phi()
           # nrg1 = getattr(tree,jet1name).E()
           # pT2  = getattr(tree,jet2name).Pt()
           # eta2 = getattr(tree,jet2name).Eta()
           # phi2 = getattr(tree,jet2name).Phi()
           # nrg2 = getattr(tree,jet2name).E()
           
                      

            #jet0 = ROOT.TLorentzVector()
            #jet0.SetPtEtaPhiE(pT0,eta0,phi0,nrg0)
            #jet1 = ROOT.TLorentzVector()
            #jet1.SetPtEtaPhiE(pT1,eta1,phi1,nrg1)
            #jet2 = ROOT.TLorentzVector()
            #jet2.SetPtEtaPhiE(pT2,eta2,phi2,nrg2)

            jet0 = getattr(tree,jet0name)
            jet1 = getattr(tree,jet1name)
            jet2 = getattr(tree,jet2name)
            
            if (jet0+jet1).M() < 120.:
                continue


            #check if jets are initially sorted by pT
            if not (jet0.Pt() >= jet1.Pt() >= jet2.Pt()):
                tmp2 = False
            #sort by eta
            liste = sorted([jet0,jet1],key = lambda jetx: jetx.Eta(), reverse = True)      
        
            #check if jets are sorted by eta correctly
            if  liste[0].Eta() <= liste[1].Eta():
                tmp = False        

            delPhi = liste[0].DeltaPhi(liste[1])
            delPhihist.Fill(delPhi)
 
    if tmp2:
        print  "anfangs pT ok sortiert"
    else:
        print "anfangs nicht nach pT sortiert!"
    if tmp:
        print "nach eta sortiert"
    else:
        print "nicht nach eta sortiert"
    return delPhihist


print "Running over data..."

dataFile  = ROOT.TFile(dataFileName,"READ")
dataTree  = dataFile.Get("NOMINAL")
dataHisto = readToHisto(dataTree,jet0Name,jet1Name,jet2Name)



# Write the two histograms to an output root file
print "Writing histograms to output file..."

#  overwriting previous files with the same name
if os.path.exists(outFileName):
    shutil.rmtree(outFileName)

os.mkdir(outFileName)
os.chdir(outFileName)
outHistFile = ROOT.TFile.Open("histo_"+outFileName+".root","RECREATE")
outHistFile.cd()
dataHisto.Write()

outHistFile.Close()

print "Done writing histogram to output file"
print "directory %s created"%(outFileName)
print "file %s created with delPhi histogram (sorted by Eta)"%(outFileName+".root")


