import ROOT
import sys
import os
import numpy as np
from scipy.stats import skellam
from scipy.optimize import minimize
from scipy.special import ive
from scipy.special import iv
import matplotlib.pyplot as plt


if len(sys.argv) != 6:
    print "USAGE: <directory name> <dtilde> <pT Cut> <mass cut> <delta eta cut>"
    sys.exit(1)

dirHist = sys.argv[1]
dtilde = sys.argv[2]
#dtilde = "-0.5"
#dtilde = -0.2
print dtilde,type(dtilde)
pTc = sys.argv[3]
massc = sys.argv[4]
deletac = sys.argv[5]
dirDiff = "diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)

HistFileName = dirHist+"/"+dirHist+".root"
diffFileName = dirDiff+"/"+dirDiff+".root"
oo1diffHistName = "oo1Hist_diff" 



def Lpdf(k,mu1,mu2):
    
    return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+np.log(iv(k,2*np.sqrt(mu1*mu2)))
    #return -mu1-mu2+(k/2.)*np.log(mu1/mu2)+2.*np.sqrt(mu1*mu2)+np.log(ive(k,2.*np.sqrt(mu1*mu2)))
    #return np.log(np.exp(-(mu1+mu2))*(mu1/mu2)**(k/2)*np.exp(2*np.sqrt(mu1*mu2))*ive(abs(k),2*np.sqrt(mu1+mu2)))

def NLL(x,hist_data,hist_template1,hist_template2):
    res = 0.
    #print "................................................."
    #print x
    for d, t1, t2 in zip(hist_data,hist_template1,hist_template2):
        f = Lpdf(d,x*t1,x*t2)
        #print d,t1,t2,ive(d,2*x*np.sqrt(t1*t2))
        #print kv(d,2*x*np.sqrt(t1*t2))
        #print f
        if np.isnan(f):
            print "NAN",d,2.*x*np.sqrt(t1*t2),ive(d,2.*x*np.sqrt(t1*t2))
        res += f
    #print res
    return -res




def minNLL(hist_data,template1,template2): #np arrays as arguments 
    
    

    #print hist_data
    #print hist_template
    x0 = np.array([2])

    res = minimize(NLL,x0,args=(hist_data,template1,template2),bounds=[(0.2,10)] )

    print(res)
    err = np.diag(res.hess_inv.todense())

    print("result for x: %s +- %s" % (res.x, err))

print "*************************oo1 Fit**************************"

def plotofdtildevals():
    global HistFileName,diffFileName,ptC,massc,deletac,oo1diffHistName
    #dtildelist = [-1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    dtildelist = ['-1.00', '-0.90', '-0.80', '-0.70', '-0.60', '-0.50', '-0.40', '-0.30', '-0.20', '-0.10', '0.10', '0.20', '0.30', '0.40', '0.50', '0.60', '0.70', '0.80', '0.90', '1.00']

    i=0
    x0 = np.array([2.])
    minvals=[]

    for dtilde in dtildelist:
        print "------------- "+str(dtilde)+"------------------------------------------------------------------------------------------------------------------"
        dirDiff1 = "diff_pos_"+dirHist+"_dtilde"+str(dtilde)+"_pTcut"+str(pTc)+"_massCut"+str(massc)+"_delEtaCut"+str(deletac)

        oo1HistName1= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
        diffFileName1 = dirDiff1+"/"+dirDiff1+".root"
        HistFile1 = ROOT.TFile.Open(HistFileName,"READ")
        diffFile1 = ROOT.TFile.Open(diffFileName, "READ")
        oo1Hist1 = HistFile1.Get(oo1HistName1)
        oo1diffHist1 = diffFile1.Get(oo1diffHistName)
        oo1Hist1.Scale( 200/oo1Hist1.Integral())
        print "INTEGRALS: ",oo1Hist1.Integral(),oo1diffHist1.Integral()
        for name,hist in zip([oo1diffHistName,oo1HistName1],[oo1diffHist1,oo1Hist1]):
            if not hist:
                print "hist %s  not found" % (name)
                sys.exit(1)
        oo1list1 = [oo1Hist1.GetBinContent(bin+1) for bin in range(oo1Hist1.GetSize()-2)]
        oo1difflist1 = [oo1diffHist1.GetBinContent(bin+1) for bin in range(oo1diffHist1.GetSize()-2)]
    #if oo1diffHist.GetBinContent(1)>=0:
    #    oo1difflist = [oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
    #else:
    #    oo1difflist = [-oo1diffHist.GetBinContent(bin+1) for bin in range(oo1diffHist.GetSize()-2)]
        mirroo1list1 = [oo1list1[len(oo1list1)-i-1] for i in range(len(oo1list1))]
        print oo1list1
        print mirroo1list1
        print oo1difflist1
        data1 = np.array([int(round(oo1difflist1[i])) for i in range(len(oo1difflist1)/2,len(oo1difflist1))],dtype = np.int)
#template = data
        template11 = np.array([oo1list1[i] for i in range(len(oo1list1)/2,len(oo1list1))],dtype = np.double)
        template21 = np.array([mirroo1list1[i] for i in range(len(mirroo1list1)/2,len(mirroo1list1))],dtype = np.double)
            #print template1
            #print template2
            #print data
            #print data-(template1-template2)
        res1 = minimize(NLL,x0,args=(data1,template11,template21),bounds=[(0.01,10.)] )
        minvals.append(res1.x[0])
    #minNLL(data,template1,template2)
        #print dtildelist
        print data1
        print template11
        print template21
        
        #print minvals
        #print dtilde,res1.x
        i+=1
    print i
    dtildevals = [float(x) for x in dtildelist]
    plt.plot(dtildevals,minvals,'o')
    plt.show()
plotofdtildevals()
#print "************************delta Phi Fit**************************"
#minNLL(delPhiHist)
    
#def NLL(x,hist_data,hist_template):
#    res = 0
#    for d, t in zip(hist_data,hist_template):
#        f = scipy.stats.poisson.logpmf(k=d, mu=x*t)
#        res += f
#    return -res

