#! /bin/env python2
#This script adds OO and sgndDelPhi Data with given Parameters to a .root file.
#all dtilde values are included in dtildelist:
dtildelist = ['-1.00', '-0.99', '-0.98', '-0.97', '-0.96', '-0.95', '-0.94', '-0.93', '-0.92', '-0.91', '-0.90', '-0.89', '-0.88', '-0.87', '-0.86', '-0.85', '-0.84', '-0.83', '-0.82', '-0.81', '-0.80', '-0.79', '-0.78', '-0.77', '-0.76', '-0.75', '-0.74', '-0.73', '-0.72', '-0.71', '-0.70', '-0.69', '-0.68', '-0.67', '-0.66', '-0.65', '-0.64', '-0.63', '-0.62', '-0.61', '-0.60', '-0.59', '-0.58', '-0.57', '-0.56', '-0.55', '-0.54', '-0.53', '-0.52', '-0.51', '-0.50', '-0.49', '-0.48', '-0.47', '-0.46', '-0.45', '-0.44', '-0.43', '-0.42', '-0.41', '-0.40', '-0.39', '-0.38', '-0.37', '-0.36', '-0.35', '-0.34', '-0.33', '-0.32', '-0.31', '-0.30', '-0.29', '-0.28', '-0.27', '-0.26', '-0.25', '-0.24', '-0.23', '-0.22', '-0.21', '-0.20', '-0.19', '-0.18', '-0.17', '-0.16', '-0.15', '-0.14', '-0.13', '-0.12', '-0.11', '-0.10', '-0.09', '-0.08', '-0.07', '-0.06', '-0.05', '-0.04', '-0.03', '-0.02', '-0.01', '0.00', '0.01', '0.02', '0.03', '0.04', '0.05', '0.06', '0.07', '0.08', '0.09', '0.10', '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17', '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24', '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31', '0.32', '0.33', '0.34', '0.35', '0.36', '0.37', '0.38', '0.39', '0.40', '0.41', '0.42', '0.43', '0.44', '0.45', '0.46', '0.47', '0.48', '0.49', '0.50', '0.51', '0.52', '0.53', '0.54', '0.55', '0.56', '0.57', '0.58', '0.59', '0.60', '0.61', '0.62', '0.63', '0.64', '0.65', '0.66', '0.67', '0.68', '0.69', '0.70', '0.71', '0.72', '0.73', '0.74', '0.75', '0.76', '0.77', '0.78', '0.79', '0.80', '0.81', '0.82', '0.83', '0.84', '0.85', '0.86', '0.87', '0.88', '0.89', '0.90', '0.91', '0.92', '0.93', '0.94', '0.95', '0.96', '0.97', '0.98', '0.99', '1.00']


#############################################################################
silent=True
useEventStore = True
useNewEvent = False
iterations = 1
import sys
import os
import shutil
import HLeptonsCPRW
import array
import ROOT
import glob
ngtot = 0
ng0 = 0
ng1 = 0
ng2 = 0
nevents = 0
nerr = 0
n3jet = 0
n2jet = 0
nevtot = 0
ng0In = 0
ng1In =  0
ng2In = 0
nq0Out = 0
nq1Out =  0
nq2Out = 0
nq3Out = 0
eventNumber = 0

if len(sys.argv) != 5:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s  <hist file name (for output)>  <pT cut value> <mass cut value> <delta Eta cut value> . dtildelist included in code."%(sys.argv[0])
    # End the program
    sys.exit(1)

# make list of all data files
dirname  = "/home/ss944/projekt/daten/"
a = "mc16a/"
e = "mc16e/"
d = "mc16d/"
dir1 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346191.PoPy8_NNPDF30_VBFH125_tautaulm15hp20.D2.e7259_s3126_r9364_p3759.smPre_w_2_HS/"
dir2 = "group.phys-higgs.Htt_lh_V02.mc16_13TeV.346192.PoPy8_NNPDF30_VBFH125_tautaulp15hm20.D2.e7259_s3126_r10724_p3759.smPre_w_0_HS/"
dirlist = []
filelist = []
#for i in [a,e,d]:
for i in [a]:
    os.chdir(dirname+i)
    for j in glob.glob("*VBF*"):
        dirlist.append(dirname+i+j)
        #filelist.append(glob.glob("*.root"))
#print dirlist
for d in dirlist:
    os.chdir(d)
    for f in glob.glob("*.root")[0:2]:
        filelist.append(d+"/"+f)

#print filelist
os.chdir("/home/ss944/projekt/rest")


#outFileName = "allHistos"


HistFileName = sys.argv[1]+".root"

pTCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]
#outFileName = "part_coll_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut    


ooES = HLeptonsCPRW.OptObsEventStore();
    
if (useEventStore):
    if (silent == False):
        print("Using EventStore")
    ooES.initPDFSet("CT10", 0, 91.2)

def readToHisto(tree,namelist,dtildelist,ptCut,massCut,delEtaCut):
    global ooES,ngtot,ng0,ng1,ng2,nevents,silent,useEventStore,useNewEvent,iterations,nerr,n3jet,n2jet,eventNumber,nevtot,ng0In,ng1In,ng2In,nq0Out,nq1Out,nq2Out,nq3Out
    #m_dtilde = float(dtilde)
    ptcut = float(ptCut)
    masscut = float(massCut)
    deletacut = float(delEtaCut)
    histlist = []
    namelistNEW = []
    for dtilde,names in zip(dtildelist,namelist):
        
        oo1HistName = names[0]
        delPhiHistName = names[1]
    
        oo1Hist = ROOT.TH1D(oo1HistName+"New","oo1",14,-15.,15.)
        delPhiHist = ROOT.TH1D(delPhiHistName+"New","signed delta Phi",14,-4.,4.)
        histlist.append([delPhiHist,oo1Hist])
        namelistNEW.append([delPhiHistName+"New",oo1HistName+"New"])

    for entryNum in range (0,tree.GetEntries()):
        tree.GetEntry(entryNum)
        #jet0_p4 = tree.jet_0_wztruth_p4
        #jet1_p4 = tree.jet_1_wztruth_p4
        #jet2_p4 = tree.jet_2_wztruth_p4
        jet0_p40 = tree.truth_reweight_info_parton_2_p4
        jet1_p40 = tree.truth_reweight_info_parton_3_p4
        jet2_p40 = tree.truth_reweight_info_parton_4_p4
        flavour1In = tree.truth_reweight_info_parton_0_pdgId;               
        flavour2In = tree.truth_reweight_info_parton_1_pdgId;               
        flavour0Out0 = tree.truth_reweight_info_parton_2_pdgId;              
        flavour1Out0 = tree.truth_reweight_info_parton_3_pdgId;              
        flavour2Out0 = tree.truth_reweight_info_parton_4_pdgId;

        
        
        jetlist0 = sorted([[jet0_p40,flavour0Out0],[jet1_p40,flavour1Out0],[jet2_p40,flavour2Out0]],key = lambda x: x[0].Pt(), reverse = True)
        
        jet0_p4 = jetlist0[0][0]
        jet1_p4 = jetlist0[1][0]
        jet2_p4 = jetlist0[2][0]
        
        flavour0Out = jetlist0[0][1]
        flavour1Out = jetlist0[1][1]
        flavour2Out = jetlist0[2][1]
        higgs_p4 = tree.boson_0_truth_p4
        if jet0_p4.Pt()>=jet1_p4.Pt()>=jet2_p4.Pt():
            pass
        else:
            print "pT sorting not ok"
        if jet1_p4.Pt() < 10.**(-5):
            continue
        
         
        pjet0 = array.array('d', [jet0_p4.Energy(), jet0_p4.Px(),jet0_p4.Py() ,jet0_p4.Pz() ])               # E,px,py,pz of nth final state parton
        pjet1 = array.array('d', [jet1_p4.Energy(), jet1_p4.Px(),jet1_p4.Py() ,jet1_p4.Pz() ])
        pjet2 = array.array('d', [jet2_p4.Energy(), jet2_p4.Px(),jet2_p4.Py() ,jet2_p4.Pz() ])
        phiggs = array.array('d',[higgs_p4.Energy(), higgs_p4.Px(),higgs_p4.Py() ,higgs_p4.Pz() ])            # E,px,py,pz of Higgs boson make sure that four-momentum conservation holds 
        ecm = 13000.;                           #proton-proton center-of mass energy in GeV
        mH = 124.999901;                       #mass of Higgs boson in Gev
        
        if jet2_p4.Pt() > 10.**(-5):
            npafin = 3;  #number of partons in final state  either  2 or 3
            n3jet += 1;
        else:
            npafin = 2;
            n2jet += 1;
        nevtot += 1
        #print "npafin:",npafin
        #print pjet0[1],pjet1[1],pjet2[1]
        delEtaAbs = abs(jet0_p4.Eta()-jet1_p4.Eta())
        mjj = (jet0_p4 + jet1_p4).M()
        
        if (jet0_p4.Pt() > ptcut and jet1_p4.Pt() > ptcut and  delEtaAbs > deletacut and mjj > masscut and abs(jet0_p4.Eta()) < 4.5 and abs(jet1_p4.Eta()) < 4.5 ):
        #Hjj_p4 = jet0_p4 + jet1_p4 + jet2_p4 + higgs_p4
        #mHjj = Hjj_p4.M()
        #yHjj = Hjj_p4.Rapidity()
        #print m_dtilde,ptcut,deletacut,masscut
        
            nevents += 1
            #print entryNum
            x1 = tree.truth_event_info_Bjorken_x1;                  #Bjorken x of incoming partons, 1 in + z , 2 in -z direction
            x2 = tree.truth_event_info_Bjorken_x2;
            Q = 125;
#flavour assignment: t = 6  b = 5 c = 4, s = 3, u = 2, d = 1 
 #anti-qarks with negative sign
 #gluon = 0 


            #flavour0Out = tree.jet_0_flavorlabel_part;             
            #flavour1Out = tree.jet_1_flavorlabel_part;          
            #flavour2Out = tree.jet_2_flavorlabel_part;
            ng = 0 #number of gluons Out
            nq=0
            flavourlist = [flavour1In,flavour2In,flavour0Out,flavour1Out,flavour2Out]
            jetlist = [pjet0,pjet1,pjet2]
            jet_p4list = [jet0_p4,jet1_p4,jet2_p4]
            #print flavourlist
            for i,flav in enumerate(flavourlist):
                if flav == 21:
                    
                    flavourlist[i] = 0  #pdg of gluon is 21, but input has to be 0
                elif flav == 5:
                    flavourlist[i] = 3
                elif flav == -5:
                    flavourlist[i] = -3
                    #ngtot +=1
            flavourOutlist = flavourlist[2:5]
            for i ,flav in enumerate(flavourOutlist):
                if flav == 0: 
                    ng += 1
                    ngtot +=1
                    indexg = i
                else:
                    nq += 1
            if ng ==2:
                ng2 += 1
            elif ng == 1:
                #if indexg != len(flavourOutlist)-1:
                #    flavourOutlist.append(flavourOutlist[indexg])
                #    del flavourOutlist[indexg]
                #    jetlist.append(jetlist[indexg])
                #    del jetlist[indexg]
                    #jet_p4list.append(jet_p4list[indexg])
                    #del jet_p4list[indexg]
                ng1 += 1
            elif ng == 0:
                ng0 += 1 
            
            if nq == 0:
                nq0Out+=1
            elif nq==1:
                nq1Out += 1
            elif nq==2:
                nq2Out += 1
            elif nq == 3:
                nq3Out +=1
            ngIn = 0
            for flav in flavourlist[0:2]:
                if flav == 0:
                    ngIn +=1
            if ngIn == 2:
                ng2In += 1
            elif ngIn == 1:
                ng1In +=1
            else:
                ng0In+=1

        

           

            #for i in range(0,iterations):
            #    if not silent:
            #        print("Running iteration {}".format(i+1))
            #    if useEventStore:
            #        if (useNewEvent):
            #            eventNumber+=i


        

            oo1 = ooES.getOptObs(0, eventNumber, ecm, mH ,x1,x2,Q,pjet0,pjet1,phiggs);
        
            #print type(m_dtilde)
            #rw =  ooES.getReweight(ecm, mH, 1 , \
          #0, 0, 0, 0, 0, # rsmin,din,dbin,dtin,dtbin \
          #0, 0, 0,           # a1hwwin,a2hwwin,a3hwwin \
          #0, 0, 0,           # a1haain,a2haain,a3haain \
          #0, 0, 0,           # a1hazin,a2hazin,a3hazin \
          #0, 0, 0,           # a1hzzin,a2hzzin,a3hzzin \
          #0,                     # lambdahvvin for formfactor if set to positive value \
          #1,0,0, m_dtilde, m_dtilde, -1, \
          #0,0,0,        \
          #0,0,0,        \
          #0,0,0,        \
          #0,0,0,        \
 
          #npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2], \
          #x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs)
            
            #oo1Hist.Fill(oo1,rw)
            #if rw ==0.:
            #    nerr +=1

            w1 = ooES.getWeightsDtilde(0, eventNumber, ecm, mH , npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2],x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs);
            w2 = ooES.getWeightsDtilde(1, eventNumber, ecm, mH , npafin,flavourlist[0],flavourlist[1],flavourOutlist[0],flavourOutlist[1],flavourOutlist[2],x1,x2,jetlist[0],jetlist[1],jetlist[2],phiggs);
            
            
            if (w1==0. and w2 == 0.):
                nerr += 1
            
            
                
            #print flavourlist
            #print flavourOutlist
            
            
       
           

            liste = sorted([jet_p4list[0],jet_p4list[1]], key = lambda jetx: jetx.Eta(), reverse = True)
            #liste = sorted([jetlist0[0][0],jetlist0[1][0]],key = lambda jetx: jetx.Eta(),reverse = True)
            #liste = jet_p4list
            delPhi_Etasorted = liste[0].DeltaPhi(liste[1])
            #if liste[0].Eta()*liste[1].Eta() < 0.:
            #    delPhiHist.Fill(delPhi_Etasorted,rw)
                
            for dtilde,histos in zip(dtildelist,histlist):
                rw = 1+w1*float(dtilde)+w2*float(dtilde)**2
                if liste[0].Eta()*liste[1].Eta() < 0.:
                    histos[0].Fill(delPhi_Etasorted,rw)
                histos[1].Fill(oo1,rw)
                
        eventNumber += 1;        
    print "nerr", nerr
    print "nevents", nevents    
    #print "resulting histos:", [delPhiHist,oo1Hist]        
    return histlist,namelistNEW

def addDataToFile(dataFileList,histfilename,dtildelist,ptc,massc,deletac):
    if not os.path.exists(histfilename[0:-5]):
        os.mkdir(histfilename[0:-5])
    os.chdir(histfilename[0:-5])
    print "FILENAME: ",histfilename
    HistFile = ROOT.TFile.Open(histfilename,"WRITE")

    ##################### create empty histogrmas and namelist:
    #list structure: [ [delPhi_dtilde1,oo1_dtilde1], ... ,[delPhi_dtildeN, oo1_dtildeN] ]
    namelist = []
    histlist = []
    for dtilde in dtildelist:
        oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(ptc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
        delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(ptc)+"_mjj"+str(massc)+"_delEta"+str(deletac)
        namelist.append([delPhiHistName,oo1HistName])
        oo1Hist = ROOT.TH1D(oo1HistName,"oo1",14,-15.,15.)
        delPhiHist = ROOT.TH1D(delPhiHistName,"signed delta Phi",14,-4.,4.)
        histlist.append([delPhiHist,oo1Hist])
        
    
    filenr = 0
    
    #print dataFileList
    
    ###############loop over data files
    for dataFileName in dataFileList:
        print "opening file ", filenr+1, " of ", len(dataFileList)
        #print "histlist at this point" , histlist

        dataFile = ROOT.TFile.Open(dataFileName, "READ")
        tree = dataFile.Get("NOMINAL")
        histlist0 = []
       
        if not tree:
            print "no tree found!"
            sys.exit(1)
        #check if histogram already exists:   
        if filenr == 0:
            
            for dtilde,names in zip(dtildelist,namelist): 
                
                delPhiHist0 = HistFile.Get(names[0])
                oo1Hist0 = HistFile.Get(names[1])
                histoexists = False
                #if delPhiHist0:
                #    print "delPhiHist %s already exists in output file. check input!"%(names[0])
                #    histoexists = True
                #if oo1Hist0:
                #    print "oo1Hist %s already exists in output file. check input!"%(names[1])
                 #   histoexists = True
            if histoexists:
                print [delPhiHist0,oo1Hist0]
                print [delPhiHist0.GetSize(),oo1Hist0.GetSize()]
                #sys.exit(1)
                
            newhistlist,newnamelist = readToHisto(tree,namelist,dtildelist,ptc,massc,deletac)
            for oldhists,newhists in zip(histlist,newhistlist):
                oldhists[0].Add(newhists[0])
                oldhists[1].Add(newhists[1])

            
            
        else:
            for hists in histlist:
                if not hists:
                    print "no histos found in histlist!"
                    sys.exit(1)
            newhistlist,newnamelist = readToHisto(tree,namelist,dtildelist,ptc,massc,deletac)
            for oldhists,newhists in zip(histlist,newhistlist):
                oldhists[0].Add(newhists[0])
                oldhists[1].Add(newhists[1])
        
        
        
        filenr += 1
        print "total number of entries in delPhi Hist: ",histlist[0][0].GetEntries()
        dataFile.Close()
#
    
        
    
    
    HistFile.cd()
    
    for hists in histlist:
        hists[0].Write()
        hists[1].Write()
    HistFile.Close()
    print "ptcut",ptc,"masscut",massc,"deletacut",deletac," added to file "+histfilename+" for all dtilde from dtildelist."



addDataToFile(filelist,HistFileName,dtildelist,pTCut,massCut,delEtaCut)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

#doAll(tree,dtilde,pTCut,massCut,delEtaCut,outFileName)

#print "ngtot, ng0, ng1, ng2,nevents", ngtot,ng0,ng1,ng2,nevents
print "-----------------------------------------------------------"
print "nerr", nerr
print "nevents_cut", nevents
print "nevents_tot",nevtot

print "-----------------------------------------------------------"
print "percentage of 0 gluon_out events: ",float(ng0)/float(nevents)
print "percentage of 1 gluon_out events: ",float(ng1)/float(nevents)
print "percentage of 2 gluon_out events: ",float(ng2)/float(nevents)


print "-----------------------------------------------------------"
print "percentage of 0 gluon_In events: ",float(ng0In)/float(nevents)
print "percentage of 1 gluon_In events: ",float(ng1In)/float(nevents)
print "percentage of 2 gluon_In events: ",float(ng2In)/float(nevents)

print "-----------------------------------------------------------"
print "percentage of 2 jet events: ",float(n2jet)/float(nevtot)
print "percentage of 3 jet events: ",float(n3jet)/float(nevtot)

print "-----------------------------------------------------------"
print "percentage of 0 quark_out : ",float(nq0Out)/float(nevents)
print "percentage of 1 quark_out : ",float(nq1Out)/float(nevents)
print "percentage of 2 quark_out : ",float(nq2Out)/float(nevents)
print "percentage of 3 quark_out : ",float(nq3Out)/float(nevents)
