import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import ROOT
from scipy.optimize import curve_fit

plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral' 
plt.rcParams.update({'lines.markeredgewidth': 1})
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']



if len(sys.argv) != 2:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print( "USAGE: %s  <directory of input root file>  dtildelist included in code."%(sys.argv[0]))
    # End the program
    sys.exit(1)

dirname = sys.argv[1]
filename = dirname+".root"
os.chdir(dirname)
histfile = ROOT.TFile.Open(filename,"READ")

oo1namelist,dpnamelist = [],[]
for d in dtildelist:
    oo1namelist.append("oo1_dtilde"+d)
    dpnamelist.append("dp_dtilde"+d)


ooIntList,dPIntList = [],[]
ooerrlist,dperrlist = [],[]
for n in oo1namelist:
    h = histfile.Get(n)
    ooIntList.append(h.Integral())
    ooerrlist.append(sum([np.sqrt(h.GetBinContent(bin+1)) for bin in range(h.GetSize()-2)]))
for n in dpnamelist:
    h = histfile.Get(n)
    dPIntList.append(h.Integral())
    dperrlist.append(np.sqrt(sum([h.GetBinError(bin+1)**2 for bin in range(h.GetSize()-2)])))
#get SM Norm
normfacoo = 200./(ooIntList[dtildelist.index('0.00')])
normfacdp = 200./(dPIntList[dtildelist.index('0.00')])

ooIntarr = np.array(ooIntList)
dPIntarr = np.array(dPIntList)
ooerrarr = np.array(ooerrlist)
dperrarr = np.array(dperrlist)

ooIntarr *= normfacoo
dPIntarr *= normfacdp
ooerrarr *= normfacoo
dperrarr *= normfacdp

print(ooIntarr[0],dPIntarr[0])


######fit c_oo,c_dp ###############
xx = np.linspace(-1.,1.,101)
def func(x,c):
    return 200. + c*x**2

poptoo1,pcovoo1 = curve_fit(func,xx,ooIntarr)
poptdp,pcovdp = curve_fit(func,xx,dPIntarr)
print(poptoo1,pcovoo1)
print(poptdp,pcovdp)
c_oo = poptoo1[0]
err_oo = np.sqrt(pcovoo1[0])
c_dp = poptdp[0]
err_dp = np.sqrt(pcovdp[0])

print("result:")
print("c_oo = ",c_oo," +- ", err_oo)
print("c_dp = ",c_dp," +- ", err_dp)


xxfit = np.linspace(-1.,1.,10000)
#plt.plot(xx,ooIntarr,label="OO")                                                                                                                                                                                                             
#plt.plot(xx,dPIntarr,label="delPhi")                                                                                                                                                                                                         

plt.plot(xxfit,200.+c_oo*xxfit**2,"Black",label=r"$200+c_{\mathcal{O}\mathcal{O}}\tilde{d}^2$",  linestyle="-",linewidth=0.5)
plt.plot(xxfit,200.+c_dp*xxfit**2,"Blue",label=r"$200+c_{\Delta \Phi}\tilde{d}^2$",  linestyle="-",linewidth=0.5)
plt.errorbar(xx,ooIntarr,xerr=None,yerr=ooerrarr,label="$ \mathcal{O}\mathcal{O}$",marker = ",",color="r",ms = 4.,linestyle="None",capsize=1,linewidth=0.5)
plt.errorbar(xx,dPIntarr,xerr=None,yerr=dperrarr,label=r"$ \Delta \Phi_{jj}^{\mathrm{sgd}}$",marker = ",",color="g",ms = 4.,linestyle="None",capsize=1,linewidth=0.5)
plt.legend(loc= "best")
#plt.title(r"Total Event Number of Histograms Normalized to $N_{\mathrm{SM}} = 200$ ")                                                                                                                                                        
plt.xlabel(r"$\tilde{d}$")
plt.ylabel(r"$N(\tilde{d})$")
plt.grid()
plt.show()
