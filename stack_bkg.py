import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import csv
#fig, ax = plt.subplots(1, 1)




import ROOT
import sys
import os

outdirname = "stack_bkg"

BGen = ROOT.TRandom3()
Background_Ratio = 1.0
templateHistFileName = "evColl_test/evColl_test.root"
sampleHistFileName = "evColl_test/AllSamples/evColl_test_0.00.root"
NBins = 12
tmplFile = ROOT.TFile.Open(templateHistFileName,"READ")
tmpl = tmplFile.Get("oo1_dtilde0.00")
tmpl.Scale(200./tmpl.Integral())
smplFile = ROOT.TFile.Open(sampleHistFileName,"READ")
smpl = smplFile.Get("oo1_dtilde0.00_35")

def backGen(b_ratio,nbins,dtilde,c,gen): #returns sum,diff arrays of background
    N = 200.
    #nbins = len(histarr)
    #nbins = hist.GetSize()-2
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = np.array([gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)])
    
    
    #print(b_list)
    
#print("backg_sum: ",b_arr_pos+b_mirr_pos)
    #print("backg_diff: ",b_arr_pos-b_mirr_pos)
    return b_list

bkg_array = backGen(Background_Ratio,NBins,0.00,None,BGen)
bkg_hist = ROOT.TH1D("bkg", " ;  OO  ; Events", 12,-15.,15.)

for i,b in enumerate(bkg_array):
    bkg_hist.SetBinContent(i+1,b)
    bkg_hist.SetBinError(i+1,np.sqrt(b))
bkg_hist.Sumw2()

## plot pseudo experiment and prediction:
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")
os.chdir(outdirname)


c1 = ROOT.TCanvas("c1")
leg1 = ROOT.TLegend(0.1,0.7,0.4,0.9)
leg1.SetTextSize(0.035)
c1.cd()
c1.SetGrid()
tmpl.SetStats(0)
smpl.SetStats(0)
tmpl.SetLineColor(1)
smpl.SetLineColor(6)
smpl.SetTitle(" ; OO ; Events")
smpl.Draw("HistE")
tmpl.Draw("HistSameE")

leg1.AddEntry(tmpl,"SM prediction") 
leg1.AddEntry(smpl,"pseudo experiment")
leg1.Draw()


c1.SaveAs("pred_pseu.pdf")

## stack plot pseudo experiment and bkg

hs = ROOT.THStack("hs"," ; OO ; Events")
bkg_hist.SetFillColor(17)
bkg_hist.SetLineColor(17)
smpl.SetFillColor(6)
hs.Add(bkg_hist)
hs.Add(smpl)
c2 = ROOT.TCanvas("c2")
c2.SetGrid()
leg2 = ROOT.TLegend(0.1,0.7,0.4,0.9)
leg2.SetTextSize(0.035)
leg2.AddEntry(smpl, "pseudo experiment")
leg2.AddEntry(bkg_hist,"background")
c2.cd()
hs.Draw("Hist")
leg2.Draw()

c2.SaveAs("bkg_stack.pdf")

