import ROOT
from scipy.interpolate import interp1d
import os
#from ROOT.TMath import LocMin
#from ROOT.TMath import LocMax

os.chdir("testres")
filename = "MeanResults/MeanRes_1sig_b0.0.root"
filename2 = "MeanResults/MeanRes_2sig_b0.0.root"
#get Eichkurve
goo1 = ROOT.TFile.Open(filename,"READ").Get("oo_sig1;5")
goo2 = ROOT.TFile.Open(filename2,"READ").Get("oo_sig2;5")
os.chdir("/home/ss944/BscPlots")
#dd,m,errl,errh = [],[],[],[]
#eGraph.SetTitle(" ; #tilde{d}  ; < #Delta #Phi_{jj}^{sgd} >" )
goo2.SetTitle(" ; #tilde{d}  ; < OO >" )
goo2.SetLineColor(18)
goo2.SetFillColor(18)
goo2.SetMarkerColor(1)
goo2.SetMarkerStyle(1)
#indexlim_l = ROOT.TMath.LocMin(goo2.GetY())
#indexlim_h = ROOT.TMath.LocMax(goo2.GetY())
#goo2.GetXaxis().SetRangeUser(-0.25,0.25)
goo1.SetTitle(" ; #tilde{d}  ; < OO >" )
goo1.SetLineColor(29)
goo1.SetFillColor(29)
goo1.SetMarkerColor(1)
goo1.SetMarkerStyle(1)
coo=ROOT.TCanvas("coo")
coo.SetGrid()
coo.cd()

#goo1.Draw("LXsame")
loo = ROOT.TLegend(0.1,0.7,0.4,0.9)
leg1 = ROOT.TH1D("l1"," ",10,-1.,1.)
leg2 = ROOT.TH1D("l2"," ",10,1.,1.)
leg1.SetFillColor(29)
leg2.SetFillColor(18)
for bin in range(leg1.GetSize()):
    leg1.SetBinContent(bin,0.)
    leg2.SetBinContent(bin,0.)
leg1.SetLineColor(0)
leg2.SetLineColor(0)
leg1.GetYaxis().SetRangeUser(-2.1,2.1)
leg1.SetStats(0)
leg2.SetStats(0)
loo.SetTextSize(0.03)
goo2.Draw("ALP")
leg1.Draw("same ")
leg2.Draw("same")
goo2.Draw("LPsame")
goo1.Draw("Psame")
goo1.Draw("PXsame")

loo.AddEntry("l1","68.3% Konfidenzband","f")
loo.AddEntry("l2","95.4% Konfidenzband","f")
loo.Draw()
coo.SaveAs("eichk_erg_oo.pdf")


os.chdir("/home/ss944/projekt/rest/testres")
gdp1 = ROOT.TFile.Open(filename,"READ").Get("oo_sig1;6")
gdp2 = ROOT.TFile.Open(filename2,"READ").Get("oo_sig2;6")
os.chdir("/home/ss944/BscPlots")
#dd,m,errl,errh = [],[],[],[]
#eGraph.SetTitle(" ; #tilde{d}  ; < #Delta #Phi_{jj}^{sgd} >" )
gdp2.SetTitle(" ; #tilde{d}  ; < #Delta#Phi_{jj}^{sgd} >" )
gdp2.SetLineColor(18)
gdp2.SetMarkerColor(1)
gdp2.SetMarkerStyle(1)
#indexlim_l = ROOT.TMath.LocMin(goo2.GetY())
#indexlim_h = ROOT.TMath.LocMax(goo2.GetY())
#goo2.GetXaxis().SetRangeUser(-0.25,0.25)
gdp1.SetTitle(" ; #tilde{d}  ; < #Delta#Phi_{jj}^{sgd} >" )
gdp1.SetLineColor(29)
gdp1.SetMarkerColor(1)
gdp1.SetMarkerStyle(1)
cdp=ROOT.TCanvas("cdp")
cdp.SetGrid()

ldp = ROOT.TLegend(0.1,0.7,0.4,0.9)
leg1 = ROOT.TH1D("l1"," ",10,-1.,1.)
leg2 = ROOT.TH1D("l2"," ",10,1.,1.)
leg1.SetFillColor(29)
leg2.SetFillColor(18)
for bin in range(leg1.GetSize()):
    leg1.SetBinContent(bin,0.)
    leg2.SetBinContent(bin,0.)
leg1.SetLineColor(0)
leg2.SetLineColor(0)
leg1.GetYaxis().SetRangeUser(-2.1,2.1)
leg1.SetStats(0)
leg2.SetStats(0)
loo.SetTextSize(0.03)
cdp.cd()
gdp2.Draw("ALP")
leg1.Draw("same ")
leg2.Draw("same")
gdp2.Draw("LPsame")
gdp1.Draw("Psame")
gdp1.Draw("PXsame")

ldp.AddEntry("l1","68.3% Konfidenzband","f")
ldp.AddEntry("l2","95.4% Konfidenzband","f")
ldp.Draw()

#gdp1.Draw("LXsame")

cdp.SaveAs("eichk_erg_dp.pdf")
