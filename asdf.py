from scipy.special import ive
from scipy.special import iv
from scipy.special import gamma
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.stats import skellam
from scipy.stats import norm
from scipy import integrate
import scipy

matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'


#from ROOT import TGraph
#print scipy.__version__
#from ROOT import TRandom3
#rand = TRandom3()
x = np.arange(0.,16.)
#print(x)
w = np.array([i for i in x])

def f(b_):
    return np.sum(x*(w+b_))/np.sum(w+b_)

#b = np.linspace(0.,100.,1000)
#fb = np.array([f(c) for c in b])
#plt.plot(b,fb)
#plt.show()

def skPDF(x,mu1,mu2):
    return np.exp(-(mu1+mu2))*(mu1/mu2)**(x/2)*np.exp(2*np.sqrt(mu1*mu2))*ive(abs(x),2*np.sqrt(mu1*mu2))
    #return np.exp(-(mu1+mu2))*(mu1/mu2)**(x/2)*iv(abs(x),2*np.sqrt(mu1*mu2))
def poisPDF(k,mu1,mu2):
    return (mu1-mu2)**k*np.exp(-(mu1-mu2))/abs(gamma(k+1))

def gausPDF(x,mu,var):
    return (1/np.sqrt(var*2*np.pi))*np.exp(-0.5*((x-mu)**2/var))
##### Plot Skelpmfpdf
xx = np.linspace(-20.,0.)
x = np.arange(-20,0)
plt.plot(x, skellam.pmf(x, 5, 15), 'bo', ms=8, label='diskrete WDF')
plt.plot(xx,skPDF(xx,5,15),"-",color="black",label="kontinuierliche WDF")
plt.legend(loc="best")
plt.grid(linestyle="dotted")
plt.xlabel("$\Delta k$")
plt.ylabel("$f_{\mathrm{Skellam}}(\Delta k ; \lambda_{1}=5,\lambda_{2}=15)$")
plt.xlim(-20.,0.)
plt.show()
######Plot SkelExamp
dd = np.linspace(-11.,12.,1000)
plt.plot(dd,skPDF(dd,5.,4.),"-",label="$\lambda_1 = 5, \lambda_2 = 4$")
plt.plot(dd,skPDF(dd,10.,9.),"--",label = "$\lambda_1 = 10, \lambda_2 = 9$")
plt.plot(dd,skPDF(dd,15.,12.),"-.",label = "$\lambda_1 = 15, \lambda_2 = 12$")
plt.legend(loc="best")
plt.grid(linestyle="dotted")
plt.xlabel("$\Delta k$")
plt.ylabel("$f_{\mathrm{Skellam}}(\Delta k; \lambda_1,\lambda_2)$")
plt.xlim(-11.,12.)
plt.show()



############Plot SkelGaus 1 ,2 
for i,j in zip([2.],[1.]):
    dd = np.linspace(-9.,11.,1000)
#mean,var,skew,kurt = norm.stats(moments='mvsk')
    plt.plot(dd,skPDF(dd,i,j),"-",color = "Blue",label = "$f_{\mathrm{Skellam}}(x; \lambda_1 = %i,\lambda_2= %i)$"%(int(i),int(j)))
    plt.plot(dd,norm.pdf(dd,i-j,np.sqrt(i+j)),"--",color="Black",label = "$f_G(x; \lambda_1-\lambda_2, \mu_1+\lambda_2)$")
    plt.legend(loc="best")
    plt.grid(linestyle="dotted")
    plt.xlabel("$\Delta k$")
    plt.ylabel("$f(\Delta k; \mathbf{\Theta})$")
    plt.xlim(-9.,11.)
    plt.show()
for i,j in zip([32],[31.]):
    dd = np.linspace(-39.,41.,1000)
#mean,var,skew,kurt = norm.stats(moments='mvsk')
    plt.plot(dd,skPDF(dd,i,j),"-",color = "Blue",label = "$f_{\mathrm{Skellam}}(\Delta k; \lambda_1 = %i,\lambda_2= %i)$"%(int(i),int(j)))
    plt.plot(dd,norm.pdf(dd,i-j,np.sqrt(i+j)),"--",color="Black",label = "$f_G(\Delta k; \lambda_1-\lambda_2, \lambda_1+\lambda_2)$")
    plt.legend(loc="best")
    plt.xlabel("$\Delta k$")
    plt.ylabel("$f(\Delta k; \mathbf{\Theta})$")
    plt.grid(linestyle="dotted")
    plt.xlim(-39.,41.)
    plt.show()


#    plt.show()
#plt.plot(skPDF(dd,30.,4.))
#plt.show()

#dd = np.linspace(0.,21.10000)
#plt.plot(dd,np.log(ive(108.9674373,dd)))
#plt.show()
fig, ax = plt.subplots(1, 1)
#Fit Result in Bin 5 (Counting from 0) :  [ 0.85419867]  +-  [ 0.37709084]
#Fit Result in Bin 0 :  [ 0.95356355]  +-  [ 0.09502745]

#SM Norm, dtilde 0.10, Fit Result in Bin 0 :  [ 0.9034634]  +-  [ 0.17058191]
#SM Norm, dtilde 0.80, Fit Result in Bin 2 :  [ 0.98625586]  +-  [ 0.02976329]

dtilde = 0.80
bin = 2
res = 0.98625586
err = 0.02976329
mu1 = 471.89381608
mu2 = 265.88241598
diff = 206.0114001
xfit = res
xfit1= res+err
xfit2 = res-err

mean, var, skew, kurt = skellam.stats(mu1, mu2, moments='mvsk')

x = np.arange(skellam.ppf(0.01, mu1, mu2),
              skellam.ppf(0.99, mu1, mu2))
xx = np.linspace(skellam.ppf(0.01, mu1, mu2),skellam.ppf(0.99, mu1, mu2),500)

#xx = np.linspace(-14.,14.)

ax.plot(x, skellam.pmf(x, mu1, mu2), 'bo', ms=8, label='skellam pmf')
ax.plot(xx,skPDF(xx,mu1,mu2),'-',label='skellam PDF')
#ax.plot(xx,poisPDF(xx,mu1,mu2),'-',label = 'poisson PDF')
#ax.vlines(x, 0, skellam.pmf(x, mu1, mu2), colors='b', lw=5, alpha=0.5)
#ax.vlines(x,0,skPDF(x,mu1,mu2), colors='b', lw=5, alpha=0.5)



#for sc in [0.8,1.0]:
#    ax.plot(xx,skPDF(xx,sc*mu1,sc*mu2),'-',label = 'sc = '+str(sc))



ax.axvline(x=diff,label = 'measured k',color="Red")

ax.plot(xx,skPDF(xx,xfit*mu1,xfit*mu2),label = "Fit Result",color="Black")
#ax.fill_between(xx,skPDF(xx,xfit1*mu1,xfit1*mu2),skPDF(xx,xfit2*mu1,xfit2*mu2),label = "fit error",color="Grey",alpha=0.5)
#ax.plot(xx,skPDF(xx,xfit1*mu1,xfit1*mu2),label = "fit error",color="Grey",alpha=0.5)
#ax.plot(xx,skPDF(xx,xfit2*mu1,xfit2*mu2),color="Grey",alpha = 0.5)

#Plot Error Bounds:
ex = np.linspace(xfit2,xfit1,100)
for i,e in enumerate(ex):
    if i==0:
        ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Grey",alpha=0.1,label='error bounds')
    else:
        ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Grey",alpha=0.1)
#ex2 = np.linspace(xfit1,1.,100)
#for i,e in enumerate(ex2):
#    ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Red",alpha=0.1)
#rv = skellam(mu1, mu2)
#ax.vlines(x, 0, rv.pmf(x), colors='k', linestyles='-', lw=1, label='frozen pmf')
plt.title("dtilde = "+str(dtilde)+", Bin Nr. "+str(bin)+", Fit Result x="+str(res)+" +- "+str(err))
ax.legend(loc='best', frameon=False)
#plt.show()

#check normalization:
yy = np.linspace(-500.,500.,100000)
#print "integral skPDF mu1-mu2: " , integrate.quad(skPDF,-1000.1234,1000.345,args=(mu1,mu2))
#print "integral skPDF fitted: " ,integrate.quad(skPDF,-1000.1234,1000.4743,args=(xfit*mu1,xfit*mu2))


