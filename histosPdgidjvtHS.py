###This returns directory with histogram file including histograms pT, eta, phi, E ###

import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os
import shutil

if len(sys.argv) != 5:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file><pT cut value> <mass cut value> <delta Eta cut value>"%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
pTCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]
outFileName = "wztruth_pdgid_jvtHS__pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut


# readToHisto returns list of histograms of 4-vector-components
def readToHisto(tree, ptCut, massCut, deletaCut):
   
    ptcut = float(ptCut)
    masscut = float(massCut)
    deletacut = float(deletaCut)

    wztruth_mjj_hist = ROOT.TH1D("mjj","2-jet invariant Mass", 50, 0., 1000.) 
    wztruth_delR_hist = ROOT.TH1D("delR", "delta R", 50, 0., 10.)
    wztruth_delEta_hist = ROOT.TH1D("delEta", "delta Eta", 50,-10.,10.)
    wztruth_delEtaAbs_hist = ROOT.TH1D("delEtaAbs","Abs(delEta)",50,0.,10.)
    wztruth_delPhi_pTsorted_hist = ROOT.TH1D("delPhi_pTsorted", "delta Phi (pT sorted)",50,-4.,4.)
    wztruth_delPhi_Etasorted_hist = ROOT.TH1D("delPhi_Etasorted", "delta Phi (Eta sorted)",50,-4.,4.)
    wztruth_sgd_delPhi_hist = ROOT.TH1D("sgd_delPhi","signed delPhi",100,-4.,4.)
    
    #mjj_hist.Sumw2()
    # Booleans are to check if sorting algorithm works
    #tmp = True # for eta sorting
    #tmp2 = True # for initial pT sorting

    for entryNum in range(0,tree.GetEntries()):
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)
               
        #boson_0_truth_p4 = tree.boson_0_truth_p4
        #dijet_p4 = tree.dijet_p4
        #ditau_matched_p4  = tree.ditau_matched_p4
        #ditau_p4 = tree.ditau_p4
        #jet_0 = tree.jet_0_p4
        jet_0_wztruth = tree.jet_0_wztruth_p4
        #jet_1 = tree.jet_1_p4
        jet_1_wztruth = tree.jet_1_wztruth_p4
        #jet_2 = tree.jet_2_p4
        jet_2_wztruth = tree.jet_2_wztruth_p4
        #met_p4 = tree.met_p4
        #met_truth_p4 = tree.met_truth_p4
        #primary_vertex_v = tree.primary_vertex_v
        #tau_0_matched_leptonic_tau_invis_p4 = tree.tau_0_matched_leptonic_tau_invis_p4
        #tau_0_matched_leptonic_tau_p4 = tree.tau_0_matched_leptonic_tau_p4
        #tau_0_matched_leptonic_tau_vis_p4 = tree.tau_0_matched_leptonic_tau_vis_p4
        #tau_0_matched_p4 = tree.tau_0_matched_p4
        #tau_0_p4 = tree.tau_0_p4
        #tau_0_trk_vx_v = tree.tau_0_trk_vx_v
        #tau_1_matched_leptonic_tau_invis_p4 = tree.tau_1_matched_leptonic_tau_invis_p4
        #tau_1_matched_leptonic_tau_p4 = tree.tau_1_matched_leptonic_tau_p4
        #tau_1_matched_leptonic_tau_vis_p4 = tree.tau_1_matched_leptonic_tau_vis_p4
        #tau_1_matched_p4 = tree.tau_1_matched_p4
        #tau_1_p4 = tree.tau_1_p4
        #tau_1_trk_vx_v = tree.tau_1_trk_vx_v
        pdgid0 = tree.jet_0_wztruth_pdgid
        pdgid1 = tree.jet_1_wztruth_pdgid
        JvtHS0 = tree.jet_0_is_Jvt_HS
        JvtHS1 = tree.jet_1_is_Jvt_HS
        if jet_1_wztruth.Pt()==0.:
            continue
        else:
            wztruth_mjj = (jet_0_wztruth+jet_1_wztruth).M()
            wztruth_delEta = jet_0_wztruth.Eta()-jet_1_wztruth.Eta()
            wztruth_delEtaAbs = abs(wztruth_delEta)
           
            if (jet_0_wztruth.Pt() >= ptcut and jet_1_wztruth.Pt() >= ptcut and  wztruth_delEtaAbs >= deletacut and wztruth_mjj >= masscut and abs(jet_0_wztruth.Eta()) < 4.5 and abs(jet_1_wztruth.Eta()) < 4.5 and pdgid0 <=5 and pdgid1 <=5 and JvtHS0 == 1 and JvtHS1 == 1):

                
                
                wztruth_mjj_hist.Fill(wztruth_mjj)
                wztruth_delR_hist.Fill(jet_0_wztruth.DeltaR(jet_1_wztruth))
                wztruth_delEta_hist.Fill(wztruth_delEta)
                wztruth_delEtaAbs_hist.Fill(wztruth_delEtaAbs)
                wztruth_delPhi_pTsorted_hist.Fill(jet_0_wztruth.DeltaPhi(jet_1_wztruth))
            
                liste = sorted([jet_0_wztruth,jet_1_wztruth], key = lambda jetx: jetx.Eta(), reverse = True)
                delPhi_Etasorted = liste[0].DeltaPhi(liste[1])
            
                wztruth_delPhi_Etasorted_hist.Fill(delPhi_Etasorted)

                if liste[0].Eta()*liste[1].Eta() < 0.:
                    wztruth_sgd_delPhi_hist.Fill(delPhi_Etasorted)
                                              

    return [wztruth_mjj_hist,wztruth_delR_hist,wztruth_delEta_hist,wztruth_delEtaAbs_hist, wztruth_delPhi_pTsorted_hist ,wztruth_delPhi_Etasorted_hist, wztruth_sgd_delPhi_hist]


print "Running over data..."

dataFile  = ROOT.TFile(dataFileName,"READ")
dataTree  = dataFile.Get("NOMINAL")
dataHistos = readToHisto(dataTree,pTCut,massCut,delEtaCut)



# Write the two histograms to an output root file
print "Writing histograms to output file..."

#  overwriting previous files with the same name
if os.path.exists(outFileName):
    shutil.rmtree(outFileName)

os.mkdir(outFileName)
os.chdir(outFileName)
outHistFile = ROOT.TFile.Open("histo_"+outFileName+".root","RECREATE")
outHistFile.cd()
for hist in dataHistos:
    hist.Write()

outHistFile.Close()

print "Done writing histograms mjj, delR, delEta, DelEtaAbs, delPhi_pTsorted, delPhi_Etasorted, sgd_delPhi to output file"
print "directory %s created"%(outFileName)
print "file %s created with histograms (sorted by Eta)"%(outFileName+".root")

namelist = ["mjj", "delR", "delEta", "DelEtaAbs", "delPhi_pTsorted", "delPhi_Etasorted", "sgd_delPhi"]
for i,hist in enumerate(dataHistos):
    # Set axis labels
    hist.GetYaxis().SetTitle("Number of events")
    hist.GetXaxis().SetTitle(namelist[i])

    # Prepare the canvas for plotting
    # Make a canvas
    canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
    canvas.cd()
    hist.Draw("h")
    canvas.SaveAs(outFileName + namelist[i]+".pdf")

