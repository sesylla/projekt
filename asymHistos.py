import ROOT
import sys
import os
import shutil
if len(sys.argv) != 2:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <Histo Directory File>"%(sys.argv[0])
    # End the program
    sys.exit(1)

dirname = sys.argv[1]
HistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"
outFileName = "asym_"+sys.argv[1]+".root"
HistFile = ROOT.TFile.Open(HistFileName, "READ")

def subtrHistos(dataFile):
    delPhiHist_diff = dataFile.Get("delPhiHist")
    oo1Hist_diff = dataFile.Get("oo1Hist")
    
    #delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist_diff.GetSize()-2,delPhiHist_diff.GetMinimumBin(),delPhiHist_diff.GetMaximumBin())
    #oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist_diff.GetSize()-2,oo1Hist_diff.GetMinimumBin(),oo1Hist_diff.GetMaximumBin())
    delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist_diff.GetSize()-2,delPhiHist_diff.GetXaxis().GetXmin(),delPhiHist_diff.GetXaxis().GetXmax())
    oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist_diff.GetSize()-2,oo1Hist_diff.GetXaxis().GetXmin(),oo1Hist_diff.GetXaxis().GetXmax())

    delPhiHist_mirr.SetEntries(delPhiHist_diff.GetEntries())
    oo1Hist_mirr.SetEntries(oo1Hist_diff.GetEntries())

    for bin in range(delPhiHist_diff.GetSize()-2):
        bin+=1 #skip underflow bin (bin 0 is underflow, last one is overflow)
        delPhiHist_mirr.SetBinContent(bin,delPhiHist_diff.GetBinContent(delPhiHist_diff.GetSize()-bin-1))
        delPhiHist_mirr.SetBinError(bin,delPhiHist_diff.GetBinError(delPhiHist_diff.GetSize()-bin-1))
        
    for bin in range(oo1Hist_diff.GetSize()-2):
        bin+=1
        oo1Hist_mirr.SetBinContent(bin,oo1Hist_diff.GetBinContent(oo1Hist_diff.GetSize()-bin-1))
        oo1Hist_mirr.SetBinError(bin,oo1Hist_diff.GetBinError(oo1Hist_diff.GetSize()-bin-1))

    #delPhiHist_diff.Scale(1/delPhiHist_diff.Integral(), "width")
    #oo1Hist_diff.Scale(1/oo1Hist_diff.Integral(), "width")
    #delPhiHist_mirr.Scale(1/delPhiHist_mirr.Integral(), "width")
    #oo1Hist_mirr.Scale(1/oo1Hist_mirr.Integral(), "width")
    #delPhiHist_diff.Scale(1, "width")
    #oo1Hist_diff.Scale(1, "width")
    #delPhiHist_mirr.Scale(1, "width")
    #oo1Hist_mirr.Scale(1, "width")

    #delPhiHist_mirr.Add(delPhiHist_diff, -1)
    #oo1Hist_mirr.Add(oo1Hist_diff, -1)
    #delPhiHist_mirr.SetName("delPhiHist")
    #oo1Hist_mirr.SetName("oo1Hist")
    delPhiAsym = delPhiHist_mirr.GetAsymmetry(delPhiHist_diff)
    oo1Asym = oo1Hist_mirr.GetAsymmetry(oo1Hist_diff)
    delPhiHist_mirr.SetEntries(delPhiHist_diff.GetEntries())
    oo1Hist_mirr.SetEntries(oo1Hist_diff.GetEntries())
    
    delPhiAsym.SetNameTitle("delPhiHist_diff","delPhi asymetry")
    oo1Asym.SetNameTitle("oo1Hist_diff","oo1 asymetry")
    
    delPhiAsym.SetStats(0)
    oo1Asym.SetStats(0)
    ### rename needed
    return [delPhiAsym,oo1Asym],["delPhiHist_diff","oo1Hist_diff"]
    #return [delPhiHist_diff,oo1Hist_diff],["delPhiHist_diff","oo1Hist_diff"]

def writeHistosToFile(histlist,outFileName):    
    if os.path.exists(outFileName):
        shutil.rmtree(outFileName)
        print "overwriting dir "+outFileName+"  ........"
    os.mkdir(outFileName)
    print "directory "+outFileName+" created!"
    os.chdir(outFileName)

    
    outHistFile = ROOT.TFile.Open(outFileName+".root","RECREATE")
    outHistFile.cd()
    for hist in histlist:
        hist.Write()
    outHistFile.Close()

def plotAll(histlist,namelist,outFileName):
    for i,hist in enumerate(histlist):
        canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
        canvas.cd()
        canvas.SetGrid()
        hist.Draw("h")
        canvas.SaveAs(outFileName + namelist[i][0:-5]+".pdf")
    
def doAll(dataFile,outFileName):
    dataHistos, namelist = subtrHistos(dataFile)
    writeHistosToFile(dataHistos,outFileName)
    plotAll(dataHistos,namelist,outFileName)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

doAll(HistFile,outFileName)
