###This returns directory with histogram file including histograms pT, eta, phi, E ###

import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os
import shutil

if len(sys.argv) != 3:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <input data file> <branch name> "%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments
dataFileName = sys.argv[1]
branchname = sys.argv[2]
#outFileName = sys.argv[3]


print "branchname: %s"%(sys.argv[2])

# readToHisto returns list of histograms of 4-vector-components
def readToHisto(tree,branch):
    pThist = ROOT.TH1D("pT","pT",100,0.,800.)
    pThist.Sumw2()
    etahist = ROOT.TH1D("eta","eta",100,-4.,4.)
    etahist.Sumw2()
    phihist = ROOT.TH1D("phi","phi",100,-4.,4.)
    phihist.Sumw2()
    Ehist = ROOT.TH1D("E","Et",100,0.,1500.)
    Ehist.Sumw2()

    for entryNum in range(0,tree.GetEntries()):
        
        
        # Get the current entry specified by the index named entryNum
        tree.GetEntry(entryNum)
        #selection: only use events pT>0
        if getattr(tree,branch).Pt() == 0:
            continue
        pT  = getattr(tree,branch).Pt()
        eta = getattr(tree,branch).Eta()
        phi = getattr(tree,branch).Phi()
        nrg = getattr(tree,branch).Et()


        truth = ROOT.TLorentzVector()
        truth.SetPtEtaPhiE(pT,eta,phi,nrg)
       
        pThist.Fill(truth.Pt())
        etahist.Fill(truth.Eta())
        phihist.Fill(truth.Phi())
        Ehist.Fill(truth.E())
    return [pThist, etahist, phihist, Ehist]



print "Running over data..."

dataFile  = ROOT.TFile(dataFileName,"READ")
dataTree  = dataFile.Get("NOMINAL")
dataHistos = readToHisto(dataTree,branchname)



# Write the two histograms to an output root file
print "Writing histograms to output file..."
#  overwriting previous files with the same name
if os.path.exists(branchname):
    shutil.rmtree(branchname)
os.mkdir(branchname)
os.chdir(branchname)
outHistFile = ROOT.TFile.Open("histo_"+branchname+".root","RECREATE")
outHistFile.cd()
dataHistos[0].Write()
dataHistos[1].Write()
dataHistos[2].Write()
dataHistos[3].Write()
outHistFile.Close()

print "Done writing histograms to output file"
print "directory %s created"%(branchname)
print "file %s created with histograms pT, eta, phi, E"%(branchname+".root")


