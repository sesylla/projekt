import ROOT

h = ROOT.TH1D("h","",1,0.,2.)

h.Fill(1.)
print h.Integral()
h.Scale(200/h.Integral())
print h.Integral()

for i in range(50):
    h.Fill(1.)

print h.Integral()

h2 = ROOT.TH1D("h","",1,0.,2.)
h2.Fill(1.)
h2.Scale(200/h2.Integral())
h2.Add(h,-1)

print h2.Integral()

