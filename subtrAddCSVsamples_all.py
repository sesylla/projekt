import ROOT
import sys
import os
import shutil
from numpy import sqrt


if len(sys.argv) != 3:
    print ("USAGE: <input directory name> <dtilde>")
    sys.exit(1)


dtilde = sys.argv[2]
dirname = sys.argv[1]+"/AllSamples"

samplefilename = sys.argv[1]+"_"+dtilde+".root"
outfilename = sys.argv[1]+"_"+dtilde+"_sumdiff.root" 
os.chdir(dirname)

print(samplefilename)
print(outfilename)
sampleFile = ROOT.TFile.Open(samplefilename,"READ")
sampleFileOut = ROOT.TFile.Open(outfilename,"RECREATE")
oo1namelist,dpnamelist = [],[]



def makesamplelist(sample_file): #returns list of sample histograms
    
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    for s in hnames:
        if "oo1_" in s:
            hnamesoo.append(s)
        elif "dp_" in s:
            hnamesdp.append(s)
      
    #print (len(hnamesoo),len(hnamesdp))
    
   

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for n in hnamesoo:
        ooh = sample_file.Get(n)
        hsamplesoo.append(ooh)
    for n in hnamesdp:
        dph = sample_file.Get(n)
        hsamplesdp.append(dph)
    
        
        
    
   
    return hsamplesoo,hsamplesdp

def subtrAddHistos(samplelist,dataFile):
    difflist,sumlist = [],[]
    for k,sp in enumerate(samplelist):
        if k%500 == 0:
            print("processing sample ",k," of ",len(samplelist))
        
        h_diff,h_sum = subtrAdd(sp)
        difflist.append(h_diff)
        sumlist.append(h_sum)
    dataFile.cd()
    print("calculations done. writing sum/diff histos to file ", outfilename, " ... ")
    for hlist in [difflist, sumlist]:
        for h in hlist:
            h.Write()
    print("done.")
    return



def subtrAdd(Hist):

    #Hist.Sumw2()
    
    
    
    
    
    

    Hist_mirr = ROOT.TH1D(Hist.GetName()+"_mirr"," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  
    #Hist_mirr.SetEntries(Hist.GetEntries())
        
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D(Hist.GetName()+"_diff"," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D(Hist.GetName()+"_sum"," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    

    #Hist_diff.GetXaxis().SetRange(Hist_diff.GetSize()//2,Hist_diff.GetSize()-2)
    #Hist_sum.GetXaxis().SetRange(Hist_sum.GetSize()//2,Hist_sum.GetSize()-2)
    
        
    return Hist_diff,Hist_sum

print("dtilde = ",dtilde)  
s_list_oo, s_list_dp = makesamplelist(sampleFile)
print("subtr/add oo samples ...")
subtrAddHistos(s_list_oo,sampleFileOut)
print("subtr/add delphi samples ...")
subtrAddHistos(s_list_dp,sampleFileOut)
