
import numpy as np
import scipy.stats
from scipy.optimize import minimize
from scipy.special import ive
from scipy.optimize import minimize
from scipy.optimize import NonlinearConstraint
from scipy.optimize import Bounds
from scipy.optimize import BFGS
from scipy.optimize import SR1
from array import array
import matplotlib.pyplot as plt
import timeit
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import csv
#fig, ax = plt.subplots(1, 1)




import ROOT
import sys
import os

if len(sys.argv) != 5:
    print ("USAGE: <source directory name> <dtilde> <N_samples> <Bkg ratio>")
    sys.exit(1)

outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 

dtilde = sys.argv[2]
N_sp = int(sys.argv[3])

HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+"_sumdiff.root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")


#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3()
BGen = ROOT.TRandom3()
#constants from fit N(d) = 200. + c*d**2
#c_oo = 3147.5214538421055
#c_dp = 3611.706482059846
#rebinned, but also with 255004: 
c_oo =  3147.521454469851  #+-  [0.61616453]
c_dp =  3611.706482060036  #+-  [0.43232619]
Background_Ratio = float(sys.argv[4])

dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

ci_oo = [256.67985876604547, 236.37902929944582, 119.18474917359077, 62.95976291095352, 38.508098540335766, 22.803682056052256, 15.919852484639478]
cierrs_oo = [0.34921445655286226, 0.3038219949611821, 0.2714139513440568, 0.22423599451122544, 0.2098730747921924, 0.18872349525623347, 0.16710806526367844]
ci_dP = [62.62255827521881, 167.38329595698812, 219.93972800634108, 200.42929342082726, 118.06756711728525, 15.885299902836735,0.]
cierrs_dP = [0.22881847404023611, 0.3947626204659464, 0.5320246854890918, 0.48166615436006305, 0.2957030309665562, 0.1000841089701317,1.]


def backGen(b_ratio,nbins,dtilde,c,gen): #returns sum,diff arrays of background
    N = 200.+c*float(dtilde)**2
    #nbins = len(histarr)
    #nbins = hist.GetSize()-2
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = [gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)]
    #b_list = [b_ratio*N/(nbins) for i in range(nbins)]
    b_mirr = [b_list[len(b_list)-i-1] for i in range(len(b_list))]
    b_arr_pos = np.array(b_list[len(b_list)//2:])
    b_mirr_pos = np.array(b_mirr[len(b_mirr)//2:])
    #print(b_list)
    
#print("backg_sum: ",b_arr_pos+b_mirr_pos)
    #print("backg_diff: ",b_arr_pos-b_mirr_pos)
    return b_arr_pos+b_mirr_pos,b_arr_pos-b_mirr_pos

#print("******* index: ",dtildelist.index("0.00")) 
#Normalize templates to data
def getnormfac(sample_hist,templateHistFile,dtilde,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" )
        #SMhist = ROOT.TH1D("oo1_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ) 
        #SMhist = ROOT.TH1D("dp_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())

    else:
        print("problem in getnormfac.")
        sys.exit(1)

    #for bin in range(SMhist.GetSize()):
    #    SMhist.SetBinContent(bin,SMhist0.GetBinContent(bin))
    #N_sample = sample_hist.Integral()
    #N_sample = np.sum(sample_hist[0]) -  Background_Ratio*(200.+c*float(dtilde)**2)/(2.*len(sample_hist[0]))
    #print("sample integral: ",N_sample)
    N_SM = 200.
    #N_SM = N_sample - c * float(dtilde)**2
    #print("N_SM : ",N_SM)
    #print("N_SM expectation: ",N_SM)
    SM_Integral = SMhist.Integral()
    #print("SM Integral: ", SM_Integral)
    #SM_Integral = np.sum(SMhist)

    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    
    
    
    return Normfac
    
def maketemplarray(hist): #returns numpy  array of arrays(posdata,mirrdata)
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()):
        
        #if hist.GetBinContent(bin) == 0.:
        #    continue
        datalist.append(hist.GetBinContent(bin))


    mirrlist = [datalist[len(datalist)-i-1] for i in range(len(datalist))]    
    hist_data     = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1)],dtype = np.double)
    mirr_data = np.array([mirrlist[i] for i in range(len(mirrlist)//2,len(mirrlist)-1) ],dtype = np.double)
    
    #delete empty last bin of delPhi histo:
    #if(hist_data[-1]==0. or mirr_data[-1] == 0.):
    #    return np.array([hist_data[:-1] , mirr_data[:-1]])
    #else:
    return np.array([hist_data , mirr_data])



def maketemplatelist(dtildelist,templateHistFile):
    #templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
   
    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()

        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
        
        
        oo1tmplarray = maketemplarray(oo1Hist1)
        delPhitmplarray = maketemplarray(delPhiHist1)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return np.array(oo1templatelist),np.array(delPhitemplatelist)



################# samples  ###########################


def makearray(hist): #for diff histo
    datalist = []
    for bin in range(hist.GetSize()-1):
        bin+=1
        datalist.append(hist.GetBinContent(bin))
    data = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1) ],dtype = np.double)
    
    return data



def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of samples histograms [[sum,diff], ... ]
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    #make list of [name_sum,name_diff]:
    for s in hnames:
        if "oo1_dtilde"+dtilde in s :
            
            if "sum" in s:
                hnamesoo.append([s,s[0:-3]+"diff"])
                #print([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for oo1.")
        elif "dp_dtilde"+dtilde in s:
            if "sum" in s:
                hnamesdp.append([s,s[0:-3]+"diff"])
            elif "diff" in s:
                pass
            else:
                print("name error for dp.")
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        

        if sample_file.Get(hnamesoo[i_sp_oo][0]).Integral() != 0.:
            
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh_sum = sample_file.Get(hnamesoo[i_sp_oo][0])
            ooh_diff = sample_file.Get(hnamesoo[i_sp_oo][1])
            print(hnamesoo[i_sp_oo])
            ooarr_sum = makearray(ooh_sum)
            ooarr_diff = makearray(ooh_diff)
            hsamplesoo.append( [ooarr_sum,ooarr_diff])

        if sample_file.Get(hnamesdp[i_sp_dp][0]).Integral() != 0.:
            
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        else:
            i_sp_dp = gen_dp.__next__()
            dph_sum = sample_file.Get(hnamesdp[i_sp_dp][0])
            dph_diff = sample_file.Get(hnamesdp[i_sp_dp][1])
            dparr_sum = makearray(dph_sum)
            dparr_diff = makearray(dph_diff)
            hsamplesdp.append( [dparr_sum,dparr_diff])
        #print("random indices :", i_sp_oo,i_sp_dp) 
        #ooarr_sum = makearray(ooh_sum)
        #ooarr_diff = makearray(ooh_diff)
        #dparr_sum = makearray(dph_sum)
        #dparr_diff = makearray(dph_diff)
        #hsamplesoo.append( [ooarr_sum,ooarr_diff])
        #hsamplesdp.append( [dparr_sum,dparr_diff])
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return hsamplesoo,hsamplesdp


def subtrAdd(Hist,normfac,d):

    #Hist.Sumw2()
    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  
    #Hist_mirr.SetEntries(Hist.GetEntries())
        
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    

    #Hist_diff.GetXaxis().SetRange(Hist_diff.GetSize()//2,Hist_diff.GetSize()-2)
    #Hist_sum.GetXaxis().SetRange(Hist_sum.GetSize()//2,Hist_sum.GetSize()-2)
    
        
    return Hist_diff,Hist_sum
                         

######### interpolation from templates to arbitrary dtilde value###########################################################################


def findx0x1(d,dtildevals0,templates):
    #print( "d value: ",d)
    dtildevals = [float(j) for j in dtildevals0] 
    
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d, [x0,x1]       
        return [x0,x1],[f0,f1]

    else:
        print("dtilde out of range!")
        #sys.exit(1)
        #return [np.nan,np.nan],[np.nan,np.nan]
        return np.nan
#def interpol(x,x_list,f_list): #f_list is array of arrays, x_list list of corresp. x values
#    #tmp=[]
#    #for x in xx:
#    
#    xvals,fvals = findx0x1(x,x_list,f_list)
#        
#    #print xvals,fvals
#    if not (xvals and fvals):
#        print("could not find x values for interpolation.")
#        sys.exit(1)
#    else:
#        x0 = xvals[0]
#        x1 = xvals[1]
#        f0 = fvals[0]
#        f1 = fvals[1]
#    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

#            #if abs(x - x0) < 10**-6:
#        if x == x0:
#                #tmp.append(f0)
#            f = f0
#            #return f
#            #elif abs(x -x1) < 10**-6:
#        elif x == x1:
#                #tmp.append(f1)
#            f = f1
#            #return f
#        else:
#                
#            f = (f0*(x1-x)/(x1-x0))+f1*(x-x0)/(x1-x0)
#            
#                    
#                        
#    #print x,[x0,x1]
#    return f
dtildevals = np.array([float(x) for x in dtildelist])

    

############################################ Fit Functions ########################################################################################################

def lpdf(b,k_pl,k_min,dtilde,diff_templ,slam,dtil): #log(pdf)
    mu1 = 0.5*(slam+diff_templ)
    mu2 = 0.5*(slam-diff_templ)
    
    
    
    #print("----------------")

    #no background:
    #result = -(mu1+mu2)+(k_min/2.)*np.log(abs(mu1/mu2))+2.*np.sqrt(abs(mu1*mu2))+np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))))+k_pl*np.log(abs(slam))-slam
    
#background:
    result = -(mu1+mu2)+(k_min)*np.log(abs((mu1+b)/(mu2+b)))+2.*np.sqrt(abs((mu1+b)*(mu2+b)))+np.log(ive(abs(k_min),2.*np.sqrt(abs((mu1+b)*(mu2+b)))))+k_pl*np.log(abs(slam+2*b))-slam-4*b
    return result

def NLL(params,dtil,sum_data,diff_data,diff_template):
    b,sum_lam = params[0],params[1:]
    #print("#####",dtilde)
    
    #print dtilde,del_lam
    res = 0
    
    
    #diff_template1 = interpol(dtil,dtildelist,templatearrlist)

    #if diff_template1[-1] == 0. :
    #    diff_template2 =np.delete(np.delete(diff_template1,-1),0)
    #    sum_data2 =np.delete(np.delete(sum_data1,-1),0)
    #    diff_data =np.delete(np.delete(diff_data1,-1),0)
    #else:
    #    diff_template2 = diff_template1
    #    sum_data2 = sum_data1
    #    diff_data2 = diff_data1
    
    # !!! can't fit bins with content 0 !!!
    # delete zeroes from end to beginning of list:
    
    
    #sum_template = hist_template1+hist_template2
    
    

    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    f = lpdf(b,sum_data,diff_data,dtilde,diff_template,sum_lam,dtil)
    res += f
    
    return -np.sum(res)

def deletezeros(indexlist,arr):
        
    tmp = [arr]
    n = -1
    for i,j in enumerate(indexlist):
        # j  have to be sorted negative indices
        n+=1
        tmp.append(np.delete(tmp[i],j+n))
    return tmp[-1]     

def minNLL(dtil,sum_data,diff_data,diff_template):
    #print("#####",dtil)
    #if not len(sum_data) == len(diff_data):
    #    print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
    #    sys.exit(1)
    #ci_list =np.array(ci_list0)
    #print(ci_list)
    #print(sum_data)
    #print(diff_data)
    #print("mean of diff_data:", sum(diff_data)/float(len(diff_data)))
    #print hist_data
    #print hist_template

    #print("diff_data, diff_template:")
    #print(diff_data)
    #print(diff_template)
    


    
    if not len(sum_data) == len(diff_data):
        print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
        sys.exit(1)

    sumlam_start = [sumval for sumval in sum_data]
    b_start = [Background_Ratio*sum(sumlam_start)/(2*len(sumlam_start))]
   
    
    #b_start = [100.]
#dtilde_start = [-0.01]
    startpar = np.array(b_start+sumlam_start)
    #print(startpar)
    
    #def cons_f(x):
    #    return x**2-diff_template**2
    

#make bounds

    
    lbnds_slam = [0. for i in range(len(diff_data))]
    ubnds_slam = [np.inf for i in range(len(diff_data))]

    lbnd_b = [1.]
    ubnd_b = [np.inf]

    lbnds = lbnd_b+lbnds_slam
    ubnds = ubnd_b+ubnds_slam
    
    #lbnds = startpar-1.
    #ubnds = startpar+1.
    #sum_lam_bounds = [[1e-6,np.inf] for l in range(len(diff_data))]
    
    bounds = Bounds(lbnds,ubnds)
    #print(bounds)

 
    #cons = ({'type': 'ineq', 'fun': lambda x:  x**2 - dtil**2*ci_list**2})
    cons = ({'type': 'ineq', 'fun': lambda x:  x[1:]**2 - diff_template**2})
    
    
    #nonlinear_constraint = NonlinearConstraint(cons_f,1e-8,np.inf,jac='2-point',hess=SR1())
    
    err = 0

    #res = minimize(NLL,startpar,method="trust-constr",args=(dtil,sum_data,diff_data,diff_template,ci_list),constraints = nonlinear_constraint,bounds=bounds,options={'maxiter':10000}  )
    res = minimize(NLL,startpar,method="SLSQP",args=(dtil,sum_data,diff_data,diff_template),constraints = cons,bounds=bounds,options={'maxiter':10000}  )
    #res = minimize(NLL,startpar,method="SLSQP",args=(dtil,sum_data,diff_data,diff_template,ci_list),bounds=bounds,options={'maxiter':10000}  )
    if not res.success:
        #print("fit failed at dtilde = ",dtil)
        #print(res.message)
        err = 1
    #print(sum_data)
    #print(res.x)
    
    #nllres = NLL(res.x,dtilde,sum_data,diff_data,ci_list)
    nllres = res.fun
    
    #print(nllres)
    
    #print("NLL at minimum: ",nllres)
    return nllres ,err,res.x[0]  



    

####### find minimum ######################


def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    #plt.plot(ddlist,tmp)
    #plt.show()
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
        
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)

def getSensitivity(func, tempMean,cal_range):# check if confidence interval exists within range of NLL curve
    
    if (func(cal_range[0])-tempMean)*(func(cal_range[1])-tempMean) > 0.:
        return None
    else:
        return root_scalar(lambda x: func(x) - tempMean, bracket = [cal_range[0],cal_range[1]]).root


def estimate(dtildelist,samplelist,templateHistFile,dtilde,c):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    Nerrs = 0
    errlist = []
    Nzeros = []
    
    #dd = np.linspace(float(dtilde)-0.5,float(dtilde)+0.5,1001)
    
    dd = np.linspace(float(dtilde)-0.2,float(dtilde)+0.2,2001)
    N_dd = len(dd)
    #print(dd)
    #dd = np.linspace(-1.,1.,100)
    dtildevals = np.array([float(x) for x in dtildelist])
    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi,bkg_estimators = [],[],[],[],[],[]
    #intlist,sampleintlist =[], []
    
    for k,sample in enumerate(samplelist):
        if np.sum(sample) == 0.:
            print("############# 0 histo found !! ##############")
            continue
        #if k < 16:
        #    continue
        if k%500 == 0:
            print(k, " of ",len(samplelist), "samples processed. ")
        #sampleintlist.append(sample.Integral())
        normfac = getnormfac(sample,templateHistFile,dtilde,c)

        oo1tmplarrlist,delPhitmplarrlist = maketemplatelist(dtildelist,templateHistFile)
       
        #oo1difftmplarrlist = [oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))] 
        #delPhidifftmplarrlist = [delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))] 
        if c == c_oo:
            oo1tmplarrlist *= normfac
            
            difftemplatelist =np.array([oo1tmplarrlist[i][0]-oo1tmplarrlist[i][1] for i in range(len(oo1tmplarrlist))])
            #sp = samplelist[0]
            #ci_list = np.array(ci_oo)
        elif c == c_dp:
            delPhitmplarrlist *= normfac
            difftemplatelist =np.array([delPhitmplarrlist[i][0]-delPhitmplarrlist[i][1] for i in range(len(delPhitmplarrlist))])
            #ci_list = np.array(ci_dP)
        else:
            print("problem with normfac in estimate.")

        f_inter = interp1d(dtildevals,difftemplatelist,axis=0)
        
        #print("NORMFAC : ",normfac)
        # !!! can't fit bins with sum_k content 0 !!!

    # delete bins with zeroes from end to beginning of list:
        
        #print("LENGTHS: ",len(sample[0]),len(sample[1]),len(ci_list))
        #print(sample[0])
        #print(sample[1])
        #plt.plot(np.arange(len(sample[0])),sample[0])
        #plt.show()
        #plt.plot(np.arange(len(sample[1])),sample[1])
        #plt.show()
        #if c == c_dp:
        #    sum_sample0 = deletezeros([-1],sample[0])
        #    diff_sample0 = deletezeros([-1],sample[1])
        #    ci_list_sample = deletezeros([-1],ci_list)

        #    bkg_sum,bkg_diff = backGen(Background_Ratio,2*len(sum_sample0),dtilde,c,BGen)
        #    sum_sample = sum_sample0 + bkg_sum
        #    diff_sample = diff_sample0 + bkg_diff
            
            
        #    diff_templates1 = f_inter(dd)
        #    diff_templates = np.array([deletezeros([-1],templ) for templ in diff_templates1])
        #elif c== c_oo:
        bkg_sum,bkg_diff = backGen(Background_Ratio,2*len(sample[0]),dtilde,c,BGen)
        sum_sample = sample[0] + bkg_sum
        diff_sample = sample[1] + bkg_diff
        #ci_list_sample = ci_list
            
            
        diff_templates1 = f_inter(dd)
        diff_templates = np.array(diff_templates1)
        #else:
        #    print("problem with constant c in estimate.")
        #    sys.exit(1)
        #print("diff_sample with no bkg: ",sample[1])
#plt.plot(np.arange(len(sum_sample)),sum_sample)
        #plt.show()
        #plt.plot(np.arange(len(diff_sample)),diff_sample)
        #plt.show()
        #print(sum_sample)
        #print(diff_sample)
        #print(ci_list_sample)
#########diff_template zeros will be deleted in  after interpolation : 
        
#print(len(dtildevals),len(difftemplatelist))
        #print(dtildevals.shape,difftemplatelist.shape)
        
        #sum_sample = sample[0]
        #diff_sample = sample[1]
        #ci_list_sample = ci_list
        #diff_templates = diff_templates1


#print("scaled 0.01 Integral", templatelist[51].Integral())
        #intlist.append(templatelist[50].Integral())
        NLLvals0 = []
        errindices = []
        backg_list = []
#NLLvals0 = [minNLL(d,sum_sample,diff_sample,templ,ci_list_sample) for d,templ in zip(dd,diff_templates)]
#NLLvals0oo1 = [minNLL(d,hist_data,dtildelist,oo1templatelist) for d in dd]
        
        for nr,(d,templ) in enumerate(zip(dd,diff_templates)):
            m,err,bkg = minNLL(d,sum_sample,diff_sample,templ)
            if err == 0:
                NLLvals0.append(m)
                backg_list.append(bkg)
            else:
                Nerrs+=1
                errindices.append(nr-len(dd))

        
        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        
#delete d values with errors:
        errindices.sort(reverse = True)
        
        dd_tmp = deletezeros(errindices,dd)

        dd = dd_tmp
        errlist.append(Nerrs)
        Nerrs = 0
        

        
        dmin = findMin(dd,NLLvals)
        
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))
        bkg_estimators.append(backg_list[NLLvals0.index(min1)])
        #find minimum and CIs via interpolation.
        #f_NLL = interp1d(dd,NLLvals,axis=0)
        #dmin = minimize_scalar(f_NLL,method='bounded',bounds=[float(dtilde)-0.19,float(dtilde)+0.19]).x
        
        #f_lim1sig = lambda x: f_NLL(x) - 0.5
        #f_lim2sig = lambda x: f_NLL(x) - 2.
        
        #range to search minimum in:
        #NLL_range_l = [dmin - 0.2,dmin]
        #NLL_range_h = [dmin,dmin+0.2]
        
        #success = True
        #dlim1_l = getSensitivity(f_lim1sig,dmin,NLL_range_l)
        #dlim1_h = getSensitivity(f_lim1sig,dmin,NLL_range_h)
        #dlim2_l = getSensitivity(f_lim2sig,dmin,NLL_range_l)
        #dlim2_h = getSensitivity(f_lim2sig,dmin,NLL_range_h)
        #for lim in [dlim1_l,dlim1_h,dlim2_l,dmin,dlim2_h]:
        #    if not lim:
        #        success = False
        #dlim1_l = root_scalar(f_lim1sig , bracket = [dmin-0.19,dmin]).root
        #dlim1_h = root_scalar(f_lim1sig , bracket = [dmin,dmin+0.19]).root
        #dlim2_l = root_scalar(f_lim2sig , bracket = [dmin-0.19,dmin]).root
        #dlim2_h = root_scalar(f_lim2sig , bracket = [dmin,dmin+0.19]).root
        #if success:
        #    estlist.append(dmin)
        #    err1sig_lo.append(abs(dlim1_l-dmin))
        #    err1sig_hi.append(abs(dlim1_h-dmin))
        #    err2sig_lo.append(abs(dlim2_l-dmin))
        #    err2sig_hi.append(abs(dlim2_h-dmin))
            #bkg_estimators.append(backg_list[NLLvals0.index(min1)])
        
       # print("dmin / LIST INDEX in dd, NLL / bkg est :: ",dmin, NLLvals0.index(min1),backg_list[NLLvals0.index(min1)] )
        #print("inices of res/true: ",np.where(dd==dmin), np.where(dd==0.20))
        #res_diff_tmpl = diff_templates1[np.where(dd==dmin)]
        #print("fit result: ",res_diff_tmpl)
        #print("true 0.20 diff templ : ",diff_templates1[np.where(dd==0.20)])
        #plt.plot(dd,NLLvals)
        #plt.show()

        #os.chdir(outdirname)
       
        
        #canvas = ROOT.TCanvas("canvas")
        #Legend = ROOT.TLegend(0.6,0.7,0.9,0.9)
            
            
        #reshist = interpol(dmin,dtildelist,templatelist)

        #canvas.cd()

        #sample.SetLineColor(4)
        #reshist.SetLineColor(2)
        #templatelist[dtildelist.index(dtilde)].SetLineColor(1)
        #sample.Draw("HistE")
        #reshist.Draw("HistSameE")
        #templatelist[dtildelist.index(dtilde)].Draw("HistSameE")
        #Legend.AddEntry(sample,"sample, d="+dtilde,"l")
        #Legend.AddEntry(reshist,"result, d="+str(dmin),"l")
        #Legend.AddEntry(templatelist[dtildelist.index(dtilde)],"dtilde= "+dtilde+" prediction","l")
        #Legend.Draw()
        #canvas.Update()
        #if c == c_dp:
        #    canvas.SaveAs("dpres"+str(k)+".png")
        #elif c == c_oo:
        #    canvas.SaveAs("oores"+str(k)+".png")
        #os.chdir("..")
            #print("Scaled SM Integral" ,templatelist[50].Integral())
            #print("sample Integral", sample.Integral())
            #print("reshist integral", reshist.Integral())

    #print("SM prediction Integrals : ",intlist)
    #print("sample integrals : ",sampleintlist)
    print("Number of Bins per sample with content 0 : ",Nzeros)
    print("background estimators : ", bkg_estimators)
    #bck_av = sum(backg_list)/len(backg_list)
    print("max. number of errors per sample : ", max(errlist))
    print("average number of errors per sample : ", sum(errlist)/len(errlist))
    print("average relative number of errors per sample : ", sum(errlist)/(len(errlist)*N_dd))
    print("maximum relative number of errors per sample : ",max(errlist)/(len(errlist)*N_dd))
    return np.array(estlist), np.array(err1sig_lo), np.array(err1sig_hi), np.array(err2sig_lo), np.array(err2sig_hi),errlist,bkg_estimators





#delPhiHist.Sumw2()
#oo1Hist.Sumw2()





#hist_data = makearray(oo1Hist)
oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
samplelist_oo,samplelist_dp = makesamplelist(N_sp,dtilde,HistFile,TGen)




############## FIT  #################### 
start = timeit.default_timer()

print("oo fit in process ...")
oo_estarr ,oo_1sigarr_lo,oo_1sigarr_hi,oo_2sigarr_lo,oo_2sigarr_hi,Nerrsoo, oo_bkg = estimate(dtildelist,samplelist_oo,templateHistFile,dtilde,c_oo)
xxo = np.arange(len(oo_estarr))
#print(oo_estarr)

#print("dp fit in process ...")
print("delPhi fit in process ...") 
dp_estarr ,dp_1sigarr_lo,dp_1sigarr_hi,dp_2sigarr_lo,dp_2sigarr_hi,Nerrsdp,dp_bkg = estimate(dtildelist,samplelist_dp,templateHistFile,dtilde,c_dp)
xxd = np.arange(len(dp_estarr))
#print(dp_estarr)

end = timeit.default_timer()

print("done with fitting.")
print("------ ", end-start , "seconds ")


print("Nerrs per sample OO :",Nerrsoo)
print("Nerrs per sample delPhi :",Nerrsdp)
print("number of successes OO/dP: ",len(oo_estarr),len(dp_estarr))
print("ratios of coverage:")
print("OO 1 sigma :", cover_ratio(oo_estarr,oo_1sigarr_lo,oo_1sigarr_hi,dtilde))
print("OO 2 sigma :", cover_ratio(oo_estarr,oo_2sigarr_lo,oo_2sigarr_hi,dtilde))
print("delPhi 1 sigma :", cover_ratio(dp_estarr,dp_1sigarr_lo,dp_1sigarr_hi,dtilde))
print("delPhi 2 sigma :", cover_ratio(dp_estarr,dp_2sigarr_lo,dp_2sigarr_hi,dtilde))


oomean = np.mean(oo_estarr) 
dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
dpvar = np.var(dp_estarr)
print("sample means :",oomean,dpmean)
print("sample variances : ", oovar,dpvar)
print("min, max estimators :",(np.amin(oo_estarr),np.amax(oo_estarr)),(np.amin(dp_estarr),np.amax(dp_estarr)))
print("max limits of NLL+2sig region : ", (np.amin(oo_estarr-oo_2sigarr_lo),np.amax(oo_estarr+oo_2sigarr_hi)),(np.amin(dp_estarr-dp_2sigarr_lo),np.amax(dp_estarr+dp_2sigarr_hi)))

oomean = np.mean(oo_estarr) 
dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
dpvar = np.var(dp_estarr)
oo1sigperc_lo = np.percentile(oo_estarr,(100.-68.27)/2.)
dp1sigperc_lo = np.percentile(dp_estarr,(100.-68.27)/2.)
oo2sigperc_lo = np.percentile(oo_estarr,(100.-95.44)/2.)
dp2sigperc_lo = np.percentile(dp_estarr,(100.-95.44)/2.)
#print(oo1sigperc_lo,oo2sigperc_lo)

oo1sigperc_hi = np.percentile(oo_estarr,(100.+68.27)/2.)
dp1sigperc_hi = np.percentile(dp_estarr,(100.+68.27)/2.)
oo2sigperc_hi = np.percentile(oo_estarr,(100.+95.44)/2.)
dp2sigperc_hi =np.percentile(dp_estarr,(100.+95.44)/2.)
oo1sigperc = [oo1sigperc_lo,oo1sigperc_hi]
oo2sigperc = [oo2sigperc_lo,oo2sigperc_hi]
dp1sigperc = [dp1sigperc_lo,dp1sigperc_hi]
dp2sigperc = [dp2sigperc_lo,dp2sigperc_hi]
print("sample means :",oomean,dpmean)
print("sample variances : ", oovar,dpvar)
print("min, max estimators :",(np.amin(oo_estarr),np.amax(oo_estarr)),(np.amin(dp_estarr),np.amax(dp_estarr)))
print("1sig percentiles oo/dp : ", oo1sigperc,dp1sigperc)
print("2sig percentiles oo/dp : ",oo2sigperc,dp2sigperc)

print("True Background events average (OO/delPhi) : ",Background_Ratio*(200.+c_oo*float(dtilde)**2)/12.,Background_Ratio*(200.+c_dp*float(dtilde)**2)/12.)
bkg_goodres_oo = [x for x in oo_bkg if x >1.1]
bkg_goodres_dp = [x for x in dp_bkg if x > 1.1]
print("percentage of unsuccessful Fit results OO / dP: ", len(bkg_goodres_oo)/len(oo_bkg))
print("background estimators average OO/delPhi : ",sum(oo_bkg)/len(oo_bkg),sum(dp_bkg)/len(dp_bkg))
print("average of successful b estimates OO/delPhi : ", sum(bkg_goodres_oo)/len(bkg_goodres_oo),sum(bkg_goodres_dp)/len(bkg_goodres_oo))


########### histograms of estimators ###########
#os.chdir(outdirname)
#coo = ROOT.TCanvas("oo")
#h_est_oo = ROOT.TH1D("h_est_oo", " ", 20, oomean-0.2, oomean+0.2)
#h_est_oo.SetStats(0)
#for est in oo_estarr:
#    h_est_oo.Fill(est)
#coo.cd()
#h_est_oo.Draw("Hist")
#coo.SaveAs("oo_est_skp_"+dtilde+"_b"+sys.argv[4]+".png")


#cdp = ROOT.TCanvas("dp")
#h_est_dp = ROOT.TH1D("h_est_dp", " ", 20, dpmean-0.2, dpmean+0.2)
#h_est_dp.SetStats(0)
#for est in dp_estarr:
#    h_est_dp.Fill(est)
#cdp.cd()
#h_est_dp.Draw("Hist")
#cdp.SaveAs("dp_est_skp_"+dtilde+"_b"+sys.argv[4]+".png")

########################## save data to csv file #################################
#if not os.path.exists("SkPResults"):
#    os.mkdir("SkPResults")
#os.chdir("SkPResults")

#with open("SkPResults_oo_"+dtilde+".csv", 'a') as csvfile:
#    writer = csv.writer(csvfile,delimiter = ";")
#    writer.writerow([Background_Ratio,oomean,oo1sigperc_lo,oo1sigperc_hi,oo2sigperc_lo,oo2sigperc_hi,oo_bkg])
#with open("SkPResults_dp_"+dtilde+".csv", 'a') as csvfile:
#    writer = csv.writer(csvfile,delimiter = ";")
#    writer.writerow([Background_Ratio,dpmean,dp1sigperc_lo,dp1sigperc_hi,dp2sigperc_lo,dp2sigperc_hi,dp_bkg] ) 



############# PLOT ####################
#plt.errorbar(xxo,oo_estarr,yerr=[oo_1sigarr_lo,oo_1sigarr_hi], fmt = "X")
#plt.title("OO 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxo,oo_estarr,yerr=[oo_2sigarr_lo,oo_2sigarr_hi], fmt = "X")
#plt.title("OO 2 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_1sigarr_lo,dp_1sigarr_hi], fmt = "X")
#plt.title("delPhi 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_2sigarr_lo,dp_2sigarr_hi], fmt = "X")
#plt.title("delPhi 2 sigma intervals")
#plt.grid()
#plt.show()



