import ROOT
import sys
import os
import shutil

if len(sys.argv) != 7:
    print("USAGE: <source directory name (PartColl)> <outdirname>  <dtildelist> <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)

dirname = sys.argv[1]
HistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"


dtildestring = sys.argv[3] #[0.00,-0.05,0.20]
#dtilde0 = "0.20"
#dtilde1 = "0.32"
#dtilde2 = "0.16"
pTCut = sys.argv[4]
massCut = sys.argv[5]
delEtaCut = sys.argv[6]

def stringToList(dtildeString):
    
    tmplist  =  dtildeString.strip("[]").split(",")
    
    

    return tmplist

dtildelist = stringToList(dtildestring)
print(dtildelist)
dtilde0 = dtildelist[0]
dtilde1 = dtildelist[1]
dtilde2 = dtildelist[2]

outdirname = sys.argv[2]
outFileName = "diffPosComp"+dtilde0+dtilde1+dtilde2 
HistFile = ROOT.TFile.Open(HistFileName, "READ")

oo1HistName0= "oo1Hist_d"+str(dtilde0)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName0= "delPhiHist_d"+str(dtilde0)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

oo1HistName1= "oo1Hist_d"+str(dtilde1)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName1= "delPhiHist_d"+str(dtilde1)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

oo1HistName2= "oo1Hist_d"+str(dtilde2)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
delPhiHistName2= "delPhiHist_d"+str(dtilde2)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

print(oo1HistName0,delPhiHistName0)
def subtrHistos(dataFile,delPhiHistName,oo1HistName):
    delPhiHist = dataFile.Get(delPhiHistName)
    oo1Hist = dataFile.Get(oo1HistName)
    for histo in [delPhiHist,oo1Hist]:
        if not histo:
            print("no histogram found. check name!!")
            sys.exit(1)
    print("INTEGRAL BEFORE SCALING: ", oo1Hist.Integral())
    oo1Hist.Scale( 200/oo1Hist.Integral())
    delPhiHist.Scale( 200/delPhiHist.Integral())
    print("INTEGRAL oo1: ",oo1Hist.Integral())
    #delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist_diff.GetSize()-2,delPhiHist_diff.GetMinimumBin(),delPhiHist_diff.GetMaximumBin())
    #oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist_diff.GetSize()-2,oo1Hist_diff.GetMinimumBin(),oo1Hist_diff.GetMaximumBin())
    delPhiHist_mirr = ROOT.TH1D("delPhiHist_mirr"," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
    oo1Hist_mirr = ROOT.TH1D("oo1Hist_mirr"," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())

    delPhiHist_mirr.SetEntries(delPhiHist.GetEntries())
    oo1Hist_mirr.SetEntries(oo1Hist.GetEntries())
    
    for bin in range(delPhiHist.GetSize()-2):
        bin+=1 #skip underflow bin (bin 0 is underflow, last one is overflow)
        delPhiHist_mirr.SetBinContent(bin,delPhiHist.GetBinContent(delPhiHist.GetSize()-bin-1))
        delPhiHist_mirr.SetBinError(bin,delPhiHist.GetBinError(delPhiHist.GetSize()-bin-1))
        
    for bin in range(oo1Hist.GetSize()-2):
        bin+=1
        oo1Hist_mirr.SetBinContent(bin,oo1Hist.GetBinContent(oo1Hist.GetSize()-bin-1))
        oo1Hist_mirr.SetBinError(bin,oo1Hist.GetBinError(oo1Hist.GetSize()-bin-1))
    print("INTEGRAL oo1Hist_mirr: ", oo1Hist_mirr.Integral())
    delPhiHist_diff = ROOT.TH1D("delPhiHist_diff"," ",delPhiHist.GetSize()-2,delPhiHist.GetXaxis().GetXmin(),delPhiHist.GetXaxis().GetXmax())
    oo1Hist_diff = ROOT.TH1D("oo1Hist_diff"," ",oo1Hist.GetSize()-2,oo1Hist.GetXaxis().GetXmin(),oo1Hist.GetXaxis().GetXmax())

    delPhiHist_diff.Add(delPhiHist,delPhiHist_mirr, 1, -1)
    oo1Hist_diff.Add(oo1Hist,oo1Hist_mirr, 1, -1)
    
    #delPhiHist_diff.SetEntries(delPhiHist_mirr.GetEntries())
    #oo1Hist_diff.SetEntries(oo1Hist_mirr.GetEntries())

    delPhiHist_diff.SetNameTitle("delPhiHist_diff","delPhi difference")
    oo1Hist_diff.SetNameTitle("oo1Hist_diff","oo1 difference")
    
    delPhiHist_diff.SetStats(0)
    oo1Hist_diff.SetStats(0)
    
    #only range >0:
    #delPhiHist_pos = ROOT.TH1D("delPhiHist_pos"," ",(delPhiHist_diff.GetSize()-2)/2,0.,delPhiHist_diff.GetXaxis().GetXmax())
    #oo1Hist_pos = ROOT.TH1D("delPhiHist_pos"," ",(oo1Hist_diff.GetSize()-2)/2,0.,oo1Hist_diff.GetXaxis().GetXmax())
    
    ##SetRange takes bin number as argument
    delPhiHist_diff.GetXaxis().SetRange(delPhiHist_diff.GetSize()//2,delPhiHist_diff.GetSize()-2)
    oo1Hist_diff.GetXaxis().SetRange(oo1Hist_diff.GetSize()//2,oo1Hist_diff.GetSize()-2)
    #delPhiHist_diff.SetAxisRange(0.,delPhiHist_diff.GetXaxis().GetXmax(),"X")
    #oo1Hist_diff.SetAxisRange(0.,oo1Hist_diff.GetXaxis().GetXmax(),"X")
    #n=["Original_Histogram","MirrHist"]
    #for i, h in enumerate([oo1Hist,oo1Hist_mirr]):
    #    c = ROOT.TCanvas("c")
    #    c.SetGrid()
    #    h.Draw("h")
    #    c.SaveAs(n[i]+".pdf")
        
    return [delPhiHist_diff,oo1Hist_diff],["delPhiHist_diff","oo1Hist_diff"]
    



def plotAll(histlist,namelist,outFileName):
    for i,hist in enumerate(histlist):
        canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
        canvas.cd()
        canvas.SetGrid()
        hist.Draw("h")
        canvas.SaveAs(outFileName + namelist[i][0:-5]+".pdf")
    
#def doAll(dataFile,outFileName):
#    dataHistos, namelist = subtrHistos(dataFile,delPhiHistName,oo1HistName)
#    writeHistosToFile(dataHistos,outFileName)
#    plotAll(dataHistos,namelist,outFileName)

#dataHistos, namelist = readToHisto(tree,dtilde,pTCut,massCut,delEtaCut)
#writeHistosToFile(dataHistos,outFileName)

#doAll(HistFile,outFileName)

print(delPhiHistName0)
print(delPhiHistName1)
print(delPhiHistName2)
print(oo1HistName0)
print(oo1HistName1)
print(oo1HistName2)


dataHistos0,namelist0 = subtrHistos(HistFile,delPhiHistName0,oo1HistName0)
dataHistos1,namelist1 = subtrHistos(HistFile,delPhiHistName1,oo1HistName1)
dataHistos2,namelist2 = subtrHistos(HistFile,delPhiHistName2,oo1HistName2)

canvasoo = ROOT.TCanvas("canvas")
Legendoo = ROOT.TLegend(0.6,0.7,0.9,0.9)
canvasdP = ROOT.TCanvas("canvas2")
LegenddP = ROOT.TLegend(0.6,0.7,0.9,0.9)


dataHistos0[0].SetLineColor(1)
dataHistos1[0].SetLineColor(2)
dataHistos2[0].SetLineColor(4)
dataHistos0[1].SetLineColor(1)
dataHistos1[1].SetLineColor(2)
dataHistos2[1].SetLineColor(4)

dataHistos0[0].SetTitle("signed #Delta#Phi_{jj} asymmetry ; signed #Delta#Phi_{jj} ; #Delta Events")
dataHistos0[1].SetTitle("OO asymmetry ; OO ; #Delta Events")

histlistdP = [dataHistos0[0],dataHistos1[0],dataHistos2[0]]
yrangedP = [min([h.GetMinimum() for h in histlistdP]),max([h.GetMaximum() for h in histlistdP])]
for h in histlistdP:
    h.GetYaxis().SetRangeUser(yrangedP[0]-yrangedP[0]/10.,yrangedP[1]+yrangedP[1]/10.)

canvasdP.cd()
canvasdP.SetGrid()
dataHistos0[0].Draw("HistE")
#dataHistos0[0].GetYaxis().SetRangeUser(-11,28)
dataHistos1[0].Draw("HistSameE")
dataHistos2[0].Draw("HistSameE")
LegenddP.AddEntry(dataHistos0[0], "#tilde{d} = "+dtilde0, "l")
LegenddP.AddEntry(dataHistos1[0], "#tilde{d} = "+dtilde1, "l")
LegenddP.AddEntry(dataHistos2[0], "#tilde{d} = "+dtilde2, "l")
LegenddP.Draw()
canvasdP.Update()

os.chdir(outdirname)
#canvasdP.SaveAs("diff_pos_all_delPhi.pdf")
canvasdP.SaveAs(outFileName+"_delPhi.png")

histlistoo = [dataHistos0[1],dataHistos1[1],dataHistos2[1]]
yrangeoo = [min([h.GetMinimum() for h in histlistoo]),max([h.GetMaximum() for h in histlistoo])]
for h in histlistoo:
    h.GetYaxis().SetRangeUser(yrangeoo[0]-yrangeoo[0]/10.,yrangeoo[1]+yrangeoo[1]/10.)


canvasoo.cd()
canvasoo.SetGrid()
dataHistos0[1].Draw("HistE")
#dataHistos0[1].GetYaxis().SetRangeUser(-13,34)
dataHistos1[1].Draw("HistSameE")
dataHistos2[1].Draw("HistSameE")
Legendoo.AddEntry(dataHistos0[1], "#tilde{d} = "+dtilde0, "l")
Legendoo.AddEntry(dataHistos1[1], "#tilde{d} = "+dtilde1, "l")
Legendoo.AddEntry(dataHistos2[1], "#tilde{d} = "+dtilde2, "l")
Legendoo.Draw()
canvasoo.Update()
#canvasoo.SaveAs("diff_pos_all_oo.pdf")
canvasoo.SaveAs(outFileName+"_oo.png")


