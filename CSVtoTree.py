
import ROOT
import sys
import os
from numpy import pi

if len(sys.argv) != 3:
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print( "USAGE: %s  <directory of input csv file> <output dir name>. "%(sys.argv[0]))
    # End the program
    sys.exit(1)

dirname = sys.argv[1]
outdirname = sys.argv[2]
os.chdir(dirname)
oo1filename = dirname+"_oo1.csv"
dpfilename = dirname+"_dP.csv"

outfilename = outdirname+".root"

Nbins = 12
lim_oo = [-15.,15.]
lim_dp = [-pi,pi]




dtildelist = ['-1.00', '-0.99', '-0.98', '-0.97', '-0.96', '-0.95', '-0.94', '-0.93', '-0.92', '-0.91', '-0.90', '-0.89', '-0.88', '-0.87', '-0.86', '-0.85', '-0.84', '-0.83', '-0.82', '-0.81', '-0.80', '-0.79', '-0.78', '-0.77', '-0.76', '-0.75', '-0.74', '-0.73', '-0.72', '-0.71', '-0.70', '-0.69', '-0.68', '-0.67', '-0.66', '-0.65', '-0.64', '-0.63', '-0.62', '-0.61', '-0.60', '-0.59', '-0.58', '-0.57', '-0.56', '-0.55', '-0.54', '-0.53', '-0.52', '-0.51', '-0.50', '-0.49', '-0.48', '-0.47', '-0.46', '-0.45', '-0.44', '-0.43', '-0.42', '-0.41', '-0.40', '-0.39', '-0.38', '-0.37', '-0.36', '-0.35', '-0.34', '-0.33', '-0.32', '-0.31', '-0.30', '-0.29', '-0.28', '-0.27', '-0.26', '-0.25', '-0.24', '-0.23', '-0.22', '-0.21', '-0.20', '-0.19', '-0.18', '-0.17', '-0.16', '-0.15', '-0.14', '-0.13', '-0.12', '-0.11', '-0.10', '-0.09', '-0.08', '-0.07', '-0.06', '-0.05', '-0.04', '-0.03', '-0.02', '-0.01', '0.00', '0.01', '0.02', '0.03', '0.04', '0.05', '0.06', '0.07', '0.08', '0.09', '0.10', '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17', '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24', '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31', '0.32', '0.33', '0.34', '0.35', '0.36', '0.37', '0.38', '0.39', '0.40', '0.41', '0.42', '0.43', '0.44', '0.45', '0.46', '0.47', '0.48', '0.49', '0.50', '0.51', '0.52', '0.53', '0.54', '0.55', '0.56', '0.57', '0.58', '0.59', '0.60', '0.61', '0.62', '0.63', '0.64', '0.65', '0.66', '0.67', '0.68', '0.69', '0.70', '0.71', '0.72', '0.73', '0.74', '0.75', '0.76', '0.77', '0.78', '0.79', '0.80', '0.81', '0.82', '0.83', '0.84', '0.85', '0.86', '0.87', '0.88', '0.89', '0.90', '0.91', '0.92', '0.93', '0.94', '0.95', '0.96', '0.97', '0.98', '0.99', '1.00']

oo1histlist,dphistlist = [],[]
oo1namelist,dpnamelist = [],[]
for d in dtildelist:
    oo1histlist.append(ROOT.TH1D("oo1_dtilde"+d," ",Nbins,lim_oo[0],lim_oo[1]))
    oo1namelist.append("oo1_dtilde"+d)
    dphistlist.append(ROOT.TH1D("dp_dtilde"+d," ",Nbins,lim_dp[0],lim_dp[1]))
    dpnamelist.append("dp_dtilde"+d)
for histo in oo1histlist:
    histo.Sumw2()
for histo in dphistlist:
    histo.Sumw2()



tree_oo = ROOT.TTree("too1","tree with oo1 events")
print("tree too1 created.")
tree_dp = ROOT.TTree("tdp","tree with dp events")
print("tree tdp created.")
tree_oo.ReadFile(oo1filename,"obs/D:w1/D:w2/D",";")
tree_dp.ReadFile(dpfilename,"obs/D:w1/D:w2/D",";")



os.chdir("..")
if not os.path.exists(outdirname):
    os.mkdir(outdirname)
else:
    print("directory ",outdirname," already exists!")
os.chdir(outdirname)

outfile = ROOT.TFile.Open(outfilename,"RECREATE")
outfile.cd()

tree_oo.Write()
tree_dp.Write()

def treeToHist(tree,dtildelist,histlist):
    dtildevals = [float(d) for d in dtildelist]
   
        
    for entryNum in range(0,tree.GetEntries()):
        
        if entryNum%50000==0:
            print(entryNum," of ",tree.GetEntries()," Entries read.")
        tree.GetEntry(entryNum)
        obs = tree.obs
        w1 = tree.w1
        w2 = tree.w2
        
        for d,h in zip(dtildevals,histlist):
            rw = 1+w1*d+w2*d**2
            h.Fill(obs,rw)
    print(tree.GetEntries()," of ",tree.GetEntries()," Entries read.")

    for h in histlist:
        h.Write()


print("***************** read oo1 tree  ******************")
treeToHist(tree_oo,dtildelist,oo1histlist)
print("***************** read delta phi tree  *************")
treeToHist(tree_dp,dtildelist,dphistlist)

print("DONE: file ",outfilename, " created .")
outfile.Close()
sys.exit(1)    
            




