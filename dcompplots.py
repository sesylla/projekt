import ROOT
import sys
import os


if len(sys.argv) != 6: 
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <histo file name > <pTCut> <massCut> <delEtaCut> <[dtlildelist] list of 3 values>."%(sys.argv[0])
    # End the program
    sys.exit(1)
histFileName = sys.argv[1]
ptCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]
dtildeString = sys.argv[5] #[0,-0.02,0.05,-0.08,0.3]

def stringToList(dtildeString):
    
    tmplist  =  dtildeString.strip("[]").split(",")
    print tmplist
    dtildelist = [float(x) for x in tmplist] 
    

    return dtildelist

dtildelist = stringToList(dtildeString)
print dtildelist
#colorlist = [1,2,3,4,6] #black,red,green,blue,magenta
colorlist = [1,2,4]

os.chdir(histFileName)

# Retrieve the histograms from the file
# Open the file in read-only mode
histFile  = ROOT.TFile.Open(histFileName+".root","READ")

hoo0 = histFile.Get("oo1Hist_d"+"0.00"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hoo0.SetTitle("Optimal Observable ; OO ; Events ")


hoo1 = histFile.Get("oo1Hist_d"+"-0.50"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
hoo2 = histFile.Get("oo1Hist_d"+"0.20"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

#hoo0 = histFile.Get("oo1Hist_d"+str(dtildelist[0])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hoo1 = histFile.Get("oo1Hist_d"+str(dtildelist[1])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hoo2 = histFile.Get("oo1Hist_d"+str(dtildelist[2])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hoo3 = histFile.Get("oo1Hist_d"+str(dtildelist[3])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hoo4 = histFile.Get("oo1Hist_d"+str(dtildelist[4])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

#for i,hist in enumerate([hoo0,hoo1,hoo2,hoo3,hoo4]):
for i,hist in enumerate([hoo0,hoo1,hoo2]):
    if not hist:
        print "hist "+str(i)+" missing!"
hoo0.Scale(200/hoo0.Integral())
hoo1.Scale(200/hoo1.Integral())
hoo2.Scale(200/hoo2.Integral())
#hoo3.Scale(200/hoo3.Integral(),"width")
#hoo4.Scale(200/hoo4.Integral(),"width")

#ooList = [hoo0,hoo1,hoo2,hoo3,hoo4]
ooList = [hoo0,hoo1,hoo2]

hdP0 = histFile.Get("delPhiHist_d"+"0.00"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hdP0.SetTitle("signed #Delta#Phi_{jj} ; signed #Delta#Phi_{jj} ; Events")

hdP1 = histFile.Get("delPhiHist_d"+"-0.05"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hdP1.SetTitle("signed #Delta#Phi_{jj} ; signed #Delta#Phi_{jj} ; Events")

hdP2 = histFile.Get("delPhiHist_d"+"0.20"+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hdP2.SetTitle("signed #Delta#Phi_{jj} ; signed #Delta#Phi_{jj} ; Events")

#hdP0 = histFile.Get("delPhiHist_d"+str(dtildelist[0])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hdP1 = histFile.Get("delPhiHist_d"+str(dtildelist[1])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hdP2 = histFile.Get("delPhiHist_d"+str(dtildelist[2])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hdP3 = histFile.Get("delPhiHist_d"+str(dtildelist[3])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
#hdP4 = histFile.Get("delPhiHist_d"+str(dtildelist[4])+"_pT"+str(ptCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hdP0.Scale(200/hdP0.Integral())
hdP1.Scale(200/hdP1.Integral())
hdP2.Scale(200/hdP2.Integral())
#hdP3.Scale(200/hdP3.Integral(),"width")
#hdP4.Scale(200/hdP4.Integral(),"width")

#dPList = [hdP0,hdP1,hdP2,hdP3,hdP4]
dPList = [hdP0,hdP1,hdP2]

canvasoo = ROOT.TCanvas("canvasoo")
canvasoo.cd()
canvasoo.SetGrid()
legendoo = ROOT.TLegend(0.1,0.65,0.3,0.9)



def sortToResize(histlist,colorlist,dtildelist): 
# moves biggest histo to beginning of list, so axes will be sized accordingly


    tmp = sorted([[i,hist.GetMaximum()] for i,hist in enumerate(histlist)],key = lambda sublist: sublist[1], reverse = True)
    histlistout = [histlist[tmp[0][0]]]+[x for x in histlist if x!=tmp[0][1]]
    colorlistout = [colorlist[tmp[0][0]]]+[x for x in colorlist if x!=tmp[0][1]]
    dtildelistout = [dtildelist[tmp[0][0]]]+[x for x in dtildelist if x!=tmp[0][1]]
    return histlistout,colorlistout,dtildelistout
                    
    
oolistSorted,ooClistSorted,oodtildelistSorted = sortToResize(ooList,colorlist,dtildelist)
dplistSorted,dpClistSorted,dpdtildelistSorted = sortToResize(dPList,colorlist,dtildelist) 


for i,hist in enumerate(oolistSorted):
    hist.SetLineColor(ooClistSorted[i])
    hist.SetStats(0)
    #hist.Draw("HistSameE")
    #if i != 0:
    #    legendoo.AddEntry(hist,"#tilde{d} = "+str(oodtildelistSorted[i]),"l") 
hoo0.Draw("HistSameE")
legendoo.AddEntry(hoo0,"#tilde{d} = 0.0","l")
hoo1.Draw("HistSameE")
legendoo.AddEntry(hoo1,"#tilde{d} = -0.05","l")
hoo2.Draw("HistSameE")
legendoo.AddEntry(hoo2,"#tilde{d} = 0.2","l")


legendoo.Draw()

#canvasoo.SaveAs("oo_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+str(dtildelist[3])+str(dtildelist[4])+".pdf")
#canvasoo.SaveAs("oo_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+".pdf")
canvasoo.SaveAs("oo_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+".png")

canvasdP = ROOT.TCanvas("canvasdP")
canvasdP.cd()
canvasdP.SetGrid()
legenddP = ROOT.TLegend(0.1,0.65,0.3,0.9)

for i,hist in enumerate(dplistSorted):
    hist.SetLineColor(dpClistSorted[i])
    hist.SetStats(0)
    hist.Draw("HistSameE")
    if i != 0:
        legenddP.AddEntry(hist,"#tilde{d} = "+str(dpdtildelistSorted[i]),"l")

    
               
legenddP.Draw()

#canvasdP.SaveAs("delPhi_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+str(dtildelist[3])+str(dtildelist[4])+".pdf")
#canvasdP.SaveAs("delPhi_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+".pdf")
canvasdP.SaveAs("delPhi_"+ptCut+massCut+delEtaCut+"_d_"+str(dtildelist[0])+str(dtildelist[1])+str(dtildelist[2])+".png")
