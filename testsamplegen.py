import random
import timeit
from ROOT import TRandom3
def sample_gen(n, forbid):
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = random.randrange(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def sample_gen_TR3(n, forbid):
    rand3gen = TRandom3()
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = rand3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)

def sample_man(N,size_sp,n_sp):
    forbidden = []
    samples = []
    gen = TRandom3()
    for k in range(n_sp):
        sp = []
        for n in range(size_sp):
            
            i = gen.Integer(N)
            while i in forbidden:
                i = gen.Integer(N)
            sp.append(i)
            forbidden.append(i)
        samples.append(sp)
    return samples


def drawsps(N,size_sp,n_sp):
    forbidden = []
    samples = []
    gen0 = sample_gen(N, forbidden)
    for n in range(n_sp):
        sp = [gen0.__next__() for i in range(size_sp)]
        samples.append(sp)
    return samples

def drawspsTR3(N,size_sp,n_sp):
    forbidden = []
    samples = []
    gen0 = sample_gen_TR3(N, forbidden)
    for n in range(n_sp):
        sp = [gen0.__next__() for i in range(size_sp)]
        samples.append(sp)
    return samples
start1 = timeit.default_timer()
aa = drawsps(100,10,10)
end1 = timeit.default_timer()

print(end1-start1)

start2 = timeit.default_timer()
bb = drawspsTR3(100,10,10)
end2 = timeit.default_timer()
print(end2-start2)


for a in aa:
    print(a)
print("-----------------")
for b in bb: 
    print(b)

def test_noreplace(samples):
    nerr = -1
    for i,sp in enumerate(samples):
        for j,k in enumerate(sp):
            
            for i2,sp2 in enumerate(samples):
                if i2 == i:
                    tmp = sp2[0:j]+sp2[(j+1):len(sp2)]
                    tmp = tmp
                else:
                    tmp = sp2
                for k2 in tmp:
                    
                    if k == k2:
                        nerr+=1
    print (nerr)

test_noreplace(aa)
test_noreplace(bb)
