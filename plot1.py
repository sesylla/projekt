import sys  # Import the sys(tem) library for arguments
import ROOT # Import the ROOT library for many uses
import os

 
# Check that the user gave the correct number of arguments
if len(sys.argv) != 2: 
    # Wrong number of arguments, tell the user what you expected
    # Note that sys.argv[0] is how the user called the python script
    print "USAGE: %s <branch name > OUTPUT: same filename"%(sys.argv[0])
    # End the program
    sys.exit(1)

# Store the user arguments

histFileName ="histo_"+ sys.argv[1]+".root" #argv[1] ist branchname
os.chdir(histFileName[6:-5])
# Retrieve the histograms from the file
# Open the file in read-only mode
histFile  = ROOT.TFile.Open(histFileName,"READ")
# Get histograms
pTHisto = histFile.Get("pT")
etaHisto  = histFile.Get("eta")
phiHisto = histFile.Get("phi")
EHisto   = histFile.Get("E")

histList = [pTHisto,etaHisto,phiHisto,EHisto]
namelist = ["pT","eta","phi","Et"]

# Make sure you got the histograms
if not pTHisto:
    print "Failed to get pT data histogram from the input file"
    sys.exit(1)
if not etaHisto:
    print "Failed to get eta histogram from the input file"
    sys.exit(1)
if not phiHisto:
    print "Failed to get phi data histogram from the input file"
    sys.exit(1)
if not EHisto:
    print "Failed to get E histogram from the input file"
    sys.exit(1)

# Set the histograms to continue to exist after closing the file
for hist in histList:
    hist.SetDirectory(0)
# Close the input histogram file
histFile.Close()


for nr,hist in enumerate(histList):
    # Turn off the statistics box
    hist.SetStats(0)

    # Set axis labels
    hist.GetYaxis().SetTitle("Number of events")
    hist.GetXaxis().SetTitle(namelist[nr])

    # Prepare the canvas for plotting
    # Make a canvas
    canvas = ROOT.TCanvas("canvas")
    # Move into the canvas (so anything drawn is part of this canvas)
    canvas.cd()
    # Set the y-axis to be logarithmic
    #canvas.SetLogy(True)


    # Open the canvas for continuous printing
    # This works for a few file types, most notably pdf files
    # This allows you to put several plots in the same pdf file
    # The key is the opening square-bracket after the desired file name
 

# Draw the histogram, as a Histogram (solid lines)
    hist.Draw("h")


    canvas.SaveAs(histFileName[0:-5] + namelist[nr]+".pdf")

