from multiprocessing import Pool
import numpy as np
import scipy.stats
from scipy.optimize import minimize
from array import array
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar
from scipy.optimize import minimize_scalar
import csv
#fig, ax = plt.subplots(1, 1)




import ROOT
import sys
import os

if len(sys.argv) != 6:
    print ("USAGE: <source directory name> <dtilde> <N_samples> <Bkg ratio> <Norm 0=SMNorm, 1=N200>")
    sys.exit(1)

outdirname = "testres"
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")

dirname = sys.argv[1] 

dtilde = sys.argv[2]
N_sp = int(sys.argv[3])
Norm = int(sys.argv[5])
HistFileName =  sys.argv[1]+"/"+"AllSamples/"+sys.argv[1]+"_"+dtilde+".root"
templateHistFileName = sys.argv[1]+"/"+sys.argv[1]+".root"

HistFile = ROOT.TFile.Open(HistFileName,"READ")
templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")


#delPhiHist = HistFile.Get(delPhiHistName)
#oo1Hist = HistFile.Get(oo1HistName)

TGen = ROOT.TRandom3()
BGen = ROOT.TRandom3()
Background_Ratio = float(sys.argv[4])

#constants from fit N(d) = 200. + c*d**2
#c_oo = 3147.5214538421055
#c_dp = 3611.706482059846
#rebinned, but also with 255004: 
c_oo =  3147.521454469851  #+-  [0.61616453]
c_dp =  3611.706482060036  #+-  [0.43232619]


dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

print("******* index: ",dtildelist.index("0.00")) 



def backGen(b_ratio,nbins,dtilde,c,gen): #returns sum,diff arrays of background
    N = 200.+c*float(dtilde)**2
    #nbins = len(histarr)
    #nbins = hist.GetSize()-2
    #background is for full distribution, will have to be subtracted/added in Skellam Fit!
    b_list = np.array([gen.PoissonD(b_ratio*N/(nbins)) for i in range(nbins)])
    
    
    #print(b_list)
    
#print("backg_sum: ",b_arr_pos+b_mirr_pos)
    #print("backg_diff: ",b_arr_pos-b_mirr_pos)
    return b_list

def getnormfac(templateHistFile,dtilde,c):
    if c == c_oo:
        SMhist = templateHistFile.Get("oo1_dtilde"+"0.00" ).Clone()
        #SMhist = ROOT.TH1D("oo1_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    elif c == c_dp:
        SMhist = templateHistFile.Get("dp_dtilde"+"0.00" ).Clone() 
        #SMhist = ROOT.TH1D("dp_SM"," ",SMhist0.GetSize()-2,SMhist0.GetXaxis().GetXmin(),SMhist0.GetXaxis().GetXmax())
    ROOT.SetOwnership(SMhist,False)
    SMhist.SetDirectory(0)

    else:
        print("problem in getnormfac.")
        sys.exit(1)

    #for bin in range(SMhist.GetSize()):
    #    SMhist.SetBinContent(bin,SMhist0.GetBinContent(bin))
    #N_sample = sample_hist.Integral()
    #N_sample = np.sum(sample_hist[0]) -  Background_Ratio*(200.+c*float(dtilde)**2)/(2.*len(sample_hist[0]))
    #print("sample integral: ",N_sample)
    N_SM = 200.
    #N_SM = N_sample - c * float(dtilde)**2
    #print("N_SM : ",N_SM)
    #print("N_SM expectation: ",N_SM)
    SM_Integral = SMhist.Integral()
    #print("SM Integral: ", SM_Integral)
    #SM_Integral = np.sum(SMhist)

    #print("SM integral: ",SM_Integral)
    Normfac = N_SM/SM_Integral
    
    
    
    return Normfac
#Normalize templates to data

    
def makearray(hist): #returns numpy  array of data bins. 
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-2):
        bin += 1

        
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
        
    hist_data     = np.array(datalist,dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return hist_data #,hist_template

def maketemplatelist(dtildelist,templateHistFile,norm):
    #templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
   
    for dtilde in dtildelist:
        oo1HistName1= "oo1_dtilde"+dtilde 
        delPhiHistName1= "dp_dtilde"+dtilde
        
        oo1Hist1 = templateHistFile.Get(oo1HistName1).Clone()
        delPhiHist1 = templateHistFile.Get(delPhiHistName1).Clone()

        ROOT.SetOwnership(oo1Hist1,False)
        oo1Hist1.SetDirectory(0)
        ROOT.SetOwnership(delPhiHist1,False)
        delPhiHist1.SetDirectory(0)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if norm==1:
            oo1Hist1.Scale(200./oo1Hist1.Integral())
            delPhiHist1.Scale(200./delPhiHist1.Integral())
        if not oo1Hist1:
            print("histogram %s not found!"%(oo1tmplHistName1))
        if not delPhiHist1:
            print("histogram %s not found!"%(delPhitmplHistName1))
        
        
        oo1tmplarray = makearray(oo1Hist1)
        delPhitmplarray = makearray(delPhiHist1)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return np.array(oo1templatelist),np.array(delPhitemplatelist)


################# samples  ###########################






def sample_gen(n,randT3gen, forbid = []): 
# generate 1 random integer in range(n) without replacement.
# optional: forbid , list of generally forbidden numbers
    state = dict()
    track = dict()
    for (i, o) in enumerate(forbid):
        x = track.get(o, o)
        t = state.get(n-i-1, n-i-1)
        state[x] = t
        track[t] = x
        state.pop(n-i-1, None)
        track.pop(o, None)
    del track
    for remaining in range(n-len(forbid), 0, -1):
        i = randT3gen.Integer(remaining)
        yield state.get(i, i)
        state[i] = state.get(remaining - 1, remaining - 1)
        state.pop(remaining - 1, None)








def makesamplelist(N_samples,dtilde,sample_file,genT3): #returns list of sample histograms
    print ("drawing " , N_samples , "samples from file ", sample_file , " ...")
    #print(dtilde,type(dtilde))
    hnames,hnamesoo,hnamesdp = [],[],[]
    for key in sample_file.GetListOfKeys():
        h = key.ReadObj()
        if h.ClassName() == 'TH1D':
            hnames.append(h.GetName())
    #print(hnames)
    
    for s in hnames:
        if "oo1_dtilde"+dtilde in s:
            hnamesoo.append(s)
        elif "dp_dtilde"+dtilde in s:
            hnamesdp.append(s)
      
    #print (len(hnamesoo),len(hnamesdp))
    
    gen_oo = sample_gen(len(hnamesoo),genT3)
    gen_dp = sample_gen(len(hnamesdp),genT3)

    hsamplesoo,hsamplesdp = [],[]
    #i = 0
    for i in range(N_samples):
    #while i < N_
        i_sp_oo = gen_oo.__next__()
        i_sp_dp = gen_dp.__next__()
        #print("random indices :", i_sp_oo,i_sp_dp) 

        if sample_file.Get(hnamesoo[i_sp_oo]).Integral() != 0.:
            
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        else:
            i_sp_oo = gen_oo.__next__()
            ooh = sample_file.Get(hnamesoo[i_sp_oo])
        
        dph = sample_file.Get(hnamesdp[i_sp_dp])
        if sample_file.Get(hnamesdp[i_sp_dp]).Integral() != 0.:
            
            dph = sample_file.Get(hnamesdp[i_sp_dp])
        else:
            i_sp_dp = gen_dp.__next__()
            dph = sample_file.Get(hnamesdp[i_sp_dp])
            
        hsamplesoo.append( makearray(ooh))
        hsamplesdp.append( makearray(dph))
    #a = ROOT.TCanvas("a")
    #a.cd()
    #hsamplesoo[1].Draw("HistE")
    #a.SaveAs("sample0.png")
    #print("##### lenght of samples lists : ",len(hsamplesoo),len(hsamplesdp))
    return hsamplesoo,hsamplesdp

                         

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildelist,templates):
    #print "d value: ",d
    i=0
    dtildevals = [float(x) for x in dtildelist]
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
               
        return [x0,x1],[f0,f1]
    else:
        print( "dtilde out of range!")
        sys.exit(1)

def interpol(x,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print ("could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #print "x= %d == x0 = %d"%(x,x0)
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            #print "x= %d == x0 = %d"%(x,x1)
            f = f1
            #return f
        else:
                
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-x)/(x1-x0), (x-x0)/(x1-x0) )
            
            
            
    
    return f



############################################ Fit Functions ########################################################################################################
     
def lpdf(b,k,mu): #log(pdf)
    
    return -mu-b + k*np.log(mu+b)


def NLL(b,dtil,hist_data,template):
    res = 0
    
    
    
    f = lpdf(b,hist_data,template)    
    res+=f    
    #print(f)
    return -np.sum(res)



def minNLL(args):
    dtil,histarr,template,nr = args
    
    #print("####",dtilde)
    #print(histarr)
    #print(template)  
    
    b_start = np.array([Background_Ratio*sum(histarr)/len(histarr)])
    #b_start = np.array([20.])
    res = minimize(NLL,b_start,args=(dtil,histarr,template),method = 'SLSQP',bounds=[(1e-6,1000.)])
    if not res.success:
        print("fit failed at dtilde = ", dtil)
        print(res.message)
    #print(res.fun)
    #print(res.x)
    #print(res)
    #print(dtilde,res.x,res.fun)
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    #err = np.diag(res.hess_inv.todense())
    #print err
    #print res.hess_inv.todense()
    #print("result for x: %s +- ??" % (res.x[0]))
    #print("result for dtilde: %s +- ??" % (res.x[1]))
    return nr,res.fun,res.x[0] 
    

####### find minimum ######################
def findMin(ddlist,NLLlist):
    NLLmin = min(NLLlist)
    indexmin = NLLlist.index(NLLmin)
    d_val_min = ddlist[indexmin]
    if (indexmin == 0 or indexmin == len(NLLlist)-1):
        print("minimum at edge of parameter region!")
    #print(d_val_min)
    return d_val_min

################ find 68.3% confidence interval: #########################
def findConfInt(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
    tmp = [x-N**2/2. for x in NLLlist ]
    minimum = min(tmp)
    indexmin = tmp.index(minimum)
    
    dlow = ddlist[0:indexmin]
    dhi = ddlist[indexmin:(len(ddlist)-1)]
    NLLlow = tmp[0:indexmin]
    NLLhi = tmp[indexmin:(len(tmp)-1)]
    tmp_lo = [abs(x) for x in NLLlow]  
    tmp_hi = [abs(x) for x in NLLhi]

    if indexmin == 0:
        d_val_lo = -1.
        print("CI at lower edge of parameter region!")
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]

    elif indexmin == len(tmp)-1:
        d_val_hi  = 1.
        print("CI at upper edge of parameter region!")
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    else:
        
        index_lo = tmp_lo.index(min(tmp_lo))
        d_val_lo = dlow[index_lo]
    
        index_hi = tmp_hi.index(min(tmp_hi))
        d_val_hi = dhi[index_hi]
    
    #NLL_val_lo = NLLlow[index_lo]
    #NLL_val_hi = NLLhi[index_hi]
    return [d_val_lo,d_val_hi]#,[NLL_val_lo,NLL_val_hi]

#def findConfInt_inter1d(ddlist,NLLlist, N): # N is quantile of standard gaussian, i.e. number of std deviations
#    tmp = [x-N**2/2. for x in NLLlist ]
#    NLL_inter = interp1d(ddlist,tmp)
#    dtmp = np.arange(ddlist[0],ddlist[len(ddlist)-1],10000)
#    minimum = np.amin(interp1d(dtmp))
    
    
# get ratio of CI which cover true value:

def cover_ratio(estarr,err_arr_lo,err_arr_hi,dtilde):
    n_cover = 0
    for l,h in zip(estarr-err_arr_lo,estarr+err_arr_hi):
        if l <= float(dtilde) <= h:
            n_cover += 1
    return n_cover/len(err_arr_lo)
def deletezeros(indexlist,arr):
        
    tmp = [arr]
    n = -1
    for i,j in enumerate(indexlist):
        # j  have to be sorted positive indices
        n+=1
        tmp.append(np.delete(tmp[i],j-n))
    return tmp[-1]     


def estimate(dtildelist,samplelist,templateHistFile,dtilde,c,norm):
    #oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    
    dtildevals = np.array([float(x) for x in dtildelist])
    dd = np.linspace(float(dtilde)-0.2,float(dtilde)+0.2,1001)

    estlist,err1sig_lo,err1sig_hi,err2sig_lo,err2sig_hi = [],[],[],[],[]
    intlist,sampleintlist =[], []
    #if c == c_oo:
    #    templatelist = oo1templatelist
    #elif c == c_dp:
    #    templatelist = delPhitemplatelist
    #else:
    #    print("problem in estimate.")
    normfac = getnormfac(templateHistFile,dtilde,c,norm)
    p = Pool(20)
    
        #sampleintlist.append(sample.Integral())
        
        oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
    if norm == 0:
        if c == c_oo:
            templatelist = oo1templatelist
            templatelist *= normfac
        elif c == c_dp:
            templatelist = delPhitemplatelist
            templatelist *= normfac
            
        else:
            print("problem with normfac in estimate.")
            sys.exit(1)
    elif norm == 1:
        if c == c_oo:
            templatelist = oo1templatelist
        elif c == c_dp:
            templatelist = delPhitemplatelist

    f_inter = interp1d(dtildevals,templatelist,axis=0)
    templates = f_inter(dd)
    for k,sample in enumerate(samplelist):
        if np.sum(sample) == 0.:
            print("############# 0 found ##############")
            continue
        if k%500 == 0:
            print(k, " of ",len(samplelist), "samples processed. ")
        #if c == c_dp:
        #    sample0 = deletezeros([0,-1],sample)
        #    bkg_sum = backGen(Background_Ratio,len(sample0),dtilde,c,BGen)
        #    sum_sample = sample0 + bkg_sum
        #    
        #    
        #    
        #    templates1 = f_inter(dd)
        #    templates = np.array([deletezeros([0,-1],templ) for templ in templates1])
        #elif c== c_oo:

        bkg_sum = backGen(Background_Ratio,len(sample),dtilde,c,BGen)
        sum_sample = sample + bkg_sum
            
            
            
            
        
            
        
        #print("NORMFAC : ",normfac)
        
        NLLvals0=[]
        backg_list = []
        #print(sum_sample)
        #print(templates)
        args = [(d,templ,nr) for nr, (d,templ) in enumerate(zip(dd,templates))]
        fitresults = p.map(minNLL,args)
        fitresults.sort(key=lambda x : x[0])
        res_nr,res_nll,res_nerrs = [],[],[]
        for result in fitresults:
            NLLvals0.append(result[1])
            backg_list.append(result[2])
         
        #for d,templ in zip(dd,templates):
        #    m,b = minNLL(d,sum_sample,templ)
        #    NLLvals0.append(m)
        #    backg_list.append(b)
        
        

        min1 = min(NLLvals0)
        NLLvals = array("d",[x - min1 for x in NLLvals0])
        
        #plt.plot(dd,NLLvals)
        #plt.show()

        f_NLL = interp1d(dd,NLLvals,axis=0)

        dmin = findMin(dd,NLLvals)
        #dmin = minimize_scalar(f_NLL,method='bounded',bounds=[float(dtilde)-0.19,float(dtilde)+0.19]).x
        #print(dmin)
        #f_lim1sig = lambda x: f_NLL(x) - 0.5
        #f_lim2sig = lambda x: f_NLL(x) - 2.
        #dlim1_l = root_scalar(f_lim1sig , bracket = [float(dtilde)-0.19,dmin]).root
        #dlim1_h = root_scalar(f_lim1sig , bracket = [dmin,float(dtilde)+0.19]).root
        #dlim2_l = root_scalar(f_lim2sig , bracket = [float(dtilde)-0.19,dmin]).root
        #dlim2_h = root_scalar(f_lim2sig , bracket = [dmin,float(dtilde)+0.19]).root
        
        
        
        
        
        dlim1 = findConfInt(dd,NLLvals,1.)
        dlim2 = findConfInt(dd,NLLvals,2.)
        

        estlist.append(dmin)
        err1sig_lo.append(abs(dlim1[0]-dmin))
        err1sig_hi.append(abs(dlim1[1]-dmin))
        err2sig_lo.append(abs(dlim2[0]-dmin))
        err2sig_hi.append(abs(dlim2[1]-dmin))
        #err1sig_lo.append(abs(dlim1_l-dmin))
        #err1sig_hi.append(abs(dlim1_h-dmin))
        #err2sig_lo.append(abs(dlim2_l-dmin))
        #err2sig_hi.append(abs(dlim2_h-dmin))
          

        #estlist.append(dmin)
        #err1sig_lo.append(abs(dlim1[0]-dmin))
        #err1sig_hi.append(abs(dlim1[1]-dmin))
        #err2sig_lo.append(abs(dlim2[0]-dmin))
        #err2sig_hi.append(abs(dlim2[1]-dmin))
        
        #plt.plot(dd,NLLvals)
        #plt.show()

        #os.chdir(outdirname)
       
        
        #canvas = ROOT.TCanvas("canvas")
        #Legend = ROOT.TLegend(0.6,0.7,0.9,0.9)
            
            
        #reshist = interpol(dmin,dtildelist,templatelist)

        #canvas.cd()

        #sample.SetLineColor(4)
        #reshist.SetLineColor(2)
        #templatelist[dtildelist.index(dtilde)].SetLineColor(1)
        #sample.Draw("HistE")
        #reshist.Draw("HistSameE")
        #templatelist[dtildelist.index(dtilde)].Draw("HistSameE")
        #Legend.AddEntry(sample,"sample, d="+dtilde,"l")
        #Legend.AddEntry(reshist,"result, d="+str(dmin),"l")
        #Legend.AddEntry(templatelist[dtildelist.index(dtilde)],"dtilde= "+dtilde+" prediction","l")
        #Legend.Draw()
        #canvas.Update()
        #if c == c_dp:
        #    canvas.SaveAs("dpres"+str(k)+".png")
        #elif c == c_oo:
        #    canvas.SaveAs("oores"+str(k)+".png")
        #os.chdir("..")
            #print("Scaled SM Integral" ,templatelist[50].Integral())
            #print("sample Integral", sample.Integral())
            #print("reshist integral", reshist.Integral())

    #print("SM prediction Integrals : ",intlist)
    #print("sample integrals : ",sampleintlist)
    bck_av = sum(backg_list)*len(sum_sample)/len(backg_list)
    return np.array(estlist), np.array(err1sig_lo), np.array(err1sig_hi), np.array(err2sig_lo), np.array(err2sig_hi),bck_av





#delPhiHist.Sumw2()
#oo1Hist.Sumw2()





#hist_data = makearray(oo1Hist)
oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,templateHistFile)
samplelist_oo,samplelist_dp = makesamplelist(N_sp,dtilde,HistFile,TGen)




############## FIT  #################### 
print("oo fit in process ...")
oo_estarr ,oo_1sigarr_lo,oo_1sigarr_hi,oo_2sigarr_lo,oo_2sigarr_hi,oo_bkg = estimate(dtildelist,samplelist_oo,templateHistFile,dtilde,c_oo,Norm)
xxo = np.arange(len(oo_estarr))
#print(oo_estarr)

#print("dp fit in process ...")
print("delPhi fit in process ...") 
dp_estarr ,dp_1sigarr_lo,dp_1sigarr_hi,dp_2sigarr_lo,dp_2sigarr_hi,dp_bkg = estimate(dtildelist,samplelist_dp,templateHistFile,dtilde,c_dp,Norm)
xxd = np.arange(len(dp_estarr))
#print(dp_estarr)

print("done with fitting.")



print("#######################################################")
print("True Background events average (OO/delPhi) : ",Background_Ratio*(200.+c_oo*float(dtilde)**2),Background_Ratio*(200.+c_dp*float(dtilde)**2))
print("background estimators average OO/delPhi : ",oo_bkg,dp_bkg)
print("ratios of coverage:")
cov1oo =  cover_ratio(oo_estarr,oo_1sigarr_lo,oo_1sigarr_hi,dtilde)
cov2oo =  cover_ratio(oo_estarr,oo_2sigarr_lo,oo_2sigarr_hi,dtilde)
cov1dp =  cover_ratio(dp_estarr,dp_1sigarr_lo,dp_1sigarr_hi,dtilde)
cov2dp =  cover_ratio(dp_estarr,dp_2sigarr_lo,dp_2sigarr_hi,dtilde)
print("OO 1 sigma :", cov1oo)
print("OO 2 sigma :", cov2oo)
print("delPhi 1 sigma :", cov1dp)
print("delPhi 2 sigma :", cov2dp)
oomean = np.mean(oo_estarr) 
dpmean = np.mean(dp_estarr)
oovar = np.var(oo_estarr)
dpvar = np.var(dp_estarr)
oo1sigperc_lo = np.percentile(oo_estarr,(100.-68.27)/2.)
dp1sigperc_lo = np.percentile(dp_estarr,(100.-68.27)/2.)
oo2sigperc_lo = np.percentile(oo_estarr,(100.-95.44)/2.)
dp2sigperc_lo = np.percentile(dp_estarr,(100.-95.44)/2.)
#print(oo1sigperc_lo,oo2sigperc_lo)

oo1sigperc_hi = np.percentile(oo_estarr,100.-(100.-68.27)/2.)
dp1sigperc_hi = np.percentile(dp_estarr,100.-(100.-68.27)/2.)
oo2sigperc_hi = np.percentile(oo_estarr,100.-(100.-95.44)/2.)
dp2sigperc_hi =np.percentile(dp_estarr,100.-(100.-95.44)/2.)
oo1sigperc = [oo1sigperc_lo,oo1sigperc_hi]
oo2sigperc = [oo2sigperc_lo,oo2sigperc_hi]
dp1sigperc = [dp1sigperc_lo,dp1sigperc_hi]
dp2sigperc = [dp2sigperc_lo,dp2sigperc_hi]
print("sample means :",oomean,dpmean)
print("sample variances : ", oovar,dpvar)
print("min, max estimators :",(np.amin(oo_estarr),np.amax(oo_estarr)),(np.amin(dp_estarr),np.amax(dp_estarr)))
print("1sig percentiles oo/dp : ", oo1sigperc,dp1sigperc)
print("2sig percentiles oo/dp : ",oo2sigperc,dp2sigperc)
 
########### histograms of estimators ###########
os.chdir(outdirname)
coo = ROOT.TCanvas("oo")
h_est_oo = ROOT.TH1D("h_est_oo", " ", 20, oomean-0.1, oomean+0.1)
h_est_oo.SetStats(0)
for est in oo_estarr:
    h_est_oo.Fill(est)
coo.cd()
h_est_oo.Draw("Hist")



cdp = ROOT.TCanvas("dp")
h_est_dp = ROOT.TH1D("h_est_dp", " ", 20, dpmean-0.1, dpmean+0.1)
h_est_dp.SetStats(0)
for est in dp_estarr:
    h_est_dp.Fill(est)
cdp.cd()
h_est_dp.Draw("Hist")


########################## save data to csv file #################################
if not os.path.exists("N200PoisResults"):
    os.mkdir("N200PoisResults")
os.chdir("N200PoisResults")
coo.SaveAs("oo_est_pois_N200_"+dtilde+"_b"+sys.argv[4]+".png")
cdp.SaveAs("dp_est_pois_N200_"+dtilde+"_b"+sys.argv[4]+".png")
with open("PoisResults_N200_oo_"+dtilde+".csv", 'a') as csvfile:
    writer = csv.writer(csvfile,delimiter = ";")
    writer.writerow([Background_Ratio,oomean,oo1sigperc_lo,oo1sigperc_hi,oo2sigperc_lo,oo2sigperc_hi,cov1oo,cov2oo])
with open("PoisResults_N200_dp_"+dtilde+".csv", 'a') as csvfile:
    writer = csv.writer(csvfile,delimiter = ";")
    writer.writerow([Background_Ratio,dpmean,dp1sigperc_lo,dp1sigperc_hi,dp2sigperc_lo,dp2sigperc_hi,cov1dp,cov2dp] ) 
outfile = ROOT.TFile.Open("PoisRes_N200_"+dtilde+"_b"+sys.argv[4]+".root","UPDATE")
outfile.cd()
h_est_oo.Write()
h_est_dp.Write()
############# PLOT ####################
#plt.errorbar(xxo,oo_estarr,yerr=[oo_1sigarr_lo,oo_1sigarr_hi], fmt = "X")
#plt.title("OO 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxo,oo_estarr,yerr=[oo_2sigarr_lo,oo_2sigarr_hi], fmt = "X")
#plt.title("OO 2 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_1sigarr_lo,dp_1sigarr_hi], fmt = "X")
#plt.title("delPhi 1 sigma intervals")
#plt.grid()
#plt.show()

#plt.errorbar(xxd,dp_estarr,yerr=[dp_2sigarr_lo,dp_2sigarr_hi], fmt = "X")
#plt.title("delPhi 2 sigma intervals")
#plt.grid()
#plt.show()



