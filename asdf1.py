#!/usr/bin/python3
import scipy
import sys
import matplotlib
print(scipy.__version__)
print(sys.version)
print(matplotlib.__version__)
import numpy as np

x=range(5,10)
for i in x:
    print(i)

ci_list = [1,1,1,1,1,1,1,1]
v0=np.array([2]+[0 for j in range(len(ci_list))])
print(v0)
print(np.transpose([v0]))
print(np.zeros((len(ci_list)+1,len(ci_list))))
m0 = np.concatenate((np.transpose([v0]),np.zeros((len(ci_list)+1,len(ci_list)))),axis=1)
print(m0)
