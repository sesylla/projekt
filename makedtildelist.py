from numpy import linspace

# stepsize 0.1 
#x = linspace(-1.,1.,21)

#stepsize 0.05
#x = linspace(-1.,1.,41)

#stepsize 0.02
#x= linspace(-1.,1.,101)

#stepsize 0.01
#x = linspace(-1.,1.,201)

#dtildelist = []
#tmp=""
#for i in x:
#    tmp += "%.2f "%i
#    dtildelist.append("%.2f"%i)
#print(tmp)
#print(dtildelist)


#-------------------------------------------------------
#stepsize 0.001
x = linspace(-1.,1.,2001)

dtildelist = []
tmp=""
for i in x:
    tmp += "%.3f "%i
    dtildelist.append("%.3f"%i)
print(tmp)
print(dtildelist)
