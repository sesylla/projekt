import numpy as np
import scipy.stats
from scipy.special import ive
from scipy.optimize import minimize

import matplotlib.pyplot as plt
from array import array



import ROOT
import sys
import os

if len(sys.argv) != 5:
    print("USAGE:  <dtilde> <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)

outdirname = "SkP_Fit"

dtilde = sys.argv[1]
pTCut = sys.argv[2]
massCut = sys.argv[3]
delEtaCut = sys.argv[4]

diffdirname = "SMNorm_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
diffHistFileName =  diffdirname+"/"+diffdirname+".root"
sumHistFileName = "xadd_pos/"+"add_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut+".root"

templateHistFileName = "PartColl_0.01_255004/PartColl_0.01_255004.root"

#these are for diff Histo:
diffHistFile = ROOT.TFile.Open(diffHistFileName,"READ")
sumHistFile = ROOT.TFile.Open(sumHistFileName,"READ")

oo1diffHistName= "oo1Hist_diff"
delPhidiffHistName= "delPhiHist_diff"

oo1sumHistName= "oo1Hist_sum"
delPhisumHistName= "delPhiHist_sum"

#get ci from np.polyfit f(dtilde) = b0+ci0*dtilde , m fit_ci.py for 7 bins (oo) and 6 bins (delphi) 
#ci_dP = [62.655820798536404, 167.39103162015857, 219.98322077225208, 200.67283538274873, 118.06178774027215, 15.872996510756938]
#ci_oo = [256.67983255560193, 236.37825712913289, 119.23667786895192, 62.959889974809649, 38.537488339153683, 22.822194536286418, 15.927109714832714]
#cierrs_oo = [0.014809687012131858, 0.32214381862312058, 0.86966529602039822, 0.083212404446532898, 0.41484759582048286, 0.32982402622456186, 0.14999678081351228]
#cierrs_dP = [0.23031055831513322, 0.38261779078064623, 1.065673285609982, 1.3539410636085316, 0.15764765712520007, 0.041830112961138158]

#get ci from ROOT , f(d) = ci*dtilde 
#ci_oo = [256.67983278385395, 236.37825767729979, 119.23667793010004, 62.95989006987449, 38.5374883646205, 22.822194565256307, 15.927109712852996]
#cierrs_oo = [0.10114727481103129, 0.47791066265052545, 1.325244208935905, 0.121519260131247, 0.6092384350499789, 0.49167778487634556, 0.21988755312746203]
#ci_dP = [62.65582078434459, 167.39103132390883, 219.98322041200169, 200.67283530993552, 118.06178778977161, 15.87299651738405]
#cierrs_dP = [0.3282314216546463, 0.5719277016276167, 1.6232676206154149, 1.963689284461896, 0.25492507741552783, 0.08411006235445558]

#get ci from ROOT/weighted fit , f(d) = ci*dtilde  
#ci_oo = [256.6785525342061, 236.37907713029296, 119.12114602194927, 62.95975423204702, 38.50570707358028, 22.788586032282243, 15.920292676214464]
#cierrs_oo = [0.3492144157042296, 0.3038219946994339, 0.27138865937444406, 0.2242359768972409, 0.2098595731931647, 0.18869868072854062, 0.16710273240876863]
#ci_dP = [62.621184011423445, 167.37822081591364, 219.87805502573852, 200.44043538875417, 118.07634195597687, 15.915425877493842]
#cierrs_dP = [0.22876391924528644, 0.394759479662034, 0.5319958131021589, 0.4814260856404572, 0.29569873585697015, 0.10002887372845523]

#get ci from ROOT/weighted fit , f(d) = ci*dtilde + b 
ci_oo = [256.67985876604547, 236.37902929944582, 119.18474917359077, 62.95976291095352, 38.508098540335766, 22.803682056052256, 15.919852484639478]
cierrs_oo = [0.34921445655286226, 0.3038219949611821, 0.2714139513440568, 0.22423599451122544, 0.2098730747921924, 0.18872349525623347, 0.16710806526367844]
ci_dP = [62.62255827521881, 167.38329595698812, 219.93972800634108, 200.42929342082726, 118.06756711728525, 15.885299902836735]
cierrs_dP = [0.22881847404023611, 0.3947626204659464, 0.5320246854890918, 0.48166615436006305, 0.2957030309665562, 0.1000841089701317]



#get SM Norm
dataFile = ROOT.TFile.Open(templateHistFileName,"READ")   
SMhistoo1 = dataFile.Get("oo1Hist_d"+"0.00"+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut) )
SMhistdP = dataFile.Get("delPhiHist_d"+"0.00"+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut) )

Normfacoo1 = 200./SMhistoo1.Integral()
NormfacdP = 200./SMhistdP.Integral()
print("Norm factors: ",Normfacoo1,NormfacdP)

#dtildelist for templates:
#dtildelist = [ '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']

#dtildelist = ['-1.00', '-0.95', '-0.90', '-0.85', '-0.80', '-0.75', '-0.70', '-0.65', '-0.60', '-0.55', '-0.50', '-0.45', '-0.40', '-0.35', '-0.30', '-0.25', '-0.20', '-0.15', '-0.10', '-0.05', '0.00', '0.05', '0.10', '0.15', '0.20', '0.25', '0.30', '0.35', '0.40', '0.45', '0.50', '0.55', '0.60', '0.65', '0.70', '0.75', '0.80', '0.85', '0.90', '0.95', '1.00']

dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']





def makearray(hist): #for diff histo
    bincontents,datalist = [],[]
    for bin in range(hist.GetSize()-1):
        bin+=1
        bincontents.append(hist.GetBinContent(bin))
        #if hist.GetBinContent(bin)==0.:
        #    continue
        datalist.append(hist.GetBinContent(bin))
    data = np.array([datalist[i] for i in range(len(datalist)//2,len(datalist)-1) if not datalist[i] == 0.],dtype = np.double)
    print(bincontents)
    return data

def maketemplarray(hist): #returns numpy  array of arrays(posdata,mirrdata)
    datalist = []
    #templatelist = []
    
    for bin in range(hist.GetSize()-1):
        

        if hist.GetBinContent(bin) == 0.:
            continue
        datalist.append(hist.GetBinContent(bin))
        #templatelist.append(hist.GetBinContent(bin))
    mirrlist = [datalist[len(datalist)-i-1] for i in range(len(datalist))]    
    hist_data     = np.array([datalist[i] for i in range(len(datalist)/2,len(datalist)-1)],dtype = np.double)
    mirr_data = np.array([mirrlist[i] for i in range(len(mirrlist)/2,len(mirrlist)-1)],dtype = np.double)
    #hist_template = np.array(templatelist,dtype = np.double)
        
    return np.array([hist_data , mirr_data])

def maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName):
    templateHistFile = ROOT.TFile.Open(templateHistFileName,"READ")
    oo1templatelist, delPhitemplatelist = [],[]
    print(templateHistFileName)
    print("oo1Hist_d"+str(dtildelist[0])+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
    for dtilde in dtildelist:
        oo1tmplHistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        delPhitmplHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
        
        oo1Hist = templateHistFile.Get(oo1tmplHistName)
        delPhiHist = templateHistFile.Get(delPhitmplHistName)
        #oo1Hist.Sumw2()
        #delPhiHist.Sumw2()
        if not oo1Hist:
            print("histogram %s not found!"%(oo1tmplHistName))
        if not delPhiHist:
            print("histogram %s not found!"%(delPhitmplHistName))
        
        oo1Hist.Scale( 200/oo1Hist.Integral())
        delPhiHist.Scale( 200/delPhiHist.Integral())
        
        oo1tmplarray = maketemplarray(oo1Hist)
        delPhitmplarray = maketemplarray(delPhiHist)

        oo1templatelist.append(oo1tmplarray)
        delPhitemplatelist.append(delPhitmplarray)
        
    return oo1templatelist,delPhitemplatelist



    

######### interpolation from templates to arbitrary dtilde value###########################################################################
def findx0x1(d,dtildevals0,templates):
    #print "d value: ",d
    dtildevals = [float(j) for j in dtildevals0] 
    
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print d, [x0,x1]       
        return [x0,x1],[f0,f1]

    else:
        print("dtilde out of range!")
        #sys.exit(1)
        #return [np.nan,np.nan],[np.nan,np.nan]
        return np.nan
def interpol(x,x_list,f_list): #f_list is array of arrays, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    
    xvals,fvals = findx0x1(x,x_list,f_list)
        
    #print xvals,fvals
    if not (xvals and fvals):
        print("could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if x == x0:
                #tmp.append(f0)
            f = f0
            #return f
            #elif abs(x -x1) < 10**-6:
        elif x == x1:
                #tmp.append(f1)
            f = f1
            #return f
        else:
                
            f = (f0*(x1-x)/(x1-x0))+f1*(x-x0)/(x1-x0)
            
                    
                        
    #print x,[x0,x1]
    return f



############################################ Fit Functions ########################################################################################################
     
#def lpdf(k,mu1,mu2): #log(pdf)
#    return -(mu1+mu2)+(k/2.)*np.log(mu1/mu2)+2.*np.sqrt(mu1*mu2)+np.log(ive(abs(k),2.*np.sqrt(mu1*mu2)))

def lpdf(k_pl,k_min,dtilde,ci,slam): #log(pdf)
    mu1 = 0.5*(slam+ci*dtilde)
    mu2 = 0.5*(slam-ci*dtilde)
    #print mu1*mu2

    result = -(mu1+mu2)+(k_min/2.)*np.log(abs(mu1/mu2))+2.*np.sqrt(abs(mu1*mu2))+np.log(ive(abs(k_min),2.*np.sqrt(abs(mu1*mu2))))+k_pl*np.log(slam)-slam
    #if mu1*mu2 < 0.:
        #print slam,ci,dtilde
        #print(dtilde,ci,slam,mu1*mu2,-result)
        
    #else:
        #print "."
    
    return result


def NLL(params,sum_data,diff_data,ci_list):
    dtilde,sum_lam = params[0],params[1:]
    
    
    #print dtilde,del_lam
    res = 0
    
    
    #hist_templates = interpol(dtilde,dtildelist,templatelist)
   
    #hist_template1 = hist_templates[0]
    #hist_template2 = hist_templates[1]
    
    #sum_template = hist_template1+hist_template2
    
    

    #delPhihist_template = makearray(interpol(dtilde,dtildelist,delPhitemplatelist))
    
    for d_pl,d_min,ci, slam  in zip(sum_data,diff_data,ci_list,sum_lam):
        
        f = lpdf(d_pl,d_min,dtilde,ci, slam)
        res += f
    #print -res
    
    #print dtilde,sum_lam,-res
    #print ci_list
    #print dtilde*del_lam, sum_lam**2-del_lam**2
    return -res



#xx = np.linspace(1e-6,2,10)
#print(NLL(xx,hist_data,hist_template))

def minNLL(sum_data,diff_data,ci_list0):
    #print sum_data
    if not len(sum_data) == len(diff_data):
        print("error: sum_data and diff_data need to have same length. sum_data has len %s , diff_data has len %s"%(sum_data,diff_data))
        sys.exit(1)
    ci_list =np.array(ci_list0)
    print(ci_list)
    print(sum_data)
    print(diff_data)
    print("mean of diff_data:", sum(diff_data)/float(len(diff_data)))
    #print hist_data
    #print hist_template
    sumlam_start = [sumval for sumval in sum_data]
    dtilde_start = [sum(diff_data/ci_list)/len(diff_data)]
    #dtilde_start = [-0.01]
    startpar = np.array(dtilde_start+sumlam_start)
    print(startpar)


    #print startpar

    dtilde_lbnd = min([dtilde_start[0]-0.10,-1.])
    dtilde_ubnd = max([dtilde_start[0]+0.10,1.])

    sum_lam_bounds = [(1e-6,np.inf) for l in range(len(diff_data))]
    dtilde_bounds = [(dtilde_lbnd,dtilde_ubnd)]
    #cons = ({'type': 'ineq','fun': lambda x: (x[2:]**2-x[1]**2)},{'type': 'ineq','fun': lambda x: x[0]*x[1]})
    #cons=({'type': 'ineq','fun': lambda x: x[0]*x[1]})
    cons =({'type': 'ineq','fun': lambda x: (x[1:]**2-(x[0]*ci_list)**2)})

    
    #cons0=[]
    #for i,ci in zip(range(len(diff_data)),ci_list):
    #    cons0.append({'type' : 'ineq','fun' : lambda x: x[i+1]-ci*x[0]})
    #    cons0.append({'type' : 'ineq','fun' : lambda x: x[i+1]+ci*x[0]})
    #cons=tuple(cons0)
    res = minimize(NLL,startpar,method="SLSQP",args=(sum_data,diff_data,ci_list),bounds=dtilde_bounds+sum_lam_bounds,constraints = cons)
    
    print(res)
       
    
    #print "result for dtilde : " ,(resdtilde.x[0], "+-", np.sqrt(np.diag(resdtilde.hess_inv.todense())[0]))
    
#https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
# for single parameter this works
    #err = np.diag(res.hess_inv.todense())
    #print res.hess_inv.todense()
    #print err

    #print("result for dtilde: %s +- %s" % (res.x[0],err[0]))
    #print("result for del_lam: %s +- %s" % (res.x[1],err[1]))
    #print("result for dtilde: %s +- ??" % (res.x[1]))
    
    return res.x[0],res.x[1:]**2-(res.x[0]*ci_list)**2


# diff Histos:

delPhiDiffHist = diffHistFile.Get(delPhidiffHistName)

oo1DiffHist = diffHistFile.Get(oo1diffHistName)

#oo1DiffHist.Scale(0.5)
#delPhiDiffHist.Scale(0.5)

print("oo diff hist bin contents:")
oo1Diffarr = makearray(oo1DiffHist)
print("oo diff array:")
print(oo1Diffarr)
print("delPhi diff hist bin contents:")
delPhiDiffarr = makearray(delPhiDiffHist)
print("delPhi diff array:")
print(delPhiDiffarr)
# sum Histos:

delPhiSumHist = sumHistFile.Get(delPhisumHistName)

oo1SumHist = sumHistFile.Get(oo1sumHistName)
#delPhiHist.Sumw2()
#oo1Hist.Sumw2()
oo1SumHist.Scale( Normfacoo1)
delPhiSumHist.Scale( NormfacdP)
print("oo sum hist bin contents:")
oo1Sumarr = makearray(oo1SumHist)
print("oo sum array:")
print(oo1Sumarr)
print("delPhi sum hist bin contents:")
delPhiSumarr = makearray(delPhiSumHist)
print("delPhi sum array:")
print(delPhiSumarr)



# templates:
#oo1templatelist,delPhitemplatelist = maketemplatelist(dtildelist,pTCut,massCut,delEtaCut,templateHistFileName)









print("*************************oo1 Fit**************************")
minNLL(oo1Sumarr,oo1Diffarr,ci_oo)

print("************************delta Phi Fit**************************")
minNLL(delPhiSumarr,delPhiDiffarr,ci_dP)


def estdtildeplot(dtildelist): # plot estimate(dtilde)
    dtildevals1 = [float(d) for d in dtildelist]
    estvalsoo1,estvalsdP =[], []
    testoo1,testdP =[], []
    for d in dtildelist:
        diffdirname1 = "SMNorm_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+d+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
        diffHistFileName1 =  diffdirname1+"/"+diffdirname1+".root"
        sumHistFileName1 = "xadd_pos/"+"add_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+d+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut+".root"
        diffHistFile1 = ROOT.TFile.Open(diffHistFileName1,"READ")
        sumHistFile1 = ROOT.TFile.Open(sumHistFileName1,"READ")
        
        delPhiDiffHist1 = diffHistFile1.Get(delPhidiffHistName)
        oo1DiffHist1 = diffHistFile1.Get(oo1diffHistName)

        oo1Diffarr1 = makearray(oo1DiffHist1)
        delPhiDiffarr1 = makearray(delPhiDiffHist1)

        delPhiSumHist1 = sumHistFile1.Get(delPhisumHistName)
        oo1SumHist1 = sumHistFile1.Get(oo1sumHistName)

        oo1SumHist1.Scale( Normfacoo1)
        delPhiSumHist1.Scale( NormfacdP)
        
        oo1Sumarr1 = makearray(oo1SumHist1)
        delPhiSumarr1 = makearray(delPhiSumHist1)
        
        dtilde_est_oo1,fin_cons_oo1 = minNLL(oo1Sumarr1,oo1Diffarr1,ci_oo)
        dtilde_est_dP, fin_cons_dP = minNLL(delPhiSumarr1,delPhiDiffarr1,ci_dP)

        testoo1.append(fin_cons_oo1)
        testdP.append(fin_cons_dP)
        estvalsoo1.append(dtilde_est_oo1)
        estvalsdP.append(dtilde_est_dP)

    yoo1 = np.array(estvalsoo1)
    ydP = np.array(estvalsdP)
    x = np.array(dtildevals1)
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print(x)
    print(yoo1)
    print(ydP)
    print("constraint values of result must be >0 ")
    print("OO :")
    print(testoo1)
    print("delPhi :")
    print(testdP)


    oo1graph = ROOT.TGraph(len(x),x,(yoo1-x)/x)
    oo1graph.SetMarkerStyle(21)
    oo1graph.SetTitle("OO ML difference #frac{#tilde{d}_{ML}(#tilde{d}_{true}) - #tilde{d}_{true}}{#tilde{d}_{true}}  ; #tilde{d}_{true}  ; #frac{#tilde{d}_{ML}(#tilde{d}_{true}) - #tilde{d}_{true}}{#tilde{d}_{true}} " )
    oo1canv = ROOT.TCanvas("oo1canv")
    oo1canv.SetGrid()
    oo1canv.cd()
    ROOT.gStyle.SetTitleFontSize(0.02)
    oo1graph.Draw("AP")
    
    if os.path.exists(outdirname):
        pass
    else:
        os.mkdir(outdirname)
        print("directory "+outdirname+" created!")
    os.chdir(outdirname)
    oo1canv.SaveAs("rel_dtilde_estimators_oo1.png" )
    dPcanv = ROOT.TCanvas("dPcanv")
    dPcanv.SetGrid()
    dPgraph = ROOT.TGraph(len(x),x,(ydP-x)/x)
    dPgraph.SetMarkerStyle(21)
    dPgraph.SetTitle("#Delta #Phi_{jj} difference #frac{#tilde{d}_{ML}(#tilde{d}_{true}) - #tilde{d}_{true}}{#tilde{d}_{true}}  ; #tilde{d}_{true}  ;#frac{#tilde{d}_{ML}(#tilde{d}_{true}) - #tilde{d}_{true}}{#tilde{d}_{true}}" )
    dPcanv.cd()
    ROOT.gStyle.SetTitleFontSize(0.02)
    dPgraph.Draw("AP")
    dPcanv.SaveAs("rel_dtilde_estimators_dP.png")
    
#dtildelist = ['-1.00', '-0.90', '-0.80', '-0.70', '-0.60', '-0.50', '-0.40', '-0.30', '-0.20', '-0.10','-0.06','-0.04','-0.02','-0.01','0.01',  '0.10', '0.20', '0.30', '0.40', '0.50', '0.60', '0.70', '0.80', '0.90', '1.00']

#estdtildeplot(dtildelist)
        
