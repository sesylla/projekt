import ROOT
import sys
import os
import shutil
from numpy import sqrt

dtildelist = ['-1.00', '-0.99', '-0.98', '-0.97', '-0.96', '-0.95', '-0.94', '-0.93', '-0.92', '-0.91', '-0.90', '-0.89', '-0.88', '-0.87', '-0.86', '-0.85', '-0.84', '-0.83', '-0.82', '-0.81', '-0.80', '-0.79', '-0.78', '-0.77', '-0.76', '-0.75', '-0.74', '-0.73', '-0.72', '-0.71', '-0.70', '-0.69', '-0.68', '-0.67', '-0.66', '-0.65', '-0.64', '-0.63', '-0.62', '-0.61', '-0.60', '-0.59', '-0.58', '-0.57', '-0.56', '-0.55', '-0.54', '-0.53', '-0.52', '-0.51', '-0.50', '-0.49', '-0.48', '-0.47', '-0.46', '-0.45', '-0.44', '-0.43', '-0.42', '-0.41', '-0.40', '-0.39', '-0.38', '-0.37', '-0.36', '-0.35', '-0.34', '-0.33', '-0.32', '-0.31', '-0.30', '-0.29', '-0.28', '-0.27', '-0.26', '-0.25', '-0.24', '-0.23', '-0.22', '-0.21', '-0.20', '-0.19', '-0.18', '-0.17', '-0.16', '-0.15', '-0.14', '-0.13', '-0.12', '-0.11', '-0.10', '-0.09', '-0.08', '-0.07', '-0.06', '-0.05', '-0.04', '-0.03', '-0.02', '-0.01', '0.00', '0.01', '0.02', '0.03', '0.04', '0.05', '0.06', '0.07', '0.08', '0.09', '0.10', '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17', '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24', '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31', '0.32', '0.33', '0.34', '0.35', '0.36', '0.37', '0.38', '0.39', '0.40', '0.41', '0.42', '0.43', '0.44', '0.45', '0.46', '0.47', '0.48', '0.49', '0.50', '0.51', '0.52', '0.53', '0.54', '0.55', '0.56', '0.57', '0.58', '0.59', '0.60', '0.61', '0.62', '0.63', '0.64', '0.65', '0.66', '0.67', '0.68', '0.69', '0.70', '0.71', '0.72', '0.73', '0.74', '0.75', '0.76', '0.77', '0.78', '0.79', '0.80', '0.81', '0.82', '0.83', '0.84', '0.85', '0.86', '0.87', '0.88', '0.89', '0.90', '0.91', '0.92', '0.93', '0.94', '0.95', '0.96', '0.97', '0.98', '0.99', '1.00']
if len(sys.argv) != 2:
    print ("USAGE: <input directory name> . output will be in the same .root file.")
    sys.exit(1)



dirname = sys.argv[1]
filename = dirname+".root"
os.chdir(dirname)


oo1namelist,dpnamelist = [],[]


def subtrAddHistos(datafilename,dtildelist):
    dataFile = ROOT.TFile.Open(datafilename,"UPDATE")
    for d in dtildelist:
        oo1namelist.append("oo1_dtilde"+d)
        dpnamelist.append("dp_dtilde"+d)
    #get SM Norm
    
    SMhistoo1 = dataFile.Get("oo1_dtilde"+"0.00")
    SMhistdP = dataFile.Get("dp_dtilde"+"0.00" )

    Normfacoo1 = 200./SMhistoo1.Integral()
    NormfacdP = 200./SMhistdP.Integral()
    oo1difflist,oo1sumlist,dpdifflist,dpsumlist = [],[],[],[]
    for i,dtilde in enumerate(dtildelist):
        #print("dtilde ",dtilde)
        hoo1 = dataFile.Get(oo1namelist[i])
        hdp = dataFile.Get(dpnamelist[i])
        #print("*************")
        #print("before scaling: ",hdp.GetBinContent(10), hdp.GetBinContent(5))
        #print("errors : ", hdp.GetBinError(10), hdp.GetBinError(5))
        oo1_diff,oo1_sum = subtrAdd(hoo1,Normfacoo1,dtilde)
        oo1_diff.SetNameTitle("oo1_diff_dtilde"+dtilde,"oo1 difference")
        oo1_sum.SetNameTitle("oo1_sum_dtilde"+dtilde,"oo1 sum")
        
        dp_diff,dp_sum = subtrAdd(hdp,NormfacdP,dtilde)
        dp_diff.SetNameTitle("dp_diff_dtilde"+dtilde,"dp difference")
        dp_sum.SetNameTitle("dp_sum_dtilde"+dtilde,"dp sum")
        
        oo1difflist.append(oo1_diff)
        oo1sumlist.append(oo1_sum)
        dpdifflist.append(dp_diff)
        dpsumlist.append(dp_sum)
        
        #print("dtilde "+dtilde+" done.")
        #print("IST:")
        #print("dp bin 3 contents (hist/mirr/diff/sum) : ", hdp.GetBinContent(10), hdp.GetBinContent(5),dp_diff.GetBinContent(10), dp_sum.GetBinContent(10))
        #print("SOLL:")
        #print("dp bin 3 contents (hist/mirr/diff/sum) : ", hdp.GetBinContent(10), hdp.GetBinContent(5),hdp.GetBinContent(10)-hdp.GetBinContent(5), hdp.GetBinContent(10)+hdp.GetBinContent(5))
        
        #print("dp bin 3 errors (hist/mirr/diff/sum) : ", hdp.GetBinError(10), hdp.GetBinError(5),dp_diff.GetBinError(10), dp_sum.GetBinError(10))
        #print("err SOLL:" ,sqrt(hdp.GetBinError(10)**2+ hdp.GetBinError(5)**2))
    
    dataFile.cd()
    for hlist in [oo1difflist,oo1sumlist,dpdifflist,dpsumlist]:
        for h in hlist:
            h.Write()
    print("diff/sum done.")        
    return



def subtrAdd(Hist,normfac,d):

    #Hist.Sumw2()
    
    Hist.Scale( normfac)
    
    
    
    

    Hist_mirr = ROOT.TH1D("Hist_mirr_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    
    Hist_mirr.Sumw2()  
    #Hist_mirr.SetEntries(Hist.GetEntries())
        
    for bin in range(Hist.GetSize()):
        
        Hist_mirr.SetBinContent(bin,Hist.GetBinContent(Hist.GetSize()-bin-1))
        Hist_mirr.SetBinError(bin,Hist.GetBinError(Hist.GetSize()-bin-1))
      
        
    
    Hist_diff = ROOT.TH1D("Hist_diff_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_sum = ROOT.TH1D("Hist_sum_dtilde"+d," ",Hist.GetSize()-2,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    Hist_diff.Sumw2()
    Hist_sum.Sumw2()
    
    Hist_diff.Add(Hist,Hist_mirr, 1, -1)
    Hist_sum.Add(Hist,Hist_mirr, 1, 1)
    
    

      
    Hist_diff.SetStats(0)
    Hist_sum.SetStats(0)
    

    #Hist_diff.GetXaxis().SetRange(Hist_diff.GetSize()//2,Hist_diff.GetSize()-2)
    #Hist_sum.GetXaxis().SetRange(Hist_sum.GetSize()//2,Hist_sum.GetSize()-2)
    
        
    return Hist_diff,Hist_sum
  

subtrAddHistos(filename,dtildelist)
