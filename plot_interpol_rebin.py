
import ROOT
import sys
import os

if len(sys.argv) != 6:
    print("USAGE: <source directory name> <dtildelist: [d_templ0,d_templ1,d_interpol]> <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)
def stringToList(dtildeString):
    
    tmplist  =  dtildeString.strip("[]").split(",")
    print( tmplist)
    dtildelist = [float(x) for x in tmplist] 
    

    return tmplist,dtildelist

dirname = sys.argv[1] 
HistFileName =  sys.argv[1]+"/"+sys.argv[1]+".root"
dtildestrlist,dtildelist = stringToList(sys.argv[2])

#dtildelist = ["0.20","0.30","0.25"]
pTCut = sys.argv[3]
massCut = sys.argv[4]
delEtaCut = sys.argv[5]

colorlist = [1,4,6]

outdirname = "InterPlots"

def findx0x1(d,dtildevalslist,templates):
    #print "d value: ",d
    dtildevals = sorted([float(x) for x in dtildevalslist])
    i=0
    while (dtildevals[i] <= d and i < len(dtildevals)-1):
        i += 1
        #print i
        #print dtildevals[i]
    if 0 < i <= len(dtildevals)-1:
        x0 = dtildevals[i-1]
        x1 = dtildevals[i]
        f0 = templates[i-1]
        f1 = templates[i]
        #print "x0,x1,x in findx0x1: ",[x0,x1,d]       
        return [x0,x1],[f0,f1]
    else:
        print("dtilde out of range!")
        sys.exit(1)

def interpol(d,x_list,f_list): #f_list is list of histograms, x_list list of corresp. x values
    #tmp=[]
    #for x in xx:
    print( "********* x = ", d , " **************")
    xvals0,fvals = findx0x1(d,x_list,f_list)
    
    xvals = [float(x) for x in xvals0]  
    #print x

    #print xvals,d
    #print xvals,fvals
    if not (xvals and fvals):
        print("could not find x values for interpolation.")
        sys.exit(1)
    else:
        x0 = xvals[0]
        x1 = xvals[1]
        f0 = fvals[0]
        f1 = fvals[1]
        print("x0,x1,x: ",[x0,x1,d])
    #f = f0+(x-x0)*(f1-f0)/(x1-x0)

            #if abs(x - x0) < 10**-6:
        if d == x0:
                #tmp.append(f0)
            print("x == x0",[x0,d])
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            f.Add(f0)
            return f
            
        elif d == x1:
            print("x == x1",[x1,d]    )
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            f.Add(f1)
            return f

        else:
                #f = f0*(x1-x)/(x1-x0) + f1*(x-x0)/(x1-x0)
            print("x inter",[x0,x1,d])
            f = ROOT.TH1D("interpol"," ",f0.GetSize()-2,f0.GetXaxis().GetXmin(),f0.GetXaxis().GetXmax() )
            
            f.Add( f0, f1, (x1-d)/(x1-x0), (d-x0)/(x1-x0) )
            
            
            return f


############not used yet: #################################################################################################################################
def writeHistosToFile(histlist,outFileName):    
    if os.path.exists(outFileName):
        #shutil.rmtree(outFileName)
        #print "overwriting dir "+outFileName+"  ........"
        pass
    #os.mkdir(outFileName)
    #print "directory "+outFileName+" created!"
    os.chdir(outFileName)

    
    outHistFile = ROOT.TFile.Open(outFileName+".root","RECREATE")
    outHistFile.cd()
    for hist in histlist:
        hist.Write()
    outHistFile.Close()
###############################################################################
print( "current directory: ",os.getcwd())
histFile  = ROOT.TFile.Open(HistFileName,"READ")
if not histFile:
    print( "histfile not found")
print( "oo1Hist_d"+dtildestrlist[0]+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))
print( "oo1Hist_d"+dtildestrlist[1]+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut))

hoo0 = histFile.Get("oo1_dtilde"+dtildestrlist[0])
hoo1 = histFile.Get("oo1_dtilde"+dtildestrlist[1])

if not hoo0:
    print( "hoo0 not found")
if not hoo1:
    print( "hoo1 not found")
#hoo0.Sumw2()
#hoo1.Sumw2()
print( HistFileName)
hoo0.Scale(200/hoo0.Integral())
hoo1.Scale(200/hoo1.Integral())
print( dtildelist[2],dtildelist[0:2],[hoo0,hoo1])
hoo_inter = interpol(dtildelist[2],dtildelist[0:2],[hoo0,hoo1])
print( "Integral_interp_oo: ", hoo_inter.Integral())
ooList = [hoo0,hoo1,hoo_inter]


hdP0 = histFile.Get("dp_dtilde"+dtildestrlist[0])
hdP1 = histFile.Get("dp_dtilde"+dtildestrlist[1])

#hdP0.Sumw2()
#hdP1.Sumw2()
hdP0.Scale(200/hdP0.Integral())
hdP1.Scale(200/hdP1.Integral())

hdP_inter = interpol(dtildelist[2],dtildelist[0:2],[hdP0,hdP1])
print( "Integral_interp_dP: ", hdP_inter.Integral())
dPList = [hdP0,hdP1,hdP_inter]


def sortToResize(histlist,colorlist,dtildelist): 
# moves biggest histo to beginning of list, so axes will be sized accordingly


    tmp = sorted([[i,hist.GetMaximum()] for i,hist in enumerate(histlist)],key = lambda sublist: sublist[1], reverse = True)
    histlistout = [histlist[tmp[0][0]]]+[x for x in histlist if x!=tmp[0][1]]
    colorlistout = [colorlist[tmp[0][0]]]+[x for x in colorlist if x!=tmp[0][1]]
    dtildelistout = [dtildelist[tmp[0][0]]]+[x for x in dtildelist if x!=tmp[0][1]]
    return histlistout,colorlistout,dtildelistout
                    
    
oolistSorted,ooClistSorted,oodtildelistSorted = sortToResize(ooList,colorlist,dtildelist)
dplistSorted,dpClistSorted,dpdtildelistSorted = sortToResize(dPList,colorlist,dtildelist) 


#oolistSorted,ooClistSorted,oodtildelistSorted = ooList,colorlist,dtildelist
#dplistSorted,dpClistSorted,dpdtildelistSorted = dPList,colorlist,dtildelist
#print oolistSorted,ooClistSorted,oodtildelistSorted
#print dplistSorted,dpClistSorted,dpdtildelistSorted
if os.path.exists(outdirname):
        pass
else:
    os.mkdir(outdirname)
    print( "directory "+outdirname+" created!")
os.chdir(outdirname)

    
#####oo1 plot for interpolation:

canvasoo = ROOT.TCanvas("canvasoo")
canvasoo.cd()
canvasoo.SetGrid()

legendoo = ROOT.TLegend(0.1,0.7,0.4,0.9)
legendoo.SetTextSize(0.035)
print("oo list sorted:", oolistSorted)
for i,hist in enumerate(oolistSorted):
    hist.SetLineColor(ooClistSorted[i])
    hist.SetStats(0)
    hist.Draw("HistSameE")
    if i != 0:
        if str(oodtildelistSorted[i]) == dtildestrlist[2]:
            legendoo.AddEntry(hist,"#tilde{d} = "+str(oodtildelistSorted[i])+" (interpolated)","l")
        else:
            legendoo.AddEntry(hist,"#tilde{d} = %.2f"%(oodtildelistSorted[i]),"l")
    else:
        #hist.SetTitle(" OO  "+", linear interpolation to #tilde{d} = %.2f ; OO ; Events" % (dtildelist[2])) 
        hist.SetTitle(" ; OO ; Events") 
legendoo.Draw()

canvasoo.SaveAs("interpol_oo_d_"+str(dtildelist[2])+".pdf")
#canvasoo.SaveAs("interpol_oo_"+pTCut+massCut+delEtaCut+"_d_"+str(dtildelist[2])+".png")
 
#####oo1 plot to compare with original dtilde template
canvasCompoo = ROOT.TCanvas("canvasCompoo")
legendCompoo = ROOT.TLegend(0.1,0.7,0.4,0.9)
legendCompoo.SetTextSize(0.035)
hooComp = histFile.Get("oo1_dtilde"+dtildestrlist[2])

if not hooComp:
    print("no original template found!")

canvasCompoo.cd()
canvasCompoo.SetGrid()

#hooComp.Sumw2()
hooComp.Scale(200/hooComp.Integral())

hooComp.SetLineColor(1)
hooComp.SetStats(0)
hooComp.SetTitle(" ; OO ; Events" )   


hooComp.Draw("HistE")
legendCompoo.AddEntry(hooComp,"#tilde{d} = "+str(dtildelist[2])+" (true)","l")
hoo_inter.SetLineColor(6)
hoo_inter.Draw("HistSameE")
legendCompoo.AddEntry(hoo_inter,"#tilde{d} = "+str(dtildelist[2])+" (interpolated)","l")
legendCompoo.Draw()
canvasCompoo.SaveAs("interpol_comp_oo_d_"+str(dtildelist[2])+".pdf")
#canvasCompoo.SaveAs("interpol_comp_oo_"+pTCut+massCut+delEtaCut+"_d_"+str(dtildelist[2])+".png")
print( "OO Kolmogorov Test: ", hooComp.KolmogorovTest(hoo_inter))
##### delta Phi plot for interpolation
canvasdP = ROOT.TCanvas("canvasdP")
canvasdP.cd()
canvasdP.SetGrid()

#legenddP = ROOT.TLegend(0.7,0.7,0.9,0.9)
legenddP = ROOT.TLegend(0.1,0.7,0.4,0.9)
legenddP.SetTextSize(0.035)
for i,hist in enumerate(dplistSorted):
    hist.SetLineColor(dpClistSorted[i])
    hist.SetStats(0)
    hist.Draw("HistSameE")
    if i != 0:
        if str(dpdtildelistSorted[i]) == dtildestrlist[2]:
            legenddP.AddEntry(hist,"#tilde{d} = "+str(dpdtildelistSorted[i])+" (interpolated)","l")
        else:
            legenddP.AddEntry(hist,"#tilde{d} = %.2f"%(dpdtildelistSorted[i]),"l")
    else:
        hist.SetTitle("  ; signed #Delta#Phi_{jj} ; Events")
   
               
legenddP.Draw()

canvasdP.SaveAs("interpol_delPhi_d_"+str(dtildelist[2])+".pdf")
#canvasdP.SaveAs("interpol_delPhi_"+pTCut+massCut+delEtaCut+"_d_"+str(dtildelist[2])+".png")

####### delta Phi plot to compare with original dtilde template

canvasCompdP = ROOT.TCanvas("canvasCompdP")
#legendCompdP = ROOT.TLegend(0.6,0.7,0.9,0.9)
legendCompdP = ROOT.TLegend(0.1,0.7,0.4,0.9)
legendCompdP.SetTextSize(0.035)
hdPComp = histFile.Get("dp_dtilde"+dtildestrlist[2])

canvasCompdP.cd()
canvasCompdP.SetGrid()
#canvasCompdP.SetYTitle("N")
#canvasCompdP.SetXTitle("OO")
hdPComp.Sumw2()
hdPComp.Scale(200/hdPComp.Integral())

hdPComp.SetLineColor(1)
hdPComp.SetStats(0)
hdPComp.SetTitle(" ; signed #Delta#Phi_{jj} ; Events" )
hdPComp.Draw("HistE")
legendCompdP.AddEntry(hdPComp,"#tilde{d} = "+str(dtildelist[2])+" (true)","l")
hdP_inter.SetLineColor(6)
hdP_inter.Draw("HistSameE")
legendCompdP.AddEntry(hdP_inter,"#tilde{d} = "+str(dtildelist[2])+" (interpolated)","l")
legendCompdP.Draw()
canvasCompdP.SaveAs("interpol_comp_delPhi_d_"+str(dtildelist[2])+".pdf")
#canvasCompdP.SaveAs("interpol_comp_delPhi_"+pTCut+massCut+delEtaCut+"_d_"+str(dtildelist[2])+".png")
print( "delPhi Kolmogorov Test: ", hdPComp.KolmogorovTest(hdP_inter))


