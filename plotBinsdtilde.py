#from mpl_toolkits.mplot3d import axes3d, Axes3D

import numpy as np
import scipy.stats
from scipy.special import iv
from scipy.optimize import minimize
import matplotlib.pyplot as plt
from matplotlib import cm


#fig, ax = plt.subplots(1, 1)


import ROOT
import sys
#import os

if len(sys.argv) != 6:
    print ("USAGE: <0 = counts / 1 = difference / 2 = sum> < 0=Unnormalized / 1 = Normalized > <pT Cut> <mass cut> <delta eta cut>")
    sys.exit(1)

diff = int(sys.argv[1])
norm = int(sys.argv[2])
ptcut = sys.argv[3]
masscut = sys.argv[4]
deletacut = sys.argv[5]

PartCollDirName = "PartColl_0.01_255004"





#dtildelist for templates:
dtildelist = ['-1.00', '-0.98', '-0.96', '-0.94', '-0.92', '-0.90', '-0.88', '-0.86', '-0.84', '-0.82', '-0.80', '-0.78', '-0.76', '-0.74', '-0.72', '-0.70', '-0.68', '-0.66', '-0.64', '-0.62', '-0.60', '-0.58', '-0.56', '-0.54', '-0.52', '-0.50', '-0.48', '-0.46', '-0.44', '-0.42', '-0.40', '-0.38', '-0.36', '-0.34', '-0.32', '-0.30', '-0.28', '-0.26', '-0.24', '-0.22', '-0.20', '-0.18', '-0.16', '-0.14', '-0.12', '-0.10', '-0.08', '-0.06', '-0.04', '-0.02', '0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12', '0.14', '0.16', '0.18', '0.20', '0.22', '0.24', '0.26', '0.28', '0.30', '0.32', '0.34', '0.36', '0.38', '0.40', '0.42', '0.44', '0.46', '0.48', '0.50', '0.52', '0.54', '0.56', '0.58', '0.60', '0.62', '0.64', '0.66', '0.68', '0.70', '0.72', '0.74', '0.76', '0.78', '0.80', '0.82', '0.84', '0.86', '0.88', '0.90', '0.92', '0.94', '0.96', '0.98', '1.00']


def makediffarrs(dtildelist,pTCut,massCut,delEtaCut,Norm,Diff):
    binarroo1,binarrdP,binerrsoo1,binerrsdP = [],[],[], [] #[[first bins for all dtilde],[second bins],....]
    if Diff == 1:
        for i,dtilde in enumerate(dtildelist):
            if Norm == 1:
                dirname = "new_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
            elif Norm == 0:
                dirname = "xunn_new_diff_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut
            else:
                print("Normalize has to be 0 or 1")
                sys.exit(1)
    
            HistFileName =  dirname+"/"+dirname+".root"
        #these are for diff Histo:
            HistFile = ROOT.TFile.Open(HistFileName,"READ")

            oo1HistName= "oo1Hist_diff"
            delPhiHistName= "delPhiHist_diff"

            oo1Hist = HistFile.Get(oo1HistName)
            delPhiHist = HistFile.Get(delPhiHistName)
        
            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])
            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))
    
    elif Diff == 0:
        HistFileName = PartCollDirName+"/"+PartCollDirName+".root"
        HistFile = ROOT.TFile.Open(HistFileName,"READ")
        for i,dtilde in enumerate(dtildelist):
            oo1HistName= "oo1Hist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)
            delPhiHistName= "delPhiHist_d"+str(dtilde)+"_pT"+str(pTCut)+"_mjj"+str(massCut)+"_delEta"+str(delEtaCut)

            oo1Hist = HistFile.Get(oo1HistName)
            delPhiHist = HistFile.Get(delPhiHistName)
        
            if Norm == 1:
                oo1Hist.Scale(200/oo1Hist.Integral())
                delPhiHist.Scale(200/delPhiHist.Integral())
        
            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])
            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))
    elif Diff == 2:
        

        for i,dtilde in enumerate(dtildelist):
            
            if Norm == 0:
                dirname = "xadd_pos"
                HistFileName = dirname+"/"+"add_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut+".root"
            elif Norm == 1:
                dirname = "add_pos"
                HistFileName = dirname+"/"+"add_pos_PartColl_0.01_"+pTCut+massCut+delEtaCut+"_dtilde"+dtilde+"_pTcut"+pTCut+"_massCut"+massCut+"_delEtaCut"+delEtaCut+".root"
            HistFile = ROOT.TFile.Open(HistFileName,"READ")
            oo1Hist = HistFile.Get("oo1Hist_sum")
            delPhiHist = HistFile.Get("delPhiHist_sum")

            if i==0:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1.append([oo1Hist.GetBinContent(bin)])
                    binerrsoo1.append([oo1Hist.GetBinError(bin)])
                for bin in range(delPhiHist.GetSize()):
                    binarrdP.append([delPhiHist.GetBinContent(bin)])
                    binerrsdP.append([delPhiHist.GetBinError(bin)])

            else:
                for bin in range(oo1Hist.GetSize()):
                    binarroo1[bin].append(oo1Hist.GetBinContent(bin))
                    binerrsoo1[bin].append(oo1Hist.GetBinError(bin))
                for bin in range(delPhiHist.GetSize()):
                    binarrdP[bin].append(delPhiHist.GetBinContent(bin))
                    binerrsdP[bin].append(delPhiHist.GetBinError(bin))

    return binarroo1,binarrdP,binerrsoo1,binerrsdP


def makeplotoo1(dtildelist,binarr,errarr):
    
    
    for bin in range(len(binarr)):
        plt.errorbar(dtildelist,binarr[bin],yerr=errarr[bin],xerr=None,label="bin "+str(bin))
    plt.legend(loc='best', frameon=False)
    plt.grid()
    plt.title("OO")
    plt.xlabel(r"$\tilde{d}$")
    plt.ylabel(r"Bin Content")
    plt.show()

def makeplotdP(dtildelist,binarr,errarr):
    
    
    for bin in range(len(binarr)):
        plt.errorbar(dtildelist,binarr[bin],yerr=errarr[bin],xerr=None,label="bin "+str(bin))
    plt.legend(loc='best', frameon=False)
    plt.grid()
    plt.title(r"$\Delta \Phi_{jj}$")
    plt.xlabel(r"$\tilde{d}$")
    plt.ylabel(r"Bin Content")
    plt.show()




binarroo10,binarrdP0,binerrsoo10,binerrsdP0 = makediffarrs(dtildelist,ptcut,masscut,deletacut,norm,diff)


dtildevals = [float(dtildelist[d]) for d in range(len(dtildelist))] 
binarroo1 = [[binarroo10[i][n] for n in range(len(dtildelist))] for i in range(len(binarroo10)//2,len(binarroo10))]
binarrdP = [[binarrdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binarrdP0)//2,len(binarrdP0))]
binerrsoo1 = [[binerrsoo10[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsoo10)//2,len(binerrsoo10))]
binerrsdP = [[binerrsdP0[i][n] for n in range(len(dtildelist))]for i in range(len(binerrsdP0)//2,len(binerrsdP0))]



makeplotoo1(dtildevals,binarroo1,binerrsoo1)

makeplotdP(dtildevals,binarrdP,binerrsdP)
