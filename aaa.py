import ROOT
dataFile = ROOT.TFile("/home/ss944/projekt/daten/mc16a/group.phys-higgs.Htt_lh_V02.mc16_13TeV.346191.PoPy8_NNPDF30_VBFH125_tautaulm15hp20.D2.e7259_s3126_r9364_p3759.smPre_w_2_HS/DAOD_HIGG4D2.17261780._000001.pool.root.1.root")
tree = dataFile.Get("NOMINAL")

for i in [15322,15330,15340,826]:
    tree.GetEntry(i)
    print "+++++++++++++++event number "+str(i)+" ++++++++++++++++++++++++++"
    print "truth_event_info_pdgId1/2:********************"
    flavour1In = tree.truth_event_info_pdgId1;                          #flavour of incoming/outgoing parton n
    flavour2In = tree.truth_event_info_pdgId2;
    print [flavour1In, flavour2In]                           #flavour assignment: t = 6  b = 5 c = 4, s = 3, u = 2, d = 1
    print "truth_reweight_info_parton_0/1/2/3/4_pdgId:*****************"   
    flavour0Out = tree.truth_reweight_info_parton_0_pdgId;                         #anti-qarks with negative sign
    flavour1Out = tree.truth_reweight_info_parton_1_pdgId;                          #gluon = 0 
    flavour2Out = tree.truth_reweight_info_parton_2_pdgId;
    flavour3Out = tree.truth_reweight_info_parton_3_pdgId;
    flavour4Out = tree.truth_reweight_info_parton_4_pdgId;
    print [flavour0Out,flavour1Out,flavour2Out,flavour3Out,flavour4Out]
    print "jet_0/1/2_wztruth_pdgid:*************************"
    flavour0Out = tree.jet_0_wztruth_pdgid;                         
    flavour1Out = tree.jet_1_wztruth_pdgid;                       
    flavour2Out = tree.jet_2_wztruth_pdgid;
    print [flavour0Out,flavour1Out,flavour2Out]
    print "jet_0/1/2_flavorlabel_part:*********************************"
    flavour0Out = tree.jet_0_flavorlabel_part;                         
    flavour1Out = tree.jet_1_flavorlabel_part;                       
    flavour2Out = tree.jet_2_flavorlabel_part;
    print [flavour0Out,flavour1Out,flavour2Out]
    print "truth_reweight_info_higgs_mother_pdgId:***********************"
    print [tree.truth_reweight_info_higgs_mother_pdgId]
