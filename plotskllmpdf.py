from scipy.special import ive
from scipy.special import iv
from scipy.special import gamma
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import skellam





def skPDF(x,mu1,mu2):
    #return np.exp(-(mu1+mu2))*(mu1/mu2)**(x/2)*np.exp(2*np.sqrt(mu1*mu2))*ive(x,2*np.sqrt(mu1*mu2))
    return np.exp(-(mu1+mu2))*(mu1/mu2)**(x/2)*iv(abs(x),2*np.sqrt(mu1*mu2))
def poisPDF(k,mu1,mu2):
    return (mu1-mu2)**k*np.exp(-(mu1-mu2))/abs(gamma(k+1))


fig, ax = plt.subplots(1, 1)
#dtilde 0.20:
#Fit Result in Bin 5 (Counting from 0) :  [ 0.85419867]  +-  [ 0.37709084]
#Fit Result in Bin 0 :  [ 0.95356355]  +-  [ 0.09502745]

#dtilde 0.50:
#Fit Result in Bin 6 :  [ 0.45070484]  +-  [ 0.26506962], diff 1.75039991, mu1 4.41698587, mu2 2.66658596


dtilde = 0.50
bin = 0
res = 0.45070484
err = 0.26506962
mu1 = 4.41698587
mu2 = 2.66658596
diff = 1.75039991
xfit = res
xfit1= res+err
xfit2 = res-err

mean, var, skew, kurt = skellam.stats(mu1, mu2, moments='mvsk')

x = np.arange(skellam.ppf(0.01, mu1, mu2),
              skellam.ppf(0.99, mu1, mu2))
xx = np.linspace(skellam.ppf(0.01, mu1, mu2),skellam.ppf(0.99, mu1, mu2),500)

#xx = np.linspace(-14.,14.)

ax.plot(x, skellam.pmf(x, mu1, mu2), 'bo', ms=8, label='skellam pmf')
ax.plot(xx,skPDF(xx,mu1,mu2),'-',label='skellam PDF')
#ax.plot(xx,poisPDF(xx,mu1,mu2),'-',label = 'poisson PDF')
#ax.vlines(x, 0, skellam.pmf(x, mu1, mu2), colors='b', lw=5, alpha=0.5)
#ax.vlines(x,0,skPDF(x,mu1,mu2), colors='b', lw=5, alpha=0.5)



#for sc in [0.8,1.0]:
#    ax.plot(xx,skPDF(xx,sc*mu1,sc*mu2),'-',label = 'sc = '+str(sc))



ax.axvline(x=diff,label = 'measured k',color="Red")

ax.plot(xx,skPDF(xx,xfit*mu1,xfit*mu2),label = "Fit Result",color="Black")
ax.plot(x,skellam.pmf(x,xfit*mu1,xfit*mu2),'o',label="pmf",color="Black")
#ax.fill_between(xx,skPDF(xx,xfit1*mu1,xfit1*mu2),skPDF(xx,xfit2*mu1,xfit2*mu2),label = "fit error",color="Grey",alpha=0.5)
#ax.plot(xx,skPDF(xx,xfit1*mu1,xfit1*mu2),label = "fit error",color="Grey",alpha=0.5)
#ax.plot(xx,skPDF(xx,xfit2*mu1,xfit2*mu2),color="Grey",alpha = 0.5)

#Plot Error Bounds:
#ex = np.linspace(xfit2,xfit1,100)
#for i,e in enumerate(ex):
#    if i==0:
#        ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Grey",alpha=0.5,label="x +- err")
    

    #elif i==len(ex)-1:
    #    ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Grey",alpha = 0.5)
    #else:
    #    ax.plot(xx,skPDF(xx,e*mu1,e*mu2),color="Grey",alpha = 0.1)

#rv = skellam(mu1, mu2)
#ax.vlines(x, 0, rv.pmf(x), colors='k', linestyles='-', lw=1, label='frozen pmf')
plt.title("Skellam PDF. dtilde = "+str(dtilde)+", Bin Nr. "+str(bin)+", Fit Result x="+str(res)+" +- "+str(err))
plt.xlabel(r"$x$")
plt.ylabel(r"$f_{sk}(x)$")
ax.legend(loc='best', frameon=False)
plt.show()


